
import { Component, OnInit} from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'stx-ui';
  direction: any;
  constructor(
    private translate: TranslateService,
    private http: HttpClient, private router: Router, private activatedRoute: ActivatedRoute) {
  }

ngOnInit() {
  this.translate.addLangs(['en']);
  this.translate.setDefaultLang('en');
  const browserLang = this.translate.getBrowserLang();
  this.translate.use(browserLang.match(/en/) ? browserLang : 'en');
  if (this.translate.getBrowserLang() === 'ar') {
    this.direction = 'rtl';
  } else {
    this.direction = 'ltr';
  }
  this.router.events.subscribe((evt) => {
    if (!(evt instanceof NavigationEnd)) {
      return;
    }
    window.scrollTo(0, 0);
  });
}
}
