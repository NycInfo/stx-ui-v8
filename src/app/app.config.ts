export const API_END_POINT = 'https://stx.yoursalonapp.com:3000';
export const BASE_URL = 'https://stx.yoursalonapp.com';
export const S3_URL = 'https://s3.amazonaws.com/stxcloud/';
export const STATIC_JSONFILES_END_POINT = '/assets/staticjsonfiles/';
export const DEFAULT_COUNTRY = 'United States';
export const DEFAULT_COLOR = '#ffffff';
export const DEFAULT_TYPE = 'Edit';
export const DEFAULT_PRICE = 0.00;
export const DEFAULT_ACTIVE = 1;
export const DEFAULT_INACTIVE = 0;
export const ANYWHERECOMMERCE_DEVELOPER_TEST_MERCHANT_ID = '3099001';
export const ANYWHERECOMMERCE_DEVELOPER_TEST_MERCHANT_KEY = 'Secret2Pass';
export const ANYWHERECOMMERCE_PAYMENT_TYPE_GATEWAY = 'AnywhereCommerce';
export const ANYWHERECOMMERCE_PAYMENT_API = 'https://testpayments.anywherecommerce.com/merchant/xmlpayment';
export const SALONCLOUDS_PLUS = 'https://saloncloudsplus.com/newhomepage/newLoginPage';
export const cloverCfg = {
  'server': 'https://sandbox.dev.clover.com',
  'remoteAppId': 'HTFNJ1ACK0HER.BD268FGF43RAR',
  'merchantId': 'XBP993VYJ2P41',
  'accessToken': '9128f661-ae8a-3257-9d88-e0abe8513921'
};
export const environment = {
  production: false,
  booleanTrue: 1,
  booleanFalse: 0,
  maxPriority: 11,
  valueNone: 'None',
  selectResource: '-- Select Resource --'
};
export const cashinout = {
  cashTypeIn: 'Cash Paid In',
  cashTypeOut: 'Cash Paid Out'
};
export const states = [{ 'name': 'Alabama', 'abbrev': 'AL' }, { 'name': 'Alaska', 'abbrev': 'AK' },
{ 'name': 'Arizona', 'abbrev': 'AZ' }, { 'name': 'Arkansas', 'abbrev': 'AR' }, { 'name': 'California', 'abbrev': 'CA' },
{ 'name': 'Colorado', 'abbrev': 'CO' }, { 'name': 'Connecticut', 'abbrev': 'CT' }, { 'name': 'Delaware', 'abbrev': 'DE' },
{ 'name': 'District of Columbia', 'abbrev': 'DC' }, { 'name': 'Florida', 'abbrev': 'FL' }, { 'name': 'Georgia', 'abbrev': 'GA' }, { 'name': 'Hawaii', 'abbrev': 'HI' },
{ 'name': 'Idaho', 'abbrev': 'ID' }, { 'name': 'Illinois', 'abbrev': 'IL' }, { 'name': 'Indiana', 'abbrev': 'IN' },
{ 'name': 'Iowa', 'abbrev': 'IA' }, { 'name': 'Kansas', 'abbrev': 'KS' }, { 'name': 'Kentucky', 'abbrev': 'KY' },
{ 'name': 'Louisiana', 'abbrev': 'LA' }, { 'name': 'Maine', 'abbrev': 'ME' }, { 'name': 'Maryland', 'abbrev': 'MD' },
{ 'name': 'Massachusetts', 'abbrev': 'MA' }, { 'name': 'Michigan', 'abbrev': 'MI' }, { 'name': 'Minnesota', 'abbrev': 'MN' },
{ 'name': 'Mississippi', 'abbrev': 'MS' }, { 'name': 'Missouri', 'abbrev': 'MO' }, { 'name': 'Montana', 'abbrev': 'MT' },
{ 'name': 'Nebraska', 'abbrev': 'NE' }, { 'name': 'Nevada', 'abbrev': 'NV' }, { 'name': 'New Hampshire', 'abbrev': 'NH' },
{ 'name': 'New Jersey', 'abbrev': 'NJ' }, { 'name': 'New Mexico', 'abbrev': 'NM' }, { 'name': 'New York', 'abbrev': 'NY' },
{ 'name': 'North Carolina', 'abbrev': 'NC' }, { 'name': 'North Dakota', 'abbrev': 'ND' }, { 'name': 'Ohio', 'abbrev': 'OH' },
{ 'name': 'Oklahoma', 'abbrev': 'OK' }, { 'name': 'Oregon', 'abbrev': 'OR' }, { 'name': 'Pennsylvania', 'abbrev': 'PA' },
{ 'name': 'Rhode Island', 'abbrev': 'RI' }, { 'name': 'South Carolina', 'abbrev': 'SC' }, { 'name': 'South Dakota', 'abbrev': 'SD' },
{ 'name': 'Tennessee', 'abbrev': 'TN' }, { 'name': 'Texas', 'abbrev': 'TX' }, { 'name': 'Utah', 'abbrev': 'UT' },
{ 'name': 'Vermont', 'abbrev': 'VT' }, { 'name': 'Virginia', 'abbrev': 'VA' }, { 'name': 'Washington', 'abbrev': 'WA' },
{ 'name': 'West Virginia', 'abbrev': 'WV' }, { 'name': 'Wisconsin', 'abbrev': 'WI' }, { 'name': 'Wyoming', 'abbrev': 'WY' }];
