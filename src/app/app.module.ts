import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HttpModule, Http } from '@angular/http';
import { HttpClients } from './common/http-client';
import { HttpClientModule, HttpClient} from '@angular/common/http';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import * as config from './app.config';
import { ToastrModule } from 'ngx-toastr';
// import { ColorPickerModule } from 'ngx-color-picker';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}
@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpModule,
    HttpClientModule,
    // ColorPickerModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot({
      timeOut: 1500,
    }),
    TranslateModule.forRoot({
      loader : {
      provide: TranslateLoader,
      useFactory: (createTranslateLoader),
      deps: [HttpClient]
      }
    })
  ],
  declarations: [
    AppComponent
  ],
  providers: [
    {
      provide: LocationStrategy,
      useClass: HashLocationStrategy
    },
    {
      provide: 'apiEndPoint',
      useValue: config.API_END_POINT
    },
    {
      provide: 'staticJsonFilesEndPoint',
      useValue: config.STATIC_JSONFILES_END_POINT
    },
    {
      provide: 'defaultCountry',
      useValue: config.DEFAULT_COUNTRY
    },
    {
      provide: 'defaultColor',
      useValue: config.DEFAULT_COLOR
    },
    {
      provide: 'defaultType',
      useValue: config.DEFAULT_TYPE
    },
    {
      provide: 'defaultPrice',
      useValue: config.DEFAULT_PRICE
    },
    {
      provide: 'defaultActive',
      useValue: config.DEFAULT_ACTIVE
    },
    {
      provide: 'defaultInActive',
      useValue: config.DEFAULT_INACTIVE
    },
    HttpClients
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
