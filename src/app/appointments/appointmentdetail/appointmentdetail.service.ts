/*
  * Display single user details of front end users
  * extractData(): To extract the data
  * handleError(): To handle error messages.
*/
import { Injectable, Inject } from '@angular/core';
import { Response } from '@angular/http';
import { Headers } from '@angular/http';
import { HttpClients } from '../../common/http-client';
import { map } from 'rxjs/operators';

@Injectable()
export class ApptDetailService {
  constructor(
    private http: HttpClients,
    @Inject('apiEndPoint') private apiEndPoint: string,
    @Inject('staticJsonFilesEndPoint') private staticJsonFilesEndPoint: string
  ) { }

  getApptDetails(apptid) {
    return this.http.get(this.apiEndPoint + '/api/appointments/' + apptid)
      .pipe(map(this.extractData));
  }
  saveAppointmentDetails(apptid, dataObj) {
    return this.http.put(this.apiEndPoint + '/api/appointment/detail/' + apptid, dataObj)
      .pipe(map(this.extractData));
  }
  getVisitTypes() {
    return this.http.get(this.apiEndPoint + '/api/setup/clientpreferences/visittype/active')
      .pipe(map(this.extractData));
  }
  sendReminderToClient(dataObj, apptid) {
    return this.http.put(this.apiEndPoint + '/api/appointments/changestatus/' + apptid, dataObj)
      .pipe(map(this.extractData));
  }
  saveNotes(id, updateNotes) {
    const notes = {
      'notes': updateNotes
    };
    return this.http.put(this.apiEndPoint + '/api/client/savenotes/' + id, notes)
      .pipe(map(this.extractData));
  }
  sendText(dataObj) {
    return this.http.post(this.apiEndPoint + '/api/client/send/text', dataObj)
      .pipe(map(this.extractData));
  }
  sendCancelReminder(apptId, type) {
    const dataObj = {
      'apptId': apptId,
    };
    if (type === false) {
      dataObj['cancel'] = true;
    }
    return this.http.post(this.apiEndPoint + '/api/notification/email/cancel', dataObj)
      .pipe(map(this.extractData));
  }
  cancelStsAppts(dataObj) {
    return this.http.post(this.apiEndPoint + '/api/appointment/changecancelstatus', dataObj)
      .pipe(map(this.extractData));
  }
  /*--- Method used to get booking data ---*/
  getBookingData() {
    return this.http.get(this.apiEndPoint + '/api/appointmentsandemails/booking')
      .pipe(map(this.extractData));
  }

  getClientPackagesData(clientId) {
    return this.http.get(this.apiEndPoint + '/api/client/packages/' + clientId)
      .pipe(map(this.extractData));
  }

  getApptServices(clientid, apptid, reqDate) {
    const headers = new Headers();
    headers.append('bookingdate', reqDate);
    return this.http.getHeader(this.apiEndPoint + '/api/appointments/services/' + clientid + '/' + apptid, headers)
      .pipe(map(this.extractData));
  }
  changeApptStatus(checkInData) {
    return this.http.put(this.apiEndPoint + '/api/appointments/changestatus/' + checkInData.apptId, checkInData)
      .pipe(map(this.extractData));
  }
  getHideCliContactInfo(id) {
    return this.http.get(this.apiEndPoint + '/api/client/getHideClientContactInfo/' + id)
      .pipe(map(this.extractData));
  }
  getClient(clientId) {
    return this.http.get(this.apiEndPoint + '/api/client/' + clientId)
      .pipe(map(this.extractData));
  }
  getServiceLog(clientId) {
    return this.http.get(this.apiEndPoint + '/api/client/servicelog/' + clientId)
      .pipe(map(this.extractData));
  }
  getProductLog(clientId) {
    return this.http.get(this.apiEndPoint + '/api/client/productlog/' + clientId)
      .pipe(map(this.extractData));
  }
  getClientRewardsData(clientId) {
    return this.http.get(this.apiEndPoint + '/api/client/rewards/' + clientId)
        .pipe(map(this.extractData));
}
  /*To extract json data*/
  private extractData(res: Response) {
    if (res.headers && res.headers.get('token')) {
      localStorage.setItem('token', res.headers.get('token'));
    }
    const body = res.json();
    return body || {};
  }
}
