/*
  * Display single user details of front end users
  * extractData(): To extract the data
  * handleError(): To handle error messages.
*/
import { Injectable, Inject } from '@angular/core';
import { Response } from '@angular/http';
// import { Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { HttpClients } from '../../common/http-client';
import { map } from 'rxjs/operators';

@Injectable()
export class BookOutApptService {
  constructor(
    private http: HttpClients,
    @Inject('apiEndPoint') private apiEndPoint: string,
    @Inject('staticJsonFilesEndPoint') private staticJsonFilesEndPoint: string
  ) { }

  getWorkerList() {
    return this.http.get(this.apiEndPoint + '/api/setupworkers/setupworkerdetail')
      .pipe(map(this.extractData));
  }
  scheduleAvailable(dates) {
    return this.http.post(this.apiEndPoint + '/api/appointments/bookoutappoinmentswithdata', dates)
      .pipe(map(this.extractData));
  }
  getbookEveryTypes() {
    return this.http.get(this.staticJsonFilesEndPoint + 'common.json')
    .pipe(map(this.extractData));
  }
  getEveryTypes() {
    return this.http.get(this.staticJsonFilesEndPoint + 'common.json')
    .pipe(map(this.extractData));
  }
  getBookOutTimeHour() {
    return this.http.get(this.staticJsonFilesEndPoint + 'common.json')
    .pipe(map(this.extractData));
  }
  getBookOutEvery() {
    return this.http.get(this.staticJsonFilesEndPoint + 'common.json')
    .pipe(map(this.extractData));
  }
  getNumberofBookOuts() {
    return this.http.get(this.staticJsonFilesEndPoint + 'common.json')
    .pipe(map(this.extractData));
  }
  getBookingData() {
    return this.http.get(this.apiEndPoint + '/api/appointmentsandemails/booking')
      .pipe(map(this.extractData));
  }
  searchForAppointmentAction(apptdata) {
    return this.http.post(this.apiEndPoint + '/api/appointments/bookoutappoinment' , apptdata )
      .pipe(map(this.extractData));
  }
  /*To extract json data*/
  private extractData(res: Response) {
    if (res.headers && res.headers.get('token')) {
      localStorage.setItem('token', res.headers.get('token'));
    }
    const body = res.json();
    return body || {};
  }
}
