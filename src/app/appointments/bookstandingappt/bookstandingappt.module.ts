import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BookStandingApptComponent } from './bookstandingappt.component';
import { BookStandingApptRoutingModule } from './bookstandingappt.routing';
import { FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { ShareModule } from '../../common/share.module';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ModalModule } from 'ngx-bootstrap/modal';
import { StatusPipe } from './status.pipe';

@NgModule({
    imports: [
        CommonModule,
        BookStandingApptRoutingModule,
        FormsModule,
        TranslateModule,
        ShareModule,
        ModalModule,
        BsDatepickerModule.forRoot()

    ],
    declarations: [
        BookStandingApptComponent,
        StatusPipe,
    ]
})
export class BookStandingApptModule {
}
