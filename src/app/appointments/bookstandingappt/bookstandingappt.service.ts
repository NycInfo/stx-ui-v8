/*
  * Display single user details of front end users
  * extractData(): To extract the data
  * handleError(): To handle error messages.
*/
import { Injectable, Inject } from '@angular/core';
import { Response } from '@angular/http';
import { Headers } from '@angular/http';
import { HttpClients } from '../../common/http-client';
import { map } from 'rxjs/operators';

@Injectable()
export class BookStandingApptService {
  constructor(
    private http: HttpClients,
    @Inject('apiEndPoint') private apiEndPoint: string,
    @Inject('staticJsonFilesEndPoint') private staticJsonFilesEndPoint: string
  ) { }

  getServiceGroups(type, reqDate) {
    const headers = new Headers();
    headers.append('bookingdate', reqDate);
    return this.http.getHeader(this.apiEndPoint + '/api/setupservices/servicegroups/active/forappts', headers)
      .pipe(map(this.extractData));
  }
  getServices(serviceName, type, bookingdate: string) {
    const headers = new Headers();
    headers.append('bookingdate', bookingdate);
    return this.http.getHeader(this.apiEndPoint + '/api/setupticketpreferences/favorites/types/' + serviceName + '/' + type, headers)
      .pipe(map(this.extractData));
  }
  getUsers(bookingdata: any) {
    const headers = new Headers();
    headers.append('bookinginfo', JSON.stringify(bookingdata));
    return this.http.getHeader(this.apiEndPoint + '/api/bookingdata/bookappt', headers)
      .pipe(map(this.extractData));
  }
  getClient(clientId) {
    return this.http.get(this.apiEndPoint + '/api/client/' + clientId)
      .pipe(map(this.extractData));
  }
  getVisitTypes() {
    return this.http.get(this.apiEndPoint + '/api/setup/clientpreferences/visittype/active')
      .pipe(map(this.extractData));
  }
  getBookOutTimeHour() {
    return this.http.get(this.staticJsonFilesEndPoint + 'common.json')
      .pipe(map(this.extractData));
  }
  getbookEveryTypes() {
    return this.http.get(this.staticJsonFilesEndPoint + 'common.json')
      .pipe(map(this.extractData));
  }
  getEveryTypes() {
    return this.http.get(this.staticJsonFilesEndPoint + 'common.json')
      .pipe(map(this.extractData));
  }
  getAllServiceDetails() {
    return this.http.get(this.apiEndPoint + '/api/setupservices/setupservice')
      .pipe(map(this.extractData));
  }
  getBookingData() {
    return this.http.get(this.apiEndPoint + '/api/appointmentsandemails/booking')
      .pipe(map(this.extractData));
  }
  searchForAppointmentAction(apptdata) {
    return this.http.post(this.apiEndPoint + '/api/appointments/bookoutappoinment', apptdata)
      .pipe(map(this.extractData));
  }
  scheduleAvailable(dates) {
    return this.http.post(this.apiEndPoint + '/api/appointments/bookstandingappoinmentswithdata', dates)
      .pipe(map(this.extractData));
  }
  getWorkerList() {
    return this.http.get(this.apiEndPoint + '/api/setupworkers/setupworkerdetail')
      .pipe(map(this.extractData));
  }
  searchAppt(dataObj) {
    return this.http.post(this.apiEndPoint + '/api/clientSearch/bookappointments', dataObj)
      .pipe(map(this.extractData));
  }
  getPackageGroups(currentDate) {
    const headers = new Headers();
    headers.append('currentdate', currentDate);
    headers.append('onlinebook', '0');
    return this.http.getHeader(this.apiEndPoint + '/api/setupservices/servicepackage/forappt', headers)
      .pipe(map(this.extractData));
  }
  /** Method to get preferences for service tax and retail tax calculation */
  getServProdTax() {
    return this.http.get(this.apiEndPoint + '/api/setup/ticketpreferences/pos')
      .pipe(map(this.extractData));
  }
  sendApptNotifs(apptsAry, sentNotify, email) {
    return this.http.post(this.apiEndPoint + '/api/notification/email', { 'apptIds': apptsAry, sentNotify , email, page: 'bookstanding' })
      .pipe(map(this.extractData));
  }
  getWorkerLists(date) {
    return this.http.get(this.apiEndPoint + '/api/appointment/workerList/' + date)
      .pipe(map(this.extractData));
  }
  expressBookingServices(data, date) {
    return this.http.get(this.apiEndPoint + '/api/appointments/expressbookingservices/' + data + '/' + date)
      .pipe(map(this.extractData));
  }
  /*To extract json data*/
  private extractData(res: Response) {
    if (res.headers && res.headers.get('token')) {
      localStorage.setItem('token', res.headers.get('token'));
    }
    const body = res.json();
    return body || {};
  }
}
