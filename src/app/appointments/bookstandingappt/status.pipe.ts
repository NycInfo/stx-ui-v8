import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'status'
})
export class StatusPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    const workers = value;
    let resultString = '';
    const workersClosedIds = [];
    for (let i = 0; i < workers.length; i++) {
      const worker = workers[i];
      if (worker.Status__c === 'Closed') {
        if (workersClosedIds.indexOf(worker.workerId) === -1) {
          resultString += worker.Status__c + ' ' + '(' + worker.FullName + '),';
          workersClosedIds.push(worker.workerId);
        }
      } else if (worker.Status__c === 'Conflicting') {
        resultString += worker.Status__c + ' ' + '(' + worker.FullName + '),';
      } else if (worker.Status__c === 'Booked') {
        resultString += 'Available,';
      }
    }
    if (resultString) {
      resultString = resultString.slice(0, -1);
    }
    return resultString;
  }

}
