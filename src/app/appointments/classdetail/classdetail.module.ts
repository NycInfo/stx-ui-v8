import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClassDetailComponent } from './classdetail.component';
import { ClassDetailRoutingModule } from './classdetail.routing';
import { FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { ShareModule } from '../../common/share.module';


@NgModule({
    imports: [
        CommonModule,
        ClassDetailRoutingModule,
        FormsModule,
        TranslateModule,
        ShareModule

    ],
    declarations: [
        ClassDetailComponent,
    ]
})
export class ClassDetailModule {
}
