import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AppointmentsComponent } from './appointments.component';
import { AppointmentsRoutingModule } from './appointments.routing';

@NgModule({
  declarations: [AppointmentsComponent],
  imports: [
    CommonModule,
    AppointmentsRoutingModule
  ]
})
export class AppointmentsModule { }
