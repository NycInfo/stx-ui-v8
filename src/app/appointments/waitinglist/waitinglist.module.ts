import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { ModalModule } from 'ngx-bootstrap/modal';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

import { WaitingListComponent } from './waitinglist.component';
import { WaitingListRoutingModule } from './waitinglist.routing';
import { DataTableModule } from '../../../custommodules/primeng/primeng';
import { ShareModule } from '../../common/share.module';

@NgModule({
    imports: [
        CommonModule,
        WaitingListRoutingModule,
        FormsModule,
        TranslateModule,
        ReactiveFormsModule,
        ModalModule.forRoot(),
        DataTableModule,
        ShareModule,
        BsDatepickerModule.forRoot(),
        NgMultiSelectDropDownModule.forRoot(),
        PerfectScrollbarModule
    ],
    declarations: [
        WaitingListComponent,
    ]
})
export class WaitingListModule {
}
