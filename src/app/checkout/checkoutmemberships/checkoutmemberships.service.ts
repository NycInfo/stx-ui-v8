import { Injectable, Inject } from '@angular/core';
import { Response } from '@angular/http';
// import { Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { HttpClients } from '../../common/http-client';
import { map } from 'rxjs/operators';

@Injectable()
export class CheckOutMembershipsService {
    constructor(private http: HttpClients,
        @Inject('apiEndPoint') private apiEndPoint: string,
        @Inject('staticJsonFilesEndPoint') private staticJsonFilesEndPoint: string
      ) { }
      /*-- Method to get memberships list --*/
  getSetupMemberships(inActive) {
    return this.http.get(this.apiEndPoint + '/api/setupmemberships/' + inActive)
    .pipe(map(this.extractData));
  }
  getpaymentList() {
    return this.http.get(this.apiEndPoint + '/api/setup/company/paymenttypes')
     .pipe(map(this.extractData));
   }
   getClient(clientId) {
    return this.http.get(this.apiEndPoint + '/api/client/' + clientId)
      .pipe(map(this.extractData));
  }
  getPosdevices() {
    return this.http.get(this.apiEndPoint + '/api/setup/ticketpreferences/posdevices')
      .pipe(map(this.extractData));
  }
  saveCheckoutMemberships(clientMembershipsObj) {
    return this.http.post(this.apiEndPoint + '/api/checkout/clientmembership', clientMembershipsObj)
    .pipe(map(this.extractData));
  }
  getClientMemberships() {
    return this.http.get(this.apiEndPoint + '/api/checkout/clientmembership')
    .pipe(map(this.extractData));
  }
  xmlPayment(reqObj) {
    return this.http.post(this.apiEndPoint + '/api/payment', reqObj)
      .pipe(map(this.extractData));
  }
    /*To extract json data*/
    private extractData(res: Response) {
      if (res.headers && res.headers.get('token')) {
        localStorage.setItem('token', res.headers.get('token'));
      }
        const body = res.json();
        return body || {};
      }
}
