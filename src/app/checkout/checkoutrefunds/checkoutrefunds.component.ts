import { Component, OnInit, ViewChild, Inject, EventEmitter, Output } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { SafeUrl, DomSanitizer } from '@angular/platform-browser';
import { CheckOutRefundsService } from './checkoutrefunds.service';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { CommonService } from '../../common/common.service';

@Component({
  selector: 'app-checkoutrefunds',
  templateUrl: './checkoutrefunds.html',
  styleUrls: ['./checkoutrefunds.scss'],
  providers: [CheckOutRefundsService, CommonService],
})
export class CheckOutRefundsComponent implements OnInit {
  clientId: any;
  clientData: any;
  clientName: any;
  ticketName: any = null;
  startDate: any;
  endDate: any;
  refund = [];
  SdateEdateError: any;
  local: any;
  apptData = [];
  serviceTotal = 0;
  productTotal = 0;
  otherTotal = 0;
  totalTotal = 0;
  totalInclude = 0;
  taxamount = 0;
  createdDate: any;
  date = new Date();
  maxdate = new Date();
  datePickerConfig: any;
  refundApptDate: any;
  constructor(
    private checkOutRefundsService: CheckOutRefundsService,
    @Inject('apiEndPoint') public apiEndPoint: string,
    private route: ActivatedRoute,
    private router: Router,
    private toastr: ToastrService,
    private commonService: CommonService,
    private translateService: TranslateService,
    private sanitizer: DomSanitizer) {
    this.route.queryParams.subscribe(params => {
      this.clientId = route.snapshot.params['clientId'];
    });
    this.datePickerConfig = Object.assign({},
      {
        showWeekNumbers: false,
        containerClass: 'theme-blue',
      });
  }
  ngOnInit() {
    this.getClientData(this.clientId);
    this.endDate = new Date(this.date.getTime());
    this.endDate.setDate(this.date.getDate());
    this.startDate = new Date(this.date.setDate(this.date.getDate() - 90));
    const local = JSON.parse(localStorage.getItem('browserObject'));
    if (localStorage.getItem('browserObject') === '' || localStorage.getItem('browserObject') === null) {
      this.local = 'N/A';
    } else {
      this.local = local.CashDrawer.split(' ')[0];
    }
  }
  getClientData(clientId) {
    this.checkOutRefundsService.getClient(clientId)
      .subscribe(data => {
        this.clientData = data['result'].results[0];
        this.clientName = this.clientData.FirstName + ' ' + this.clientData.LastName;
      }, error => {
        const status = JSON.parse(error['status']);
        const statuscode = JSON.parse(error['_body']).status;
        switch (JSON.parse(error['_body']).status) {
          case '2033':
            window.scrollTo(0, 0);
            break;
        }
        if (statuscode === '2085' || statuscode === '2071') {
          if (this.router.url !== '/') {
            localStorage.setItem('page', this.router.url);
            this.router.navigate(['/']).then(() => { });
          }
        }
      });
  }
  /*
    * Method to get sorted list as per the requirement.
    * @param day is coming from html with 'today' and with empty string.
    * @param searchType is coming from html with 'searchNum', 'searchDate'.
    */
  refundSearch(day, searchType) {
    let stDate;
    let edDate;
    /* stDate ,edDate are passing in service for db date purpose only */
    if (!day) {
      stDate = this.startDate.getFullYear() + '-' + (this.startDate.getMonth() + 1) + '-' + this.startDate.getDate();
      edDate = this.endDate.getFullYear() + '-' + (this.endDate.getMonth() + 1) + '-' + this.endDate.getDate();
    } else {
      this.startDate = new Date();
      this.endDate = new Date();
      stDate = new Date().getFullYear() + '-' + (new Date().getMonth() + 1) + '-' + new Date().getDate();
      edDate = new Date().getFullYear() + '-' + (new Date().getMonth() + 1) + '-' + new Date().getDate();
    }
    if (this.startDate.setHours(0, 0, 0, 0) > this.endDate.setHours(0, 0, 0, 0)) {
      this.SdateEdateError = 'REPORT_TICKET_DETAILS.INVALID_BEGIN_DATE';
    }
    const refundObj = {
      clientId: this.clientId
    };
    this.checkOutRefundsService.getTicketDetails(stDate, edDate, refundObj)
      .subscribe(data => {
        this.serviceTotal = 0;
        this.productTotal = 0;
        this.otherTotal = 0;
        this.totalTotal = 0;
        this.totalInclude = 0;
        this.taxamount = 0;
        this.apptData = data['result']['apptData'];
        localStorage.setItem('ticketName', data['result']['apptData'].Name);
        localStorage.setItem('date', data['result']['apptData'].Name);
        this.apptData = this.apptData.filter(filterList => filterList.appId);
        for (let i = 0; i < this.apptData.length; i++) {
          if (this.apptData[i].Client_Type__c === 'null' || this.apptData[i].Client_Type__c === undefined || this.apptData[i].Client_Type__c === 'undefined'
            || !this.apptData[i].Client_Type__c) {
            this.apptData[i].Client_Type__c = '';
          }
          this.refundApptDate = this.apptData[i].Appt_Date_Time__c;
          this.apptData[i].disaplayDate = this.commonService.getUsrDtStrFrmDBStr(this.apptData[i].Appt_Date_Time__c);
          this.serviceTotal += this.apptData[i].Service_Sales__c;
          this.productTotal += this.apptData[i].Product_Sales__c;
          this.otherTotal += this.apptData[i].Other_Sales__c;
          this.totalInclude += this.apptData[i].Included_Ticket_Amount__c;
          this.taxamount += this.apptData[i].TotalTax;
          this.totalTotal += this.apptData[i].Ticket_Total__c;

        }
      }, error => {
        const status = JSON.parse(error['status']);
        const statuscode = JSON.parse(error['_body']).status;
        switch (JSON.parse(error['_body']).status) {
          case '2033':
            window.scrollTo(0, 0);
            break;
        }
        if (statuscode === '2085' || statuscode === '2071') {
          if (this.router.url !== '/') {
            localStorage.setItem('page', this.router.url);
            this.router.navigate(['/']).then(() => { });
          }
        }
      });

  }
  clear() {
    this.SdateEdateError = '';
  }
}
