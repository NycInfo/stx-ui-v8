import { Injectable, Inject } from '@angular/core';
import { Response, Headers } from '@angular/http';
// import { Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { HttpClients } from '../../common/http-client';
import { map } from 'rxjs/operators';

@Injectable()
export class CheckOutRefundsService {
  constructor(private http: HttpClients,
    @Inject('apiEndPoint') private apiEndPoint: string,
    @Inject('staticJsonFilesEndPoint') private staticJsonFilesEndPoint: string
  ) { }
  /*-- Method to get memberships list --*/
  getClient(clientId) {
    return this.http.get(this.apiEndPoint + '/api/client/' + clientId)
      .pipe(map(this.extractData));
  }

  /*-- Method to get refund list --*/
  getRefund(refunddata) {
    return this.http.post(this.apiEndPoint + '/api/checkout/refund', refunddata)
      .pipe(map(this.extractData));
  }

  getRefundTO(refunddata) {
    return this.http.get(this.apiEndPoint + '/api/checkout/refund/' + refunddata.Appt_Ticket__c)
      .pipe(map(this.extractData));
  }

  postRefundData(refunddata) {
    return this.http.post(this.apiEndPoint + '/api/checkout/refund/payment', refunddata)
      .pipe(map(this.extractData));
  }

  getCloverDevices() {
    return this.http.get(this.apiEndPoint + '/api/clover/device/list')
      .pipe(map(this.extractData));
  }
  getCloverInfo() {
    return this.http.get(this.apiEndPoint + '/api/clover/info')
      .pipe(map(this.extractData));
  }
  xmlPayment(reqObj) {
    return this.http.post(this.apiEndPoint + '/api/payment', reqObj)
      .pipe(map(this.extractData));
  }
  getTicketDetails(stDate, edDate, refundObj) {
    const headers = new Headers();
    headers.append('refundObj', JSON.stringify(refundObj));
    return this.http.getHeader(this.apiEndPoint + '/api/checkout/refund/' + stDate + '/' + edDate, headers)
    .pipe(map(this.extractData));
  }
  /*To extract json data*/
  private extractData(res: Response) {
    if (res.headers && res.headers.get('token')) {
      localStorage.setItem('token', res.headers.get('token'));
    }
    const body = res.json();
    return body || {};
  }
}
