import { Injectable, Inject } from '@angular/core';
import { Response } from '@angular/http';
// import { Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { HttpClients } from '../../common/http-client';
import { map } from 'rxjs/operators';

@Injectable()
export class CompletedTicketService {
    constructor(
        private http: HttpClients,
        @Inject('apiEndPoint') private apiEndPoint: string,
        @Inject('staticJsonFilesEndPoint') private staticJsonFilesEndPoint: string
    ) { }
    getApptDetails(apptid) {
        return this.http.get(this.apiEndPoint + '/api/appointments/' + apptid)
            .pipe(map(this.extractData));
    }
    /*
   * to get the ticket services from ticketservice table
   */
    getTicketServicesByApptId(apptid) {
        return this.http.get(this.apiEndPoint + '/api/checkout/services/' + apptid)
            .pipe(map(this.extractData));
    }
    /*
    * to get the ticket products from ticketproduct table
    */
    getTicketProducts(apptId) {
        return this.http.get(this.apiEndPoint + '/api/checkout/ticketproducts/' + apptId)
            .pipe(map(this.extractData));
    }
    /*
    * To get ticket payment Records by ApptId
    */
    getTicketPaymentData(apptId) {
        return this.http.get(this.apiEndPoint + '/api/checkout/ticketpayments/' + apptId)
            .pipe(map(this.extractData));
    }
    getClientRewards(clientId) {
        return this.http.get(this.apiEndPoint + '/api/client/rewards/' + clientId)
            .pipe(map(this.extractData));
    }
    /**
     * To get workerTips list
     */
    getTipsList(apptId) {
        return this.http.get(this.apiEndPoint + '/api/checkout/tips/' + apptId)
            .pipe(map(this.extractData));
    }
    /**
     * To get other list
     */
    getOthersTicketList(ticketId) {
        return this.http.get(this.apiEndPoint + '/api/checkout/ticketother/' + ticketId)
            .pipe(map(this.extractData));
    }
    /*
    * To get Included Tickets
    */
    getIncludedTicketList(ticketId) {
        return this.http.get(this.apiEndPoint + '/api/checkout/apptincludetickets/' + ticketId)
            .pipe(map(this.extractData));
    }
    /*
   * To get other list
   */
    sendReciept(dataObj) {
        return this.http.post(this.apiEndPoint + '/api/checkout/emailreciept', { 'data': dataObj })
            .pipe(map(this.extractData));
    }
    /*
      * To get clientRewards List
      */
    clientRewardDatabyApptId(apptid, isRefund) {
        return this.http.get(this.apiEndPoint + '/api/checkout/clientRewards/' + apptid + '/' + isRefund)
            .pipe(map(this.extractData));
    }
    /**
     * @param
     * worker lists only active  worker for day only
     */
    getWorkerLists() {
        return this.http.get(this.apiEndPoint + '/api/setupworkers/setupworkerdetail')
            .pipe(map(this.extractData));
    }
    serviceNoteUpdate(obj) {
        return this.http.put(this.apiEndPoint + '/api/checkout/serviceUpdateWrkNotes/' + obj.apptId, obj)
            .pipe(map(this.extractData));
    }
    productUpdate(obj) {
        return this.http.put(this.apiEndPoint + '/api/checkout/productUpdateWrk/' + obj.productId, obj)
            .pipe(map(this.extractData));
    }
    otherUpdate(obj) {
        return this.http.put(this.apiEndPoint + '/api/checkout/otherUpdateWrk/' + obj.otherId, obj)
            .pipe(map(this.extractData));
    }
    tipAmountUpdate(obj) {
        return this.http.post(this.apiEndPoint + '/api/checkout/tipAmountUpdate', obj)
            .pipe(map(this.extractData));
    }
    ratingUpdate(obj) {
        return this.http.post(this.apiEndPoint + '/api/checkout/ratingUpdate', obj)
            .pipe(map(this.extractData));
    }
    AdditionalTipAmount(obj) {
        return this.http.post(this.apiEndPoint + '/api/checkout/extraTipAmt', obj)
            .pipe(map(this.extractData));
    }
    getPaymentList() {
        return this.http.get(this.apiEndPoint + '/api/checkout/getPaymentList')
            .pipe(map(this.extractData));
    }
    updatePaymentList(obj) {
        return this.http.post(this.apiEndPoint + '/api/checkout/updatePaymentList', obj)
            .pipe(map(this.extractData));
    }
    addPaymentTipAmt(obj) {
        return this.http.post(this.apiEndPoint + '/api/checkout/addPaymentTipAmt', obj)
            .pipe(map(this.extractData));
    }
    activeSerive(id) {
        return this.http.get(this.apiEndPoint + '/api/checkout/activeSerive/' + id)
            .pipe(map(this.extractData));
    }
    getPosdevices() {
        return this.http.get(this.apiEndPoint + '/api/setup/ticketpreferences/posdevices')
            .pipe(map(this.extractData));
    }
    updateTipAmount(obj) {
        return this.http.post(this.apiEndPoint + '/api/checkout/updateTipAmount', obj)
            .pipe(map(this.extractData));
    }
    /*To extract json data*/
    private extractData(res: Response) {
        if (res.headers && res.headers.get('token')) {
            localStorage.setItem('token', res.headers.get('token'));
        }
        const body = res.json();
        return body || {};
    }
}
