/*
  * Display single user details of front end users
  * extractData(): To extract the data
  * handleError(): To handle error messages.
*/
import { Injectable, Inject } from '@angular/core';
import { Response } from '@angular/http';
import { Headers } from '@angular/http';
import { HttpClients } from '../../common/http-client';
import { map } from 'rxjs/operators';

@Injectable()
export class CheckOutEditTicketService {
  constructor(private http: HttpClients,
    @Inject('apiEndPoint') private apiEndPoint: string,
    @Inject('staticJsonFilesEndPoint') private staticJsonFilesEndPoint: string
  ) { }
  getServices() {
    return this.http.get(this.apiEndPoint + '/api/CheckOut/services')
      .pipe(map(this.extractData));
  }
  getFavourites() {
    return this.http.get(this.apiEndPoint + '/api/setupticketpreferences/favorites')
      .pipe(map(this.extractData));
  }
  getVisitTypes() {
    return this.http.get(this.apiEndPoint + '/api/setup/clientpreferences/visittype/active')
      .pipe(map(this.extractData));
  }
  getApptDetails(apptid) {
    return this.http.get(this.apiEndPoint + '/api/appointments/' + apptid)
      .pipe(map(this.extractData));
  }
  clientRewardData(clientId) {
    return this.http.get(this.apiEndPoint + '/api/checkout/client/rewards/' + clientId)
      .pipe(map(this.extractData));
  }
  getClientRewards(clientId) {
    return this.http.get(this.apiEndPoint + '/api/client/rewards/' + clientId)
      .pipe(map(this.extractData));
  }
  getRewardsData() {
    return this.http.get(this.apiEndPoint + '/api/marketing/rewards')
      .pipe(map(this.extractData));
  }
  getData(searchKey) {
    return this.http.get(this.apiEndPoint + '/api/clientsearch/' + searchKey)
      .pipe(map(this.extractData));
  }
  addClient(clientId, apptId, clientInfo) {
    return this.http.put(this.apiEndPoint + '/api/appointments/client/add/' + apptId, clientInfo)
      .pipe(map(this.extractData));
  }
  editVisitType(apptId, visitType) {
    const dataObj = {
      'vistTypeVal': visitType
    };
    return this.http.put(this.apiEndPoint + '/api/checkout/visittype/' + apptId, dataObj)
      .pipe(map(this.extractData));
  }
  /**
   * Method to get preferences for service tax and retail tax calculation
   */
  getServProdTax() {
    return this.http.get(this.apiEndPoint + '/api/setup/ticketpreferences/pos')
      .pipe(map(this.extractData));
  }
  /**
   * to check the service is associated with Any worker
   */
  isWorkerAssociated(Id) {
    return this.http.get(this.apiEndPoint + '/api/CheckOut/worker/services/' + Id)
      .pipe(map(this.extractData));
  }
  /**
  * to insert the record into ticket service Table
  */
  addToTicketService(serviceData, type) {
    return this.http.post(this.apiEndPoint + '/api/checkout/addtoticketservices/' + type, serviceData)
      .pipe(map(this.extractData));
  }
  /**
    * to add the promotion values to The existing Ticketservice Table and Ticekt product table
    */
  addPromotion(dataObj) {
    return this.http.post(this.apiEndPoint + '/api/checkout/addpromotion', dataObj)
      .pipe(map(this.extractData));
  }
  /**
   * to get the ticket services from ticketservice table
   */
  getTicketServicesByApptId(apptid) {
    return this.http.get(this.apiEndPoint + '/api/checkout/services/' + apptid)
      .pipe(map(this.extractData));
  }
  /**
    * To get Promotions Data
    */
  getPromotionsData() {
    return this.http.get(this.apiEndPoint + '/api/marketing/promotion')
      .pipe(map(this.extractData));
  }
  /**
   * To get getIncludeTickets Data
   */
  getIncludeTickets(apptId) {
    return this.http.get(this.apiEndPoint + '/api/checkout/includetickets/' + apptId)
      .pipe(map(this.extractData));
  }

  /**
   * To get paymenttypes Data
   */
  getPaymentTypesData() {
    return this.http.get(this.apiEndPoint + '/api/setup/company/paymenttypes')
      .pipe(map(this.extractData));
  }
  getGiftData(giftNumber) {
    return this.http.get(this.apiEndPoint + '/api/checkout/giftbalancingsearch/' + giftNumber)
      .pipe(map(this.extractData));
  }
  /**
     * To add data to ticket payment
     */
  addToPaymentsTicket(paymentObj) {
    return this.http.post(this.apiEndPoint + '/api/checkout/ticketpayments', paymentObj)
      .pipe(map(this.extractData));
  }
  /**
   * To get ticket payment Records by ApptId
   */
  getTicketPaymentData(apptId) {
    return this.http.get(this.apiEndPoint + '/api/checkout/ticketpayments/' + apptId)
      .pipe(map(this.extractData));
  }
  updateTicketPayment(ticketPaymentId, paymentObj) {
    return this.http.put(this.apiEndPoint + '/api/checkout/ticketpayments/' + ticketPaymentId, paymentObj)
      .pipe(map(this.extractData));
  }
  deleteTicketPayment(paymentId, paymentObj) {
    return this.http.post(this.apiEndPoint + '/api/checkout/ticketpayments/delete/' + paymentId, paymentObj)
      .pipe(map(this.extractData));
  }
  // '/api/checkout/ticketpayments/
  /**
  * To get getServiceGroups
  */
  getServiceGroups(type) {
    return this.http.get(this.apiEndPoint + '/api/setupservices/servicegroups/' + type)
      .pipe(map(this.extractData));
  }
  /**
   * To getWorkerMerchantsData for payments
   */
  getWorkerMerchantsData() {
    return this.http.get(this.apiEndPoint + '/api/checkout/ticketpayments/worker/merchant')
      .pipe(map(this.extractData));
  }
  /**
   * Service list modal code starts
   */
  updateServicesListTicket(Id, updateServicesObj) {
    return this.http.put(this.apiEndPoint + '/api/checkout/services/' + Id, updateServicesObj)
      .pipe(map(this.extractData));
  }

  removeTicketSerices(ticketServiceId, amountDetails) {
    const headers = new Headers();
    headers.append('amountdetails', JSON.stringify(amountDetails));
    return this.http.deleteHeader(this.apiEndPoint + '/api/checkout/services/' + ticketServiceId, headers)
      .pipe(map(this.extractData));
  }

  /**
   * Service list modal code ends
   */

  /**
  * Products code starts
  */
  getProductsBySKU(searchKeyWord) {
    return this.http.get(this.apiEndPoint + '/api/inventory/usage/' + searchKeyWord)
      .pipe(map(this.extractData));
  }
  getWorkersList() {
    return this.http.get(this.apiEndPoint + '/api/checkout/product/workers')
      .pipe(map(this.extractData));
  }
  getProductsList() {
    return this.http.get(this.apiEndPoint + '/api/checkout/products')
      .pipe(map(this.extractData));
  }
  productAddToTicket(productData, action) {
    return this.http.post(this.apiEndPoint + '/api/checkout/addtoproduct/' + action, productData)
      .pipe(map(this.extractData));
  }
  getTicketProducts(apptId) {
    return this.http.get(this.apiEndPoint + '/api/checkout/ticketproducts/' + apptId)
      .pipe(map(this.extractData));
  }
  // products list model code starts
  updateTicket(Id, updateTicketObj) {
    return this.http.put(this.apiEndPoint + '/api/checkout/products/' + Id, updateTicketObj)
      .pipe(map(this.extractData));
  }
  removeTicketProduct(ticketProductId, amountDetails) {
    const headers = new Headers();
    headers.append('amountdetails', JSON.stringify(amountDetails));
    return this.http.deleteHeader(this.apiEndPoint + '/api/checkout/products/' + ticketProductId, headers)
      .pipe(map(this.extractData));
  }
  /**
  /**
 * Products code ends
 */

  /**
   * misc code starts
   */
  saveMisc(calObj, action) {
    return this.http.post(this.apiEndPoint + '/api/checkout/miscsale/' + action, calObj)
      .pipe(map(this.extractData));
  }
  // getCalList(miscSale, apptId) {
  //   return this.http.get(this.apiEndPoint + '/api/checkout/ticketother/' + miscSale + '/' + apptId)
  //     .pipe(map(this.extractData));
  // }
  updateMiscTicket(id, amount) {
    return this.http.put(this.apiEndPoint + '/api/checkout/miscsale/' + id, amount)
      .pipe(map(this.extractData));
  }
  deleteMiscTicket(miscId, amountDetails) {
    const headers = new Headers();
    headers.append('amountdetails', JSON.stringify(amountDetails));
    return this.http.deleteHeader(this.apiEndPoint + '/api/checkout/miscsale/' + miscId, headers)
      .pipe(map(this.extractData));
  }
  /**
   * misc code ends
   */

  /**
   * others code starts
   */
  getAllServiceDetails(type) {
    return this.http.get(this.apiEndPoint + '/api/setupservices/servicepackages/' + type)
      .pipe(map(this.extractData));
  }

  addToTicket(ticketObj, action) {
    return this.http.post(this.apiEndPoint + '/api/checkout/ticketother/' + action, ticketObj)
      .pipe(map(this.extractData));
  }
  getAllWorkers() {
    return this.http.get(this.apiEndPoint + '/api/setupworkers/setupworkerdetail')
      .pipe(map(this.extractData));
  }
  getOthersTicketList(ticketId) {
    return this.http.get(this.apiEndPoint + '/api/checkout/ticketother/' + ticketId)
      .pipe(map(this.extractData));
  }
  updateOthersTicket(ticketId, ticketObj) {
    return this.http.put(this.apiEndPoint + '/api/checkout/ticketother/' + ticketId, ticketObj)
      .pipe(map(this.extractData));
  }
  detleteOthersTicket(ticketId, amountDetails, clientId) {
    const headers = new Headers();
    headers.append('amountdetails', JSON.stringify(amountDetails));
    headers.append('clientId', clientId);
    return this.http.deleteHeader(this.apiEndPoint + '/api/checkout/ticketother/' + ticketId, headers)
      .pipe(map(this.extractData));
  }
  /**
   * other Code ends
   */
  /**
 * Worker Tip Code starts
 */
  addTipToTicket(ticketObj, action) {
    return this.http.post(this.apiEndPoint + '/api/checkout/tips/' + action, ticketObj)
      .pipe(map(this.extractData));
  }
  updateTipToTicket(tipId, ticketObj) {
    return this.http.put(this.apiEndPoint + '/api/checkout/tips/' + tipId, ticketObj)
      .pipe(map(this.extractData));
  }
  getTipsList(apptId) {
    return this.http.get(this.apiEndPoint + '/api/checkout/tips/' + apptId)
      .pipe(map(this.extractData));
  }
  deleteWorkerTip(tipId, amountDetails) {
    const headers = new Headers();
    headers.append('amountdetails', JSON.stringify(amountDetails));
    return this.http.deleteHeader(this.apiEndPoint + '/api/checkout/tips/' + tipId, headers)
      .pipe(map(this.extractData));
  }
  deleteClearSale(tipId) {
    return this.http.delete(this.apiEndPoint + '/api/checkout/ticket/' + tipId)
      .pipe(map(this.extractData));
  }
  getRateToTicket(value, apptId) {
    const dataObj = {
      'rateValue': value
    };
    return this.http.put(this.apiEndPoint + '/api/checkout/ticketrating/' + apptId, dataObj)
      .pipe(map(this.extractData));

  }
  xmlPayment(reqObj) {
    return this.http.post(this.apiEndPoint + '/api/payment', reqObj)
      .pipe(map(this.extractData));
  }
  getCloverDevices() {
    return this.http.get(this.apiEndPoint + '/api/clover/device/list')
      .pipe(map(this.extractData));
  }
  insertCloverTip(reqObj) {
    return this.http.post(this.apiEndPoint + '/api/clover/tip/insert', reqObj)
      .pipe(map(this.extractData));
  }
  getCloverInfo() {
    return this.http.get(this.apiEndPoint + '/api/clover/info')
      .pipe(map(this.extractData));
  }
  /**
   * Worker Tip Code Ends
   */
  getCompanyInfo() {
    return this.http.get(this.apiEndPoint + '/api/setup/compananyinfo')
      .pipe(map(this.extractData));
  }
  /*To extract json data*/
  private extractData(res: Response) {
    if (res.headers && res.headers.get('token')) {
      localStorage.setItem('token', res.headers.get('token'));
    }
    const body = res.json();
    return body || {};
  }
}
