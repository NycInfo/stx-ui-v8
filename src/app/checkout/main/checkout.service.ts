/*
  * Display single user details of front end users
  * extractData(): To extract the data
  * handleError(): To handle error messages.
*/
import { Injectable, Inject } from '@angular/core';
import { Response } from '@angular/http';
// import { Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { HttpClients } from '../../common/http-client';
import { map } from 'rxjs/operators';

@Injectable()
export class CheckOutService {
  constructor(private http: HttpClients,
    @Inject('apiEndPoint') private apiEndPoint: string,
    @Inject('staticJsonFilesEndPoint') private staticJsonFilesEndPoint: string
  ) { }
  getData(searchKey) {
    return this.http.get(this.apiEndPoint + '/api/clientsearch/' + searchKey)
      .pipe(map(this.extractData));
  }
  // getClientAutoSearch(id) {
  //   return this.http.get(this.apiEndPoint + '/api/appointment/getclientnames/' + id)
  //     .pipe(map(this.extractData));
  // }
  getCheckOutList(datedate: Date, endDate: Date) {
    const stDtSt = datedate.getFullYear() + '-' + (datedate.getMonth() + 1) + '-' + datedate.getDate();
    const endDtSt = endDate.getFullYear() + '-' + (endDate.getMonth() + 1) + '-' + endDate.getDate();
    return this.http.get(this.apiEndPoint + '/api/checkout/list/' + stDtSt + '/' + endDtSt)
      .pipe(map(this.extractData));
  }
  addTickets(ticketsData) {
    return this.http.post(this.apiEndPoint + '/api/checkout/includetickets', ticketsData)
      .pipe(map(this.extractData));
  }
  getHideCliContactInfo(id) {
    return this.http.get(this.apiEndPoint + '/api/client/getHideClientContactInfo/' + id)
      .pipe(map(this.extractData));
  }
  xmlPayment(reqObj) {
    return this.http.post(this.apiEndPoint + '/api/payment', reqObj)
      .pipe(map(this.extractData));
  }
  addToClient(obj) {
    return this.http.post(this.apiEndPoint + '/api/membership/client', { 'obj': [obj] })
      .pipe(map(this.extractData));
  }
  detClientId(id) {
    return this.http.get(this.apiEndPoint + '/api/setupmemberships/delete/' + id)
      .pipe(map(this.extractData));
  }
  addToPaymentsTicket(paymentObj, obj) {
    return this.http.post(this.apiEndPoint + '/api/membership/ticketpayments', { 'paymentObj': [paymentObj], 'obj': [obj] })
      .pipe(map(this.extractData));
  }
  getPaymentTypesData() {
    return this.http.get(this.apiEndPoint + '/api/setup/company/paymenttypes')
      .pipe(map(this.extractData));
  }
  getSetupMemberships(inActive) {
    return this.http.get(this.apiEndPoint + '/api/setupmemberships/' + inActive)
      .pipe(map(this.extractData));
  }
  /*To extract json data*/
  private extractData(res: Response) {
    if (res.headers && res.headers.get('token')) {
      localStorage.setItem('token', res.headers.get('token'));
    }
    const body = res.json();
    return body || {};
  }
}
