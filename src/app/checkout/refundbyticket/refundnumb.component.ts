import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { SafeUrl, DomSanitizer } from '@angular/platform-browser';
import { RefundNumbService } from './refundnumb.service';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { CommonService } from '../../common/common.service';
@Component({
  selector: 'app-reports-app',
  templateUrl: './refundnumb.html',
  styleUrls: ['./refundnumb.scss'],
  providers: [RefundNumbService, CommonService],
})
export class RefundNumbComponent implements OnInit, AfterViewInit {
  startDate: Date;
  endDate: Date;
  todayDate: Date;
  itemsDisplay = false;
  workerTipsData: any;
  datePickerConfig: any;
  dateError: any;
  apptTicketData: any;
  apptcashinoutData: any;
  totalService = 0;
  totalProduct = 0;
  totalOther = 0;
  alltotal = 0;
  cashInOutdata = [];
  apptValuesTotals__c: any;
  apptData = [];
  serviceTotal = 0;
  productTotal = 0;
  otherTotal = 0;
  totalTotal = 0;
  totalInclude = 0;
  taxamount = 0;
  SdateEdateError: any;
  createdDate: any;
  sortField = 'Date';
  ticketName: any = null;
  sortfieldList = [];
  refundApptDate :any;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private toastr: ToastrService, private commonService: CommonService,
    private translateService: TranslateService,
    private refundNumbService: RefundNumbService) {
    this.datePickerConfig = Object.assign({},
      {
        showWeekNumbers: false,
        containerClass: 'theme-blue',
      });
  }
  ngOnInit() {
    const date = new Date();
    const sendDate = {
      'todayDate': this.commonService.getDBDatTmStr(this.todayDate),
      'search': 'todayDate'
    };
    this.getTicketList(sendDate);
    this.getCashInOutList(sendDate);
    this.getSortFields();
    this.startDate = new Date();
    this.endDate = new Date();
    this.todayDate = new Date();
    this.getTicketRefundDetails('today', 'searchDate');
  }
  ngAfterViewInit() {
    this.headerDateFormat();
  }
  headerDateFormat() {
    let sDate;
    let sMonth;
    let eDate;
    let eMonth;
    this.startDate = new Date();
    this.endDate = new Date();
    this.todayDate = new Date();
    if (this.startDate.getDate() < 10) {
      sDate = '0' + this.startDate.getDate();
    } else {
      sDate = this.startDate.getDate();
    }
    if ((this.startDate.getMonth() + 1) < 10) {
      sMonth = '0' + (this.startDate.getMonth() + 1);
    } else {
      sMonth = (this.startDate.getMonth() + 1);
    }
    if (this.endDate.getDate() < 10) {
      eDate = '0' + this.endDate.getDate();
    } else {
      eDate = this.endDate.getDate();
    }
    if ((this.endDate.getMonth() + 1) < 10) {
      eMonth = '0' + (this.endDate.getMonth() + 1);
    } else {
      eMonth = (this.endDate.getMonth() + 1);
    }
    const displayName = document.getElementById('displayNameId');
    if (displayName) {
      displayName.innerHTML = ' Ticket Details ' + sMonth + '/' + sDate + '/' + this.startDate.getFullYear() + ' - ' + eMonth + '/' + eDate + '/' + this.endDate.getFullYear();
    }
  }
  /**
   * Method to get ticket list.
   * @param sendDate is coming from html with date.
   */
  getTicketList(sendDate) {
    this.refundNumbService.searchTicketData(sendDate)
      .subscribe(data => {
        this.apptTicketData = data['result'];
        for (let i = 0; i < this.apptTicketData.length; i++) {
          this.totalService += this.apptTicketData[i].servicePrice;
          this.totalProduct += this.apptTicketData[i].productPrice;
          this.totalOther += this.apptTicketData[i].otherAmount;
          this.alltotal += this.apptTicketData[i].Total;
        }
      }, error => {
        const status = JSON.parse(error['status']);
        const statuscode = JSON.parse(error['_body']).status;
        switch (JSON.parse(error['_body']).status) {
          case '2033':
            window.scrollTo(0, 0);
            break;
        }
        if (statuscode === '2085' || statuscode === '2071') {
          if (this.router.url !== '/') {
            localStorage.setItem('page', this.router.url);
            this.router.navigate(['/']).then(() => { });
          }
        }
      });
  }
  /**
   * Method to get cash in out list.
   * @param sendDate is coming from html with date.
   */
  getCashInOutList(sendDate) {
    this.refundNumbService.searchcashinoutData(sendDate)
      .subscribe(data => {
        this.apptcashinoutData = data['result'];
      }, error => {
        const status = JSON.parse(error['status']);
        const statuscode = JSON.parse(error['_body']).status;
        switch (JSON.parse(error['_body']).status) {
          case '2033':
            window.scrollTo(0, 0);
            break;
        }
        if (statuscode === '2085' || statuscode === '2071') {
          if (this.router.url !== '/') {
            localStorage.setItem('page', this.router.url);
            this.router.navigate(['/']).then(() => { });
          }
        }
      });
  }
  /**
   * Method to get sorted list as per the requirement.
   * @param day is coming from html with 'today' and with empty string.
   * @param searchType is coming from html with 'searchNum', 'searchDate'.
   */
  getTicketRefundDetails(day, searchType) {
    let stDate;
    let edDate;
    /* stDate ,edDate are passing in service for db date purpose only */
    if (!day) {
      stDate = this.startDate.getFullYear() + '-' + (this.startDate.getMonth() + 1) + '-' + this.startDate.getDate();
      edDate = this.endDate.getFullYear() + '-' + (this.endDate.getMonth() + 1) + '-' + this.endDate.getDate();
    } else {
      this.startDate = new Date();
      this.endDate = new Date();
      stDate = new Date().getFullYear() + '-' + (new Date().getMonth() + 1) + '-' + new Date().getDate();
      edDate = new Date().getFullYear() + '-' + (new Date().getMonth() + 1) + '-' + new Date().getDate();
    }
    if (this.startDate.setHours(0, 0, 0, 0) > this.endDate.setHours(0, 0, 0, 0)) {
      this.SdateEdateError = 'REPORT_TICKET_DETAILS.INVALID_BEGIN_DATE';
    } else {
      if (this.ticketName === '' && searchType === 'searchNum') {
        this.ticketName = null;
        searchType = 'searchDate';
      }

      this.refundNumbService.getTicketRefundDetails(stDate, edDate, this.sortField, this.ticketName, searchType)
        .subscribe(data => {
          this.serviceTotal = 0;
          this.productTotal = 0;
          this.otherTotal = 0;
          this.totalTotal = 0;
          this.totalInclude = 0;
          this.taxamount = 0;
          // if (data['result']['cashInOutdata'][0]) {
          //   this.cashInOutdata = data['result']['cashInOutdata'][0];
          //   this.cashInOutdata.forEach((obj, i) => {
          //     this.cashInOutdata[i]['createdDate'] = this.commonService.getUsrDtStrFrmDBStr(obj.CreatedDate);
          //   });
          // }
          this.apptData = data['result']['apptData'];
          this.apptData = this.apptData.filter(filterList => filterList.appId);

          for (let i = 0; i < this.apptData.length; i++) {
            if (this.apptData[i].Client_Type__c === 'null' || this.apptData[i].Client_Type__c === undefined || this.apptData[i].Client_Type__c === 'undefined'
              || !this.apptData[i].Client_Type__c) {
              this.apptData[i].Client_Type__c = '';
            }
            this.refundApptDate = this.apptData[i].Appt_Date_Time__c;
            this.apptData[i].disaplayDate = this.commonService.getUsrDtStrFrmDBStr(this.apptData[i].Appt_Date_Time__c);
            this.serviceTotal += this.apptData[i].Service_Sales__c;
            this.productTotal += this.apptData[i].Product_Sales__c;
            this.otherTotal += this.apptData[i].Other_Sales__c;
            this.totalInclude += this.apptData[i].Included_Ticket_Amount__c;
            this.taxamount += this.apptData[i].TotalTax;
            this.totalTotal += this.apptData[i].Ticket_Total__c;

          }
        }, error => {
          const status = JSON.parse(error['status']);
          const statuscode = JSON.parse(error['_body']).status;
          switch (JSON.parse(error['_body']).status) {
            case '2033':
              window.scrollTo(0, 0);
              break;
          }
          if (statuscode === '2085' || statuscode === '2071') {
            if (this.router.url !== '/') {
              localStorage.setItem('page', this.router.url);
              this.router.navigate(['/']).then(() => { });
            }
          }
        });

    }
  }
  /**
   * Method to clear error message.
   */
  clear() {
    this.SdateEdateError = '';
  }
  /**
   * Method to get static sort fields.
   */
  getSortFields() {
    this.refundNumbService.getSortFields().subscribe(
      data => {
        this.sortfieldList = data['SORTFIELDS'];
        this.sortField = this.sortfieldList[0].option;
      },
      error => {
        const errStatus = JSON.parse(error['_body'])['status'];
        if (errStatus === '2085' || errStatus === '2071') {
          if (this.router.url !== '/') {
            localStorage.setItem('page', this.router.url);
            this.router.navigate(['/']).then(() => { });
          }
        }
      });

  }
  /**
   * Method to get sorted value by changing the option.
   */
  sortingField(value) {
    this.ticketName = null;
    this.sortField = value;
    this.getTicketRefundDetails('', 'searchDate');
  }
  printDiv() {
    let printContents;
    let popupWin;
    printContents = document.getElementById('dvContents').innerHTML;
    printContents = printContents.replace('value="' + this.ticketName + '"', 'Input value ="' + this.ticketName + '"');
    printContents = printContents.replace('bsdatepicker=""', 'value="' + (this.startDate.getMonth() + 1) + '/'
      + this.startDate.getDate() + '/' + this.startDate.getFullYear() + '"');
    printContents = printContents.replace('bsdatepicker=""', 'value="' + (this.endDate.getMonth() + 1) + '/'
      + this.endDate.getDate() + '/' + this.endDate.getFullYear() + '"');
    printContents = printContents.replace('value="' + this.sortField + '"', 'selected value="' + this.sortField + '"');
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <title>Ticket Details </title>
          <style>
            table {
              border-collapse: collapse;
            }
            table, th, td {
                border: 0.5px solid black;
            }
            .pri td {
              padding:6px;
            }
            .arc {
              float:left;
              margin:12px;
            }
            .arc button {
              margin-top:10px;
            }
            .total {
              margin:6px;
            }
          </style>
        </head>
        <body onload="window.print();window.close()">${printContents}</body>
      </html>`);
    popupWin.document.close();
  }
}
