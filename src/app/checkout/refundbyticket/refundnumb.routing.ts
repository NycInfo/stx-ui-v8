import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import {  RefundNumbComponent } from './refundnumb.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '',
                component:  RefundNumbComponent,
                children: [
                    {
                        path: '',
                        component:  RefundNumbComponent
                    }
                ]
            }
        ])
    ],
    exports: [
        RouterModule
    ]
})
export class  RefundNumbRoutingModule {
}
