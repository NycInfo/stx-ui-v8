import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { RefundNewService } from './refundnew.service';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { CommonService } from '../../common/common.service';
import { Md5 } from 'ts-md5/dist/md5';
import * as config from '../../app.config';
import * as clover from 'remote-pay-cloud';

@Component({
  selector: 'app-refundnew',
  templateUrl: './refundnew.html',
  styleUrls: ['./refundnew.scss'],
  providers: [RefundNewService, CommonService],
})
export class RefundNewComponent implements OnInit {
  clientId: any;
  apptId: any;
  clientData: any;
  refundData: any;
  clientName: any;
  refundPostData: any;
  ElectronicPayment = '';
  Electronicdisabled = false;
  refundedBy: any;
  startDate: any;
  endDate: any;
  refundTOData: any;
  refundSaveBut = false;
  taxvalue = 0;
  totalAmt = 0;
  totalfixAmt: any;
  totaltaxAmt: any;
  oldAmt: any;
  amountMatcherror: any;
  searchList: any = false;
  refundtype: any = 'Service Refund';
  refundByError: any;
  checkedData = [];
  refundSaveData = [];
  refund = [];
  refundErr = '';
  local: any;
  servicelist = false;
  productlist = false;
  electroniclist = false;
  date = new Date();
  maxdate = new Date();
  refundto: any = false;
  OriginalPaymentAmount: any;
  datePickerConfig: any;
  cloverDevice = '';
  cloverDeviceList = [];
  cloverConnector: any;
  cloverConnectorListener: any;
  cloverIndex: any;
  cloverInfo: any = [];
  toastermessage: any;
  accountChanrgeId: any;
  accountChanrgeName: any;
  emitapptDataInfo: any;
  ticketName: any;
  ticketDate: any;
  workerList: any;
  selectVal = false;
  selectItem = 0;
  refundBy: any;
  ecommerceData: any;
  selectValAr = [];
  showCheckbox = false;
  Product_Tax__c: any = 0;
  Service_Tax: any = 0;
  @ViewChild('cloverModal', { static: false }) cloverModal: ModalDirective;
  @ViewChild('popupModal', { static: false }) public popupModal: ModalDirective;
  constructor(
    private refundNewService: RefundNewService,
    @Inject('apiEndPoint') public apiEndPoint: string,
    private route: ActivatedRoute,
    private router: Router,
    private toastr: ToastrService,
    private commonService: CommonService,
    private translateService: TranslateService,
    private sanitizer: DomSanitizer) {
    this.route.queryParams.subscribe(params => {
      this.clientId = params['clientId'];
      this.apptId = params['apptId'];
      this.ticketName = params['apptDataName'];
      this.ticketDate = this.commonService.getUsrDtStrFrmDBStr(params['apptDataDate']);
    });
    this.datePickerConfig = Object.assign({},
      {
        showWeekNumbers: false,
        containerClass: 'theme-blue',
      });
  }
  ngOnInit() {
    if (this.clientId) {
      this.getClientData(this.clientId);
    }
    this.endDate = new Date(this.date.getTime());
    this.endDate.setDate(this.date.getDate());
    this.startDate = new Date(this.date.setMonth(this.date.getMonth() - 1));
    this.getWorkerDetails();
    this.getRefundData();
    this.getCloverInfo();
    if (JSON.parse(localStorage.getItem('browserObject')) !== null) {
      const local = JSON.parse(localStorage.getItem('browserObject'));
      if (!local.CashDrawer) {
        this.local = 'N/A';
      } else {
        this.local = local.CashDrawer.split(' ')[0];
      }
    } else {
      this.local = 'N/A';
    }
    // this.refundSearch();
  }
  getWorkerDetails() {
    this.refundNewService.getWorkerDetails().subscribe(data => {
      this.workerList = data['result'].filter(filterList => filterList.IsActive);
      this.refundBy = '';
    });
  }
  getRefundData() {
    this.refundNewService.getRefundData(this.apptId).subscribe(data => {
      this.refundData = data['result'];
      this.totalfixAmt = 0.00;
      this.refundData.forEach(element => {
        element.Net_Price__c = element.Net_Price__c ? element.Net_Price__c.toFixed(2) : element.Net_Price__c;
        element.oldAmt = +element.Net_Price__c;
        this.totalAmt += +element.Net_Price__c;
        element['showCheckbox'] = false;
        //  this.totalfixAmt = this.totalAmt.toFixed(2);
        if (element.Booked_Package__c) {
          element.Name = element.Name + ' (' + 'Package' + ')';
        }
        if (element.Name === 'Gift') {
          element.Name = element.Name + ' (' + element.Gift_Number__c + ')';
          element.Net_Price__c = (element.Net_Price__c - element.currentBalnce).toFixed(2);
          // this.showCheckbox = true;
          element['showCheckbox'] = true;
        }
      });
    });
  }
  getClientData(clientId) {
    this.refundNewService.getClient(clientId)
      .subscribe(data => {
        this.clientData = data['result'].results[0];
        if (this.clientData.FirstName && this.clientData.LastName) {
          this.clientName = this.clientData.FirstName + ' ' + this.clientData.LastName;
        } else {
          this.clientName = '';
        }
      }, error => {
        const status = JSON.parse(error['status']);
        const statuscode = JSON.parse(error['_body']).status;
        switch (JSON.parse(error['_body']).status) {
          case '2033':
            window.scrollTo(0, 0);
            break;
        }
        if (statuscode === '2085' || statuscode === '2071') {
          if (this.router.url !== '/') {
            localStorage.setItem('page', this.router.url);
            this.router.navigate(['/']).then(() => { });
          }
        }
      });
  }

  hide() {
    this.totalfixAmt = 0.00;
    this.totaltaxAmt = 0.00;
    this.searchList = false;
    this.refundData = [];
    this.electroniclist = true;
    this.refundTOData = [];
    this.refundto = false;
    this.refundSaveBut = false;
  }
  clear() {
    this.refundByError = '';
    this.refundErr = '';
    this.amountMatcherror = '';
  }
  checkAll(value) {
    let temp = 0;
    this.selectValAr = [];
    if (!value) {
      this.refundto = false;
      this.totalAmt = 0.00;
      this.totalfixAmt = 0.00;
      for (let i = 0; i < this.refundData.length; i++) {
        this.selectValAr[i] = value;
      }
    } else {
      for (let i = 0; i < this.refundData.length; i++) {
        if (this.refundData[i]['Name'].indexOf('Gift') === -1) {
          this.selectValAr[i] = value;
          temp++;
        }
      }
      if (temp === 0) {
        value = false;
      }
      if (value === true) {
        this.calculateRefundAmount(value, 0);
      }
    }
  }
  calculateRefundAmount(value, index) {
    this.selectValAr[index] = value;
    if (this.selectValAr[index] === true) {
      this.taxvalue = 0;
      this.totalAmt = 0;
      this.totalfixAmt = 0.00;
      this.selectValAr[index] = value;
      // start refund to list data
      this.refundNewService.getRefundTO(this.apptId)
        .subscribe(data => {
          this.refund = data['result'][0];
          this.accountChanrgeId = data['result'][1][0]['Id'];
          this.accountChanrgeName = data['result'][1][0]['Name'];
          this.refund.forEach(element => {
            element.OriginalPaymentAmount = element.Amount_Paid__c;
          });
          const Cindex = this.refund.findIndex(el => el.Name === 'Cash');
          const Aindex = this.refund.findIndex(el => el.Name === 'Account Charge');
          const temp = this.refund.filter(el => el.Name === 'Prepaid Package');
          let Amount_Paid__c = 0;
          if (temp.length > 0) {
            Amount_Paid__c = temp[0]['Amount_Paid__c'];
            this.refund.forEach(element => {
              if (element['Name'] === 'Prepaid Package') {
                this.refund.push({
                  Id: data['result'][1][0]['Id'],
                  ptId: data['result'][1][0]['Id'],
                  Amount_Paid__c: Amount_Paid__c,
                  OriginalPaymentAmount: Amount_Paid__c,
                  Name: data['result'][1][0]['Name'] + ' (' + 'Prepaid package' + ')',
                });
              }
            });
          }
          if (Cindex === -1) {
            this.refund.push({
              Id: data['result'][1][1]['Id'],
              ptId: data['result'][1][1]['Id'],
              Name: data['result'][1][1]['Name'],
              Amount_Paid__c: '0.00',
              OriginalPaymentAmount: '0.00'
            });
          }
          if (Aindex === -1 && temp.length === 0) {
            this.refund.push({
              Id: data['result'][1][0]['Id'],
              ptId: data['result'][1][0]['Id'],
              Amount_Paid__c: Amount_Paid__c > 0 ? Amount_Paid__c : '0.00',
              OriginalPaymentAmount: '0.00',
              Name: data['result'][1][0]['Name'],
            });
          }
          if (this.refund.length > 0) {
            this.refundTOData = this.refund.filter(function (obj) { return obj.Id; });
            this.refundTOData = this.refundTOData.filter(ele => ele.Name !== 'Prepaid Package');
            this.refundSaveBut = true;
          }
          for (let i = 0; i < this.refundTOData.length; i++) {
            if (this.refundTOData[i]['Name'] === 'Gift Redeem') {
              this.refundTOData[i]['Name'] = this.refundTOData[i]['Name'] + ' (' + this.refundTOData[i]['Gift_Number__c'] + ')';
            }
          }
        }, error => {
          const status = JSON.parse(error['status']);
          const statuscode = JSON.parse(error['_body']).status;
          switch (JSON.parse(error['_body']).status) {
            case '2033':
              window.scrollTo(0, 0);
              break;
          }
          if (statuscode === '2085' || statuscode === '2071') {
            if (this.router.url !== '/') {
              localStorage.setItem('page', this.router.url);
              this.router.navigate(['/']).then(() => { });
            }
          }
        });
      // end refund to list data
      this.refundto = true;
      this.refundData.forEach((element, index) => {
        if (element.tax === null) {
          element.tax = 0;
        }
        if (this.selectValAr[index] === true) {
          if (element.oldAmt !== 0) {
            let co_tax = 0;
            if (element.Taxable__c === 1) {
              co_tax = element.tax;
            } else { co_tax = 0; }
            if (element.Net_Price__c !== element.oldAmt) {
              if (element.Name === 'Package') {
                this.totalAmt += Number(element.remianingPakagePrice) + (Number(element.remianingPakagePrice) / Number(element.oldAmt)) * co_tax;
              } else {
                this.totalAmt += Number(element.Net_Price__c) + (Number(element.Net_Price__c) / Number(element.oldAmt)) * co_tax;
              }
            } else {
              this.totalAmt += Number(element.oldAmt) + co_tax;
            }
          } else {
            if (element.Taxable__c === 1) {
              this.totalAmt += Number(element.Net_Price__c) + element.tax;
            } else {
              this.totalAmt += Number(element.Net_Price__c);
            }
          }
        }
      });
      this.totalfixAmt = this.totalAmt.toFixed(2);
    } else if (this.selectValAr[index] === false) {  // if user unchecked
      this.amountMatcherror = '';
      for (let t = 0; t < this.refundData.length; t++) {
        if (index === t) {
          if (this.refundData[t].oldAmt !== 0) {
            let co_tax = 0;
            if (this.refundData[t].Taxable__c === 1) {
              co_tax = this.refundData[t].tax;
              this.taxvalue -= (this.refundData[t].Net_Price__c / this.refundData[t].oldAmt) * this.refundData[t].tax;
            } else {
              co_tax = 0;
            }
            this.totalAmt -= Number(this.refundData[t].Net_Price__c) + (Number(this.refundData[t].Net_Price__c) / Number(this.refundData[t].oldAmt)) * co_tax;
          } else {
            if (this.refundData[t].Taxable__c === 1) {
              this.totalAmt -= Number(this.refundData[t].Net_Price__c) + this.refundData[t].tax;
            } else {
              this.totalAmt -= Number(this.refundData[t].Net_Price__c);
            }
          }
          const uncheck = this.selectValAr.filter(function (obj) {
            return obj === true;
          });
          if (uncheck.length === 0) {
            this.refundto = false;
            this.refundSaveBut = false;
          }
        }
      }
      this.totaltaxAmt = this.taxvalue.toFixed(2);
      this.totalfixAmt = this.totalAmt.toFixed(2);
      if (this.totaltaxAmt === '-0.00' || this.totaltaxAmt === '0.00') {
        this.totaltaxAmt = 0.00;
      }
      if (this.totalfixAmt === '-0.00' || this.totalfixAmt === '0.00') {
        this.totalfixAmt = 0.00;
      }
    }
  }

  cancel() {
    this.router.navigate(['/checkout']);
  }

  RefundToSave() {
    this.checkedData = [];
    this.refundSaveData = [];
    this.refundTOData.forEach((element, index) => {
      if (element.Amount_Paid__c !== 0 && element.Amount_Paid__c !== 0.00 && element.Amount_Paid__c !== null) {
        this.refundSaveData.push({
          PaymentType: element.Name,
          AmountToRefund: element.Amount_Paid__c,
          OriginalPaymentAmount: element.OriginalPaymentAmount,
          MerchantAccountName: element.Merchant_Account_Name__c === null ? '' : element.Merchant_Account_Name__c,
          ReferenceNumber: element.Reference_Number__c === null ? '' : element.Reference_Number__c,
          ApprovalCode: element.Approval_Code__c === null ? '' : element.Approval_Code__c,
          giftNumber: element.Gift_Number__c,
          Id: element.Id,
          Process_Electronically__c: element.Process_Electronically__c,
          ptId: element.ptId
        });
      }
    });
    for (let t = 0; t < this.refundData.length; t++) {
      if (this.selectValAr[t] === true) {
        if (this.refundData[t].type === 'Product') {
          if (Number(this.refundData[t].Net_Price__c) > Number(this.refundData[t].oldAmt)) {
            this.refundErr = 'CHECK_OUTS.REFUND.REFUND_AMOUNT_NOT_MATCH';
          }
          if (this.refundData[t].Do_Not_Deduct_From_Worker__c === true) {
            this.refundData[t].Do_Not_Deduct_From_Worker__c = 1;
          } else if (this.refundData[t].Do_Not_Deduct_From_Worker__c === false) {
            this.refundData[t].Do_Not_Deduct_From_Worker__c = 0;
          }
          if (this.refundData[t].oldAmt !== 0) {
            this.Product_Tax__c = parseFloat(this.refundData[t].tax) === null ? 0 :
              (parseFloat(this.refundData[t].Net_Price__c) / parseFloat(this.refundData[t].oldAmt) * parseFloat(this.refundData[t].tax)).toFixed(2);
          }
          this.checkedData.push({
            ProductId: this.refundData[t].Product__c, WorkerId: this.refundData[t].Worker__c, Taxable: this.refundData[t].Taxable__c,
            deductFromWorker: this.refundData[t].Do_Not_Deduct_From_Worker__c, Amount: Number(this.refundData[t].Net_Price__c), 'Ticket#': this.refundData[t].apptName,
            Product: this.refundData[t].Name, Quantity: this.refundData[t].Qty_Sold__c, OriginalAmount: Number(this.refundData[t].oldAmt),
            Product_Tax__c: +this.Product_Tax__c, Date: this.refundData[t].Service_Date_Time__c, id: this.refundData[t].Id,
            refundType: this.refundData[t].type, originalId: this.refundData[t].originalId

          });
        } else if (this.refundData[t].type === 'Service') {
          if (this.refundData[t].deductFromWorker === true) {
            this.refundData[t].deductFromWorker = 1;
          } else if (this.refundData[t].deductFromWorker === false) {
            this.refundData[t].deductFromWorker = 0;
          }
          if (Number(this.refundData[t].Net_Price__c) > Number(this.refundData[t].oldAmt)) {
            this.refundErr = 'CHECK_OUTS.REFUND.REFUND_AMOUNT_NOT_MATCH';
          }
          if (this.refundData[t].oldAmt !== 0) {
            this.Service_Tax = parseFloat(this.refundData[t].tax) === null ? 0 :
              ((parseFloat(this.refundData[t].Net_Price__c) / parseFloat(this.refundData[t].oldAmt)) * parseFloat(this.refundData[t].tax)).toFixed(2);
          }
          this.checkedData.push({
            ServiceId: this.refundData[t].Service__c, WorkerId: this.refundData[t].Worker__c, Taxable: this.refundData[t].Taxable__c,
            deductFromWorker: this.refundData[t].Do_Not_Deduct_From_Worker__c, Amount: Number(this.refundData[t].Net_Price__c), OriginalAmount: Number(this.refundData[t].oldAmt),
            Service_Tax: +this.Service_Tax, Date: this.refundData[t].Service_Date_Time__c, id: this.refundData[t].Id,
            Service_Group_Color__c: this.refundData[t].Service_Group_Color__c, refundType: this.refundData[t].type, originalId: this.refundData[t].originalId
          });
        } else if (this.refundData[t].type === 'Other') {
          if (Number(this.refundData[t].Net_Price__c) > Number(this.refundData[t].oldAmt)) {
            this.refundErr = 'CHECK_OUTS.REFUND.REFUND_AMOUNT_NOT_MATCH';
          }
          if (this.refundData[t]['Name'] === 'Package') {
            this.refundData[t].Net_Price__c = this.refundData[t].remianingPakagePriceWotTax;
          }
          this.checkedData.push({
            deductFromWorker: this.refundData[t].Do_Not_Deduct_From_Worker__c, giftNumber: this.refundData[t].Gift_Number__c,
            WorkerId: this.refundData[t].Worker__c, Amount: Number(this.refundData[t].Net_Price__c),
            Taxable: this.refundData[t].Taxable__c, OriginalAmount: Number(this.refundData[t].oldAmt),
            otherTax: parseFloat(this.refundData[t].tax) === null ? 0 :
              ((parseFloat(this.refundData[t].Net_Price__c) / parseFloat(this.refundData[t].oldAmt)) * parseFloat(this.refundData[t].tax)).toFixed(2),
            workerName: this.refundData[t].workerName, refundType: this.refundData[t].type, originalId: this.refundData[t].originalId,
            Name: this.refundData[t].Name, Package_Price__c: this.refundData[t].Package_Price__c, Package__c: this.refundData[t].Package__c,
            Membership__c: this.refundData[t].Membership__c
          });
        } else {
          if (Number(this.refundData[t].Net_Price__c) > Number(this.refundData[t].oldAmt)) {
            this.refundErr = 'CHECK_OUTS.REFUND.REFUND_AMOUNT_NOT_MATCH';
          }
          this.checkedData.push({
            WorkerId: this.refundData[t].Worker__c, Amount: Number(this.refundData[t].Net_Price__c), deductFromWorker: this.refundData[t].Do_Not_Deduct_From_Worker__c,
            Taxable: this.refundData[t].Taxable__c, OriginalAmount: Number(this.refundData[t].oldAmt),
            otherTax: parseFloat(this.refundData[t].tax) === null ? 0 :
              ((parseFloat(this.refundData[t].Net_Price__c) / parseFloat(this.refundData[t].oldAmt)) * parseFloat(this.refundData[t].tax)).toFixed(2),
            workerName: this.refundData[t].workerName, refundType: this.refundData[t].type, originalId: this.refundData[t].originalId,
            tipOption: this.refundData[t].Tip_Option__c
          });
        }
      }
    }
    const saveRec = {
      clientId: this.clientId,
      clientname: this.clientName,
      totalAmt: this.totalfixAmt,
      refundBy: this.refundBy,
      selectList: this.checkedData,
      Drawer_Number__c: this.local !== 'N/A' ? this.local : '',
      refundToList: this.refundSaveData.filter(function (obj) { return obj.AmountToRefund && Number(obj.AmountToRefund) !== 0; }),
      Appt_Date_Time__c: this.commonService.getDBDatTmStr(new Date()),
      apptId: this.apptId
    };
    this.amountMatcherror = '';
    const refundCardOnFile = saveRec.refundToList.filter((obj) => obj.Process_Electronically__c === 1 && obj.PaymentType === 'Card On File');
    if (refundCardOnFile.length > 0) {
      for (let i = 0; i < refundCardOnFile.length; i++) {
        this.checkRefund(refundCardOnFile[i], saveRec);
      }
    } else {
      this.savePaymentsData(null, saveRec);
    }
  }
  checkRefund(obj, saveRec) {
    const d = new Date();
    let paymentDatas;
    const url = config.ANYWHERECOMMERCE_PAYMENT_API;
    const dateTime = ('00' + (d.getMonth() + 1)).slice(-2) + '-' + ('00' + d.getDate()).slice(-2) + '-' +
      (d.getFullYear() + '').slice(-2) + ':' +
      ('00' + d.getHours()).slice(-2) + ':' +
      ('00' + d.getMinutes()).slice(-2) + ':' +
      ('00' + d.getSeconds()).slice(-2) + ':000';
    const refNumber = obj['ReferenceNumber'];
    const refAmount = +obj['AmountToRefund'].toFixed(2);
    this.commonService.ecommerceDetails().subscribe(data => {
      this.ecommerceData = JSON.parse(data.result[0].JSON__c);
      const hashs = Md5.hashStr(this.ecommerceData.storeTerminalID + refNumber + refAmount
        + dateTime + this.ecommerceData.sharedSecret);
      const paymentObject = {
        uniqueref: refNumber,
        terminalid: this.ecommerceData.storeTerminalID,
        amountRefund: refAmount,
        dateTime: dateTime,
        hash: hashs,
        clientName: this.clientName,
        refund: 'Refund'
      };
      const tokenBody = this.commonService.refundPayment(paymentObject);
      const reqObj = {
        url: url,
        xml: tokenBody,
        saveRec: saveRec,
        reference: paymentObject.uniqueref
      };
      if (this.ecommerceData.storeTerminalID && this.ecommerceData.sharedSecret) {
        this.refundNewService.xmlPayment(reqObj).subscribe(
          data1 => {
            const parseString = require('xml2js').parseString;
            parseString(data1['result'], function (err, result) {
              paymentDatas = result;
            });
            if (paymentDatas.ERROR && paymentDatas.ERROR.ERRORSTRING[0] === 'Invalid UNIQUEREF field') {
              saveRec.refundToList.forEach(element => {
                if (element.ReferenceNumber === reqObj.reference) {
                  element.ptId = this.accountChanrgeId;
                  element.Id = this.accountChanrgeId;
                  element.PaymentType = this.accountChanrgeName;
                  element.Process_Electronically__c = 0;
                }
              });
              this.toastr.warning('Payment Failed due to payment done already', null, { timeOut: 3000 });
            } else if (paymentDatas.REFUNDRESPONSE && paymentDatas.REFUNDRESPONSE.RESPONSECODE[0] === 'A') {
              this.savePaymentsData(paymentDatas, saveRec);
              this.toastr.success('Your payment was successfully completed', null, { timeOut: 3000 });
              localStorage.removeItem('client_token');
            } else {
              this.toastermessage = this.translateService.get('Payment Failed');
              this.toastr.warning(this.toastermessage.value, null, { timeOut: 3000 });
            }
          });
      } else {
        this.toastermessage = this.translateService.get('COMMON_TOAST_MESSAGES.TOAST_NO_MERCHANT');
        this.toastr.error(this.toastermessage.value, null, { timeOut: 3000 });
      }
    });
  }

  savePaymentsData(paymentDatas, saveRec) {
    if (paymentDatas) {
      saveRec.refundToList[0]['ReferenceNumber'] = paymentDatas.REFUNDRESPONSE.UNIQUEREF[0];
    }
    if (this.refundErr === '') {
      this.refundNewService.postRefundData(saveRec)
        .subscribe(data => {
          this.refundPostData = data;
          this.popupModal.hide();
          this.router.navigate(['/checkout']);
        }, error => {
          const status = JSON.parse(error['status']);
          const statuscode = JSON.parse(error['_body']).status;
          switch (JSON.parse(error['_body']).status) {
            case '2033':
              window.scrollTo(0, 0);
              break;
          }
          if (statuscode === '2085' || statuscode === '2071') {
            if (this.router.url !== '/') {
              localStorage.setItem('page', this.router.url);
              this.router.navigate(['/']).then(() => { });
            }
          }
        }
        );
    }
  }

  save() {
    const cloverRefAry = [];
    let toref = 0;
    if (this.refundBy === '' || this.refundBy === undefined || this.refundBy === null) {
      this.refundByError = 'CHECK_OUTS.REFUND.SELECT_WORKER';
      scrollTo(0, 0);
    } else {
    this.refundTOData.forEach((element, index) => {
      if (index === 0) {
        toref = element.Amount_Paid__c ? Number(element.Amount_Paid__c) : 0;
      } else {
        toref += element.Amount_Paid__c ? Number(element.Amount_Paid__c) : 0;
      }
      if (element['Name'] === 'Clover') {
        this.popupModal.show();
        cloverRefAry.push({
          payId: element['Approval_Code__c'],
          refId: element['Reference_Number__c'],
          amt: element['Amount_Paid__c'] * 100,
          index: index
        });
        this.cloverIndex = index;
      }
    });
      if (this.totalfixAmt === toref.toFixed(2).toString()) {
        const refId = <HTMLInputElement>document.getElementById('refundId');
        if (cloverRefAry.length > 0 && cloverRefAry[0]['amt'] > 0 && !refId.value) {
          const paymId = <HTMLInputElement>document.getElementById('paymentId');
          paymId.value = cloverRefAry[0]['payId'];
          const ordId = <HTMLInputElement>document.getElementById('orderId');
          ordId.value = cloverRefAry[0]['refId'];
          const amt = <HTMLInputElement>document.getElementById('amtId');
          amt.value = cloverRefAry[0]['amt'];
          this.refundNewService.getCloverDevices().subscribe(data => {
            this.cloverDeviceList = data['result'];
            if (this.cloverDeviceList.length === 0) {
              this.toastr.warning('No Clover Device found to refund', null, { timeOut: 4000 });
            } else if (this.cloverDeviceList.length === 1) {
              this.cloverDevice = this.cloverDeviceList[0]['id'];
              // this.connectCloverDevice();
            } else {
              this.cloverDevice = this.cloverDeviceList[0]['id'];
              this.cloverModal.show();
            }
          }, error => {
            const errStatus = JSON.parse(error['_body'])['status'];
            switch (JSON.parse(error['_body'])['status']) {
              case '2102':
                this.toastermessage = this.translateService.get(JSON.parse(error['_body']).message);
                this.toastr.error(this.toastermessage.value, null, { timeOut: 3000 });
                break;
              case '2103':
                this.toastermessage = this.translateService.get(JSON.parse(error['_body']).result);
                this.toastr.error(this.toastermessage.value, null, { timeOut: 3000 });
                break;
              case 500:
                break;
            }
            if (errStatus === '2085' || errStatus === '2071') {
              if (this.router.url !== '/') {
                localStorage.setItem('page', this.router.url);
                this.router.navigate(['/']).then(() => { });
              }
            }
          });
        } else {
          this.RefundToSave();
        }
      } else {
        this.amountMatcherror = 'CHECK_OUTS.REFUND.AMOUNT_NOT_MATCH';
      }
    }
  }
  getCloverInfo() {
    this.refundNewService.getCloverInfo().subscribe(
      data => {
        this.cloverInfo = data['result'];

      }, error => {
        const status = JSON.parse(error['status']);
        const statuscode = JSON.parse(error['_body']).status;
        if (statuscode === '2085' || statuscode === '2071') {
          if (this.router.url !== '/') {
            localStorage.setItem('page', this.router.url);
            this.router.navigate(['/']).then(() => { });
          }
        }
      });
  }
  connectCloverDevice() {
    this.toastr.info('Please wait, connecting to Clover device', null, { timeOut: 5000 });
    const args = [this, config.cloverCfg.remoteAppId,
      clover.BrowserWebSocketImpl.createInstance,
      new clover.ImageUtil(),
      config.cloverCfg.server,
      this.cloverInfo['accessToken'],
      new clover.HttpSupport(XMLHttpRequest),
      this.cloverInfo['merchantId'],
      this.cloverDevice,
      'guestId'];
    const cloverConnectorFactoryConfiguration = {};
    cloverConnectorFactoryConfiguration[clover.CloverConnectorFactoryBuilder.FACTORY_VERSION] = clover.CloverConnectorFactoryBuilder.VERSION_12;
    const cloverConnectorFactory = clover.CloverConnectorFactoryBuilder.createICloverConnectorFactory(cloverConnectorFactoryConfiguration);
    if (this.cloverConnector) {
      this.cloverConnector.dispose();
    }
    this.cloverConnector = cloverConnectorFactory.createICloverConnector(new (Function.prototype.bind.apply(clover.WebSocketCloudCloverDeviceConfiguration, args)));
    this.setCloverConnectorListener(this.cloverConnector);
    this.setDisposalHandler();
    this.cloverConnector.initializeConnection();
    this.cloverModal.hide();
  }

  setCloverConnectorListener(cloverConnector) {
    const CloverConnectorListener = function (connector) {
      const clvObj = new clover.remotepay.ICloverConnectorListener();
      this.cloverConnector = connector;
    };
    CloverConnectorListener.prototype = Object.create(clover.remotepay.ICloverConnectorListener.prototype);
    CloverConnectorListener.prototype.constructor = CloverConnectorListener;
    CloverConnectorListener.prototype.onDeviceConnected = function () {
    };
    CloverConnectorListener.prototype.onDeviceReady = function (merchInfo) {
      const toastEle = <HTMLInputElement>document.getElementById('toastMsgId');
      toastEle.value = 'Connectd to device ' + merchInfo.deviceInfo.name + '---' + 'success';
      const evObj = document.createEvent('Events');
      evObj.initEvent('click', true, false);
      toastEle.dispatchEvent(evObj);
      const rpr = new clover.remotepay.RefundPaymentRequest();
      const paymId = <HTMLInputElement>document.getElementById('paymentId');
      const ordId = <HTMLInputElement>document.getElementById('orderId');
      const amt = <HTMLInputElement>document.getElementById('amtId');
      rpr.setPaymentId(paymId.value);
      rpr.setOrderId(ordId.value);
      rpr.setAmount(parseInt(amt.value, 10));
      // rpr.setFullRefund(true);
      cloverConnector.refundPayment(rpr);
    };
    CloverConnectorListener.prototype.onDeviceError = function (deviceErrorEvent) {
      if (deviceErrorEvent.message.indexOf('java.lang.IllegalArgumentException') === -1) {
        const toastEle = <HTMLInputElement>document.getElementById('toastMsgId');
        toastEle.value = deviceErrorEvent.getMessage() + '---' + 'error';
        const evObj = document.createEvent('Events');
        evObj.initEvent('click', true, false);
        toastEle.dispatchEvent(evObj);
      }
    };
    CloverConnectorListener.prototype.onDeviceDisconnected = function () {
      const toastEle = <HTMLInputElement>document.getElementById('toastMsgId');
      toastEle.value = 'Clover device disconnected' + '---' + 'error';
      const evObj = document.createEvent('Events');
      evObj.initEvent('click', true, false);
      toastEle.dispatchEvent(evObj);
    };
    CloverConnectorListener.prototype.onRefundPaymentResponse = function (onRefundPaymentResponse) {
      if (onRefundPaymentResponse.result === 'SUCCESS') {
        const refId = <HTMLInputElement>document.getElementById('refundId');
        refId.value = onRefundPaymentResponse.refund.id;
        const completeTrs: any = document.getElementById('completeTrsId');
        const evObj = document.createEvent('Events');
        evObj.initEvent('click', true, false);
        completeTrs.dispatchEvent(evObj);
      } else {
        const toastEle = <HTMLInputElement>document.getElementById('toastMsgId');
        toastEle.value = 'Failed to refund' + '---' + 'error';
        const evObj = document.createEvent('Events');
        evObj.initEvent('click', true, false);
        toastEle.dispatchEvent(evObj);
      }
    };
    this.cloverConnectorListener = new CloverConnectorListener(this.cloverConnector);
    this.cloverConnector.addCloverConnectorListener(this.cloverConnectorListener);
  }

  setDisposalHandler() {
    window.onbeforeunload = function (event) {
      try {
        this.cloverConnector.dispose();
      } catch (e) {
        // console.error(e);
      }
    }.bind(this);
  }

  displayToast() {
    let toastMsg: any = <HTMLInputElement>document.getElementById('toastMsgId');
    toastMsg = toastMsg.value.split('---');
    if (toastMsg[1] === 'error') {
      this.toastr.error(toastMsg[0], null, { timeOut: 4000 });
    } else if (toastMsg[1] === 'info') {
      this.toastr.info(toastMsg[0], null, { timeOut: 2000 });
    } else {
      this.toastr.success(toastMsg[0], null, { timeOut: 1000 });
    }
  }

  cloverRefund() {
    this.toastr.success('Completed refund through clover', null, { timeOut: 4000 });
    const refId = <HTMLInputElement>document.getElementById('refundId');
    this.refundTOData[this.cloverIndex]['Reference_Number__c'] = refId.value;
    this.RefundToSave();
  }
  calQuantity(quantity, item, i) {
    this.taxvalue = 0;
    this.totalAmt = 0;
    const refundList = item;
    if (refundList.Qty_Sold__c !== 'null' || refundList.Qty_Sold__c !== null) {
      if (refundList.Qty_Sold__c <= refundList.dummyQty) {
        const divPrice = (refundList.newPrice / refundList.dummyQty);
        refundList.newPrice = refundList.dummyPrice * quantity;
        let ser_tax = 0;
        if (refundList.Taxable__c === 1) {
          ser_tax = refundList.Product_Tax__c;
          this.taxvalue += (refundList.newPrice / (+refundList.dummyPrice * refundList.dummyQty)) * refundList.Product_Tax__c;
        } else { ser_tax = 0; }
        this.totalAmt += (refundList.newPrice) + (refundList.newPrice / (+refundList.dummyPrice * refundList.dummyQty)) * ser_tax;
        this.totaltaxAmt = this.taxvalue.toFixed(2);
        this.totalfixAmt = this.totalAmt.toFixed(2);
        if (this.totaltaxAmt === '-0.00' || this.totaltaxAmt === '0.00') {
          this.totaltaxAmt = 0;
        }
        if (this.totalfixAmt === '-0.00' || this.totalfixAmt === '0.00') {
          this.totalfixAmt = 0;
        }
      } else {
        this.toastr.warning('Refund quantity should not be exceeded to the purchased quantity', null, { timeOut: 4000 });
      }
    }
  }
}
