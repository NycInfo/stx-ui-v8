import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RefundNewComponent } from './refundnew.component';
import { RefundNewRoutingModule } from './refundnew.routing';
import { FormsModule } from '@angular/forms';
import { ShareModule } from '../../common/share.module';
import { TranslateModule } from '@ngx-translate/core';
import { ModalModule } from 'ngx-bootstrap/modal';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        TranslateModule,
        RefundNewRoutingModule,
        BsDatepickerModule.forRoot(),
        ShareModule,
        ModalModule.forRoot()
    ],
    declarations: [
        RefundNewComponent
    ]
})
export class RefundNewModule {
}
