
import { Injectable, Inject } from '@angular/core';
import { Response } from '@angular/http';
import { Headers } from '@angular/http';
import { HttpClients } from '../common/http-client';
import { map } from 'rxjs/operators';

@Injectable()
export class ClientappointmentsService {
  constructor(
    private http: HttpClients,
    @Inject('apiEndPoint') private apiEndPoint: string
  ) { }
  /*To extract json data*/
  getVisitTypes() {
    return this.http.get(this.apiEndPoint + '/api/setup/clientpreferences/visittype/active')
      .pipe(map(this.extractData));
  }
  getServiceGroups(type, reqDate) {
    const headers = new Headers();
    headers.append('onlinebooking', 'false');
    headers.append('bookingdate', reqDate);
    return this.http.getHeader(this.apiEndPoint + '/api/setupservices/servicegroups/active/forappts', headers)
      .pipe(map(this.extractData));
  }
  searchAppt(dataObj) {
    return this.http.post(this.apiEndPoint + '/api/clientSearch/bookappointments', dataObj)
      .pipe(map(this.extractData));
  }
  searchForAppts(dataObj) {
    const header = new Headers();
    header.append('params', JSON.stringify(dataObj));
    return this.http.getHeader(this.apiEndPoint + '/api/appointment/search', header)
      .pipe(map(this.extractData));
  }
  getBookingData() {
    return this.http.get(this.apiEndPoint + '/api/appointmentsandemails/booking')
      .pipe(map(this.extractData));
  }
  getClient(clientId) {
    return this.http.get(this.apiEndPoint + '/api/client/' + clientId)
      .pipe(map(this.extractData));
  }
  getAllActivePackages(currentDate) {
    const headers = new Headers();
    headers.append('currentdate', currentDate);
    headers.append('onlinebook', '0');
    return this.http.getHeader(this.apiEndPoint + '/api/setupservices/servicepackage/forappt', headers)
      .pipe(map(this.extractData));
  }
  fetchingActiveMembers(dataObj) {
    return this.http.post(this.apiEndPoint + '/api/clientsearch/activemembers', dataObj)
      .pipe(map(this.extractData));
  }
  appointmentBooking(apptdata) {
    return this.http.post(this.apiEndPoint + '/api/clientSearch/appointmentbooking', apptdata)
      .pipe(map(this.extractData));
  }
  getServices(serviceName, type, bookingdate: string) {
    const headers = new Headers();
    headers.append('bookingdate', bookingdate);
    return this.http.getHeader(this.apiEndPoint + '/api/setupticketpreferences/favorites/types/' + serviceName + '/' + type, headers)
      .pipe(map(this.extractData));
  }
  getUsers(bookingdata: any) {
    const headers = new Headers();
    headers.append('onlinebooking', 'false');
    headers.append('bookinginfo', JSON.stringify(bookingdata));
    return this.http.getHeader(this.apiEndPoint + '/api/bookingdata/bookappt', headers)
      .pipe(map(this.extractData));
  }
  getApptServices(clientId, apptId, reqDate) {
    const headers = new Headers();
    headers.append('bookingdate', reqDate);
    return this.http.getHeader(this.apiEndPoint + '/api/appointments/services/' + clientId + '/' + apptId, headers)
      .pipe(map(this.extractData));
  }
  editWaitListingData(id) {
    return this.http.get(this.apiEndPoint + '/api/waitinglistdata/' + id)
      .pipe(map(this.extractData));
  }
  /** Method to get preferences for service tax and retail tax calculation */
  getServProdTax() {
    return this.http.get(this.apiEndPoint + '/api/setup/ticketpreferences/pos')
      .pipe(map(this.extractData));
  }
  sendApptNotifs(apptsAry) {
    return this.http.post(this.apiEndPoint + '/api/notification/email', { 'apptIds': apptsAry, sentNotify: true, page : 'findappt'})
      .pipe(map(this.extractData));
  }
  expressBookingServices(data, date) {
    return this.http.get(this.apiEndPoint + '/api/appointments/expressbookingservices/' + data + '/' + date)
      .pipe(map(this.extractData));
  }
  getWorkerLists(date) {
    return this.http.get(this.apiEndPoint + '/api/appointment/workerList/' + date)
      .pipe(map(this.extractData));
  }
  private extractData(res: Response) {
    if (res.headers && res.headers.get('token')) {
      localStorage.setItem('token', res.headers.get('token'));
    }
    const body = res.json();
    return body || {};
  }
}
