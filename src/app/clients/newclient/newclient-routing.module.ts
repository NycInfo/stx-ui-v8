import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { NewClientComponent } from './newclient.component';
@NgModule({
  imports: [RouterModule.forChild([
    {
        path: '',
        component: NewClientComponent,
        children: [
            {
                path: '',
                component: NewClientComponent
            }
        ]
    }
])],
  exports: [RouterModule]
})
export class NewClientRoutingModule { }
