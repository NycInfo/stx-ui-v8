/*
  * Display single user details of front end users
  * extractData(): To extract the data
  * handleError(): To handle error messages.
*/
import { Injectable, Inject } from '@angular/core';
import { Response } from '@angular/http';
// import { Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { HttpClients } from '../../common/http-client';
import { map } from 'rxjs/operators';

@Injectable()
export class NewClientFormService {
    constructor(
        private http: HttpClients,
        @Inject('apiEndPoint') private apiEndPoint: string,
        @Inject('staticJsonFilesEndPoint') private staticJsonFilesEndPoint: string
    ) { }
    getClientFields() {
        return this.http.get(this.apiEndPoint + '/api/setup/clientpreferences/clientfields')
            .pipe(map(this.extractData));
    }
    getOccupations() {
        return this.http.get(this.apiEndPoint + '/api/setup/clientpreferences/occupations')
            .pipe(map(this.extractData));
    }
    getStates(countryName) {
        return this.http.get(this.apiEndPoint + '/api/v1/lookups/states/' + countryName)
            .pipe(map(this.extractData));
    }
    getHideCliContactInfo(id) {
        return this.http.get(this.apiEndPoint + '/api/client/getHideClientContactInfo/' + id)
            .pipe(map(this.extractData));
    }
    /* Method to get consultation questions data */
    getformQuestions() {
        return this.http.get(this.apiEndPoint + '/api/setup/clientpreferences/formquestions')
            .pipe(map(this.extractData));
    }
    /* Method to get consultation checked data data */
    getApplyData() {
        return this.http.get(this.apiEndPoint + '/api/setup/clientpreferences/formcheckboxes')
            .pipe(map(this.extractData));
    }
    getClient(clientId) {
        return this.http.get(this.apiEndPoint + '/api/client/' + clientId)
            .pipe(map(this.extractData));
    }
    saveClient(clientId, clientObj) {
        if (!clientId) {
            clientId = undefined;
        }
        const formData: any = new FormData();
        formData.append('clientObj', JSON.stringify(clientObj));
        return this.http.put(this.apiEndPoint + '/api/client/' + clientId, formData)
            .pipe(map(this.extractData));
    }
    /*To extract json data*/
    private extractData(res: Response) {
        if (res.headers && res.headers.get('token')) {
            localStorage.setItem('token', res.headers.get('token'));
        }
        const body = res.json();
        return body || {};
    }
}
