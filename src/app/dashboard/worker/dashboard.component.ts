import { Component, OnInit, Inject } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DashBoardService } from './dashboard.service';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { TotalSheetsService } from '../../../app/reports/transactionreports/totalsheets/totalsheets.service';
import { TicketAnalysisReportService } from '../../../app/reports/businessanalysisreports/ticketanalysisreport/ticketanalysisreport.service';
import { filter } from 'rxjs/operator/filter';
import * as config from '../../app.config';
interface TicketAnalysis {
  serviceSales?: number;
  productSales?: number;
  isRefund?: boolean;
  productTicketCount?: number;
  serviceTicketCount?: number;
  bookedOnline?: boolean;
  noService?: boolean;
  newVisit?: boolean;
  prdtCount?: number;
  servcCount?: number;
}
@Component({
  selector: 'app-setuprewards-app',
  templateUrl: './dashboard.html',
  providers: [DashBoardService, TotalSheetsService, TicketAnalysisReportService],
  styleUrls: ['./dashboard.css']
})

export class DashBoardComponent implements OnInit {
  workerSalesObj = [];
  companySalesObj = [];
  totalSales = 0;
  serviceSales = 0;
  productSales = 0;
  averegeNetSales = 0;
  avergeProductSales = 0;
  totalNetAveregeTicket = 0;
  GiftSales = 0;
  // totalNumberOfTickets = 0;
  // totalNumberOfServices = 0;
  // totalNumberOfProducts = 0;
  public reports = new Map<string, TicketAnalysis>();
  packageSales: any = {};
  highestServiceSales: any = {};
  serviceWorkerImage: any = {};
  productWorkerImage: any = {};
  public reportDetails: any = {};
  highestProductSales: any = {};
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private toastr: ToastrService,
    private translateService: TranslateService,
    private totalSheetsService: TotalSheetsService,
    private ticketAnalysisService: TicketAnalysisReportService,
    private DashboardService: DashBoardService,
    @Inject('apiEndPoint') private apiEndPoint: string) {

  }
  ngOnInit() {
    this.generateReport();
    this.reportDetails = {};

  }
  generateReport() {
    const date = new Date();
    const firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
    const lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
    const reportObj = {
      startDate: new Date(date.getFullYear(), date.getMonth(), 1),
      endDate: new Date(date.getFullYear(), date.getMonth() + 1, 0)
    };
    this.ticketAnalysisService.getTicketAnalysisReport(reportObj).subscribe(data => {
      const reportsResult: any = data['result'];
      reportsResult['serviceTxList'].forEach(service => {
        let serviceObj: any = {
          serviceSales: 0,
          productSales: 0,
          isRefund: false,
          productTicketCount: 0,
          serviceTicketCount: 0,
          newVisit: false,
          bookedOnline: false,
          noService: false,
          Qty_Sold__c: 0,
          prdtCount: 0,
          servcCount: 0
        };
        if (this.reports.has(serviceObj.Appt_Ticket__c)) {
          serviceObj = this.reports.get(serviceObj.Appt_Ticket__c);
        }
        serviceObj.serviceSales = service.Service_Sales__c;
        serviceObj.isRefund = service.isRefund__c;
        serviceObj.newVisit = service.New_Client__c;
        serviceObj.bookedOnline = service.Booked_Online__c;
        serviceObj.serviceTicketCount = service.servcCount;
        serviceObj.noService = service.isNoService__c;
        serviceObj.servcCount = service.servcCount;
        this.reports.set(service.Appt_Ticket__c, serviceObj);
      });
      reportsResult['productTxList'].forEach(product => {
        let productObj: any = {};
        if (this.reports.has(product.Appt_Ticket__c)) {
          productObj = this.reports.get(product.Appt_Ticket__c);
        } else {
          productObj = {
            serviceSales: 0,
            productSales: 0,
            isRefund: false,
            productTicketCount: 0,
            serviceTicketCount: 0,
            newVisit: false,
            bookedOnline: false,
            noService: false,
            Qty_Sold__c: 0,
            prdtCount: 0,
            servcCount: 0
          };
        }
        productObj.productSales = product.Product_Sales__c;
        productObj.isRefund = product.isRefund__c;
        productObj.newVisit = product.New_Client__c;
        productObj.bookedOnline = product.Booked_Online__c;
        productObj.productTicketCount = product.prdtCount;
        productObj.noService = product.isNoService__c;
        productObj.Qty_Sold__c = product.Qty_Sold__c;
        productObj.prdtCount = product.prdtCount;
        this.reports.set(product.Appt_Ticket__c, productObj);
      });

      // this.totalNumberOfTickets = 0;
      // this.totalNumberOfServices = 0;
      // this.totalNumberOfProducts = 0;
      this.reportDetails = {};
      this.reportDetails.serviceOnlyCount = 0;
      this.reportDetails.productOnlyCount = 0;
      this.reportDetails.bothCount = 0;
      this.reportDetails.serviceRevenue = 0;
      this.reportDetails.productRevenue = 0;
      this.reportDetails.totalTicketCount = 0;
      this.reportDetails.fullTicketRevenue = 0;
      //// Ticket Average Revenue Details
      this.reportDetails.Avg_Revenue_per_Ticket = 0;
      this.reportDetails.Avg_Service_Revenue_per_Ticket = 0;
      this.reportDetails.Service_Revenue_per_Service_Only_Ticket = 0;
      this.reportDetails.Avg_Product_Revenue_per_Ticket = 0;
      this.reportDetails.Avg_Product_Revenue_per_Product_Only_Ticket = 0;
      this.reportDetails.Avg_Product_Revenue_per_All_Service = 0;
      this.reportDetails.Avg_Product_Revenue_per_Service_Tx_Products = 0;
      //// Ticket Average Revenue Details
      this.reports.forEach((report) => {
        if (report.serviceSales > 0 && (report.productSales == null || report.productSales === 0)) {
          this.reportDetails.serviceOnlyCount++;
        }

        if (report.productSales > 0 && (report.serviceSales == null || report.serviceSales === 0)) {
          this.reportDetails.productOnlyCount++;
        }
        if (report.serviceSales > 0 && report.productSales > 0) {
          this.reportDetails.bothCount++;
        }
        // includes refunds in revenue totals
        this.reportDetails.serviceRevenue += report.serviceSales;
        this.reportDetails.productRevenue += report.productSales;

        this.reportDetails.totalTicketCount++;
      });

      // this.totalNumberOfTickets = this.reportDetails.serviceOnlyCount + this.reportDetails.productOnlyCount + this.reportDetails.bothCount;
      // this.totalNumberOfServices = this.reportDetails.serviceOnlyCount + this.reportDetails.bothCount;
      // this.totalNumberOfProducts = this.reportDetails.productOnlyCount + this.reportDetails.bothCount;
      //// Ticket Average Revenue Calculations
      this.reportDetails.Avg_Revenue_per_Ticket = this.calculateAverage(this.reportDetails.serviceRevenue + this.reportDetails.productRevenue, this.reportDetails.totalTicketCount);
      this.reportDetails.Avg_Service_Revenue_per_Ticket = this.calculateAverage(this.reportDetails.serviceRevenue, this.reportDetails.totalTicketCount);
      this.reportDetails.Service_Revenue_per_Service_Only_Ticket = this.calculateAverage(this.reportDetails.serviceRevenue, this.reportDetails.serviceOnlyCount);
      this.reportDetails.Avg_Product_Revenue_per_Ticket = this.calculateAverage(this.reportDetails.productRevenue, this.reportDetails.totalTicketCount);
      this.reportDetails.Avg_Product_Revenue_per_Product_Only_Ticket = this.calculateAverage(this.reportDetails.productRevenue, this.reportDetails.productOnlyCount);
      this.reportDetails.Avg_Product_Revenue_per_All_Service = this.calculateAverage(this.reportDetails.productRevenue, this.reportDetails.serviceOnlyCount + this.reportDetails.bothCount);
      this.reportDetails.Avg_Product_Revenue_per_Service_Tx_Products = this.calculateAverage(this.reportDetails.productRevenue, this.reportDetails.multipleBothCount);
      //// Ticket Average Revenue Calculations
      this.totalSheetsService.getDailyTotalSheetRecords(firstDay, lastDay).subscribe(
        data1 => {
          this.workerSalesObj = data1['result']['workerSalesObj'];
          this.companySalesObj = data1['result']['companySalesObj'];
          this.totalSales = this.workerSalesObj.filter((obj) => obj.workerName === 'Totals')[0]['totalSales'];
          this.serviceSales = this.workerSalesObj.filter((obj) => obj.workerName === 'Totals')[0]['serviceSales'];
          this.productSales = this.workerSalesObj.filter((obj) => obj.workerName === 'Totals')[0]['productSales'];
          /* For Averege Ticket ,Averege sales and Averege Products  */
          if (this.reportDetails.Avg_Service_Revenue_per_Ticket === 0) {
            this.averegeNetSales = 0;
          } else {
            this.averegeNetSales = (this.serviceSales / this.reportDetails.Avg_Service_Revenue_per_Ticket); /* avrege service sales amount */
          }
          if (this.reportDetails.Avg_Product_Revenue_per_Ticket === 0) {
            this.avergeProductSales = 0;
          } else {
            this.avergeProductSales = (this.productSales / this.reportDetails.Avg_Product_Revenue_per_Ticket); /* avrege Productsales amount */
          }
          if (this.reportDetails.Avg_Revenue_per_Ticket === 0) {
            this.totalNetAveregeTicket = 0;
          } else {
            this.totalNetAveregeTicket = (this.totalSales / this.reportDetails.Avg_Revenue_per_Ticket); /* averege ticket amount */
          }
          /* For Averege Ticket ,Averege sales and Averege Products  End */
          /* For highest Service Sales  */
          this.workerSalesObj.forEach((obj, i) => {
            if (obj.workerName !== 'Totals') {
              if (Object.keys(this.highestServiceSales).length === 0) {
                this.highestServiceSales = obj;
              } else {
                if (obj.serviceSales > this.highestServiceSales.serviceSales) {
                  this.highestServiceSales = obj;
                }
              }
            }
          });
          if (this.highestServiceSales.image) {
            this.serviceWorkerImage.srcURL = config.S3_URL + this.highestServiceSales.image + '?time=' + new Date().getTime();
          }
          /* For highest Service Sales  end */
          /* For highest Product Sales  */
          this.workerSalesObj.forEach((obj, i) => {
            if (obj.workerName !== 'Totals') {
              if (Object.keys(this.highestProductSales).length === 0) {
                this.highestProductSales = obj;
              } else {
                if (obj.productSales > this.highestProductSales.productSales) {
                  this.highestProductSales = obj;
                }
              }
            }
          });
          if (this.highestProductSales.image) {
            this.productWorkerImage.srcURL = config.S3_URL + this.highestProductSales.image + '?time=' + new Date().getTime();
          }
          /* For highest Product Sales end */
          /* For Gift Sales and Package Sales */
          this.GiftSales = 0;
          this.companySalesObj.forEach((obj, i) => {
            if (obj.salesType === 'Gift Sales' || obj.salesType === 'Gift Sales Online') {
              this.GiftSales += obj.fullPrice;
            }
            if (obj.salesType === 'Package Sales') {
              this.packageSales = obj;
            }
          });
          /* For Gift Sales and Package Sales End */
        },
        error => {
          const status = JSON.parse(error['status']);
          const statuscode = JSON.parse(error['_body']).status;
          switch (status) {
            case 500:
              break;
            case 400:
              break;
          }
          if (statuscode === '2085' || statuscode === '2071') {
            if (this.router.url !== '/') {
              localStorage.setItem('page', this.router.url);
              this.router.navigate(['/']).then(() => { });
            }
          }
        }
      );
    },
      error => {
        const errStatus = JSON.parse(error['_body'])['status'];
        if (errStatus === '2085' || errStatus === '2071') {
          if (this.router.url !== '/') {
            localStorage.setItem('page', this.router.url);
            this.router.navigate(['/']).then(() => { });
          }
        }
      });
  }
  imageErrorHandler(event) {
    event.target.src = '/assets/images/no-preview.png';
  }
  calculateAverage(firstValue: number, secondValue: number): number {
    if (secondValue === 0) {
      return 0.00;
    } else {
      let percentage = (firstValue / secondValue);
      percentage = +percentage.toFixed(2);
      return percentage;
    }
  }
}
