import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ManageInventoryComponent } from './manageinventory.component';
import { ManageInventoryRoutingModule } from './manageinventory.routing';
import { TranslateModule } from '@ngx-translate/core';
import { AccordionModule } from 'ngx-bootstrap/accordion';

import { ShareModule } from '../../common/share.module';
import { FormsModule } from '@angular/forms';
@NgModule({
    imports: [
        CommonModule,
        ManageInventoryRoutingModule,
        TranslateModule,
        FormsModule,
        ShareModule,
        AccordionModule
    ],
    declarations: [
        ManageInventoryComponent
    ]
})
export class ManageInventoryModule {
}
