import { Injectable, Inject } from '@angular/core';
import { Response } from '@angular/http';
// import { Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { HttpClients } from '../../common/http-client';
import { map } from 'rxjs/operators';
@Injectable()
export class PurchaseOrderService {
  constructor(private http: HttpClients,
    @Inject('apiEndPoint') private apiEndPoint: string,
    @Inject('staticJsonFilesEndPoint') private staticJsonFilesEndPoint: string
  ) { }
  /* Method to get purchase order data */
  getOrdersData() {
    return this.http.get(this.apiEndPoint + '/api/inventory/purcahseorders')
      .pipe(map(this.extractData));
  }
  /* method for updating purchase order data */
  editPurchaseOrderData(updateId, editDataObj) {
    return this.http.put(this.apiEndPoint + '/api/inventory/purcahseorders/' + updateId, editDataObj)
      .pipe(map(this.extractData));
  }
  /* Method to get Suppliers data */
  getSuppliersData() {
    return this.http.get(this.apiEndPoint + '/api/Inventory/purcahseorders/suppliers')
      .pipe(map(this.extractData));
  }
  /* Method to get product data by search with sku */
  getProductsBySKU(searchKeyWord, supplierId) {
    return this.http.get(this.apiEndPoint + '/api/Inventory/purcahseorders/productsbysuppliers/' + searchKeyWord + '/' + supplierId)
      .pipe(map(this.extractData));
  }
  /*Method to save purchase order data */
  saveData(productsList) {
    return this.http.post(this.apiEndPoint + '/api/inventory/purcahseorders', productsList)
      .pipe(map(this.extractData));
  }
  /* method to show product data (show data) */
  getProductsSupplier(purchageOrderId, supplierId) {
    return this.http.get(this.apiEndPoint + '/api/Inventory/purcahseorders/' + purchageOrderId + '/' + supplierId)
      .pipe(map(this.extractData));
  }
  /* Method to get product list */
  getProducts(supId) {
    return this.http.get(this.apiEndPoint + '/api/inventory/products/' + supId)
      .pipe(map(this.extractData));
  }
  /*method to check uniqness With SupplierName And OrderDate */
  checkIfSupplierAndOrderDateisExist(supplierName, bsValue) {
    return this.http.get(this.apiEndPoint + '/api/inventory/purcahseorders/supplieridandorderdate/' + supplierName + '/' + bsValue)
      .pipe(map(this.extractData));
  }
  /*To extract json data*/
  private extractData(res: Response) {
    if (res.headers && res.headers.get('token')) {
      localStorage.setItem('token', res.headers.get('token'));
    }
    const body = res.json();
    return body || {};
  }
}
