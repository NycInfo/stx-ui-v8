import { Injectable, Inject } from '@angular/core';
import { Response } from '@angular/http';
import { HttpClients } from '../../common/http-client';
import { map } from 'rxjs/operators';
@Injectable()
export class MarketingEmailService {
    constructor(private http: HttpClients,
        @Inject('apiEndPoint') private apiEndPoint: string,
    ) { }

    getEMURL() {
        return this.http.get(this.apiEndPoint + '/api/em/url')
            .pipe(map(this.extractData));
    }
    private extractData(res: Response) {
        if (res.headers && res.headers.get('token')) {
            localStorage.setItem('token', res.headers.get('token'));
          }
        const body = res.json();
        return body || {};
    }


}
