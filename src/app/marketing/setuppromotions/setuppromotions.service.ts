/*
  * Display single user details of front end users
  * extractData(): To extract the data
  * handleError(): To handle error messages.
*/
import { Injectable, Inject } from '@angular/core';
import { Response } from '@angular/http';
// import { Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { HttpClients } from '../../common/http-client';
import { map } from 'rxjs/operators';

@Injectable()
export class SetupPromotionsService {
  constructor(private http: HttpClients,
    @Inject('apiEndPoint') private apiEndPoint: string,
    @Inject('staticJsonFilesEndPoint') private staticJsonFilesEndPoint: string
  ) { }
  getPromotions() {
    return this.http.get(this.apiEndPoint + '/api/marketing/promotion')
     .pipe(map(this.extractData));
   }
   createPromotion(promotionData) {
    return this.http.post(this.apiEndPoint + '/api/marketing/promotion', promotionData)
    .pipe(map(this.extractData));
   }
   editPromotion(updatePromotionId, updatePromotionData) {
     return this.http.put(this.apiEndPoint + '/api/marketing/promotion/' + updatePromotionId, updatePromotionData)
     .pipe(map(this.extractData));
   }
   getSortList(promotionSortOrderData) {
    return this.http.put(this.apiEndPoint + '/api/marketing/promotions/sortorder', promotionSortOrderData)
    .pipe(map(this.extractData));
   }
  /*To extract json data*/
  private extractData(res: Response) {
    if (res.headers && res.headers.get('token')) {
      localStorage.setItem('token', res.headers.get('token'));
    }
    const body = res.json();
    return body || {};
  }
}
