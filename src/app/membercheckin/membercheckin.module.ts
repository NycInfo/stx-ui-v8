import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShareModule } from '../common/share.module';
import { MembercheckinRoutingModule } from './membercheckin-routing.module';
import { MembercheckinComponent } from './membercheckin.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { ModalModule } from 'ngx-bootstrap/modal';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';

import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule,
    MembercheckinRoutingModule,
    ShareModule,
    BsDatepickerModule.forRoot(),
    ModalModule.forRoot(),
    PerfectScrollbarModule
  ],
  declarations: [MembercheckinComponent]
})
export class MemberCheckInModule { }
