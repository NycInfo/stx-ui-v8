import { Injectable, Inject } from '@angular/core';
import { Response } from '@angular/http';
import { URLSearchParams } from '@angular/http';
import { HttpClients } from '../common/http-client';
import { map } from 'rxjs/operators';

@Injectable()
export class MembercheckinService {
    constructor(
        private http: HttpClients,
        @Inject('apiEndPoint') private apiEndPoint: string,
        @Inject('staticJsonFilesEndPoint') private staticJsonFilesEndPoint: string
    ) { }

    getApptDetails(id) {
        return this.http.get(this.apiEndPoint + '/api/membership/getMemberSearch/' + id)
            .pipe(map(this.extractData));
    }
    clientDetails(id) {
        return this.http.get(this.apiEndPoint + '/api/membership/clientDetails/' + id)
            .pipe(map(this.extractData));
    }
    getmemberDetails(id, date) {
        return this.http.get(this.apiEndPoint + '/api/membership/getmemberDetails/' + id + '/' + date)
            .pipe(map(this.extractData));
    }
    xmlPayment(reqObj) {
        return this.http.post(this.apiEndPoint + '/api/payment', reqObj)
            .pipe(map(this.extractData));
    }
    /** To get paymenttypes Data */
    getPaymentTypesData() {
        return this.http.get(this.apiEndPoint + '/api/setupmembership/getPaymentList')
            .pipe(map(this.extractData));
    }
    addToClient(obj) {
        return this.http.post(this.apiEndPoint + '/api/membership/client', { 'obj': [obj] })
            .pipe(map(this.extractData));
    }
    updateClientInfo(obj) {
        return this.http.post(this.apiEndPoint + '/api/membership/ticketpaymentsExist', { 'obj': [obj] })
            .pipe(map(this.extractData));
    }
    addToPaymentsTicket(paymentObj, obj) {
        return this.http.post(this.apiEndPoint + '/api/membership/ticketpayments', { 'paymentObj': [paymentObj], 'obj': [obj] })
            .pipe(map(this.extractData));
    }
    getWorkerMerchantsData() {
        return this.http.get(this.apiEndPoint + '/api/checkout/ticketpayments/worker/merchant')
            .pipe(map(this.extractData));
    }
    getSetupMemberships(inActive) {
        return this.http.get(this.apiEndPoint + '/api/setupmemberships/' + inActive)
            .pipe(map(this.extractData));
    }
    detClientId(id) {
        return this.http.get(this.apiEndPoint + '/api/setupmemberships/delete/' + id)
            .pipe(map(this.extractData));
    }
    getListOFAutoBilling() {
        return this.http.get(this.apiEndPoint + '/api/setupmembership/getListAutoBilling')
            .pipe(map(this.extractData));
    }
    saveAutoBilling(payment, memberData) {
        return this.http.post(this.apiEndPoint + '/api/setupmembership/saveAutoBilling',
         { 'payment': [payment], 'memberData': [memberData] })
            .pipe(map(this.extractData));
    }
    getHideCliContactInfo(id) {
        return this.http.get(this.apiEndPoint + '/api/client/getHideClientContactInfo/' + id)
          .pipe(map(this.extractData));
      }
    /*To extract json data*/
    private extractData(res: Response) {
        if (res.headers && res.headers.get('token')) {
            localStorage.setItem('token', res.headers.get('token'));
        }
        const body = res.json();
        return body || {};
    }
}
