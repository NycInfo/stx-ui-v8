import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { JwtHelper } from 'angular2-jwt';
import { MarketingEmailService } from '../../app/marketing/marketingemail/marketingemail.service';
import * as config from '../app.config';

@Component({
    selector: 'app-menu',
    templateUrl: './menu.html',
    styleUrls: ['./menu.scss'],
    providers: [MarketingEmailService]
})
export class MenuComponent implements OnInit {

    navURL = '';
    decodedToken: any;
    decodedUserToken: any;
    toastermessage: any;
    emURL = config.SALONCLOUDS_PLUS;
    constructor(
        private serv: MarketingEmailService,
        private route: ActivatedRoute,
        private router: Router,
        private toastr: ToastrService,
        private translateService: TranslateService) { }

    ngOnInit() {
        // ---Start of code for Permissions Implementation--- //
        try {
            this.decodedToken = new JwtHelper().decodeToken(localStorage.getItem('rights'));
            this.decodedUserToken = new JwtHelper().decodeToken(localStorage.getItem('token'));
        } catch (error) {
            this.decodedToken = {};
        }
        if (this.decodedToken.data && this.decodedToken.data.permissions) {
            this.decodedToken = JSON.parse(this.decodedToken.data.permissions);
        } else {
            this.decodedToken = {};
        }
        // ---End of code for permissions Implementation--- //
        this.navURL = window.location.href.replace(window.location.origin, '').substring(3);
        this.serv.getEMURL().subscribe(
            data => {
                if (data['result']) {
                    this.emURL = data['result'];
                }
            },
            error => {
                const status = JSON.parse(error['status']);
                const statuscode = JSON.parse(error['_body']).status;
                switch (JSON.parse(error['_body']).status) {
                    case '2033':
                        break;
                }
                if (statuscode === '2085' || statuscode === '2071') {
                    if (this.router.url !== '/') {
                        localStorage.setItem('page', this.router.url);
                        this.router.navigate(['/']).then(() => { });
                    }
                }
            }
        );
    }
    invAddWork() {
        this.toastermessage = this.translateService.get('MENU.PLEASE_UPGRADE_YOUR_PACKAGE');
        this.toastr.info(this.toastermessage.value, null, { timeOut: 3000 });
    }
}
