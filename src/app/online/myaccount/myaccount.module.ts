import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MyaccountComponent } from './myaccount.component';
import { MyaccountRoutingModule } from './myaccount.routing';
import { FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
@NgModule({
    imports: [
        CommonModule,
        MyaccountRoutingModule,
        FormsModule,
        TranslateModule,
        //  ModalModule
       // BsDatepickerModule.forRoot()
    ],
    declarations: [
        MyaccountComponent
    ]
})
export class MyaccountModule {
}
