import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MyaccountComponent } from './myaccount.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '',
                component: MyaccountComponent,
                children: [
                    {
                        path: '',
                        component: MyaccountComponent
                    }
                ]
            }
        ])
    ],
    exports: [
        RouterModule
    ]
})
export class MyaccountRoutingModule {
}
