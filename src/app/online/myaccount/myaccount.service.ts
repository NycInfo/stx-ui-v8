import { Injectable, Inject } from '@angular/core';
import { Headers, Response } from '@angular/http';
import { HttpClients } from '../../common/http-client';
import { map } from 'rxjs/operators';

@Injectable()
export class MyaccountService {

  constructor(private http: HttpClients,
    @Inject('apiEndPoint') private apiEndPoint: string
  ) { }

  getClientInfo(dbName) {
    const headers = new Headers();
    headers.append('token', localStorage.getItem('clienttoken'));
    return this.http.getHeader(this.apiEndPoint + '/api/client/info/' + dbName, headers)
      .pipe(map(this.extractData));
  }
  getClient(id) {
    const headers = new Headers();
    headers.append('token', localStorage.getItem('clienttoken'));
    return this.http.getHeader(this.apiEndPoint + '/api/client/' + id, headers)
      .pipe(map(this.extractData));
  }
  saveClientProfile(clientObj, clientPictureFile: File, companyId) {
    const formData: any = new FormData();
    formData.append('clientPictureFile', clientPictureFile);
    formData.append('clientObj', JSON.stringify(clientObj));
    const headers = new Headers();
    headers.append('cid', companyId);
    headers.append('token', localStorage.getItem('clienttoken'));
    return this.http.postHeader(this.apiEndPoint + '/api/onlineclientlogin', formData, headers)
      .pipe(map(this.extractData));
  }
  getStates(countryName) {
    const headers = new Headers();
    headers.append('token', localStorage.getItem('clienttoken'));
    return this.http.getHeader(this.apiEndPoint + '/api/v1/lookups/states/' + countryName, headers)
        .pipe(map(this.extractData));
}
  private extractData(res: Response) {
    if (res.headers && res.headers.get('token')) {
      localStorage.setItem('clienttoken', res.headers.get('token'));
    }
    const body = res.json();
    return body || {};
  }

}
