import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { OnlineBookService } from './onlinebook.service';
import { JwtHelper } from 'angular2-jwt';
import { CommonService } from '../../common/common.service';
import * as config from '../../app.config';
import { isNullOrUndefined } from 'util';
import { HttpClient } from '@angular/common/http';
import { Md5 } from 'ts-md5/dist/md5';
import { ModalDirective } from 'ngx-bootstrap/modal';
import * as moment from 'moment/moment';
import { SafeUrl } from '@angular/platform-browser';
@Component({
  selector: 'app-onlinebook',
  templateUrl: './onlinebook.component.html',
  styleUrls: ['./onlinebook.component.scss'],
  providers: [OnlineBookService, CommonService]
})
export class OnlineBookComponent implements OnInit, OnDestroy {
  @ViewChild('serviceNotesModal', { static: false }) serviceNotesModal: ModalDirective;
  error: any;
  min: any;
  confirmTime = '';
  showMore = false;
  showLess = true;
  setTime = 0;
  newclientPictureFileView: SafeUrl = '';
  companyName = '';
  companyLogo = 'assets/images/logo.png';
  decodedToken: any;
  clientId: any = '';
  apptId: any;
  minDate: Date;
  maxDate: Date;
  bsValue: Date = new Date();
  serviceGroupName: any;
  type: any;
  datePickerConfig: any;
  workerName: any;
  workername: any;
  serviceTax: any;
  onlineBookingErr: any = '';
  onlineBookingEr: any = '';
  apptNotes: any = '';
  apptDate: any;
  appData: any;
  workerList: any = [];
  clientsList: any = [];
  packageGroupList: any = [];
  serviceGroupList: any = [];
  serviceDetailsList: any = [];
  rows: any = [];
  deleteArray: any = [];
  apiEndPoints = config['S3_URL'];
  sumOfServiceDurations = 0;
  sumOfServiceDurationss = 0;
  ServiceSales = 0;
  totalServicePrice: any = 0;
  prepaidDepositAmount = 0;
  selectedIndex: any;
  serviceDetailKeys = ['Duration_1__c', 'Duration_2__c', 'Duration_3__c',
    'Buffer_After__c', 'Guest_Charge__c', 'Net_Price__c'];
  apptSearchData: any = [];
  showScheduleButton = false;
  isRebookAppt: any = false;
  paymentDetails = false;
  depositAlert = '';
  timealert = '';
  appointBookingData: any;
  /// payments
  yearList = [];
  expYear: any;
  statesList: any;
  purchaseGiftButt: any;
  merchantWorkerList: any = [];
  MailingPostalCode = '';
  mailingCountry = 'United States';
  mailingCountriesList = [{ 'NAME': 'Canada' }, { 'NAME': 'United States' }];
  monthList = ['01 - January', '02 - February', '03 - March', '04 - April', '05 - May', '06 - June',
    '07 - July', '08 - August', '09 - September', '10 - October', '11 - November', '12 - December'];
  mailingState = '';
  mailingCity: any;
  paymentGateWay: any;
  merchantAccntName: any;
  cardTypes: any;
  orderId: any;
  cardType: any = '';
  expMonth = 1;
  cardNumber = '';
  clientName: any;
  clientFirstName: any;
  clientLastName: any;
  clientEmailAddress: any;
  cvv = '';
  apptStatus: any;
  serviceNotes: any;
  companyBookingRestriction: any;
  windowStartOption: any;
  windowEndOption: any;
  showTotalDuration: any;
  showTotalPrice: any;
  Id: any;
  cliData: any = [];
  windowStartHour: any = 0;
  indParam = 0;
  showDashboard = true;
  searchresults = false;
  confirmationDiv = false;
  listingName: any;
  commonTime: any = [];
  displayTimes: any = [];
  displayNmb = 0;
  disableNext = false;
  disablePrevious = false;
  descShow = true;
  searchFail: any = '';
  searchAgain = false;
  isPreferredDuration: any;
  preferredDur: any = 0;
  prefService: 0;
  packageServices = [];
  bookedOnline = 1;
  search = false;
  showText = false;
  slots: any = [];
  doubleClick = false;
  /* pos start */
  posDataObjList: any = [];
  storeTerminalID = '';
  sharedSecret = '';
  addTest = false;
  electronicPaymentData: any;
  paymentCardId: any;
  constructor(
    private http: HttpClient,
    private toastr: ToastrService,
    private translateService: TranslateService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private onlineBookService: OnlineBookService,
    private commonService: CommonService,
  ) {
    if (localStorage.getItem('clienttoken')) {
      const clientInfo = localStorage.getItem('clienttoken');
      this.companyName = localStorage.getItem('compname');
      this.companyLogo = this.apiEndPoints + localStorage.getItem('complogo') + '?time=' + new Date().getTime();
      this.decodedToken = new JwtHelper().decodeToken(clientInfo);
      this.clientId = this.decodedToken['data']['id'];
    } else {
      this.router.navigate(['clientlogin/' + localStorage.getItem('param')]);
    }
    this.activatedRoute.queryParamMap.subscribe((param) => {
      this.apptId = param.get('apptid');
      this.isRebookAppt = param.get('rebook') ? true : false;
      if (this.isRebookAppt || this.apptId) {
        this.showDashboard = false;
        this.searchresults = false;
        this.confirmationDiv = false;
        this.searchAgain = false;
      }
    });
  }
  /* Method is used for back button */
  ngOnDestroy() {
    sessionStorage.removeItem('olbdata');
  }
  ngOnInit() {
    this.datePickerValues();
    sessionStorage.removeItem('olbdata');
    this.clientId = localStorage.getItem('clientid');
    this.minDate = new Date();
    this.maxDate = new Date();
    this.datePickerConfig = Object.assign({},
      {
        showWeekNumbers: false,
        containerClass: 'theme-blue',
      });
    this.getClientOnlineData();
    this.getAllActivePackages();
    if (this.apptId) {
      this.getApptServiceDetails(this.clientId, this.apptId);
    } else {
      this.getServiceGroups();
    }
    this.getServRetTaxs();
    this.getClientAppointmemts(this.clientId);
    this.createYearsList();
    this.getCountry('United States');
    this.getPaymentTypes();
    this.getDoNotBook(this.clientId);
    this.onlinepos();
    // this.datePickerValues();
  }
  onlinepos() {
    this.onlineBookService.getPos().subscribe(data => {
      this.posDataObjList = data['result'];
      if (this.posDataObjList[0].JSON__c) {
        const merchantData1 = JSON.parse(this.posDataObjList[0].JSON__c);
        this.storeTerminalID = merchantData1.storeTerminalID;
        this.sharedSecret = merchantData1.sharedSecret;
        // this.addTest = merchantData1.test;
      }
    },
      error => {
        const errStatus = JSON.parse(error['_body'])['status'];
        if (errStatus === '2085' || errStatus === '2071') {
          if (this.router.url !== '/') {
            localStorage.setItem('page', this.router.url);
            this.router.navigate(['/clientlogin/' + localStorage.getItem('param')]);
          }
        }
      });
  }
  home() {
    this.showDashboard = true;
    this.searchresults = false;
    this.confirmationDiv = false;
    this.searchAgain = false;
    this.paymentDetails = false;
    this.onlineBookingErr = '';
    this.onlineBookingEr = '';
    this.minDate = new Date();
    this.maxDate = new Date();
    this.datePickerValues();
    this.rows = [];
    this.getServiceGroups();
    this.commonTime = [];
    this.onlinepos();
    this.router.navigate(['/onlinebook']);
  }
  bookNow() {
    this.showDashboard = false;
    this.confirmationDiv = false;
    this.searchAgain = false;
    this.minDate = new Date();
    this.maxDate = new Date();
    this.datePickerValues();
  }
  bkNowAppt() {
    this.rows = [];
    this.commonTime = [];
    this.getServiceGroups();
    this.showDashboard = false;
    this.confirmationDiv = false;
    this.searchAgain = false;
    this.minDate = new Date();
    this.maxDate = new Date();
    this.datePickerValues();
    this.router.navigate(['/onlinebook']);
  }
  datePickerValues() {
    this.onlineBookService.getOnlineBookingData().subscribe(res => {
      this.windowStartOption = res.result.windowStartOption;
      this.windowEndOption = res.result.windowEndOption;
      this.showTotalDuration = res.result.showTotalDuration;
      this.showTotalPrice = res.result.showTotalPrice;
      if (this.windowStartOption === 'Days') {
        this.minDate.setDate(this.minDate.getDate() + parseInt(res.result.windowStartNumber, 10));
      } else if (this.windowStartOption === 'Hours') {
        this.windowStartHour = res.result.windowStartNumber;
        this.minDate.setHours(this.minDate.getHours() + parseInt(res.result.windowStartNumber, 10));
      }
      this.bsValue = new Date(this.minDate);
      if (this.windowEndOption === 'Days') {
        this.maxDate.setDate(this.maxDate.getDate() + parseInt(res.result.windowEndNumber, 10));
      } else if (this.windowEndOption === 'Hours') {
        this.maxDate.setDate(this.maxDate.getDate() + parseInt(res.result.windowEndNumber, 10) / 24);
      }
    },
      error => {
        const status = JSON.parse(error['status']);
        const statuscode = JSON.parse(error['_body']).status;
        switch (JSON.parse(error['_body']).status) {
          case '2033':
            break;
        }
        if (statuscode === '2085' || statuscode === '2071') {
          if (this.router.url !== '/') {
            localStorage.setItem('page', this.router.url);
            this.router.navigate(['clientlogin/' + localStorage.getItem('param')]);
          }
        }
      });
  }
  changeClient(id) {
    this.clientId = id;
    this.getDoNotBook(this.clientId);
    localStorage.setItem('clientid', this.clientId);
  }

  /* Method to get service details */
  getApptServiceDetails(clientId, apptId) {
    const reqDate = this.commonService.getDBDatStr(new Date());
    this.onlineBookService.getApptServices(clientId, apptId, reqDate).subscribe(data => {
      const resData = data['result'];
      this.rows = resData.srvcresult;
      this.appData = resData.apptrst[0];
      this.bookedOnline = resData.apptrst[0]['Booked_Online__c'];
      this.apptStatus = this.appData.apstatus;
      if (this.isRebookAppt) {
        this.apptNotes = '';
      } else {
        this.apptNotes = this.appData.Notes__c === 'null' || this.appData.Notes__c === 'undefined' ? null : this.appData.Notes__c;
      }
      const pckgList = resData.pckgResult;
      this.serviceGroupList = resData.srvgResult;
      for (let i = 0; i < this.serviceGroupList.length; i++) {
        this.serviceGroupList[i]['serviceGroupName1'] = this.serviceGroupList[i]['clientFacingServiceGroupName'] ?
          this.serviceGroupList[i]['clientFacingServiceGroupName'] : this.serviceGroupList[i]['serviceGroupName'];
      }
      this.serviceGroupName = this.serviceGroupList.length > 0 ? this.serviceGroupList[0].serviceGroupName + '$' + this.serviceGroupList[0].serviceGroupColor : undefined;
      this.updateBookedRecords(pckgList);
      this.bsValue = this.isRebookAppt ? this.bsValue : this.commonService.getDateTmFrmDBDateStr(this.appData.Appt_Date_Time__c);
    },
      error => {
        this.rows = [{}];
        const status = JSON.parse(error['status']);
        const statuscode = JSON.parse(error['_body']).status;
        switch (JSON.parse(error['_body']).status) {
          case '2033':
            break;
        }
        if (statuscode === '2085' || statuscode === '2071') {
          if (this.router.url !== '/') {
            localStorage.setItem('page', this.router.url);
            this.router.navigate(['clientlogin/' + localStorage.getItem('param')]);
          }
        }
      });
  }
  /**
  * Method to get service tax  and retail tax calculation
  */
  popOk() {
    this.router.navigate(['clientlogin/' + localStorage.getItem('param')]);
  }
  getServRetTaxs() {
    this.onlineBookService.getServProdTax().subscribe(
      data => {
        const taxData = JSON.parse(data['result'][3].JSON__c);
        const merchantData = JSON.parse(data['result'][0].JSON__c);
        this.serviceTax = taxData.serviceTax;
        if (merchantData.storeTerminalID && merchantData.sharedSecret) {
          this.purchaseGiftButt = true;
        } else {
          this.purchaseGiftButt = false;
        }
      },
      error => {
        const errStatus = JSON.parse(error['_body'])['status'];
        if (errStatus === '2085' || errStatus === '2071') {
          if (this.router.url !== '/') {
            localStorage.setItem('page', this.router.url);
            this.router.navigate(['clientlogin/' + localStorage.getItem('param')]);
          }
        }
      });
  }

  updateBookedRecords(pckgList?: Array<any>) {
    if (this.serviceGroupList.length > 0 && this.rows.length > 0) {
      if (this.rows && this.rows.length > 0) {
        for (let i = 0; i < this.rows.length; i++) {
          this.rows[i]['IsPackage'] = 0;
          this.rows[i]['Booked_Package__c'] = '';
          this.rows[i]['Deposit_Required__c'] = 0;
          this.rows[i]['Deposit_Amount__c'] = 0;
          this.rows[i]['descp'] = this.rows[i]['Notes__c'];
          this.serviceDetailsList[i] = this.rows[i].servList;
          for (let j = 0; j < this.serviceDetailsList[i].length; j++) {
            this.serviceDetailsList[i][j]['Name'] = this.serviceDetailsList[i][j]['Client_Facing_Name__c'] &&
              this.serviceDetailsList[i][j]['Client_Facing_Name__c'] !== 'null' && this.serviceDetailsList[i][j]['Client_Facing_Name__c'] !== undefined
              && this.serviceDetailsList[i][j]['Client_Facing_Name__c'] !== 'undefined' ?
              this.serviceDetailsList[i][j]['Client_Facing_Name__c'] : this.serviceDetailsList[i][j]['Name'];
          }
          this.workerList[i] = this.rows[i].workerList;
          this.assaignServiceDurations(this.workerList[i], this.rows[i].workername, i, true);
          this.rows[i]['serviceName'] = this.rows[i]['Id'];
          const pckgId = this.rows[i]['pckgId'];
          if (!isNullOrUndefined(pckgId) && pckgId !== '') {
            this.rows[i]['serviceGroupName'] = pckgId;
            this.rows[i]['IsPackage'] = 1;
            this.rows[i]['Booked_Package__c'] = pckgId.split(':')[1];
            const index = this.packageGroupList.findIndex((pckg) => pckg.Id === this.rows[i]['Booked_Package__c'] && pckg['isAdded'] === 0);
            if (index === -1) {
              const selectedPackg = pckgList.filter((pck) => pck.Id === this.rows[i]['Booked_Package__c']);
              if (selectedPackg.length === 1) {
                selectedPackg[0]['isAdded'] = 1;
                this.rows[i]['isAdded'] = 1;
                if (this.packageGroupList.findIndex((pckg) => pckg.Id === this.rows[i]['Booked_Package__c']) === -1) {
                  this.packageGroupList.push(selectedPackg[0]);
                }
              }
            }
          } else {
            const serviceGroup = this.rows[i]['serviceGroupName'];
            this.serviceGroupList.filter((service) => service.serviceGroupName === serviceGroup).map((service) => {
              this.rows[i]['serviceGroupName'] = service.serviceGroupName + '$' + service.serviceGroupColor;
            });
          }
        }
        this.calculateServiceDurations();
      }
    }
  }

  assaignServiceDurations(workers: Array<any>, workerId: string, index: number, onLoading?: boolean) {
    const selectedWorker = workers.filter((worker) => worker.workername === workerId).map((worker) => {
    });
    if (selectedWorker.length === 0) {
      const serviceIndex = this.serviceDetailsList[index].findIndex((service) => service.Id === this.rows[index]['Id']);
      this.workerList[index].push(
        {
          workername: workerId,
          workerName: '(' + this.rows[index].name + ')',
          sduration1: this.serviceDetailsList[index][serviceIndex]['Duration_1__c'],
          sduration2: this.serviceDetailsList[index][serviceIndex]['Duration_2__c'],
          sduration3: this.serviceDetailsList[index][serviceIndex]['Duration_3__c'],
          sbuffer: this.serviceDetailsList[index][serviceIndex]['Buffer_After__c'],
          sDuration_1_Available_for_Other_Work__c: this.serviceDetailsList[index][serviceIndex]['Duration_1_Available_for_Other_Work__c'],
          sDuration_2_Available_for_Other_Work__c: this.serviceDetailsList[index][serviceIndex]['Duration_2_Available_for_Other_Work__c'],
          sDuration_3_Available_for_Other_Work__c: this.serviceDetailsList[index][serviceIndex]['Duration_3_Available_for_Other_Work__c'],
          Net_Price__c: this.serviceDetailsList[index][serviceIndex]['Net_Price__c'],
          Guest_Charge__c: this.serviceDetailsList[index][serviceIndex]['Guest_Charge__c'],
          Taxable__c: this.serviceDetailsList[index][serviceIndex]['Taxable__c']
        });
    }
  }

  // Method for service groups
  getServiceGroups() {
    const reqDate = this.commonService.getDBDatStr(this.bsValue);
    this.onlineBookService.getServiceGroups('Service', reqDate).subscribe(data => {
      this.serviceGroupList = data['result'].filter(filterList => filterList.active && !filterList.isSystem);
      for (let i = 0; i < this.serviceGroupList.length; i++) {
        this.serviceGroupList[i]['serviceGroupName1'] = this.serviceGroupList[i]['clientFacingServiceGroupName'] ?
          this.serviceGroupList[i]['clientFacingServiceGroupName'] : this.serviceGroupList[i]['serviceGroupName'];
      }
      if (this.serviceGroupList.length > 0) {
        this.serviceGroupName = this.serviceGroupList[0].serviceGroupName + '$' + this.serviceGroupList[0].serviceGroupColor;
        if (!this.apptId) {
          this.addServices(0);
          for (let i = 0; i < this.serviceGroupList.length; i++) {
            this.serviceGroupName = this.serviceGroupList[i]['serviceGroupName'] + '$' + this.serviceGroupList[i].serviceGroupColor;
            this.categoryOfService(this.serviceGroupList[i]['serviceGroupName'], i);
          }
        } else {
          this.updateBookedRecords();
        }
      }
    },
      error => {
        const status = JSON.parse(error['status']);
        const statuscode = JSON.parse(error['_body']).status;
        switch (JSON.parse(error['_body']).status) {
          case '2033':
            break;
        }
        if (statuscode === '2085' || statuscode === '2071') {
          if (this.router.url !== '/') {
            localStorage.setItem('page', this.router.url);
            this.router.navigate(['clientlogin/' + localStorage.getItem('param')]);
          }
        }
      });
  }

  /**
   * Method to get package groups
  */
  getAllActivePackages() {
    const currentDate = this.commonService.getDBDatStr(new Date()).split(' ')[0];
    this.onlineBookService.getPackageGroups(currentDate)
      .subscribe(data => {
        this.packageServices = data['result']['packagesevices'];
        for (let i = 0; i < data['result']['packages'].length; i++) {
          data['result']['packages'][i]['Name'] = data['result']['packages'][i]['Client_Facing_Name__c'] &&
            data['result']['packages'][i]['Client_Facing_Name__c'] !== 'null' ?
            data['result']['packages'][i]['Client_Facing_Name__c'] : data['result']['packages'][i]['Name'];
        }
        this.packageGroupList = data['result']['packages'].filter((obj) => obj.Active__c === 1 && obj.Available_Client_Self_Booking__c === 1 && obj.IsDeleted === 0);
        this.packageGroupList = this.packageGroupList.map((pckg) => {
          pckg['isAdded'] = 0;
          return pckg;
        });
        this.packageGroupList.forEach(pkg => {
          this.packageServices[pkg['Id']]['serviceresultJson'].forEach(pkgSrv => {
            pkgSrv['Id'] = 'pkgsrv:' + pkg['Id'] + ':' + pkgSrv['Id'];
            pkgSrv['serviceGroupName'] = pkgSrv['Service_Group__c'] + '$' + pkgSrv['serviceGroupColor'];
            pkgSrv['IsPackage'] = 1;
            pkgSrv['Booked_Package__c'] = pkg['Id'];
            pkgSrv['serName'] = pkgSrv['Name'];
          });
        });
        if (this.apptId) {
          this.getApptServiceDetails(this.clientId, this.apptId);
        }
      },
        error => {
          const errStatus = JSON.parse(error['_body'])['status'];
          if (errStatus === '2085' || errStatus === '2071') {
            if (this.router.url !== '/') {
              localStorage.setItem('page', this.router.url);
              this.router.navigate(['clientlogin/' + localStorage.getItem('param')]);
            }
          }
        });
  }

  getClientOnlineData() {
    this.onlineBookService.getClientOnlineData().subscribe(
      data => {
        this.clientsList = data['result'];
      },
      error => {
        const status = JSON.parse(error['status']);
        const statuscode = JSON.parse(error['_body']).status;
        switch (status) {
          case 500:
            break;
          case 400:
            if (statuscode === '2085' || statuscode === '2071') {
              if (this.router.url !== '/') {
                localStorage.setItem('page', this.router.url);
                this.router.navigate(['clientlogin/' + localStorage.getItem('param')]);
              }
            }
            break;
        }
      }
    );
  }
  categoryOfService(value, i) {
    if (value.indexOf('scale') === 0) {
      this.type = 'Package';
      this.rows.splice(i, 1);
      const packageId = value.split(':')[1];
      this.packageServices[packageId]['serviceresultJson'].forEach((element, index) => {
        const pcksrv = JSON.stringify(element);
        this.rows.splice(i + index, 0, JSON.parse(pcksrv));
        this.servicesListOnChange(element['Id'].split(':')[2], i + index);
      });
    } else if (value.indexOf('pkgsrv') === 0) {
      this.type = 'Package';
      this.rows[i]['Booked_Package__c'] = value.split(':')[1];
      this.servicesListOnChange(value.split(':')[2], i);
    } else {
      this.type = 'onlineBook';
      this.apptSearchData = [];
      const serviceGroupName = value.split('$')[0];
      this.onlineBookService.getServices(serviceGroupName, this.type, this.commonService.getDBDatStr(this.bsValue)).subscribe(data => {
        this.serviceDetailsList[i] = data['result']['result'];
        for (let j = 0; j < this.serviceDetailsList[i].length; j++) {
          this.serviceDetailsList[i][j]['Name'] = this.serviceDetailsList[i][j]['Client_Facing_Name__c'] &&
            this.serviceDetailsList[i][j]['Client_Facing_Name__c'] !== 'null' ?
            this.serviceDetailsList[i][j]['Client_Facing_Name__c'] : this.serviceDetailsList[i][j]['Name'];
        }
        this.rows[0]['IsPackage'] = 0;
        this.rows[0]['Booked_Package__c'] = '';
        this.rows[0].serviceGroupColour = value.split('$')[1];
        this.calculateServiceDurations();
      },
        error => {
          const status = JSON.parse(error['status']);
          const statuscode = JSON.parse(error['_body']).status;
          switch (JSON.parse(error['_body']).status) {
            case '2033':
              break;
          }
          if (statuscode === '2085' || statuscode === '2071') {
            if (this.router.url !== '/') {
              localStorage.setItem('page', this.router.url);
              this.router.navigate(['clientlogin/' + localStorage.getItem('param')]);
            }
          }
        });
    }
  }
  // add new service dynamically
  addServices(i) {
    if (this.validateRows()) {
      this.rows.push({ Id: '', serviceGroupName: this.serviceGroupName, workername: '' });
      this.workerList[i] = [];
      this.serviceDetailsList[i] = [];
      const index = this.rows.length - 1;
      if (index !== 0) {
        for (let j = 0; j < this.serviceGroupList.length; j++) {
          this.serviceGroupName = this.serviceGroupList[j]['serviceGroupName'];
          this.categoryOfService(this.serviceGroupList[j]['serviceGroupName'], j);
        }
      }
    }
  }
  removeServiceDetails(index) {
    this.serviceDetailKeys.map((key) => {
      delete this.rows[index][key];
    });
  }
  // Method to chane worker
  workerListOnChange(value, i) {
    this.apptSearchData = [];
    this.workername = value;
    let totalDuration = 0;
    let totalServiceSales = 0;
    this.sumOfServiceDurationss = 0;
    this.workerList[i].filter((worker) => worker.workername === this.workername).map((worker) => {
      this.rows[i].workerName = worker.workerName;
      this.rows[i].FullName = worker.FullName;
      this.rows[i].Net_Price__c = worker.Price;
      this.rows[i].Duration_1__c = parseInt(worker.Duration_1__c, 10);
      this.rows[i].Duration_2__c = parseInt(worker.Duration_2__c, 10);
      this.rows[i].Duration_3__c = parseInt(worker.Duration_3__c, 10);
      this.rows[i].Buffer_After__c = parseInt(worker.Buffer_After__c, 10);
      this.rows[i].Duration_1_Available_for_Other_Work__c = worker.Duration_1_Available_For_Other_Work__c;
      this.rows[i].Duration_2_Available_for_Other_Work__c = worker.Duration_2_Available_For_Other_Work__c;
      this.rows[i].Duration_3_Available_for_Other_Work__c = worker.Duration_3_Available_For_Other_Work__c;
      this.rows[i].Duration__c = this.rows[i].Duration_1__c
        + this.rows[i].Duration_2__c
        + this.rows[i].Duration_3__c
        + this.rows[i].Buffer_After__c;
      this.rows[i].bookEvery = worker.Book_Every__c;
      this.rows[i].StartDay = worker.StartDay;
      this.rows[i].Resource_Filter__c = worker.Resource_Filter__c;
      this.rows[i].Resources__c = worker.Resources__c;
      totalDuration += this.rows[i].Duration_1__c;
      totalDuration += this.rows[i].Duration_2__c;
      totalDuration += this.rows[i].Duration_3__c;
      totalDuration += this.rows[i].Buffer_After__c;
      this.rows[i].serviceGroupColour = this.rows[i].serviceGroupName.split('$')[1];
      this.rows[i].Taxable__c = worker.Taxable__c;
      totalServiceSales += worker.Price;
      this.ServiceSales = totalServiceSales;
      this.sumOfServiceDurationss = (this.sumOfServiceDurationss + totalDuration);
    });
    if (!this.clientId) {
      this.isPreferredDuration = 0;
      this.preferredDur = 0;
    } else {
      if (this.cliData) {
        this.isPreferredDuration = 0;
        this.preferredDur = 0;
        this.isPreferredDuration = this.getPreferredDuration(this.cliData, this.workerList[i], this.rows[i]);
        if (this.isPreferredDuration.length > 0) {
          this.preferredDur = 0;
          for (let j = 0; j < this.workerList[i].length; j++) {
            if (this.isPreferredDuration[0].Service__c === this.workerList[i][j].serviceId) {
              this.workerList[i][j]['Duration_1__c'] = this.isPreferredDuration[0].Duration_1__c;
              this.workerList[i][j]['Duration_2__c'] = this.isPreferredDuration[0].Duration_2__c;
              this.workerList[i][j]['Duration_3__c'] = this.isPreferredDuration[0].Duration_3__c;
              this.workerList[i][j]['Buffer_after__c'] = this.isPreferredDuration[0].Buffer_after__c;
              this.workerList[i][j]['sumDurationBuffer'] = this.isPreferredDuration[0].Duration__c;
              this.workerList[i][j]['Duration__c'] = this.isPreferredDuration[0].Duration__c;
              this.workerList[i][j]['PrefDur'] = this.preferredDur;
              this.prefService = this.isPreferredDuration[0].Duration__c;
              this.rows[i].Duration__c = this.workerList[i][j]['Duration__c'];
            }
          }
        }
      }
      this.checkForPrepaidServices(this.rows[i]['Id'], i);
      this.calculateServiceDurations();
    }
  }
  getPreferredDuration(cliData, servicelist, rows) {
    for (let i = 0; i < this.cliData.length; i++) {
      for (let t = 0; t < servicelist.length; t++) {
        if ((servicelist[t].serviceId === this.cliData[i].Service__c) && (rows.workername === this.cliData[i].Worker__c)) {
          if (this.cliData[i].PrefDur === 1) {
            const obj = [];
            obj.push({
              'Service__c': cliData[i].Service__c,
              'Duration__c': cliData[i].Duration__c,
              'Duration_1__c': cliData[i].Duration_1__c,
              'Duration_2__c': cliData[i].Duration_2__c,
              'Duration_3__c': cliData[i].Duration_3__c,
              'Buffer_after__c': cliData[i].Buffer_After__c
            });
            return obj;
          }
        }
      }
    }
    return 0;
  }

  calculateServiceTax(taxableObj): number {
    return this.commonService.calculatePercentage(this.serviceTax, taxableObj['Net_Price__c'], taxableObj['Taxable__c']);
  }
  servicesListOnChange(serviceId, i) {
    this.type = 'onlineBook';
    if (serviceId.indexOf('scale:') === -1 && serviceId.indexOf('pkgsrv:') === -1) {
      let temp = [];
      if (this.serviceDetailsList[i]) {
        temp = this.serviceDetailsList[i].filter((obj) => obj.Id === serviceId);
        if (temp && temp.length > 0) {
          this.rows[i]['serName'] = temp[0]['Name'];
        }
      }
      this.apptSearchData = [];
      this.workerList[i] = [];
      this.rows[i]['serviceName'] = serviceId;
      let totalDuration = 0;
      let totalServiceSales = 0;
      const bookingdata = {
        bookingdate: this.commonService.getDBDatStr(this.bsValue),
        serviceIds: [this.rows[i].Id]
      };
      this.onlineBookService.checkWorkerList(this.rows[i]['serviceName'], this.type).subscribe(data => {
        this.workerList[i] = data['result'];
        if (data['result'] && data['result'].length > 0) {
          this.rows[i].workername = '';
          this.rows[i].FullName = this.workerList[i][0].FullName;
          this.rows[i].serName = this.workerList[i][0].serviceName;
          this.rows[i].descp = this.workerList[i][0].Description__c;
          this.rows[i].Net_Price__c = this.workerList[i][0].Price;
          this.rows[i].Duration_1__c = parseInt(this.workerList[i][0].Duration_1__c, 10);
          this.rows[i].Duration_2__c = parseInt(this.workerList[i][0].Duration_2__c, 10);
          this.rows[i].Duration_3__c = parseInt(this.workerList[i][0].Duration_3__c, 10);
          this.rows[i].Buffer_After__c = parseInt(this.workerList[i][0].Buffer_After__c, 10);
          this.rows[i].Duration_1_Available_for_Other_Work__c = this.workerList[i][0].Duration_1_Available_For_Other_Work__c;
          this.rows[i].Duration_2_Available_for_Other_Work__c = this.workerList[i][0].Duration_2_Available_For_Other_Work__c;
          this.rows[i].Duration_3_Available_for_Other_Work__c = this.workerList[i][0].Duration_3_Available_For_Other_Work__c;
          this.rows[i].Duration__c = this.rows[i].Duration_1__c
            + this.rows[i].Duration_2__c
            + this.rows[i].Duration_3__c
            + this.rows[i].Buffer_After__c;
          this.rows[i].bookEvery = this.workerList[i][0].Book_Every__c;
          this.rows[i].Resource_Filter__c = this.workerList[i][0].Resource_Filter__c;
          this.rows[i].Resources__c = this.workerList[i][0].Resources__c;
          totalDuration += this.rows[i].Duration_1__c;
          totalDuration += this.rows[i].Duration_2__c;
          totalDuration += this.rows[i].Duration_3__c;
          totalDuration += this.rows[i].Buffer_After__c;
          this.rows[i].Taxable__c = this.workerList[i][0]['Taxable__c'];
          totalServiceSales += this.workerList[i][0].Price;
          this.ServiceSales = totalServiceSales;
          this.sumOfServiceDurationss = (this.sumOfServiceDurationss + totalDuration);
        }
        if (!this.clientId) {
          this.isPreferredDuration = 0;
          this.preferredDur = 0;
        } else {
          if (this.cliData) {
            this.isPreferredDuration = 0;
            this.preferredDur = 0;
            this.isPreferredDuration = this.getPreferredDuration(this.cliData, this.workerList[i], this.rows[i]);
            if (this.isPreferredDuration.length > 0) {
              this.preferredDur = 0;
              for (let j = 0; j < this.workerList[i].length; j++) {
                if (this.isPreferredDuration[0].Service__c === this.workerList[i][j].serviceId) {
                  this.workerList[i][j]['Duration_1__c'] = this.isPreferredDuration[0].Duration_1__c;
                  this.workerList[i][j]['Duration_2__c'] = this.isPreferredDuration[0].Duration_2__c;
                  this.workerList[i][j]['Duration_3__c'] = this.isPreferredDuration[0].Duration_3__c;
                  this.workerList[i][j]['Buffer_after__c'] = this.isPreferredDuration[0].Buffer_after__c;
                  this.workerList[i][j]['sumDurationBuffer'] = this.isPreferredDuration[0].Duration__c;
                  this.workerList[i][j]['Duration__c'] = this.isPreferredDuration[0].Duration__c;
                  this.workerList[i][j]['PrefDur'] = this.preferredDur;
                  this.prefService = this.isPreferredDuration[0].Duration__c;
                  this.rows[i].Duration__c = this.workerList[i][j]['Duration__c'];
                }
              }
            }
          }
          this.updatePreAmt(serviceId, i);
          this.calculateServiceDurations();
        }

      },
        error => {
          const status = JSON.parse(error['status']);
          const statuscode = JSON.parse(error['_body']).status;
          switch (JSON.parse(error['_body']).status) {
            case '2033':
              break;
          }
          if (statuscode === '2085' || statuscode === '2071') {
            if (this.router.url !== '/') {
              localStorage.setItem('page', this.router.url);
              this.router.navigate(['clientlogin/' + localStorage.getItem('param')]);
            }
          }
        });
    } else {
      this.categoryOfService(serviceId, i);
      this.updatePreAmt(serviceId, i);
    }

  }
  // Method to calculate the service durations
  calculateServiceDurations() {
    this.apptSearchData = [];
    setTimeout(() => {
      if (this.rows && this.rows.length > 0) {
        this.sumOfServiceDurations = 0;
        this.totalServicePrice = 0;
        this.prepaidDepositAmount = 0;
        for (let j = 0; j < this.rows.length; j++) {
          let totalDuration = 0;
          if (this.rows[j].type === 'package') {
            if (!isNullOrUndefined(this.rows[j]['workername']) && this.rows[j]['workername'] !== '') {
              if (this.rows[j]['Deposit_Required__c'] === 1) {
                this.prepaidDepositAmount += this.rows[j]['Deposit_Amount__c'];
              }
              totalDuration = parseInt(this.rows[j]['Duration_1__c'], 10) +
                parseInt(this.rows[j]['Duration_2__c'], 10) +
                parseInt(this.rows[j]['Duration_3__c'], 10) +
                parseInt(this.rows[j]['Buffer_After__c'], 10);
              this.rows[j].Duration__c = totalDuration;
            }
          } else {
            if (!isNullOrUndefined(this.rows[j]['workername']) && this.rows[j]['workername'] !== '') {
              if (this.rows[j]['Deposit_Required__c'] === 1) {
                this.prepaidDepositAmount += this.rows[j]['Deposit_Amount__c'];
              }
              totalDuration += parseInt(this.rows[j]['Duration__c'], 10);
              this.rows[j].Duration__c = this.rows[j]['Duration__c'];
            }

          }
          this.totalServicePrice += +this.rows[j].Net_Price__c;
          this.rows[j]['totalServicePrice'] = parseInt(this.totalServicePrice, 10);
          this.sumOfServiceDurations = this.sumOfServiceDurations + totalDuration;
        }
        this.depositAlert = this.prepaidDepositAmount !== 0 ? '$' + `${this.prepaidDepositAmount.toFixed(2)} Booking Deposit Required` : '';
        this.prepaidDepositAmount = this.prepaidDepositAmount ? +this.prepaidDepositAmount.toFixed(2) : 0;
      }
    }, 1000);
  }
  // Method to clear error messages
  clearErrorMsg() {
    this.onlineBookingErr = '';
  }
  checkForPrepaidServices(serviceId, i) {
    const serviceSelected = this.serviceDetailsList[i].filter((service) => serviceId === service.Id);
    if (serviceSelected.length > 0) {

      if (serviceSelected[0]['Deposit_Required__c'] ? +serviceSelected[0]['Deposit_Required__c'] === 1 : false) {
        this.rows[i]['Deposit_Required__c'] = serviceSelected[0]['Deposit_Required__c'];
        if (serviceSelected[0]['Deposit_Amount__c'] ? +serviceSelected[0]['Deposit_Amount__c'] > 0 : false) {

          this.rows[i]['Deposit_Amount__c'] = +serviceSelected[0]['Deposit_Amount__c'];
        } else {
          /// Here taxable feild in calculatePercentage is set to 1 to calculate percentage but it does not mean tax is applied.
          this.rows[i]['Deposit_Amount__c'] = this.commonService.calculatePercentage(+serviceSelected[0]['Deposit_Percent__c'], this.rows[i]['Net_Price__c'], 1);
        }
      } else {
        this.rows[i]['Deposit_Required__c'] = 0;
      }
    }
  }
  getWorkersFromDate2() {
    if (this.indParam > 1) {
      this.getWorkersFromDate();
    } else {
      this.indParam++;
    }
  }
  getWorkersFromDate() {
    const serviceIds = [];
    const selectedIds = [];
    this.apptSearchData = [];
    this.rows.filter((data) => {
      if (data['Id'] !== '' || !isNullOrUndefined(data['Id'])) {
        serviceIds.push(data['Id']);
        selectedIds.push(data['Id']);
      } else {
        serviceIds.push(data['']);
      }
    });
    if (selectedIds.length > 0) {
      const bookingdata = {
        bookingdate: this.commonService.getDBDatStr(this.bsValue),
        serviceIds: selectedIds
      };
      this.onlineBookService.getUsers(bookingdata).subscribe(data => {
        const workerservices = data['result'];
        serviceIds.forEach((id, i) => {
          if (id !== '' && !isNullOrUndefined(id)) {
            this.workerList[i] = workerservices.filter((worker) => worker.sId === id);
            const isExsists = this.workerList[i].findIndex((worker) => worker.workerName === this.rows[i]['workerName']) !== -1 ? true : false;
            if (!isExsists) {
              this.rows[i]['workerName'] = this.workerList[i].length > 0 ? this.workerList[i][0]['workerName'] : '';
              this.assaignServiceDurations(this.workerList[i], this.rows[i]['workername'], i);
              this.calculateServiceDurations();
            }
          }
        });
      },
        error => {
          const status = JSON.parse(error['status']);
          const statuscode = JSON.parse(error['_body']).status;
          switch (JSON.parse(error['_body']).status) {
            case '2033':
              break;
          }
          if (statuscode === '2085' || statuscode === '2071') {
            if (this.router.url !== '/') {
              localStorage.setItem('page', this.router.url);
              this.router.navigate(['clientlogin/' + localStorage.getItem('param')]);
            }
          }
        });
    }
  }
  goBack() {
    this.onlinepos();
    const tampData = JSON.parse(sessionStorage.getItem('olbdata'));
    this.rows = tampData['rows'];
    this.workerList = tampData['workerlist'];
    this.bsValue = new Date(tampData['date']);
    this.paymentDetails = false;
    this.showDashboard = false;
    this.searchresults = false;
    this.confirmationDiv = false;
    this.searchAgain = false;
    this.apptSearchData = [];
    this.displayTimes = [];
    this.commonTime = [];
    this.search = false;
    this.showText = false;
    this.onlineBookingEr = '';
    this.onlineBookingErr = '';
    setTimeout(() => {
      sessionStorage.removeItem('olbdata');
    }, 500);
    const element = document.getElementById('countdown');
    if (element !== null) {
      element.style.display = 'none';
      const highestTimeoutId = setTimeout(';');
      for (let i = 0; i < highestTimeoutId; i++) {
        clearTimeout(i);
      }
    }
  }
  searchForAppointment() {
    sessionStorage.setItem('olbdata', JSON.stringify({
      'rows': this.rows,
      'workerlist': this.workerList,
      'date': this.bsValue
    }));
    this.showDashboard = false;
    this.confirmationDiv = false;
    this.searchAgain = false;
    this.showScheduleButton = false;
    if (this.validateRows()) {
      this.selectedIndex = undefined;
      if ((this.bsValue.setHours(0, 0, 0, 0) >= this.minDate.setHours(0, 0, 0, 0)) && (this.bsValue.setHours(0, 0, 0, 0) <= this.maxDate.setHours(0, 0, 0, 0))) {
        const searchDate = this.bsValue.getFullYear()
          + '-' + ('0' + (this.bsValue.getMonth() + 1)).slice(-2)
          + '-' + ('0' + this.bsValue.getDate()).slice(-2);
        this.selectDate(searchDate);
      } else if (this.bsValue.setHours(0, 0, 0, 0) <= this.minDate.setHours(0, 0, 0, 0)) {
        const searchDate = this.minDate.getFullYear()
          + '-' + ('0' + (this.minDate.getMonth() + 1)).slice(-2)
          + '-' + ('0' + this.minDate.getDate()).slice(-2);
        this.selectDate(searchDate);
      } else {
        this.toastr.warning('The requested Search Date is not available. Please select a new date.', null, { timeOut: 1500 });
        this.apptSearchData = [];
      }
      this.searchresults = true;
    }
  }
  selectDate(searchDate) {
    if (typeof searchDate === 'string') {
      this.bsValue = this.commonService.getDateFrmDBDateStr(searchDate);
    } else {
      this.bsValue = searchDate;
    }
    const workerIds = [];
    const durations = [];
    const bookEvery = [];
    const resources = [];
    const resFilter = [];
    this.listingName = [];
    let tempSrcDate = this.bsValue;
    for (let i = 0; i < this.rows.length; i++) {
      this.listingName.push({
        'wrkName': this.rows[i].FullName,
        'serviceName': this.rows[i].serName,
        'duration': this.rows[i].Duration__c,
        'description': this.rows[i].descp,
        'price': this.rows[i].Net_Price__c
      });
      this.descShow = true;
      /* below if condition added for worker future start date assigning purpose */
      let tempDt;
      if (this.rows[i].workername && this.workerList[i].filter(obj => obj['workername'] === this.rows[i].workername)[0]['StartDay']) {
        tempDt = this.commonService.getDateFrmDBDateStr(this.workerList[i].filter(obj => obj['workername'] === this.rows[i].workername)[0]['StartDay']);
      } else {
        tempDt = this.commonService.getDateFrmDBDateStr(this.rows[i]['StartDay']);
      }
      if (tempDt > tempSrcDate) {
        tempSrcDate = tempDt;
      }
      workerIds.push(this.rows[i].workername);
      durations.push(this.rows[i].Duration__c);
      resources.push(this.rows[i].Resources__c);
      resFilter.push(this.rows[i].Resource_Filter__c);
      bookEvery.push(this.rows[i].bookEvery ? this.rows[i].bookEvery : this.rows[i].Book_Every__c);
    }
    const onlineSearchdate = new Date();
    onlineSearchdate.setHours(onlineSearchdate.getHours() + parseInt(this.windowStartHour, 10));
    const dataObj = {
      'date': this.commonService.getDBDatStr(tempSrcDate).split(' ')[0],
      'id': workerIds,
      'dateformat': 'MM/DD/YYYY hh:mm:ss a',
      'durations': durations,
      'mindate': this.commonService.getDBDatTmStr(onlineSearchdate),
      'Booked_Online__c': 1,
      'bookEvery': bookEvery,
      'resources': resources,
      'resFilter': resFilter
    };
    const tempSrvData = [];
    this.rows.forEach(obj => {
      tempSrvData.push({
        'duration1': obj['Duration_1__c'],
        'duration2': obj['Duration_2__c'],
        'duration3': obj['Duration_3__c'],
        'bufferafter': obj['Buffer_After__c'],
        'available1': obj['Duration_1_Available_for_Other_Work__c'],
        'available2': obj['Duration_2_Available_for_Other_Work__c'],
        'available3': obj['Duration_3_Available_for_Other_Work__c']
      });
    });
    dataObj['serviceData'] = tempSrvData;
    this.onlineBookService.searchForAppts(dataObj)
      .subscribe(data => {
        if (data['result'].length > 0) {
          //  this.disableNext = false;
          // this.disablePrevious = false;
          this.apptSearchData = data['result'];
          this.apptSearchData.forEach(element => {
            const ele = element.value.split(' ');
            element = ele[1] + ' ' + ele[2];
            if (ele[0] === moment(this.bsValue).format('MM/DD/YYYY')) {
              this.commonTime.push({
                'time': element,
                'date': ele[0]
              });
            } else {
              this.showText = false;
              this.search = true;
              this.onlineBookingEr = 'ONLINEBOOK.EMPTY_SLOTS_FOUND';
            }
          });
        } else {
          this.showText = true;
          this.onlineBookingEr = 'ONLINEBOOK.EMPTY_SLOTS_FOUND';
        }
        this.displayTimeSlots(0);
        this.upDatePreNextIcon();
      },
        error => {
          const errStatus = JSON.parse(error['_body'])['status'];
          if (errStatus === '2085' || errStatus === '2071') {
            if (this.router.url !== '/') {
              localStorage.setItem('page', this.router.url);
              this.router.navigate(['clientlogin/' + localStorage.getItem('param')]);
            }
          }
        });
  }
  checkForServices(services: Array<any>, property1, property2, property3): boolean {
    const properties = [property1, property2, property3];
    if (properties.map((property) => this.checkForServiceObject(services, property)).indexOf(false) !== -1) {
      return true;
    }
    return false;
  }
  checkForServiceObject(services: Array<any>, propertyName: string): boolean {
    const isProperty = services.map((obj) => obj.hasOwnProperty(propertyName)).indexOf(false) !== -1 ? false : true;
    if (isProperty) {
      if (services.filter((obj) => obj[propertyName] === '').length !== 0) {
        return false;
      } else {
        return true;
      }
    } else {
      return false;
    }
  }

  bookAppointmentInfo(type) {
    const IsPackage = this.rows.filter((obj) => obj['IsPackage'] === 1).length > 0 ? true : false;
    const serviceTaxResult = this.commonService.calculateServiceTax(+this.serviceTax, this.rows, IsPackage);
    const servicesData = serviceTaxResult.bookingData;
    for (let i = 0; i < servicesData.length; i++) {
      for (let j = 0; j < this.serviceDetailsList[i].length; j++) {
        if (this.serviceDetailsList[i][j]['Id'] === servicesData[i]['Id']) {
          servicesData[i]['Resources__c'] = this.serviceDetailsList[i][j]['Resources__c'];
        }
      }
    }
    serviceTaxResult.bookingData.forEach(element => {
      if (element['Id'].indexOf('pkgsrv:') === 0) {
        element['Id'] = element['Id'].split(':')[2];
      }
      this.serviceDetailsList.forEach((srv, index) => {
        if (srv.filter(obj => obj['Id'] === element['Id']).length > 0) {
          if (this.serviceGroupList[index]) {
            element['serviceGroupColour'] = this.serviceGroupList[index]['serviceGroupColor'];
            element['serviceGroupName'] = this.serviceGroupList[index]['serviceGroupName'];
          } else {
            element['serviceGroupColour'] = this.serviceGroupList[0]['serviceGroupColor'];
            element['serviceGroupName'] = this.serviceGroupList[0]['serviceGroupName'];
          }
        }
      });
    });
    this.appointBookingData = {
      'apptId': this.isRebookAppt ? '' : this.apptId,
      'Client__c': this.clientId,
      'Duration__c': (this.sumOfServiceDurationss),
      'Appt_Date_Time__c': this.apptDate,
      'servicesData': serviceTaxResult.bookingData,
      'Notes__c': this.apptNotes ? this.apptNotes : null,
      //   'Rebooked__c': this.isRebooking,
      'IsPackage': IsPackage ? 1 : 0,
      'Service_Tax__c': serviceTaxResult.serviceTax,
      'Service_Sales__c': this.ServiceSales,
      'Booked_Online__c': 1,
      'daleteArray': this.deleteArray,
      'bookingType': this.isRebookAppt ? 'findappt' : type,
      'Status__c': this.apptStatus,
      'apptCreatedDate': this.commonService.getDBDatTmStr(new Date()),
      'isDepositRequired': (serviceTaxResult.bookingData.filter((obj) => obj.Deposit_Required__c && obj.Deposit_Required__c === 1).length > 0) ? true : false
    };
  }
  bookAppointment(type) {
    this.appointBookingData = this.prepaidDepositAmount !== 0 ? true : false;
    this.bookAppointmentInfo(type);
    this.onlineBookService.appointmentBooking(this.appointBookingData).subscribe(data => {
      const apptStatus = data['result'];
      this.Id = apptStatus.apptId;
      this.paymentDetails = this.prepaidDepositAmount !== 0 ? true : false;
      if (!this.paymentDetails) {
        if (this.Id) {
          this.confirmationDiv = true;
          this.searchresults = false;
          this.showDashboard = false;
          this.searchAgain = false;
        }
        this.onlineBookService.sendEmailToOwner(this.Id).subscribe(data1 => {
          const dataStatus = data1['result'];
        }, error1 => {
          const status = JSON.parse(error1['status']);
          const statuscode = JSON.parse(error1['_body']).status;
          switch (JSON.parse(error1['_body']).status) {
            case '2033':
              break;
          }
          if (statuscode === '2085' || statuscode === '2071') {
            if (this.router.url !== '/') {
              localStorage.setItem('page', this.router.url);
              this.router.navigate(['clientlogin/' + localStorage.getItem('param')]);
            }
          } else if (statuscode === '2091') {
            const bookingError = JSON.parse(error1['_body']).message;
            // Warning Don't Delete This alert Code//
          }
        });
        this.onlineBookService.sendApptNotifs([this.Id]).subscribe(data2 => { }, error => { });
      }
    },
      error => {
        const status = JSON.parse(error['status']);
        const statuscode = JSON.parse(error['_body']).status;
        switch (JSON.parse(error['_body']).status) {
          case '2033':
            break;
        }
        if (statuscode === '2085' || statuscode === '2071') {
          if (this.router.url !== '/') {
            localStorage.setItem('page', this.router.url);
            this.router.navigate(['clientlogin/' + localStorage.getItem('param')]);
          }
        } else if (statuscode === '2091') {
          const bookingError = JSON.parse(error['_body']).message;
          this.searchFail = 'ONLINEBOOK.SERCH_RESULTS_NOT_FOUND';
          this.searchAgain = true;
          this.searchresults = false;
        }
      });
    if (type === 'findappt') {
      setTimeout(() => {
        this.countdown();
      }, 1000);
    }
  }

  scheduleButtonShow(searchData, i) {
    this.apptDate = this.commonService.getDBDatTmStr2(searchData.date + ' ' + searchData.time, 'MM/DD/YYYY hh:mm:ss a');
    this.confirmTime = searchData.time;
    this.selectedIndex = i;
    this.showScheduleButton = true;
  }
  logout() {
    localStorage.removeItem('clienttoken');
    localStorage.removeItem('fname');
    localStorage.removeItem('lname');
    localStorage.removeItem('clientid');
    this.router.navigate(['clientlogin/' + localStorage.getItem('param')]);
  }
  // Remove current service dynamically
  removeServices(index) {
    if (this.rows[index].tsId) {
      this.rows[index]['delete'] = true;
      this.deleteArray.push(this.rows[index]);
    }
    this.rows.splice(index, 1);
    this.workerList.splice(index, 1);
    this.calculateServiceDurations();
  }

  createYearsList() {
    const curtYear = new Date().getFullYear();
    for (let i = 0; i < 10; i++) {
      this.yearList.push(curtYear + i);
    }
    this.expYear = this.yearList[0];
  }
  getCountry(coun) {
    this.onlineBookService.getStates(coun)
      .subscribe(statesValues => {
        this.statesList = statesValues['result'];
      },
        error => {
          this.onlineBookingErr = <any>error;
          const errStatus = JSON.parse(error['_body'])['status'];
          if (errStatus === '2085' || errStatus === '2071') {
            if (this.router.url !== '/') {
              localStorage.setItem('page', this.router.url);
              this.router.navigate(['clientlogin/' + localStorage.getItem('param')]);
            }
          }
        });
  }

  getLocation() {
    if (this.MailingPostalCode.length > 4) {
      this.http.get('https://ziptasticapi.com/' + this.MailingPostalCode).subscribe(
        result => {
          if (result['error']) {
            const toastermessage: any = this.translateService.get('SETUPCOMPANY.ZIP_CODE_NOT_FOUND');
            this.toastr.error(toastermessage.value, null, { timeOut: 1500 });
          } else {
            if (result['country'] === 'US') {
              this.mailingCountry = 'United States';
              this.getCountry(this.mailingCountry);
              config.states.forEach(state => {
                if (state.abbrev === result['state']) {
                  this.mailingState = state.name;
                }
              });
            }
            const cityArray = result['city'].split(' ');
            for (let i = 0; i < cityArray.length; i++) {
              if (i === 0) {
                this.mailingCity = cityArray[i].charAt(0).toUpperCase() + cityArray[i].slice(1).toLowerCase() + ' ';
              } else {
                this.mailingCity += cityArray[i].charAt(0).toUpperCase() + cityArray[i].slice(1).toLowerCase() + ' ';
              }
            }
          }
        },
        error => {
        }
      );
    }
  }
  getWorkerMerchants() {
    this.onlineBookService.getWorkerMerchantsData()
      .subscribe(data => {
        this.merchantWorkerList.push({
          Payment_Gateway__c: 'AnywhereCommerce', FirstName: 'STX',
          LastName: 'QA 2017', Id: 'default_stx'
        });
        if (data['result'] && data['result'].length > 0) {
          this.merchantWorkerList = this.merchantWorkerList.concat(data['result']);
        }
        // for default values
        this.paymentGateWay = this.merchantWorkerList[0]['Payment_Gateway__c'];
        this.merchantAccntName = this.merchantWorkerList[0]['FirstName'] + ' ' + this.merchantWorkerList[0]['LastName'];
      },

        error => {
          const status = JSON.parse(error['status']);
          const statuscode = JSON.parse(error['_body']).status;
          switch (status) {
            case 500:
              break;
            case 400:
              if (statuscode === '2040') {
                this.onlineBookingErr = 'COMMON_STATUS_CODES.' + JSON.parse(error['_body']).status;
                window.scrollTo(0, 0);
              } else if (statuscode === '2085' || statuscode === '2071') {
                if (this.router.url !== '/') {
                  localStorage.setItem('page', this.router.url);
                  this.router.navigate(['clientlogin/' + localStorage.getItem('param')]);
                }
              } break;
          }
        });
  }
  getPaymentTypes() {
    this.onlineBookService.getPaymentTypesData().subscribe(data => {
      this.cardTypes = data.result.paymentResult.filter(filterList => filterList.Process_Electronically_Online__c === 1 && filterList.Active__c === 1);
      this.orderId = data.result.Id;
      this.electronicPaymentData = data.result.paymentResult.filter(filterList => filterList.Name === 'Electronic Payment');
      if (this.electronicPaymentData.length > 0) {
        this.paymentCardId = this.electronicPaymentData[0]['Id'];
      } else {
        this.paymentCardId = this.cardTypes[0]['Id'];
      }
    },
      error => {
        const status = JSON.parse(error['status']);
        const statuscode = JSON.parse(error['_body']).status;
        switch (JSON.parse(error['_body']).status) {
          case '2033':
            break;
        }
        if (statuscode === '2085' || statuscode === '2071') {
          if (this.router.url !== '/') {
            localStorage.setItem('page', this.router.url);
            this.router.navigate(['clientlogin/' + localStorage.getItem('param')]);
          }
        }
      });
  }
  makePayment() {
    if (this.storeTerminalID && this.sharedSecret) {
      let paymentData;
      const d = new Date();
      const cvvtest = /^[0-9]{3,4}$/;
      if (this.clientFirstName === '' || this.clientFirstName === undefined) {
        this.onlineBookingErr = 'Card Holder First Name is required';
        window.scrollTo(0, 0);
      } else if (this.clientLastName === '' || this.clientLastName === undefined) {
        this.onlineBookingErr = 'Card Holder Last Name is required';
        window.scrollTo(0, 0);
      } else if (this.cardNumber === '' || this.cardNumber === undefined) {
        this.onlineBookingErr = 'Card Number is required';
        window.scrollTo(0, 0);
      } else if (this.cardNumber.length < 0) {
        this.onlineBookingErr = 'Card Number: Only a number may be entered';
        window.scrollTo(0, 0);
      } else if (this.cardNumber.length >= 0 && this.cardNumber.length < 14) {
        this.onlineBookingErr = 'Card Number: Enter Valid Card Number';
        window.scrollTo(0, 0);
      } else if (this.MailingPostalCode === '' || this.MailingPostalCode === undefined) {
        this.onlineBookingErr = 'ZipCode is required';
        window.scrollTo(0, 0);
      } else if (this.cvv === '' || this.cvv === undefined) {
        this.onlineBookingErr = 'CVV is required';
        window.scrollTo(0, 0);
      } else if (this.cvv.length < 3 || this.cvv.length >= 5) {
        this.onlineBookingErr = 'CVV: length max 4 is required';
        window.scrollTo(0, 0);
      } else if (this.cvv.length < 0) {
        this.onlineBookingErr = 'CVV: Only a number may be entered';
        window.scrollTo(0, 0);
      } else if ((this.expYear <= d.getFullYear()) && ((this.expMonth) < d.getMonth() + 1)) {
        this.onlineBookingErr = 'Invalid Expiry Date.';
        window.scrollTo(0, 0);
      } else if (!cvvtest.test(this.cvv)) {
        this.onlineBookingErr = 'CVV: Only a number may be entered';
        window.scrollTo(0, 0);
      } else {
        const reqObj = {
          cardNumber: this.cardNumber,
          cardType: this.commonService.getCardType(this.cardNumber),
          currency: 'USD',
          terminalType: '1',
          transactionType: '4',
          amount: this.prepaidDepositAmount,
          expMonth: this.expMonth,
          expYear: this.expYear,
          cvv: this.cvv
        };
        const depositData = {
          Amount__c: this.prepaidDepositAmount,
          Transaction_Type__c: 'Deposit',
          Ticket__c: 'dp318sa9jjlj9i9bi',
          Appt_Date_Time__c: this.commonService.getDBDatTmStr(new Date()),
          pckgObj: {},
          type: 'Online',
          online_c: 1,
          Client__c: this.clientId
        };
        this.onlineBookService.addDepositToOthers(depositData).subscribe(data => {
          const apptStatus = data['result'];
          const appointmentId = this.apptId ? this.apptId : apptStatus.apptId;
          this.onlineBookService.cloverApiPayment(reqObj).subscribe(
            data2 => {
              paymentData = data2['result'];
              if (paymentData && paymentData['result'] === 'APPROVED') {
                this.savePaymentsData(paymentData, appointmentId, this.Id);
                const toastermessage: any = this.translateService.get('LOGIN.PAYMENT_SUCCESS');
                this.toastr.success(toastermessage.value, null, { timeOut: 1500 });
                this.onlineBookService.sendEmailToOwner(this.Id).subscribe(data1 => {
                  const dataStatus = data1['result'];
                }, error1 => {
                  const status = JSON.parse(error1['status']);
                  const statuscode = JSON.parse(error1['_body']).status;
                  switch (JSON.parse(error1['_body']).status) {
                    case '2033':
                      break;
                  }
                  if (statuscode === '2085' || statuscode === '2071') {
                    if (this.router.url !== '/') {
                      localStorage.setItem('page', this.router.url);
                      this.router.navigate(['clientlogin/' + localStorage.getItem('param')]);
                    }
                  } else if (statuscode === '2091') {
                    const bookingError = JSON.parse(error1['_body']).message;
                  }
                });
                const element = document.getElementById('countdown');
                if (element !== null) {
                  element.style.display = 'none';
                  const highestTimeoutId = setTimeout(';');
                  for (let i = 0; i < highestTimeoutId; i++) {
                    clearTimeout(i);
                  }
                }
                this.onlineBookService.sendApptNotifs([this.Id]).subscribe(data3 => { }, error => { });
              } else {
                this.deleteDataWhenPaymentFailed(appointmentId);
                this.onlineBookingErr = 'Error Occured, Invalid Details';
                window.scrollTo(0, 0);
              }
            },
            error => {
              const status = JSON.parse(error['status']);
              const statuscode = JSON.parse(error['_body']).status;
              switch (status) {
                case 500:
                  break;
                case 400:
                  if (statuscode === '2040') {
                    this.onlineBookingErr = 'COMMON_STATUS_CODES.' + JSON.parse(error['_body']).status;
                    window.scrollTo(0, 0);
                  } else if (statuscode === '2039') {
                    this.onlineBookingErr = JSON.parse(error['_body']).result;
                    window.scrollTo(0, 0);
                  } else if (statuscode === '2085' || statuscode === '2071') {
                    if (this.router.url !== '/') {
                      localStorage.setItem('page', this.router.url);
                      this.router.navigate(['clientlogin/' + localStorage.getItem('param')]);
                    }
                  }
                  break;
              }
            });
        },
          error => {
            const status = JSON.parse(error['status']);
            const statuscode = JSON.parse(error['_body']).status;
            switch (JSON.parse(error['_body']).status) {
              case '2033':
                break;
            }
            if (statuscode === '2085' || statuscode === '2071') {
              if (this.router.url !== '/') {
                localStorage.setItem('page', this.router.url);
                this.router.navigate(['clientlogin/' + localStorage.getItem('param')]);
              }
            }
          });
      }
    } else {
      const toastermessage: any = this.translateService.get('COMMON_TOAST_MESSAGES.TOAST_NO_ONLINE_MERCHANT');
      this.toastr.error(toastermessage.value, null, { timeOut: 7000 });
    }
  }
  deleteDataWhenPaymentFailed(apptId) {
    this.onlineBookService.deleteThePaymentFailedRecords(apptId).subscribe(
      data => {
        const dataStatus = data['result'];
      },
      error => {
        const status = JSON.parse(error['status']);
        const statuscode = JSON.parse(error['_body']).status;
        switch (status) {
          case 500:
            break;
          case 400:
            if (statuscode === '2085' || statuscode === '2071') {
              if (this.router.url !== '/') {
                localStorage.setItem('page', this.router.url);
                this.router.navigate(['clientlogin/' + localStorage.getItem('param')]);
              }
            }
            break;
        }
      }
    );
  }
  savePaymentsData(paymentData, appointmentId, apptId2) {
    let approvalCode = '';
    let refCode = '';
    if (paymentData === null) {
      approvalCode = '';
      refCode = '';
    } else {
      approvalCode = paymentData['authCode'];
      refCode = paymentData['token'];
    }
    const IsPackage = this.rows.filter((obj) => obj['IsPackage'] === 1).length > 0 ? true : false;
    const serviceTaxResult = this.commonService.calculateServiceTax(+this.serviceTax, this.rows, IsPackage);
    const servesData: any = serviceTaxResult.bookingData;

    const paymentObj = {
      'apptId': appointmentId,
      'merchantAccnt': this.merchantAccntName,
      'paymentGateWay': this.paymentGateWay,
      'cardHolderName': this.clientName,
      'cardNumber': this.cardNumber,
      'zipCode': this.MailingPostalCode,
      'expMonth': this.expMonth,
      'paymentType': this.paymentCardId,
      'expYear': this.expYear,
      'cvv': this.cvv,
      'amountToPay': this.prepaidDepositAmount,
      'approvalCode': approvalCode,
      'refCode': refCode,
      'isGiftPurchase': false,
      'isDepositRequired': (servesData.filter((obj) => obj.Deposit_Required__c && obj.Deposit_Required__c === 1).length > 0) ? true : false,
      'apptId1': this.Id,
      'isUpdateAppt1': true,
      'Online__c': 1,
      'clientId': this.clientId
    };
    this.onlineBookService.addToPaymentsTicket(paymentObj)
      .subscribe(data1 => {
        const dataObj = data1['result'];
        this.confirmationDiv = true;
        this.searchresults = false;
        this.showDashboard = false;
        this.paymentDetails = false;
        this.searchAgain = false;
        this.clearErr();
      },
        error => {
        });
  }
  changeCard(type) {
    this.cardType = type;
  }

  cancel() {
    this.deleteDataWhenPaymentFailed(this.Id);
    this.onlineBookingErr = '';
    this.MailingPostalCode = '';
    this.cardNumber = '';
    this.cvv = '';
    this.expMonth = 1;
    this.expYear = 0;
    this.paymentGateWay = '';
    this.merchantAccntName = '';
    this.paymentDetails = false;
    window.location.reload();
  }
  clearErr() {
    this.onlineBookingErr = '';
  }
  getDoNotBook(clientId) {
    this.onlineBookService.getClientData(clientId).subscribe(data => {
      if (data.result.results[0].Client_Pic__c) {
        this.newclientPictureFileView = config.S3_URL + data.result.results[0]['Client_Pic__c'];
      }
      this.companyBookingRestriction = data.result.results[0].Booking_Restriction_Type__c;
      this.clientName = data.result.results[0].FirstName + ' ' + data.result.results[0].LastName;
      this.clientFirstName = data.result.results[0].FirstName;
      this.clientLastName = data.result.results[0].LastName;
      this.clientEmailAddress = data.result.results[0].Email;
      if (this.companyBookingRestriction === 'Do Not Book') {
        this.serviceNotesModal.show();
        this.onlineBookService.getOnlineBookingData().subscribe(res => {
          if (document.getElementById('loginMessageId')) {
            document.getElementById('loginMessageId').innerHTML = res.result.loginMessage1;
          }
        },
          error => {
            const status = JSON.parse(error['status']);
            const statuscode = JSON.parse(error['_body']).status;
            switch (JSON.parse(error['_body']).status) {
              case '2033':
                break;
            }
            if (statuscode === '2085' || statuscode === '2071') {
              if (this.router.url !== '/') {
                localStorage.setItem('page', this.router.url);
                this.router.navigate(['clientlogin/' + localStorage.getItem('param')]);
              }
            }
          });
      }
    },
      error => {
        const status = JSON.parse(error['status']);
        const statuscode = JSON.parse(error['_body']).status;
        switch (JSON.parse(error['_body']).status) {
          case '2033':
            break;
        }
        if (statuscode === '2085' || statuscode === '2071') {
          if (this.router.url !== '/') {
            localStorage.setItem('page', this.router.url);
            this.router.navigate(['clientlogin/' + localStorage.getItem('param')]);
          }
        }
      });
  }
  deleteAppt() {
    this.deleteDataWhenPaymentFailed(this.Id);
  }
  countdown() {
    let element, endTime, hours, mins, msLeft, time;
    function twoDigits(n) {
      return (n <= 9 ? '0' + n : n);
    }
    function updateTimer() {
      msLeft = endTime - (+new Date);
      if (msLeft < 1000) {
        time = new Date(msLeft);
        const countdown = document.getElementById('countdown');
        if (countdown) {
          countdown.style.color = 'red';
        }
        element.innerHTML = 'Your payment session has timed out, and your request has been removed!';
        const deleteRcrd = <HTMLInputElement>document.getElementById('deleteApptId');
        deleteRcrd.click();
        setTimeout(reload, time.getUTCMilliseconds() + 8000);
      } else {
        time = new Date(msLeft);
        hours = time.getUTCHours();
        mins = time.getUTCMinutes();
        if (element) {
          element.innerHTML = (hours ? hours + ':' + twoDigits(mins) : 'Time Remaining: 0' + mins) + ':' + twoDigits(time.getUTCSeconds() + '');
        }
        setTimeout(updateTimer, time.getUTCMilliseconds() + 500);
      }
    }
    function reload() {
      window.location.reload();
    }
    element = document.getElementById('countdown');
    endTime = (+new Date) + 1000 * (60 * 5 + 0) + 500;
    updateTimer();
  }
  getClientAppointmemts(id) {
    const client = {
      'clientId': id,
      'apptViewValue': 'All'
    };
    this.onlineBookService.getClientAppointmentsData(client).subscribe(
      data => {
        this.cliData = data['result'].AppointmenServices.filter(filterList => filterList.PrefDur === 1);
      },
      error => {
        const errStatus = JSON.parse(error['_body'])['status'];
        if (errStatus === '2085' || errStatus === '2071') {
          if (this.router.url !== '/') {
            localStorage.setItem('page', this.router.url);
            this.router.navigate(['/']).then(() => { });
          }
        }
      }
    );
  }
  nextDate() {
    this.search = false;
    this.commonTime = [];
    this.bsValue = new Date(this.bsValue.setDate(this.bsValue.getDate() + 1));
    if (this.bsValue <= this.maxDate) {
      this.selectDate(this.bsValue);
    }
    // this.apptSearchData.forEach(element => {
    //   const ele = element.value.split(' ');
    //   element = ele[1] + ' ' + ele[2];
    //   if (ele[0] === moment(this.bsValue).format('MM/DD/YYYY')) {
    //     this.onlineBookingEr = '';
    //     this.commonTime.push({
    //       'time': element,
    //       'date': ele[0]
    //     });
    //   } else {
    //     this.onlineBookingEr = 'ONLINEBOOK.EMPTY_SLOTS_FOUND';
    //   }
    // });
    this.displayTimeSlots(0);
    this.upDatePreNextIcon();
    this.selectedIndex = -1;
    this.showScheduleButton = false;
  }
  previousDate() {
    this.search = false;
    this.commonTime = [];
    this.bsValue = new Date(this.bsValue.setDate(this.bsValue.getDate() - 1));
    if (this.bsValue >= this.minDate) {
      this.selectDate(this.bsValue);
    }
    // this.apptSearchData.forEach(element => {
    //   const ele = element.value.split(' ');
    //   element = ele[1] + ' ' + ele[2];
    //   if (ele[0] === moment(this.bsValue).format('MM/DD/YYYY')) {
    //     this.commonTime.push({
    //       'time': element,
    //       'date': ele[0]
    //     });
    //   } else {
    //     this.onlineBookingEr = 'ONLINEBOOK.EMPTY_SLOTS_FOUND';
    //   }
    // });
    this.displayTimeSlots(0);
    this.upDatePreNextIcon();
    this.selectedIndex = -1;
    this.showScheduleButton = false;
  }
  moreTimes() {
    this.displayTimeSlots(this.displayNmb);
    this.selectedIndex = -1;
  }
  lessTimes() {
    this.displayTimeSlots(this.displayNmb - 18);
    this.selectedIndex = -1;
  }
  convertAptSrDate(strDate) {
    const dateTimeAry = strDate.split(' ');
    const dateAry = dateTimeAry[0].split('/');
    const datObj = new Date(parseInt(dateAry[2], 10), (parseInt(dateAry[0], 10) - 1), parseInt(dateAry[1], 10));
    return datObj;
  }
  updatePreAmt(serviceId, i) {
    this.serviceDetailsList.forEach(srvGrp => {
      srvGrp.forEach(srv => {
        if (srv['Id'] === serviceId && srv['Deposit_Required__c']) {
          setTimeout(() => {
            if (srv['Deposit_Percent__c']) {
              if (this.rows[i]['Net_Price__c']) {
                this.rows[i]['Deposit_Amount__c'] = this.commonService.calculatePercentage(+srv['Deposit_Percent__c'], parseInt(this.rows[i]['Net_Price__c'], 10), 1);
              } else {
                this.rows[i]['Deposit_Amount__c'] = this.commonService.calculatePercentage(+srv['Deposit_Percent__c'], srv['Net_Price__c'], 1);
              }
            } else {
              this.rows[i]['Deposit_Amount__c'] = parseInt(srv['Deposit_Amount__c'], 10);
            }
            this.rows[i]['Deposit_Required__c'] = srv['Deposit_Required__c'];
          }, 500);
        }
      });
    });
  }
  displayTimeSlots(startNumb) {
    if (this.commonTime.length > 0) {
      if (startNumb < 0) {
        startNumb = 0;
      }
      if (this.commonTime && this.commonTime.length > (9 + startNumb)) {
        this.displayTimes = this.commonTime.slice(startNumb, startNumb + 9);
        this.displayNmb = 9 + startNumb;
      } else {
        this.displayTimes = this.commonTime.slice(startNumb, this.commonTime.length);
        this.displayNmb = this.commonTime.length;
      }
      if ((this.commonTime.length - this.displayNmb) > 0) {
        this.showMore = true;
      } else {
        this.showMore = false;
      }
      if ((this.displayNmb - 9) > 0) {
        this.showLess = true;
      } else {
        this.showLess = false;
      }
    } else {
      this.displayTimes = [];
      this.showMore = false;
      this.showLess = false;
      this.displayNmb = 0;
    }
  }
  /* Method to validate rows to select service and worker combination */
  validateRows() {
    let rtnObj = true;
    this.rows.forEach(element => {
      if (!element['Id'] || !element['workername']) {
        this.onlineBookingErr = 'Please select a service & worker combination';
        window.scrollTo(0, 0);
        rtnObj = false;
      }
    });
    return rtnObj;
  }
  upDatePreNextIcon() {
    if (this.bsValue.setHours(0, 0, 0, 0) > this.minDate.setHours(0, 0, 0, 0)) {
      this.disablePrevious = false;
    } else {
      this.disablePrevious = true;
    }
    if (this.bsValue.setHours(0, 0, 0, 0) >= this.maxDate.setHours(0, 0, 0, 0)) {
      this.disableNext = true;
    } else {
      this.disableNext = false;
    }
  }
  // starting jump dates search for slots //
  // new variables used (search, showText,slots)for this functionality.
  findNextAppt() {
    this.commonTime = [];
    // this.findNext = true;
    if (this.commonTime.length === 0) {
      const finnxtdate = new Date(this.bsValue);
      const nxtdate = new Date(finnxtdate.setDate(finnxtdate.getDate() + 1));
      this.searchSlots(nxtdate, this.search);
      this.selectedIndex = -1;
      this.showScheduleButton = false;
    }

  }
  searchSlots(date, search) {
    if (!this.doubleClick) {
      this.doubleClick = true;
      const workerIds = [];
      const durations = [];
      const bookEvery = [];
      const resources = [];
      const resFilter = [];
      this.listingName = [];

      let tempSrcDate = date;
      for (let i = 0; i < this.rows.length; i++) {
        this.listingName.push({
          'wrkName': this.rows[i].FullName,
          'serviceName': this.rows[i].serName,
          'duration': this.rows[i].Duration__c,
          'description': this.rows[i]['descp'],
          'price': this.rows[i].Net_Price__c
        });
        this.descShow = true;
        /* below if condition added for worker future start date assigning purpose */
        let tempDt;
        if (this.rows[i].workername && this.workerList[i].filter(obj => obj['workername'] === this.rows[i].workername)[0]['StartDay']) {
          tempDt = this.commonService.getDateFrmDBDateStr(this.workerList[i].filter(obj => obj['workername'] === this.rows[i].workername)[0]['StartDay']);
        } else {
          tempDt = this.commonService.getDateFrmDBDateStr(this.rows[i]['StartDay']);
        }
        if (tempDt > tempSrcDate) {
          tempSrcDate = tempDt;
        }
        workerIds.push(this.rows[i].workername);
        durations.push(this.rows[i].Duration__c);
        resources.push(this.rows[i].Resources__c);
        resFilter.push(this.rows[i].Resource_Filter__c);
        bookEvery.push(this.rows[i].bookEvery ? this.rows[i].bookEvery : this.rows[i].Book_Every__c);
      }
      const onlineSearchdate = new Date();
      onlineSearchdate.setHours(onlineSearchdate.getHours() + parseInt(this.windowStartHour, 10));
      const dataObj = {
        'date': this.commonService.getDBDatStr(tempSrcDate).split(' ')[0],
        'id': workerIds,
        'dateformat': 'MM/DD/YYYY hh:mm:ss a',
        'durations': durations,
        'mindate': this.commonService.getDBDatTmStr(onlineSearchdate),
        'Booked_Online__c': 1,
        'bookEvery': bookEvery,
        'resources': resources,
        'resFilter': resFilter
      };
      const tempSrvData = [];
      this.rows.forEach(obj => {
        tempSrvData.push({
          'duration1': obj['Duration_1__c'],
          'duration2': obj['Duration_2__c'],
          'duration3': obj['Duration_3__c'],
          'bufferafter': obj['Buffer_After__c'],
          'available1': obj['Duration_1_Available_for_Other_Work__c'],
          'available2': obj['Duration_2_Available_for_Other_Work__c'],
          'available3': obj['Duration_3_Available_for_Other_Work__c']
        });
      });
      dataObj['serviceData'] = tempSrvData;
      this.onlineBookService.searchForAppts(dataObj).subscribe(data => {
        if (data['result'].length > 0) {
          if (date <= this.maxDate) {
            this.selectDate(date);
            this.search = true;
            this.slots = [];
            this.slots = data['result'];
            this.slots.forEach(element => {
              const ele = element.value.split(' ');
              element = ele[1] + ' ' + ele[2];
              const tempTimes = [];
              if (ele[0] === moment(this.bsValue).format('MM/DD/YYYY')) {
                tempTimes.push({
                  'time': element,
                  'date': ele[0]
                });
              } else {
                this.slots = [];
                this.showText = false;
                this.onlineBookingEr = 'ONLINEBOOK.EMPTY_SLOTS_FOUND';
              }
            });
          } else {
            this.showText = false;
            this.onlineBookingEr = 'No appointments meet the criteria.  Click Go Back to try a new search';
          }
        } else {
          this.slots = [];
          const tempDate = new Date(date.setDate(date.getDate() + 1));
          if (tempDate <= this.maxDate) {
            if ((data['result'].length === 0 && !this.search)) {
              this.onlineBookingEr = 'ONLINEBOOK.EMPTY_SLOTS_FOUND';
              this.doubleClick = false;
              this.searchSlots(tempDate, false);
            }
          } else {
            this.showText = false;
            this.onlineBookingEr = 'No appointments meet the criteria.  Click Go Back to try a new search';
          }
        }
        this.displayTimeSlots(0);
        this.upDatePreNextIcon();
        this.selectedIndex = -1;
        this.showScheduleButton = false;
        this.doubleClick = false;
      },
        error => {
          const errStatus = JSON.parse(error['_body'])['status'];
          if (errStatus === '2085' || errStatus === '2071') {
            if (this.router.url !== '/') {
              localStorage.setItem('page', this.router.url);
              this.router.navigate(['clientlogin/' + localStorage.getItem('param')]);
            }
          }
          this.doubleClick = false;
        }
      );
    }
  }
  // end of  jump dates search for slots //
}
