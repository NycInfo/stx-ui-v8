import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { OnlineClientAddService } from './onlineclientadd.service';
import * as config from '../../app.config';
import { Password } from '../../../custommodules/primeng/primeng';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { TranslateService } from '@ngx-translate/core';
import { SafeUrl, DomSanitizer } from '@angular/platform-browser';


@Component({
  selector: 'app-onlineclientadd',
  templateUrl: './onlineclientadd.component.html',
  styleUrls: ['./onlineclientadd.component.css'],
  providers: [OnlineClientAddService]
})

export class OnlineClientAddComponent implements OnInit {

  apiEndPoints = config['S3_URL'];
  companyLogo = '';
  companyName = '';
  companyId = '';
  dbName = '';
  clientInfo: any = {
    firstname: '',
    lastname: '',
    email: '',
    Notification_Primary_Email__c: 0,
    Marketing_Primary_Email__c: 1,
    Reminder_Primary_Email__c: 1,
    mobilePhone: '',
    mailingPostalCode: '',
    mailingCountry: '',
    mailingCity: '',
    address: '',
    gender: '',
    birthDate: '',
    day: '',
    month: '',
    year: '',
    mailingState: '',
    cmpName: '',
    Password1: '',
    Password2: '',
    Notification_Mobile_Phone__c: 0,
    Marketing_Mobile_Phone__c: 0,
    Reminder_Mobile_Phone__c: 0,
    Sms_Consent__c: 0
  };
  statesList: any = [];
  displayDiv: any = true;
  saveDiv: any = false;
  mobileReq: any = false;
  isClientSaved = false;
  isApprovelSent = false;
  glyphiconClass1 = 'glyphicon-eye-open';
  passwordType1 = 'password';
  glyphiconClass2 = 'glyphicon-eye-open';
  passwordType2 = 'password';
  clientErr = '';
  passwrdErr = '';
  newClientPictureFile: File;
  // clientPictureFileView: SafeUrl = '';
  newclientPictureFileView: SafeUrl = '';
  mailingCountriesList = [{ 'NAME': 'Canada' }, { 'NAME': 'United States' }];
  primaryCountryCode = '01';
  cliImage: any = {};
  image: any = '';
  constructor(
    private toastr: ToastrService,
    private router: Router,
    private http: HttpClient,
    private sanitizer: DomSanitizer,
    private translateService: TranslateService,
    private activatedRoute: ActivatedRoute,
    private clientaddService: OnlineClientAddService,
    private onlineClientAddService: OnlineClientAddService
  ) {
    this.activatedRoute.queryParams.subscribe(params => {
      this.dbName = activatedRoute.snapshot.params['db'];
    });
  }

  ngOnInit() {
    this.companyName = localStorage.getItem('compname');
    this.companyId = localStorage.getItem('compid');
    this.companyLogo = this.apiEndPoints + localStorage.getItem('complogo') + '?time=' + new Date().getTime();
    this.clientaddService.getClientInfo(this.dbName).subscribe(
      data => {
        if (data['result']['mobileRequired']) {
          this.mobileReq = data['result']['mobileRequired'];
        } else {
          this.mobileReq = false;
        }
      },
      error => {
        const status = JSON.parse(error['status']);
        const statuscode = JSON.parse(error['_body']).status;
        switch (status) {
          case 500:
            break;
          case 400:
            if (statuscode === '2085' || statuscode === '2071') {
              if (this.router.url !== '/') {
                localStorage.setItem('page', this.router.url);
                this.router.navigate(['clientlogin/' + this.dbName]);
              }
            }
            break;
        }
        // this.toastr.error('Invalid URL', null, { timeOut: 3000 });
      }
    );
  }
  togglePassword(index) {
    if (index === 1) {
      if (this.glyphiconClass1 === 'glyphicon-eye-open') {
        this.glyphiconClass1 = 'glyphicon-eye-close';
        this.passwordType1 = 'text';
      } else {
        this.glyphiconClass1 = 'glyphicon-eye-open';
        this.passwordType1 = 'password';
      }
    } else if (index === 2) {
      if (this.glyphiconClass2 === 'glyphicon-eye-open') {
        this.glyphiconClass2 = 'glyphicon-eye-close';
        this.passwordType2 = 'text';
      } else {
        this.glyphiconClass2 = 'glyphicon-eye-open';
        this.passwordType2 = 'password';
      }
    }
  }
  reDirectRoute() {
    this.router.navigate(['/clientlogin/' + this.dbName]);
  }
  checkData(value) {
    if (value === 'check1') {
      const EMAIL_REGEXP = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      if (!this.clientInfo['firstname']) {
        this.clientErr = 'ONLINE_CLIENT_ADD.FIRST_NAME_REQUIRED';
        window.scrollTo(0, 0);
      } else if (!this.clientInfo['lastname']) {
        this.clientErr = 'ONLINE_CLIENT_ADD.LAST_NAME_REQUIRED';
        window.scrollTo(0, 0);
      } else if (!this.clientInfo['email']) {
        this.clientErr = 'ONLINE_CLIENT_ADD.EMAIL_IS_REQUIRED';
        window.scrollTo(0, 0);
      } else if (!EMAIL_REGEXP.test(this.clientInfo['email'])) {
        this.clientErr = 'ONLINE_CLIENT_ADD.INVALID_EMAIL_ADDRESS';
        window.scrollTo(0, 0);
      } else if ((this.clientInfo['Notification_Mobile_Phone__c'] === 1 || this.clientInfo['Marketing_Mobile_Phone__c'] === 1
        || this.clientInfo['Reminder_Mobile_Phone__c'] === 1 || this.mobileReq) && this.clientInfo['mobilePhone'] === '') {
        this.clientErr = 'ONLINE_CLIENT_ADD.MOBILE_PONE_REQUIRED';
        window.scrollTo(0, 0);
      } else if (this.clientInfo['mobilePhone'] !== '' && (this.clientInfo['mobilePhone'].length < 13 || this.clientInfo['mobilePhone'].length > 13)) {
        this.clientErr = 'ONLINE_CLIENT_ADD.MOBILE_PHONE_INVALID';
        window.scrollTo(0, 0);
      } else if (this.clientInfo['birthDate']) {
        // First check for the pattern
        const datePattern = /^\d{1,2}\/\d{1,2}\/\d{4}$/;
        if (!datePattern.test(this.clientInfo['birthDate'])) {
          this.clientErr = 'ONLINE_CLIENT_ADD.INVALID_DATE_FORMAT';
          return false;
        }
        // Parse the date parts to integers
        const parts = this.clientInfo['birthDate'].split('/');
        const day = parseInt(parts[1], 10);
        const month = parseInt(parts[0], 10);
        const year = parseInt(parts[2], 10);
        // Check the ranges of month and year
        if (year < 1000 || year > 3000 || month === 0 || month > 12) {
          this.clientErr = 'ONLINE_CLIENT_ADD.INVALID_DATE_FORMAT';
          return false;
        }
        const monthLength = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

        // Adjust for leap years
        if (year % 400 === 0 || (year % 100 !== 0 && year % 4 === 0)) {
          monthLength[1] = 29;
        }
        // Check the range of the day
        this.clientInfo['birthDate'] = month + '/' + day + '/' + year;
        this.clientInfo['day'] = day;
        this.clientInfo['month'] = month;
        this.clientInfo['year'] = year;
      }
      if (this.clientErr === '') {
        this.displayDiv = false;
      } else {
        this.displayDiv = true;
      }
    } else {
      if (this.clientErr === '') {
        this.displayDiv = false;
        this.saveDiv = true;
      } else {
        this.saveDiv = false;
      }
    }
  }

  assaignValues(key) {
    this.clientInfo[key] = !this.clientInfo[key];
    this.clientInfo[key] = this.clientInfo[key] ? 1 : 0;
  }

  clearError() {
    this.clientErr = '';
  }
  saveProfile() {
    // const EMAIL_REGEXP = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    // if (!this.clientInfo['firstname']) {
    //   this.clientErr = 'FirstName is required';
    //   window.scrollTo(0, 0);
    // } else if (!this.clientInfo['lastname']) {
    //   this.clientErr = 'LastName is required';
    //   window.scrollTo(0, 0);
    // } else if ((this.clientInfo['Notification_Mobile_Phone__c'] === 1 || this.clientInfo['Marketing_Mobile_Phone__c'] === 1
    //   || this.clientInfo['Reminder_Mobile_Phone__c'] === 1 || this.mobileReq) && this.clientInfo['mobilePhone'] === '') {
    //   this.clientErr = 'Mobile Phone is required';
    //   window.scrollTo(0, 0);
    // } else if (!this.clientInfo['email']) {
    //   this.clientErr = 'Email is required';
    //   window.scrollTo(0, 0);
    // } else if (!EMAIL_REGEXP.test(this.clientInfo['email'])) {
    //   this.clientErr = 'Invalid Email Address';
    //   window.scrollTo(0, 0);
    if (this.clientInfo.Password1 !== this.clientInfo.Password2) {
      this.passwrdErr = 'ONLINE_CLIENT_ADD.PASSWORD_SHOULD_BE_SAME';
    } else {
      const currentDate = new Date();
      const dtStr = currentDate.getFullYear()
        + '-' + ('0' + (currentDate.getMonth() + 1)).slice(-2)
        + '-' + ('0' + currentDate.getDate()).slice(-2)
        + ' ' + ('0' + currentDate.getHours()).slice(-2)
        + ':' + ('0' + currentDate.getMinutes()).slice(-2)
        + ':' + ('0' + currentDate.getSeconds()).slice(-2);
      this.clientInfo['dbName'] = this.dbName;
      this.clientInfo['cmpName'] = this.companyName;
      this.clientInfo['cmpId'] = this.companyId;
      this.clientInfo['clientInfoMailingCountry'] = 'United States';
      this.clientInfo['email_c'] = localStorage.getItem('compEmail__c');
      this.clientInfo['mobilePhone'] = this.clientInfo['mobilePhone'] ? this.primaryCountryCode + '-' + this.clientInfo['mobilePhone'] : '';
      this.clientInfo['dt'] = dtStr;
      this.clientInfo['clientId'] = ''; /**  clientId is used in myaccount page edit puprpose dont change */
      this.onlineClientAddService.saveClientProfile(this.clientInfo, this.newClientPictureFile, this.companyId).subscribe(
        // this.onlineClientAddService.saveClientProfile(this.clientInfo).subscribe(

        data => {
          if (data['result'] === 'Approvel Sent') {
            this.isApprovelSent = true;
          } else {
            this.isClientSaved = true;
          }
        },
        error => {
          const status = JSON.parse(error['status']);
          const statuscode = JSON.parse(error['_body']).status;
          switch (status) {
            case 500:
              break;
            case 400:
              if (statuscode === '2088') {
                this.clientErr = 'CLIENTS.DUPLICATE_CLIENT';
                this.displayDiv = true;
                this.saveDiv = false;
                window.scrollTo(0, 0);
              } if (statuscode === '2085' || statuscode === '2071') {
                if (this.router.url !== '/') {
                  localStorage.setItem('page', this.router.url);
                  this.router.navigate(['clientlogin/' + this.dbName]);
                }
              } break;
          }
          // this.clientErr = 'CLIENTS.DUPLICATE_CLIENT';
          // this.displayDiv = true;
          // this.saveDiv = false;
          this.clientInfo['mobilePhone'] = this.clientInfo['mobilePhone'] ? this.clientInfo['mobilePhone'].split(this.primaryCountryCode + '-')[1] : '';
        }
      );
    }

  }

  login() {
    this.router.navigate(['clientlogin/' + localStorage.getItem('param')]);
  }
  hyphen_generate_Wphone(value) {
    if (value === undefined || value === null) {
      value = '';
    }
    if (value.length === 0) {
      (<HTMLInputElement>document.getElementById('workPhone')).value = value.concat('(');
    }
    if (value.length === 4) {
      (<HTMLInputElement>document.getElementById('workPhone')).value = value.concat(')');
    } if (value.length === 8) {
      (<HTMLInputElement>document.getElementById('workPhone')).value = value.concat('-');
    }
  }
  pasteNumPhone(value) {
    let temp = '';
    if (value.indexOf('(') !== 0) {
      for (let i = 0; i < value.length; i++) {
        if (i === 0) {
          temp += '(' + value[i];
        } else if (i === 2) {
          temp += value[i] + ')';
        } else if (i === 5) {
          temp += value[i] + '-';
        } else {
          temp += value[i];
        }
        this.clientInfo.mobilePhone = temp.substr(0, 13);
      }
    }
  }
  SmscheckOrUnCheck(che) {
    let value;
    if (che) {
      value = 1;
    } else {
      value = 0;
    }
    this.clientInfo['Sms_Consent__c'] = value;
    if (che) {
      this.clientInfo['Marketing_Mobile_Phone__c'] = value;
      this.clientInfo['Notification_Mobile_Phone__c'] = value;
      this.clientInfo['Reminder_Mobile_Phone__c'] = value;
    }
  }
  mobileSms(val) {
    if (!val) {
      this.clientInfo['Sms_Consent__c'] = 0;
    }
  }
  getLocation() {
    if (this.clientInfo['mailingPostalCode'].length > 4) {
      this.http.get('https://ziptasticapi.com/' + this.clientInfo['mailingPostalCode']).subscribe(
        result => {
          if (result['error']) {
            this.clientErr = 'SETUPCOMPANY.ZIP_CODE_NOT_FOUND';
            this.clientInfo['mailingCountry'] = '';
            this.clientInfo['mailingState'] = '';
            this.clientInfo['mailingCity'] = '';
            // const toastermessage: any = this.translateService.get('SETUPCOMPANY.ZIP_CODE_NOT_FOUND');
            // this.toastr.error(toastermessage.value, null, { timeOut: 1500 });
          } else {
            if (result['country'] === 'US') {
              this.clientInfo['mailingCountry'] = 'United States';
              this.getCountry(this.clientInfo['mailingCountry']);
              config.states.forEach(state => {
                if (state.abbrev === result['state']) {
                  this.clientInfo['mailingState'] = state.name;
                }
              });
            }
            const cityArray = result['city'].split(' ');
            for (let i = 0; i < cityArray.length; i++) {
              if (i === 0) {
                this.clientInfo['mailingCity'] = cityArray[i].charAt(0).toUpperCase() + cityArray[i].slice(1).toLowerCase() + ' ';
              } else {
                this.clientInfo['mailingCity'] += cityArray[i].charAt(0).toUpperCase() + cityArray[i].slice(1).toLowerCase() + ' ';
              }
            }
          }
        },
        error => {
          const status = JSON.parse(error['status']);
          const statuscode = JSON.parse(error['_body']).status;
          switch (status) {
            case 500:
              break;
            case 400:
              if (statuscode === '2085' || statuscode === '2071') {
                if (this.router.url !== '/') {
                  localStorage.setItem('page', this.router.url);
                  this.router.navigate(['clientlogin/' + this.dbName]);
                }
              }
              break;
          }
        }
      );
    }
  }
  getCountry(coun) {
    this.clientaddService.getStates(coun)
      .subscribe(statesValues => {
        this.statesList = statesValues['result'];
      },
        error => {
          const errorMessage = <any>error;
          const errStatus = JSON.parse(error['_body'])['status'];
          if (errStatus === '2085' || errStatus === '2071') {
            if (this.router.url !== '/') {
              localStorage.setItem('page', this.router.url);
              this.router.navigate(['clientlogin/' + this.dbName]);
            }
          }
        });
  }
  imageUpload(fileEvent) {
    // this.newClientPictureFile = fileEvent.target.files[0];
    // this.newclientPictureFileView = this.sanitizer.bypassSecurityTrustUrl(window.URL.createObjectURL(this.newClientPictureFile));
    // this.imgErr = '';
    if (this.cliImage.hasOwnProperty('error')) {
      delete this.cliImage.error;
    }
    const eventObj: MSInputMethodContext = <MSInputMethodContext>fileEvent;
    const target: HTMLInputElement = <HTMLInputElement>eventObj.target;
    const files: FileList = fileEvent.target.files;
    if (files.length > 0) {
      this.cliImage.file = files[0];
      this.cliImage.fileName = files[0].name;
      const fSize = this.cliImage.file.size;
      if (!this.cliImage.fileName.match(/.(jpg|jpeg|png|gif|svg|bmp)$/i)) {
        this.cliImage.error = 'COMMON_STATUS_CODES.2058';
        delete this.cliImage.file;
      } else if (fSize > 1000000) {
        this.cliImage.allowableSize = false;
        this.cliImage.error = 'SETUPCOMPANY.VALID_IMAGE_FILENAME';
        delete this.cliImage.file;
      } else {
        this.newClientPictureFile = fileEvent.target.files[0];
        this.newclientPictureFileView = this.sanitizer.bypassSecurityTrustUrl(window.URL.createObjectURL(this.newClientPictureFile));
        // this.cliImage.srcURL = this.apiEndPoints + this.image + '?time=' + new Date().getTime();
        // this.cliImage.allowableSize = true;
        // this.cliImage.srcURL = this.sanitizer.bypassSecurityTrustUrl(window.URL.createObjectURL(this.cliImage.file));
      }
    }
  }
  imageErrorHandler(event) {
    event.target.src = '/assets/images/no-preview.png';
  }
}
