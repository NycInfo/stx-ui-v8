import { Injectable, Inject } from '@angular/core';
import { Headers, Response } from '@angular/http';
import { HttpClients } from '../../common/http-client';
import { map } from 'rxjs/operators';

@Injectable()
export class OnlineClientAddService {

  constructor(private http: HttpClients,
    @Inject('apiEndPoint') private apiEndPoint: string
  ) { }
  saveClientProfile(clientObj, clientPictureFile: File, companyId) {
    const formData: any = new FormData();
    formData.append('clientPictureFile', clientPictureFile);
    formData.append('clientObj', JSON.stringify(clientObj));
    const headers = new Headers();
    headers.append('cid', companyId);
    return this.http.postHeader(this.apiEndPoint + '/api/onlineclientlogin', formData, headers)
      .pipe(map(this.extractData));
  }
  getClientInfo(dbName) {
    return this.http.get(this.apiEndPoint + '/api/client/info/' + dbName)
      .pipe(map(this.extractData));
  }
  getStates(countryName) {
    return this.http.get(this.apiEndPoint + '/api/v1/lookups/states/' + countryName)
      .pipe(map(this.extractData));
  }
  private extractData(res: Response) {
    if (res.headers && res.headers.get('token')) {
      localStorage.setItem('clienttoken', res.headers.get('token'));
    }
    const body = res.json();
    return body || {};
  }

}
