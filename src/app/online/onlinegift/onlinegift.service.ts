import { Injectable, Inject } from '@angular/core';
import { Headers, Response } from '@angular/http';
import { HttpClients } from '../../common/http-client';
import { map } from 'rxjs/operators';

@Injectable()
export class OnlineGiftService {

    constructor(
        private http: HttpClients,
        @Inject('apiEndPoint') private apiEndPoint: string
    ) { }


    onlineGiftPurchase(purchaseGiftObj) {
        const headers = new Headers();
        headers.append('token', localStorage.getItem('clienttoken'));
        return this.http.postHeader(this.apiEndPoint + '/api/onlinegift/purchase', purchaseGiftObj, headers)
            .pipe(map(this.extractData));
    }
    onlineGiftPurchaseWithoutlogin(purchaseGiftObj) {
        return this.http.post(this.apiEndPoint + '/api/onlinegift/purchase/withoutlogin/' + localStorage.getItem('param') + '/' + localStorage.getItem('compname'), purchaseGiftObj)
            .pipe(map(this.extractData));
    }
    getStates(countryName) {
        return this.http.get(this.apiEndPoint + '/api/v1/lookups/states/' + countryName)
            .pipe(map(this.extractData));
    }
    getClientInfo(dbName) {
        return this.http.get(this.apiEndPoint + '/api/client/info/' + dbName)
          .pipe(map(this.extractData));
      }
    /**
   * To get paymenttypes Data
   */
    getPaymentTypesData() {
        const headers = new Headers();
        headers.append('token', localStorage.getItem('clienttoken'));
        return this.http.getHeader(this.apiEndPoint + '/api/setup/company/paymenttypes', headers)
            .pipe(map(this.extractData));
    }

    getPaymentTypesDataGlobal() {
        return this.http.get(this.apiEndPoint + '/api/setup/company/pt/' + localStorage.getItem('param'))
            .pipe(map(this.extractData));
    }

    deleteThePaymentFailedRecords(apptId) {
        const headers = new Headers();
        headers.append('token', localStorage.getItem('clienttoken'));
        return this.http.deleteHeader(this.apiEndPoint + '/api/payment/' + apptId, headers)
            .pipe(map(this.extractData));
    }
    addToPaymentsTicket(paymentObj, withoutlogin) {
        if (withoutlogin === 'any') {
            return this.http.post(this.apiEndPoint + '/api/checkout/ticketpayments/withoutlogin/' + localStorage.getItem('param') + '/' + localStorage.getItem('compid'), paymentObj)
                .pipe(map(this.extractData));
        } else {
            const headers = new Headers();
            headers.append('token', localStorage.getItem('clienttoken'));
            return this.http.postHeader(this.apiEndPoint + '/api/checkout/ticketpayments', paymentObj, headers)
                .pipe(map(this.extractData));
        }
    }
    /**
   * To getWorkerMerchantsData for payments
   */
    getWorkerMerchantsData() {
        const headers = new Headers();
        headers.append('token', localStorage.getItem('clienttoken'));
        return this.http.getHeader(this.apiEndPoint + '/api/checkout/ticketpayments/worker/merchant', headers)
            .pipe(map(this.extractData));
    }
    xmlPayment(reqObj, checklogin) {
        const headers = new Headers();
        headers.append('token', localStorage.getItem('clienttoken'));
        return this.http.post(this.apiEndPoint + '/api/payment', reqObj)
            .pipe(map(this.extractData));
    }
    /** below api's are used forcardholder email reciept purpose */
    getPosdevices() {
        const headers = new Headers();
        headers.append('token', localStorage.getItem('clienttoken'));
        return this.http.getHeader(this.apiEndPoint + '/api/setup/ticketpreferences/posdevices', headers)
            .pipe(map(this.extractData));
    }
    getPos() {
        const headers = new Headers();
        headers.append('token', localStorage.getItem('clienttoken'));
        return this.http.getHeader(this.apiEndPoint + '/api/setup/ticketpreferences/pos', headers)
          .pipe(map(this.extractData));
      }
    sendReciept(dataObj) {
        const headers = new Headers();
        headers.append('token', localStorage.getItem('clienttoken'));
        return this.http.postHeader(this.apiEndPoint + '/api/checkout/emailreciept', { 'data': dataObj }, headers)
            .pipe(map(this.extractData));
    }
    sendReciept1(dataObj) {
        return this.http.post(this.apiEndPoint + '/api/checkout/emailreciept/withoutlogin', { 'data': dataObj })
            .pipe(map(this.extractData));
    }
    getCompanyInfo() {
        const headers = new Headers();
        headers.append('token', localStorage.getItem('clienttoken'));
        return this.http.getHeader(this.apiEndPoint + '/api/setup/compananyinfo', headers)
        .pipe(map(this.extractData));
    }


    private extractData(res: Response) {
        if (res.headers && res.headers.get('token')) {
            localStorage.setItem('clienttoken', res.headers.get('token'));
        }
        const body = res.json();
        return body || {};
    }

}
