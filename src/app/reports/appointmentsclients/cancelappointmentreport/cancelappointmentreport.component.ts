import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { SafeUrl, DomSanitizer } from '@angular/platform-browser';
import { CancelAppointmentReportService } from './cancelappointmentreport.service';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { TotalSheetsService } from '../../transactionreports/totalsheets/totalsheets.service';
import { JwtHelper } from 'angular2-jwt';
import { CommonService } from '../../../common/common.service';
@Component({
  selector: 'app-reports-app',
  templateUrl: './cancelappointmentreport.html',
  styleUrls: ['./cancelappointmentreport.css'],
  providers: [CancelAppointmentReportService, TotalSheetsService, CommonService],
})
export class CancelAppointmentReportComponent implements OnInit {

  minDate: any = new Date();
  startDate = new Date();
  // startDate: any = new Date('1/1/' + (new Date()).getFullYear());
  endDate = new Date();
  staticData: any = [];
  selectType = '';
  sortField = '';
  sortfieldList: any = [];
  canceledAppt: any = [];
  itemsDisplay = false;
  date: any;
  datePickerConfig: any;
  toastermessage: any;
  decodeUserToken: any;
  decodedToken: any;
  apptId: any;
  currentDate: any;
  constructor(
    private cancelApptReportservice: CancelAppointmentReportService,
    private router: Router,
    private commonService: CommonService,
    private toastr: ToastrService,
    private translateService: TranslateService,
    private route: ActivatedRoute) {
    this.datePickerConfig = Object.assign({},
      {
        showWeekNumbers: false,
        containerClass: 'theme-blue',
      });
    this.route.queryParams.subscribe(params => {
      this.apptId = route.snapshot.params.Appt_Ticket__c;
    });
  }
  ngOnInit() {
    // ---Start of code for Permissions Implementation--- //
    try {
      this.decodedToken = new JwtHelper().decodeToken(localStorage.getItem('rights'));
      this.decodeUserToken = new JwtHelper().decodeToken(localStorage.getItem('token'));
    } catch (error) {
      this.decodedToken = {};
      this.decodeUserToken = {};
    }
    if (this.decodedToken.data && this.decodedToken.data.permissions) {
      this.decodedToken = JSON.parse(this.decodedToken.data.permissions);
    } else {
      this.decodedToken = {};
    }
    // ---End of code for Permissions Implementation--- //
    this.getStaticSortingField();
    this.getApptStatus();
  }
  selectTypeOnChange(value) {
    this.selectType = value;
  }
  sortingField(value) {
    this.sortField = value;
    if (this.sortField) {
      this.generateCanceledAppt();
    }
  }
  getStaticSortingField() {
    this.cancelApptReportservice.getSortingFields().subscribe(
      data => {
        this.sortfieldList = data.SORTFILEDSDATA;
        this.sortField = this.sortfieldList[0].option;

      },
      error => {
        const errStatus = JSON.parse(error._body).status;
        if (errStatus === '2085' || errStatus === '2071') {
          if (this.router.url !== '/') {
            localStorage.setItem('page', this.router.url);
            this.router.navigate(['/']).then(() => { });
          }
        }
      });
  }
  getApptStatus() {
    this.cancelApptReportservice.getSortingFields().subscribe(
      data => {
        this.staticData = data.APPOINTMENTSTATUS;
        this.selectType = this.staticData[0].value;
      },
      error => {
        const errStatus = JSON.parse(error._body).status;
        if (errStatus === '2085' || errStatus === '2071') {
          if (this.router.url !== '/') {
            localStorage.setItem('page', this.router.url);
            this.router.navigate(['/']).then(() => { });
          }
        }
      });
  }
  generateCanceledAppt() {
    this.currentDate = this.commonService.getDBDatTmStr(new Date());
    this.canceledAppt = [];
    if (this.startDate === null) {
      this.itemsDisplay = false;
      this.toastermessage = this.translateService.get('TICKET_ANALYSIS_REPORT.PLEASE_SELECT_BEGIN_DATE');
      this.toastr.error(this.toastermessage.value, null, { timeOut: 3000 });
    } else if (isNaN(this.startDate.getTime())) {
      this.itemsDisplay = false;
      this.toastermessage = this.translateService.get('TICKET_ANALYSIS_REPORT.BEGIN_DATE_IS_INVALID');
      this.toastr.error(this.toastermessage.value, null, { timeOut: 3000 });
    } else if (this.endDate === null) {
      this.itemsDisplay = false;
      this.toastermessage = this.translateService.get('TICKET_ANALYSIS_REPORT.PLEASE_SELECT_END_DATE');
      this.toastr.error(this.toastermessage.value, null, { timeOut: 3000 });
    } else if (isNaN(this.endDate.getTime())) {
      this.itemsDisplay = false;
      this.toastermessage = this.translateService.get('TICKET_ANALYSIS_REPORT.END_DATAE_IS_INVALID');
      this.toastr.error(this.toastermessage.value, null, { timeOut: 3000 });
    } else {
      const startTime = (this.startDate.getFullYear() + '') + '-' + ('00' + (this.startDate.getMonth() + 1)).slice(-2) + '-' + ('00' + this.startDate.getDate()).slice(-2);
      const endTime = (this.endDate.getFullYear() + '') + '-' + ('00' + (this.endDate.getMonth() + 1)).slice(-2) + '-' + ('00' + this.endDate.getDate()).slice(-2);
      if (startTime > endTime) {
        this.toastermessage = this.translateService.get('TICKET_ANALYSIS_REPORT.BEGIN_DATE_MUST_BE_BEFORE_THE_END_DATE');
        this.toastr.error(this.toastermessage.value, null, { timeOut: 3000 });
      } else {
        this.cancelApptReportservice.getCancelledAppt(startTime, endTime, this.selectType, this.sortField, this.currentDate).subscribe(
          data => {
            if (data.result.length === 0) {
              this.itemsDisplay = false;
              this.toastermessage = this.translateService.get('TICKET_ANALYSIS_REPORT.NO_DATA_FOUND');
              this.toastr.error(this.toastermessage.value, null, { timeOut: 3000 });
            } else {
              data.result.forEach(elem => {
                if (elem.Appt_Date_Time__c) {
                  elem.date = this.commonService.getUsrDtStrFrmDBStr(elem.Appt_Date_Time__c);
                }
              });
              data.result.forEach(elem => {
                if (elem.futureAppt_Date_Time__c) {
                  elem.futureApptDate = this.commonService.getUsrDtStrFrmDBStr(elem.futureAppt_Date_Time__c);
                }
              });
              this.canceledAppt = data.result;
              this.itemsDisplay = true;
            }
          },
          error => {
            const status = JSON.parse(error.status);
            const statuscode = JSON.parse(error._body).status;
            switch (status) {
              case 500:
                break;
              case 400:
                break;
            }
            if (statuscode === '2085' || statuscode === '2071') {
              if (this.router.url !== '/') {
                localStorage.setItem('page', this.router.url);
                this.router.navigate(['/']).then(() => { });
              }
            }
          }
        );
      }
    }
  }
  printDiv() {
    let printContents, popupWin;
    printContents = document.getElementById('dvContents').innerHTML;
    printContents = printContents.replace('bsdatepicker=""', 'value="' + (this.startDate.getMonth() + 1) + '/'
      + this.startDate.getDate() + '/' + this.startDate.getFullYear() + '"');
    printContents = printContents.replace('bsdatepicker=""', 'value="' + (this.endDate.getMonth() + 1) + '/'
      + this.endDate.getDate() + '/' + this.endDate.getFullYear() + '"');
    printContents = printContents.replace('value="' + this.selectType + '"', 'selected value="' + this.selectType + '"');
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <title>Cancel Appointment Report</title>
          <style>
            table {
              border-collapse: collapse;
            }
            table, th, td {
                border: 0.5px solid black;
            }
            .pri td {
              padding:6px;
            }
            .arc {
              float:left;
              margin:12px;
            }
            .arc button {
              margin-top:10px;
            }
            .total {
              margin:6px;
            }
          </style>
        </head>
        <body onload="window.print();window.close()">${printContents}</body>
      </html>`);
    popupWin.document.close();
  }
}

