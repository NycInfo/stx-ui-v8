import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CancelAppointmentReportComponent } from './cancelappointmentreport.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '',
                component: CancelAppointmentReportComponent,
                children: [
                    {
                        path: '',
                        component: CancelAppointmentReportComponent
                    }
                ]
            }
        ])
    ],
    exports: [
        RouterModule
    ]
})
export class CancelAppointmentReportRoutingModule {
}
