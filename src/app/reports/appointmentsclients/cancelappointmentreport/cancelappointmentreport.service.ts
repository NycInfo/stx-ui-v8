/*
  * Display single user details of front end users
  * extractData(): To extract the data
  * handleError(): To handle error messages.
*/
import { Injectable, Inject } from '@angular/core';
import { Response } from '@angular/http';
// import { Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { HttpClients } from '../../../common/http-client';
import { Headers } from '@angular/http';
import { map } from 'rxjs/operators';

@Injectable()
export class CancelAppointmentReportService {
  constructor(
    private http: HttpClients,
    @Inject('apiEndPoint') private apiEndPoint: string,
    @Inject('staticJsonFilesEndPoint') private staticJsonFilesEndPoint: string
  ) { }

  getSortingFields() {
    return this.http.get(this.staticJsonFilesEndPoint + 'common.json')
      .pipe(map(this.extractData));
  }

  getCancelledAppt(startTime, endTime, status, sortField, currentDate) {
    const headers = new Headers();
    headers.append('startdate', startTime);
    headers.append('enddate', endTime);
    headers.append('status', status);
    headers.append('orderby', sortField);
    headers.append('currentdate', currentDate);
    return this.http.getHeader(this.apiEndPoint + '/api/reports/cancelnoshowappointments', headers)
      .pipe(map(this.extractData));
  }

  /*To extract json data*/
  private extractData(res: Response) {
    if (res.headers && res.headers.get('token')) {
      localStorage.setItem('token', res.headers.get('token'));
    }
    const body = res.json();
    return body || {};
  }
}
