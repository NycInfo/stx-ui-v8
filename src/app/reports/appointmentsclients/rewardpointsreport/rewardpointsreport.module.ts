import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RewardPointsComponent } from './rewardpointsreport.component';
import { RewardPointsRoutingModule } from './rewardpointsreport.routing';
import { FormsModule } from '@angular/forms';
import { ShareModule } from '../../../common/share.module';
import { TranslateModule } from '@ngx-translate/core';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
@NgModule({
    imports: [
        CommonModule,
        RewardPointsRoutingModule,
        FormsModule,
        TranslateModule,
        ShareModule,
        BsDatepickerModule.forRoot(),
    ],
    declarations: [
        RewardPointsComponent
    ]
})
export class RewardPointsModule {
}
