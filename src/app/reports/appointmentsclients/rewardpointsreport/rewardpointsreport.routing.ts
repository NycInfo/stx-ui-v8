import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { RewardPointsComponent } from './rewardpointsreport.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '',
                component: RewardPointsComponent,
                children: [
                    {
                        path: '',
                        component: RewardPointsComponent
                    }
                ]
            }
        ])
    ],
    exports: [
        RouterModule
    ]
})
export class RewardPointsRoutingModule {
}
