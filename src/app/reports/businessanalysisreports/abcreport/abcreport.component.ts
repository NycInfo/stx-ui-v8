import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { SafeUrl, DomSanitizer } from '@angular/platform-browser';
import { AbcReportService } from './abcreport.service';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { TotalSheetsService } from '../../../reports/transactionreports/totalsheets/totalsheets.service';
import { ReportVisitTypeOverviewService } from '../../../reports/businessanalysisreports/reportvisittypeoverview/reportvisittypeoverview.service';
import { JwtHelper } from 'angular2-jwt';
@Component({
  selector: 'app-reports-app',
  templateUrl: './abcreport.html',
  styleUrls: ['./abcreport.scss'],
  providers: [AbcReportService, TotalSheetsService, ReportVisitTypeOverviewService],
})
export class AbcReportComponent implements OnInit {
  startDate = new Date();
  endDate = new Date();
  minDate = new Date();
  isGenerate = false;
  workerTipsData: any;
  datePickerConfig: any;
  companySalesObj = [];
  accountBalanceObj = {
    'deposit': 0,
    'depositOnline': 0,
    'prepayment': 0,
    'receivedOnAccount': 0,
    'totals': 0
  };
  error: any;
  decodedToken: any;
  decodeUserToken: any;
  companyName: any;
  newClients = [];
  nonserviceClients = [];
  allrecord: any;
  visittypeRecord = [];
  totalNewClients = 0;
  totalRebooked = 0;
  totalBookedOnline = 0;
  totalServiceSales = 0;
  totalNonService = 0;
  totalRecurringClients = 0;
  totalProductSales = 0;
  NumberTickets = 0;
  ProductperTicket = 0;
  newVisitTotal = [];
  visitTypesHeader = [];
  toastermessage: any;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private toastr: ToastrService,
    private translateService: TranslateService,
    private totalSheetsService: TotalSheetsService,
    private reportVisitTypeOverviewService: ReportVisitTypeOverviewService,
    private abcReportService: AbcReportService) {
    this.datePickerConfig = Object.assign({},
      {
        showWeekNumbers: false,
        containerClass: 'theme-blue',
      });

  }
  ngOnInit() {
    // ---Start of code for Permissions Implementation--- //
    try {
      this.decodedToken = new JwtHelper().decodeToken(localStorage.getItem('rights'));
      this.decodeUserToken = new JwtHelper().decodeToken(localStorage.getItem('token'));
    } catch (error) {
      this.decodedToken = {};
      this.decodeUserToken = {};
    }
    if (this.decodedToken.data && this.decodedToken.data.permissions) {
      this.decodedToken = JSON.parse(this.decodedToken.data.permissions);
    } else {
      this.decodedToken = {};
    }
    // ---End of code for Permissions Implementation--- //
  }
  loadCompanyInfo(data) {
    this.companyName = data.Name;
  }
  generateReport() {
    const date = new Date();
    /* reqData is used to send dates for visitypes data  */
    if (this.startDate === null) {
      this.toastermessage = this.translateService.get('TICKET_ANALYSIS_REPORT.PLEASE_SELECT_BEGIN_DATE');
      this.toastr.error(this.toastermessage.value, null, { timeOut: 3000 });
    } else if (this.endDate === null) {
      this.toastermessage = this.translateService.get('TICKET_ANALYSIS_REPORT.PLEASE_SELECT_END_DATE');
      this.toastr.error(this.toastermessage.value, null, { timeOut: 3000 });
    } else if (this.startDate.setHours(0, 0, 0, 0) > this.endDate.setHours(0, 0, 0, 0)) {
      this.toastermessage = this.translateService.get('TICKET_ANALYSIS_REPORT.BEGIN_DATE_MUST_BE_BEFORE_THE_END_DATE');
      this.toastr.error(this.toastermessage.value, null, { timeOut: 3000 });
    } else {
      const reqData = {
        startDate: this.startDate.getFullYear() + '-' + (this.startDate.getMonth() + 1) + '-' + this.startDate.getDate(),
        endDate: this.endDate.getFullYear() + '-' + (this.endDate.getMonth() + 1) + '-' + this.endDate.getDate()
      };
      this.totalSheetsService.getDailyTotalSheetRecords(this.startDate, this.endDate).subscribe(
        data => {
          this.companySalesObj = data.result.companySalesObj;
          this.accountBalanceObj = data.result.accountBalanceObj;
          this.isGenerate = true;
          this.callingClientsData(reqData);
        },
        error => {
          const status = JSON.parse(error.status);
          const statuscode = JSON.parse(error._body).status;
          switch (status) {
            case 500:
              break;
            case 400:
              break;
          }
          if (statuscode === '2085' || statuscode === '2071') {
            if (this.router.url !== '/') {
              localStorage.setItem('page', this.router.url);
              this.router.navigate(['/']).then(() => { });
            }
          }
        }
      );
    } /* 1st else End */
  }
  callingClientsData(data) {
    this.clear(); /* clear method is used to clear all totals every time when generete a report */
    this.reportVisitTypeOverviewService.getClientInfo(data).subscribe(res => {
      let records = [];
      // this.itemsDisplay = true;
      this.newClients = res.result.newClients;
      this.nonserviceClients = res.result.nonserviceClients;
      const onlineClients = res.result.onlineClients;
      const productSales = res.result.productSales;
      const rebookedClients = res.result.rebookedClients;
      const recurringClients = res.result.recurringClients;
      const serviceSales = res.result.serviceSales;
      const visitTypeData = res.result.visitTypeData;
      // this is for get nonserviceClients data
      records = this.newClients.concat(this.nonserviceClients);
      for (let i = 0; i < records.length; i++) {
        if (!records[i].dupl) {
          for (let j = i + 1; j < records.length; j++) {
            if (records[i].workerId === records[j].workerId) {
              records[j].dupl = true;
              records[i].nonServiceClients = records[j].nonServiceClients ? records[j].nonServiceClients : '';
            }
          }
        }
      }

      // this is for get onlineClients data
      let onlinerecord = [];
      onlinerecord = records.concat(onlineClients);
      for (let i = 0; i < onlinerecord.length; i++) {
        if (!onlinerecord[i].dupl) {
          for (let j = i + 1; j < onlinerecord.length; j++) {
            if (onlinerecord[i].workerId === onlinerecord[j].workerId) {
              onlinerecord[j].dupl = true;
              onlinerecord[i].onlineClients = onlinerecord[j].onlineClients ? onlinerecord[j].onlineClients : '';
            }
          }
        }
      }

      // this is for get productSales data
      let productSalesrecord = [];
      productSalesrecord = onlinerecord.concat(productSales);
      for (let i = 0; i < productSalesrecord.length; i++) {
        if (!productSalesrecord[i].dupl) {
          for (let j = i + 1; j < productSalesrecord.length; j++) {
            if (productSalesrecord[i].workerId === productSalesrecord[j].workerId) {
              productSalesrecord[j].dupl = true;
              productSalesrecord[i].productSales = productSalesrecord[j].productSales ? productSalesrecord[j].productSales : '';
            }
          }
        }
      }

      // this is for get rebookedClients data
      let rebookedrecord = [];
      rebookedrecord = productSalesrecord.concat(rebookedClients);
      for (let i = 0; i < rebookedrecord.length; i++) {
        if (!rebookedrecord[i].dupl) {
          for (let j = i + 1; j < rebookedrecord.length; j++) {
            if (rebookedrecord[i].workerId === rebookedrecord[j].workerId) {
              rebookedrecord[j].dupl = true;
              rebookedrecord[i].rebookedClients = rebookedrecord[j].rebookedClients ? rebookedrecord[j].rebookedClients : '';
            }
          }
        }
      }

      // this is for get recurringClients data
      let recurringrecord = [];
      recurringrecord = rebookedrecord.concat(recurringClients);
      for (let i = 0; i < recurringrecord.length; i++) {
        if (!recurringrecord[i].dupl) {
          for (let j = i + 1; j < recurringrecord.length; j++) {
            if (recurringrecord[i].workerId === recurringrecord[j].workerId) {
              recurringrecord[j].dupl = true;
              recurringrecord[i].recurringClients = recurringrecord[j].recurringClients ? recurringrecord[j].recurringClients : '';
            }
          }
        }
      }

      // this is for get serviceSales data
      let serviceSalesrecord = [];
      serviceSalesrecord = recurringrecord.concat(serviceSales);
      for (let i = 0; i < serviceSalesrecord.length; i++) {
        if (!serviceSalesrecord[i].dupl) {
          for (let j = i + 1; j < serviceSalesrecord.length; j++) {
            if (serviceSalesrecord[i].workerId === serviceSalesrecord[j].workerId) {
              serviceSalesrecord[j].dupl = true;
              serviceSalesrecord[i].serviceSales = serviceSalesrecord[j].serviceSales ? serviceSalesrecord[j].serviceSales : '';
            }
          }
        }
      }
      const calrecord = serviceSalesrecord.filter(function (obj) { return !obj.dupl; });

      // calculation - Number Of Tickets
      calrecord.forEach(element => {
        element.totalNumberOfTickets = (element.recurringClients ? Number(element.recurringClients) : 0) +
          (element.newClients ? Number(element.newClients) : 0) + (element.nonServiceClients ? Number(element.nonServiceClients) : 0);
        if (element.productSales !== 0 && element.totalNumberOfTickets !== 0) {
          element.totalProductperTicket = ((element.productSales ? Number(element.productSales) : 0) / 
          (element.totalNumberOfTickets ? Number(element.totalNumberOfTickets) : 0)).toFixed(2);
        }
        this.totalRecurringClients += element.recurringClients ? Number(element.recurringClients) : 0;
        this.totalNewClients += element.newClients ? Number(element.newClients) : 0;
        this.totalRebooked += element.rebookedClients ? Number(element.rebookedClients) : 0;
        this.totalBookedOnline += element.onlineClients ? Number(element.onlineClients) : 0;
        this.totalNonService += element.nonServiceClients ? Number(element.nonServiceClients) : 0;
        this.totalServiceSales += element.serviceSales ? Number(element.serviceSales) : 0;
        this.totalProductSales += element.productSales ? Number(element.productSales) : 0;
        this.NumberTickets += element.totalNumberOfTickets ? Number(element.totalNumberOfTickets) : 0;
        this.ProductperTicket += element.totalProductperTicket ? Number(element.totalProductperTicket) : 0;
      });
      this.allrecord = calrecord.filter(function (obj) { return !obj.dupl; });

      const visitTypes: any = [];
      const empUniList = [];
      for (let v = 0; v < visitTypeData.length; v++) {
        if (visitTypes.indexOf(visitTypeData[v].visitType) === -1 &&
          visitTypeData[v].visitType !== '' &&
          visitTypeData[v].visitType !== 'undefined' &&
          visitTypeData[v].visitType !== '0' &&
          visitTypeData[v].visitType !== 'null') {
          visitTypes.push(visitTypeData[v].visitType);
        }
        if (empUniList.indexOf(visitTypeData[v].workerName) === -1 && visitTypeData[v].visitType !== '' &&
          visitTypeData[v].visitType !== 'undefined' &&
          visitTypeData[v].visitType !== '0' &&
          visitTypeData[v].visitType !== 'null') {
          empUniList.push(visitTypeData[v].workerName);
        }
      }
      const disObj = [];
      for (let i = 0; i < empUniList.length; i++) {
        disObj[i] = [empUniList[i]];
        for (let j = 0; j < visitTypes.length; j++) {
          const tempObj = visitTypeData.filter(function (obj) {
            return (obj.workerName === empUniList[i] &&
              obj.visitType === visitTypes[j]);
          });
          if (tempObj.length > 0) {
            disObj[i][j + 1] = tempObj[0].visitTypeClients;
          } else {
            disObj[i][j + 1] = 0;
          }
        }
      }
      const totalAry = [];
      for (let i = 0; i < visitTypes.length; i++) {
        let tot = 0;
        for (let j = 0; j < disObj.length; j++) {
          tot += disObj[j][i + 1];
        }
        totalAry.push(tot);
      }

      this.visitTypesHeader = visitTypes;
      this.visittypeRecord = disObj;
      this.newVisitTotal = totalAry;
    }, error => {
      const errStatus = JSON.parse(error._body).status;
      if (errStatus === '2085' || errStatus === '2071') {
        if (this.router.url !== '/') {
          localStorage.setItem('page', this.router.url);
          this.router.navigate(['/']).then(() => { });
        }
      }
    });
  }
  clearMsg() {
    this.error = '';
  }
  clear() {
    this.totalRecurringClients = 0;
    this.totalNewClients = 0;
    this.totalRebooked = 0;
    this.totalBookedOnline = 0;
    this.totalNonService = 0;
    this.totalServiceSales = 0;
    this.totalProductSales = 0;
    this.NumberTickets = 0;
    this.ProductperTicket = 0;
    this.newVisitTotal = [];
    this.allrecord = [];
    this.visittypeRecord = [];
  }
  printDiv() {
    let printContents;
    let popupWin;
    printContents = document.getElementById('dvContents').innerHTML;
    printContents = printContents.replace('bsdatepicker=""', 'value="' + (this.startDate.getMonth() + 1) + '/'
      + this.startDate.getDate() + '/' + this.startDate.getFullYear() + '"');
    printContents = printContents.replace('bsdatepicker=""', 'value="' + (this.endDate.getMonth() + 1) + '/'
      + this.endDate.getDate() + '/' + this.endDate.getFullYear() + '"');
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
  <html>
    <head>
      <title>ABC Report</title>
      <style>
      //........Customized style.......
      table {
        border-collapse: collapse;
       }
      table, th, td {
          border: 0.5px solid black;
      }
      .pri td {
        padding:6px;
      }
      .arc {
        float:left;
        margin:12px;
      }
      .arc button {
        margin-top:10px;
      }
      .total {
        margin:6px;
      }
      </style>
    </head>
<body onload="window.print();window.close()">${printContents}</body>
  </html>`
    );
    popupWin.document.close();
  }
}
