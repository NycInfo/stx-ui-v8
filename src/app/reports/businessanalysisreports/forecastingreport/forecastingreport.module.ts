import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ForecastingReportComponent } from './forecastingreport.component';
import { ForecastingReportRoutingModule } from './forecastingreport.routing';
import { FormsModule } from '@angular/forms';
import { ShareModule } from '../../../common/share.module';
import { TranslateModule } from '@ngx-translate/core';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        TranslateModule,
        ForecastingReportRoutingModule,
        ShareModule,
        BsDatepickerModule.forRoot(),
    ],
    declarations: [
        ForecastingReportComponent
    ]
})
export class ForecastingReportModule {
}
