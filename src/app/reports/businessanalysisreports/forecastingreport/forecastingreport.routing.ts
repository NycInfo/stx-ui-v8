import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ForecastingReportComponent } from './forecastingreport.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '',
                component: ForecastingReportComponent,
                children: [
                    {
                        path: '',
                        component: ForecastingReportComponent
                    }
                ]
            }
        ])
    ],
    exports: [
        RouterModule
    ]
})
export class ForecastingReportRoutingModule {
}
