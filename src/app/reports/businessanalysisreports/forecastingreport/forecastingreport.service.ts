import { Injectable, Inject } from '@angular/core';
import { Response, Http } from '@angular/http';
import { HttpClients } from '../../../common/http-client';
import { map } from 'rxjs/operators';

@Injectable()
export class ForecastingReportService {

  constructor(
    private http: HttpClients,
    @Inject('apiEndPoint') private apiEndPoint: string,
    @Inject('staticJsonFilesEndPoint') private staticJsonFilesEndPoint: string
  ) {
  }
  getMonthsList() {
    return this.http.get(this.staticJsonFilesEndPoint + 'common.json')
      .pipe(map(this.extractData));
  }
  sendMonthYear(monvalue, yearval) {
    return this.http.get(this.apiEndPoint + '/api/reports/forecasting/' + monvalue + '/' + yearval)
      .pipe(map(this.extractData));
  }
  private extractData(res: Response) {
    if (res.headers && res.headers.get('token')) {
      localStorage.setItem('token', res.headers.get('token'));
    }
    const body = res.json();
    return body || {};
  }
}
