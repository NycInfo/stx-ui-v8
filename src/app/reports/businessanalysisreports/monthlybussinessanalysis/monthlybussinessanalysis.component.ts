import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { SafeUrl, DomSanitizer } from '@angular/platform-browser';
import { MonthlyBussinessAnalysisService } from './monthlybussinessanalysis.service';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { JwtHelper } from 'angular2-jwt';
interface TicketAnalysis {
  serviceSales?: number;
  productSales?: number;
  isRefund?: boolean;
  productTicketCount?: number;
  serviceTicketCount?: number;
  bookedOnline?: boolean;
  noService?: boolean;
  newVisit?: boolean;
  prdtCount?: number;
  servcCount?: number;
}
@Component({
  selector: 'app-reports-app',
  templateUrl: './monthlybussinessanalysis.html',
  styleUrls: ['./monthlybussinessanalysis.scss'],
  providers: [MonthlyBussinessAnalysisService],
})
export class MonthlyBussinessAnalysisComponent implements OnInit {
  itemsDisplay: any = false;
  public serviceGroupList = [];
  public productLinesList = [];
  tbpList: any;
  tbpList1: any;
  decodedToken: any;
  decodeUserToken: any;
  public reportAnalysis: any = {
    'year': undefined,
    'month': undefined,
    'type': 'Comapny Analysis',
    'worker': '',
    'serviceGroup': '',
    'pdLine': ''
  };
  public workerList = [];
  public years = [];
  lastyear: any;
  CompanyName: any;
  service: any;
  selectWorkerName = '';
  monthlyBussinessAnalysisReport = [];
  seleMonth = '';
  totalSerSales = 0;
  totalProSales = 0;
  totalSales = 0;
  totalServiceGroup = 0;
  totalInventoryGroup = 0;
  reebookPec = 0.00;
  srvcCountatlestonePrdct = 0.00;

  totalSerSales1 = 0;
  totalProSales1 = 0;
  totalSales1 = 0;
  totalServiceGroup1 = 0;
  totalInventoryGroup1 = 0;
  reebookPec1 = 0.00;
  srvcCountatlestonePrdct1 = 0.00;
  totalSerSalesPer = 0.00;
  totalProSalesPer = 0.00;
  totalSalesPer = 0.00;
  totalServiceGroupPer = 0.00;
  totalInventoryGroupPer = 0.00;
  reebookPecPer = 0.00;
  srvcCountatlestonePrdctPer = 0.00;
  totalProductivityValPer = 0.00;
  totalticketCount = 0.00;
  totalticketCount1 = 0.00;
  totalticketCountPer = 0.00;
  averageSerivceAmount = 0.00;
  averageSerivceAmount1 = 0.00;
  averageSerivcePer = 0.00;
  averageSerivceAmountPer = 0.00;
  SeleSerGroName = '';
  SeleSerPldLineName = '';
  public months = [
    { Id: '01', name: 'January' },
    { Id: '02', name: 'February' },
    { Id: '03', name: 'March' },
    { Id: '04', name: 'April' },
    { Id: '05', name: 'May' },
    { Id: '06', name: 'June' },
    { Id: '07', name: 'July' },
    { Id: '08', name: 'August' },
    { Id: '09', name: 'September' },
    { Id: '10', name: 'October' },
    { Id: '11', name: 'November' },
    { Id: '12', name: 'December' },
  ];
  public reports = new Map<string, TicketAnalysis>();
  totalNumberOfTickets = 0;
  public reportDetails: any = {};
  public reports1 = new Map<string, TicketAnalysis>();
  totalNumberOfTickets1 = 0;
  public reportDetails1: any = {};
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private toastr: ToastrService,
    private translateService: TranslateService,
    private monthlyBussinessAnalysisService: MonthlyBussinessAnalysisService) {

  }
  ngOnInit() {
    // ---Start of code for Permissions Implementation--- //
    try {
      this.decodedToken = new JwtHelper().decodeToken(localStorage.getItem('rights'));
      this.decodeUserToken = new JwtHelper().decodeToken(localStorage.getItem('token'));
    } catch (error) {
      this.decodedToken = {};
      this.decodeUserToken = {};
    }
    if (this.decodedToken.data && this.decodedToken.data.permissions) {
      this.decodedToken = JSON.parse(this.decodedToken.data.permissions);
    } else {
      this.decodedToken = {};
    }
    // ---End of code for Permissions Implementation--- //
    const currentDate = new Date();
    this.lastyear = (currentDate.getFullYear() - 1);
    for (let i = 0; i < 6; i++) {
      this.years.push(currentDate.getFullYear() - i);
    }
    this.reportAnalysis.year = this.years[0];
    this.reportAnalysis.lastyear = this.lastyear;
    this.reportAnalysis.month = ('00' + (currentDate.getMonth() + 1)).slice(-2);
    this.seleMonth = this.months.filter(function (obj) { return obj.Id === ('00' + (currentDate.getMonth() + 1)).slice(-2); })[0]['name'];
    this.getServiceGroupList();
    this.getProductLines();
    this.getWorkerList();
  }
  loadCompanyInfo(data) {
    this.CompanyName = data.Name;
  }
  hideData() {
    this.itemsDisplay = false;
    this.service = this.reportAnalysis.serviceGroup
  }
  getWorkerList() {
    this.monthlyBussinessAnalysisService.getUserList().subscribe(data => {
      this.workerList = data['result'];
    },
      error => {
        const errStatus = JSON.parse(error['_body'])['status'];
        if (errStatus === '2085' || errStatus === '2071') {
          if (this.router.url !== '/') {
            localStorage.setItem('page', this.router.url);
            this.router.navigate(['/']).then(() => { });
          }
        }
      });
  }

  getServiceGroupList() {
    this.monthlyBussinessAnalysisService.getSetupServiceGroupsList().subscribe(data => {
      this.serviceGroupList = data['result'].filter((servcieGroup) => !servcieGroup.isSystem);
      if (this.serviceGroupList.length > 0) {
        this.reportAnalysis.serviceGroup = this.serviceGroupList[0].serviceGroupName;
      }

    },
      error => {
        const errStatus = JSON.parse(error['_body'])['status'];
        if (errStatus === '2085' || errStatus === '2071') {
          if (this.router.url !== '/') {
            localStorage.setItem('page', this.router.url);
            this.router.navigate(['/']).then(() => { });
          }
        }
      });
  }

  selectReportsType(value) {
    if (value === 'Comapny Analysis') {
      this.reportAnalysis.worker = '';
      this.selectWorkerName = '';
      // this.workerName = '';
    }
  }

  clear() {

  }
  getProductLines() {
    this.monthlyBussinessAnalysisService.getProductLines().subscribe(
      data => {
        // this.inventoryGroupsData = data['result'];
        this.productLinesList = data['result'];
        if (this.productLinesList.length > 0) {
          this.reportAnalysis.pdLine = this.productLinesList[0].Id;
        }
      },
      error => {
        const errStatus = JSON.parse(error['_body'])['status'];
        if (errStatus === '2085' || errStatus === '2071') {
          if (this.router.url !== '/') {
            localStorage.setItem('page', this.router.url);
            this.router.navigate(['/']).then(() => { });
          }
        }
      });
  }
  generateReport() {
    if (this.reportAnalysis.type === 'Worker Analysis' && !this.reportAnalysis.worker) {
      this.toastr.warning('Worker is required.', null, { timeOut: 3000 });
    } else {
      const reportData = {
        startYear: this.reportAnalysis.year,
        startmonth: this.reportAnalysis.month,
        startDate: this.reportAnalysis.year + this.reportAnalysis.month,
        worker: this.reportAnalysis.worker,
        serviceGroup: this.reportAnalysis.serviceGroup,
        pdLine: this.reportAnalysis.pdLine,
        lastyear: this.reportAnalysis.lastyear + this.reportAnalysis.month
      };
      const FirstDay = new Date(this.reportAnalysis.year, this.reportAnalysis.month - 1, 1);
      const LastDay = new Date(this.reportAnalysis.year, this.reportAnalysis.month, 0);
      const lastFirstDay = new Date(this.reportAnalysis.year - 1, this.reportAnalysis.month - 1, 1);
      const lastLastDay = new Date(this.reportAnalysis.year - 1, this.reportAnalysis.month, 0);
      const startTime = FirstDay.getFullYear() + '-' + ('00' + (FirstDay.getMonth() + 1)).slice(-2) + '-' + ('00' + FirstDay.getDate()).slice(-2);
      const endTime = LastDay.getFullYear() + '-' + ('00' + (LastDay.getMonth() + 1)).slice(-2) + '-' + ('00' + LastDay.getDate()).slice(-2);
      const laststartTime = lastFirstDay.getFullYear() + '-' + ('00' + (lastFirstDay.getMonth() + 1)).slice(-2) + '-' + ('00' + lastFirstDay.getDate()).slice(-2);
      const lastendTime = lastLastDay.getFullYear() + '-' + ('00' + (lastLastDay.getMonth() + 1)).slice(-2) + '-' + ('00' + lastLastDay.getDate()).slice(-2);
      this.monthlyBussinessAnalysisService.getBussinessAnalysisReport(reportData).subscribe(
        data => {
          this.monthlyBussinessAnalysisReport = data['result'];
          this.monthlyBussinessAnalysisService.getTbpReport(startTime, endTime).subscribe(
            data => {
              this.tbpList = data['result'][0];
              this.populateArrays(this.monthlyBussinessAnalysisReport);

            },
            error => {
              const status = JSON.parse(error['status']);
              const statuscode = JSON.parse(error['_body']).status;
              switch (status) {
                case 500:
                  break;
                case 400:
                  break;
              }
              if (statuscode === '2085' || statuscode === '2071') {
                if (this.router.url !== '/') {
                  localStorage.setItem('page', this.router.url);
                  this.router.navigate(['/']).then(() => { });
                }
              }
            }
          );
        },
        error => {
          const errStatus = JSON.parse(error['_body'])['status'];
          if (errStatus === '2085' || errStatus === '2071') {
            if (this.router.url !== '/') {
              localStorage.setItem('page', this.router.url);
              this.router.navigate(['/']).then(() => { });
            }
          }
        });

      this.monthlyBussinessAnalysisService.getTbpReport(laststartTime, lastendTime).subscribe(
        data => {
          this.tbpList1 = data['result'][0];
        },
        error => {
          const status = JSON.parse(error['status']);
          const statuscode = JSON.parse(error['_body']).status;
          switch (status) {
            case 500:
              break;
            case 400:
              break;
          }
          if (statuscode === '2085' || statuscode === '2071') {
            if (this.router.url !== '/') {
              localStorage.setItem('page', this.router.url);
              this.router.navigate(['/']).then(() => { });
            }
          }
        });
      const reportObj = {
        startDate: startTime,
        endDate: endTime,
        worker: this.reportAnalysis.worker
      };
      this.monthlyBussinessAnalysisService.getTicketAnalysisReport(reportObj).subscribe(data => {
        this.reports.clear();
        const reportsResult: any = data['result'];
        reportsResult['serviceTxList'].forEach(service => {
          let serviceObj: any = {
            serviceSales: 0,
            productSales: 0
          };
          if (this.reports.has(serviceObj.Appt_Ticket__c)) {
            serviceObj = this.reports.get(serviceObj.Appt_Ticket__c);
          }
          serviceObj.serviceSales = service.Service_Sales__c;
          if (this.reportAnalysis.worker && service.Guest_Charge__c) {
            serviceObj.serviceSales -= service.Guest_Charge__c;
          }
          this.reports.set(service.Appt_Ticket__c, serviceObj);
        });
        reportsResult['productTxList'].forEach(product => {
          let productObj: any = {};
          if (this.reports.has(product.Appt_Ticket__c)) {
            productObj = this.reports.get(product.Appt_Ticket__c);
          } else {
            productObj = {
              serviceSales: 0,
              productSales: 0
            };
          }
          productObj.productSales = product.Product_Sales__c;
          this.reports.set(product.Appt_Ticket__c, productObj);
        });

        this.totalNumberOfTickets = 0;
        this.reportDetails = {};
        this.reportDetails.serviceOnlyCount = 0;
        this.reportDetails.productOnlyCount = 0;
        this.reportDetails.bothCount = 0;
        this.reports.forEach((report) => {
          if (report.serviceSales > 0 && (report.productSales == null || report.productSales === 0)) {
            this.reportDetails.serviceOnlyCount++;
          }
          if (report.productSales > 0 && (report.serviceSales == null || report.serviceSales === 0)) {
            this.reportDetails.productOnlyCount++;
          }
          if (report.serviceSales > 0 && report.productSales > 0) {
            this.reportDetails.bothCount++;
          }
        });
        if (this.reportDetails) {
          this.totalNumberOfTickets = this.reportDetails.serviceOnlyCount + this.reportDetails.productOnlyCount + this.reportDetails.bothCount;
        }

        const reportObj1 = {
          startDate: laststartTime,
          endDate: lastendTime,
          worker: this.reportAnalysis.worker
        };
        this.monthlyBussinessAnalysisService.getTicketAnalysisReport(reportObj1).subscribe(data => {
          this.reports1.clear();
          const reportsResult1: any = data['result'];
          reportsResult1['serviceTxList'].forEach(service => {
            let serviceObj1: any = {
              serviceSales: 0,
              productSales: 0
            };
            if (this.reports1.has(serviceObj1.Appt_Ticket__c)) {
              serviceObj1 = this.reports1.get(serviceObj1.Appt_Ticket__c);
            }
            serviceObj1.serviceSales = service.Service_Sales__c;
            if (this.reportAnalysis.worker && service.Guest_Charge__c) {
              serviceObj1.serviceSales -= service.Guest_Charge__c;
            }
            this.reports1.set(service.Appt_Ticket__c, serviceObj1);
          });
          reportsResult1['productTxList'].forEach(product => {
            let productObj1: any = {};
            if (this.reports1.has(product.Appt_Ticket__c)) {
              productObj1 = this.reports1.get(product.Appt_Ticket__c);
            } else {
              productObj1 = {
                serviceSales: 0,
                productSales: 0
              };
            }
            productObj1.productSales = product.Product_Sales__c;
            this.reports1.set(product.Appt_Ticket__c, productObj1);
          });

          this.totalNumberOfTickets1 = 0;
          this.reportDetails1 = {};
          this.reportDetails1.serviceOnlyCount = 0;
          this.reportDetails1.productOnlyCount = 0;
          this.reportDetails1.bothCount = 0;
          this.reports1.forEach((report) => {
            if (report.serviceSales > 0 && (report.productSales == null || report.productSales === 0)) {
              this.reportDetails1.serviceOnlyCount++;
            }
            if (report.productSales > 0 && (report.serviceSales == null || report.serviceSales === 0)) {
              this.reportDetails1.productOnlyCount++;
            }
            if (report.serviceSales > 0 && report.productSales > 0) {
              this.reportDetails1.bothCount++;
            }
          });
          if (this.reportDetails1) {
            this.totalNumberOfTickets1 = this.reportDetails1.serviceOnlyCount + this.reportDetails1.productOnlyCount + this.reportDetails1.bothCount;
          }
          if (this.totalNumberOfTickets || this.totalNumberOfTickets1) {
            if (this.totalNumberOfTickets1 === 0) {
              this.totalticketCountPer = 0;
            } else {
              this.totalticketCountPer = ((this.totalNumberOfTickets - this.totalNumberOfTickets1) / this.totalNumberOfTickets1) * 100;
            }
          }
        },
          error => {
            const errStatus = JSON.parse(error['_body'])['status'];
            if (errStatus === '2085' || errStatus === '2071') {
              if (this.router.url !== '/') {
                localStorage.setItem('page', this.router.url);
                this.router.navigate(['/']).then(() => { });
              }
            }
          });
      },
        error => {
          const errStatus = JSON.parse(error['_body'])['status'];
          if (errStatus === '2085' || errStatus === '2071') {
            if (this.router.url !== '/') {
              localStorage.setItem('page', this.router.url);
              this.router.navigate(['/']).then(() => { });
            }
          }
        });

    }
  }
  getDetails(val, detail) {
    if (val && detail === 'workername') {
      this.selectWorkerName = this.workerList.filter(function (obj) { return val === obj.Id; })[0]['FullName'];
    } else if (val && detail === 'month') {
      this.seleMonth = this.months.filter(function (obj) { return obj.Id === val; })[0]['name'];
    }
  }
  populateArrays(data) {
    this.SeleSerGroName = this.reportAnalysis.serviceGroup;
    this.productLinesList.forEach(element => {
      if (element.Id === this.reportAnalysis.pdLine) {
        this.SeleSerPldLineName = element.Name;
      }
    });

    this.totalProSales = 0;
    this.totalSerSales = 0;
    this.totalSales = 0;
    this.totalServiceGroup = 0;
    this.totalInventoryGroup = 0;
    this.reebookPec = 0.00;
    this.srvcCountatlestonePrdct = 0.00;

    this.totalProSales1 = 0;
    this.totalSerSales1 = 0;
    this.totalSales1 = 0;
    this.totalServiceGroup1 = 0;
    this.totalInventoryGroup1 = 0;
    this.reebookPec1 = 0.00;
    this.srvcCountatlestonePrdct1 = 0.00;
    this.totalSerSalesPer = 0.00;
    this.totalProSalesPer = 0.00;
    this.totalSalesPer = 0.00;
    this.totalticketCount = 0;
    this.totalticketCount1 = 0;
    if (data) {
      // SALES: SERVICE SALES
      if (data['srvcTickets'].length > 0) {
        data['srvcTickets'].forEach(element => {
          this.totalSerSales += Math.round((Number(element.serviceTotal)));
        });
        this.averageSerivceAmount = this.totalSerSales / data['srvcTickets'].length;
      }

      // LAST SALES: SERVICE SALES
      if (data['srvcTickets1'].length > 0) {
        data['srvcTickets1'].forEach(element => {
          this.totalSerSales1 += Math.round((Number(element.serviceTotal)));
          this.averageSerivceAmount1 = this.totalSerSales1 / data['srvcTickets1'].length;
        });
      }
      if (this.totalSerSales || this.totalSerSales1) {
        if (this.totalSerSales1 === 0) {
          this.totalSerSalesPer = 0;
        } else {
          this.totalSerSalesPer = ((this.totalSerSales - this.totalSerSales1) / this.totalSerSales1) * 100;
        }
      }
      if (this.averageSerivceAmount || this.averageSerivceAmount1) {
        if (this.averageSerivceAmount1 === 0) {
          this.averageSerivceAmountPer = 0;
        } else {
          this.averageSerivceAmountPer = ((this.averageSerivceAmount - this.averageSerivceAmount1) / this.averageSerivceAmount1) * 100;
        }
      }
      // SALES: PRODUCT SALES
      if (data['prdctTickets'].length > 0) {
        data['prdctTickets'].forEach(element => {
          this.totalProSales += Math.round((Number(element.productTotal)));
        });
      }


      // LAST SALES: PRODUCT SALES
      if (data['prdctTickets1'].length > 0) {
        data['prdctTickets1'].forEach(element => {
          this.totalProSales1 += (Number(element.productTotal));
        });
      }
      if (this.totalProSales || this.totalProSales1) {
        if (this.totalProSales1 === 0) {
          this.totalProSalesPer = 0;
        } else {
          this.totalProSalesPer = ((this.totalProSales - this.totalProSales1) / this.totalProSales1) * 100;
        }
      }
      // SALES: TOTAL SALES
      if (this.totalSerSales || this.totalProSales) {
        this.totalSales = this.totalSerSales + this.totalProSales;
      }

      // LAST SALES: TOTAL SALES
      if (this.totalSerSales1 || this.totalProSales1) {
        this.totalSales1 = this.totalSerSales1 + this.totalProSales1;
      }
      if (this.totalSales || this.totalSales1) {
        if (this.totalSales1 === 0) {
          this.totalSalesPer = 0;
        } else {
          this.totalSalesPer = ((this.totalSales - this.totalSales1) / this.totalSales1) * 100;
        }
      }
      // SALES: Service Group
      let sum = 0;
      if (data['srvcServiceGroup'].length > 0) {
        let scount = 0;
        data['srvcServiceGroup'].forEach(element => {
          sum += element.scount;

          if (element.Service_Group__c === this.SeleSerGroName) {
            scount = element.scount;
          }
        });
        this.totalServiceGroup += (scount / sum) * 100;
      }

      // LAST SALES: Service Group
      let sum1 = 0
      if (data['srvcServiceGroup1'].length > 0) {
        let scount1 = 0;
        data['srvcServiceGroup1'].forEach(element => {
          sum1 += element.scount;

          if (element.Service_Group__c === this.SeleSerGroName) {
            scount1 = element.scount;
          }
        });
        this.totalServiceGroup1 += (scount1 / sum1) * 100;
      }
      if (this.totalServiceGroup || this.totalServiceGroup1) {
        if (this.totalServiceGroup1 === 0) {
          this.totalServiceGroupPer = 0;
        } else {
          this.totalServiceGroupPer = ((this.totalServiceGroup - this.totalServiceGroup1) / this.totalServiceGroup1) * 100;
        }
      }
      // SALES: Inventory Group
      if (data['productLine'].length > 0) {
        data['productLine'].forEach(element => {
          this.totalInventoryGroup += Math.round((Number(element.productTotal)));
        });
      }

      // LAST SALES: Inventory Group
      if (data['productLine1'].length > 0) {
        data['productLine1'].forEach(element => {
          this.totalInventoryGroup1 += Math.round((Number(element.productTotal)));
        });
      }
      if (this.totalInventoryGroup || this.totalInventoryGroup1) {
        if (this.totalInventoryGroup1 === 0) {
          this.totalInventoryGroupPer = 0;
        } else {
          this.totalInventoryGroupPer = ((this.totalInventoryGroup - this.totalInventoryGroup1) / this.totalInventoryGroup1) * 100;
        }
      }
      if (data['rebooked'].length > 0) {
        this.reebookPec += (data['rebooked'].length / data['srvcTickets'].length) * 100;
      }

      if (data['rebooked1'].length > 0) {
        this.reebookPec1 += (data['rebooked1'].length / data['srvcTickets1'].length) * 100;
      }
      if (this.reebookPec || this.reebookPec1) {
        if (this.reebookPec1 === 0) {
          this.reebookPecPer = 0;
        } else {
          this.reebookPecPer = ((this.reebookPec - this.reebookPec1) / this.reebookPec1) * 100;
        }
      }
      if (data['srvcCountatlestonePrdct'] > 0) {
        this.srvcCountatlestonePrdct += (data['srvcCountatlestonePrdct'] / data['srvcTickets'].length) * 100;
      }
      if (data['lastsrvcCountatlestonePrdct'] > 0) {
        this.srvcCountatlestonePrdct1 += (data['lastsrvcCountatlestonePrdct'] / data['srvcTickets1'].length) * 100;
      }
      if (this.srvcCountatlestonePrdct || this.srvcCountatlestonePrdct1) {
        if (this.srvcCountatlestonePrdct1 === 0) {
          this.srvcCountatlestonePrdctPer = 0;
        } else {
          this.srvcCountatlestonePrdctPer = ((this.srvcCountatlestonePrdct - this.srvcCountatlestonePrdct1) / this.srvcCountatlestonePrdct1) * 100;
        }
      }
      if (this.tbpList.totalProductivityVal || (this.tbpList1 && this.tbpList1.totalProductivityVal)) {
        if (this.tbpList1.totalProductivityVal === 0) {
          this.totalProductivityValPer = 0;
        } else {
          this.totalProductivityValPer = ((this.tbpList.totalProductivityVal - this.tbpList1.totalProductivityVal) / this.tbpList1.totalProductivityVal) * 100;
        }
      }
      if (data['apptCount']['apptCount']) {
        this.totalticketCount += data['apptCount']['apptCount'];
      }
      if (data['apptCount1']['apptCount']) {
        this.totalticketCount1 += data['apptCount1']['apptCount'];
      }

    }
    this.itemsDisplay = true;
  }
  printDiv() {
    let printContents, popupWin;
    printContents = document.getElementById('dvContents').innerHTML;
    printContents = printContents.replace('value="' + this.reportAnalysis.year + '"', 'selected value="' + this.reportAnalysis.year + '"');
    printContents = printContents.replace('value="' + this.reportAnalysis.month + '"', 'selected value="' + this.reportAnalysis.month + '"');
    printContents = printContents.replace('value="' + this.reportAnalysis.type + '"', 'selected value="' + this.reportAnalysis.type + '"');
    printContents = printContents.replace('value="' + this.reportAnalysis.worker + '"', 'selected value="' + this.reportAnalysis.worker + '"');
    printContents = printContents.replace('value="' + this.reportAnalysis.serviceGroup + '"', 'selected value="' + this.reportAnalysis.serviceGroup + '"');
    printContents = printContents.replace('value="' + this.reportAnalysis.pdLine + '"', 'selected value="' + this.reportAnalysis.pdLine + '"');
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <title>Monthly Business Analysis </title>
          <style>
            table {
              border-collapse: collapse;
            }
            table, th, td {
                border: 0.5px solid black;
            }
            .pri td {
              padding:6px;
            }
            .arc {
              float:left;
              margin:12px;
            }
            .arc button {
              margin-top:10px;
            }
            .total {
              margin:6px;
            }
          </style>
        </head>
        <body onload="window.print();window.close()">${printContents}</body>
      </html>`);
    popupWin.document.close();
  }
}
