import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReferralReportComponent } from './referralreport.component';
import { ReferralReportRoutingModule } from './referralreport.routing';
import { FormsModule } from '@angular/forms';
import { ShareModule } from '../../../common/share.module';
import { TranslateModule } from '@ngx-translate/core';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
@NgModule({
    imports: [
        CommonModule,
        ReferralReportRoutingModule,
        FormsModule,
        TranslateModule,
        ShareModule,
        BsDatepickerModule.forRoot(),
    ],
    declarations: [
        ReferralReportComponent
    ]
})
export class ReferralReportModule {
}
