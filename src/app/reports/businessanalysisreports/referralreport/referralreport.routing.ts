import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ReferralReportComponent } from './referralreport.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '',
                component: ReferralReportComponent,
                children: [
                    {
                        path: '',
                        component: ReferralReportComponent
                    }
                ]
            }
        ])
    ],
    exports: [
        RouterModule
    ]
})
export class ReferralReportRoutingModule {
}
