import { Injectable, Inject } from '@angular/core';
import { Response } from '@angular/http';
import { Headers } from '@angular/http';
import { HttpClients } from '../../../common/http-client';
import { map } from 'rxjs/operators';


@Injectable()
export class ReferralReportService {
  constructor(
    private http: HttpClients,
    @Inject('apiEndPoint') private apiEndPoint: string,
    @Inject('staticJsonFilesEndPoint') private staticJsonFilesEndPoint: string
  ) { }

  getStaticRewardValue() {
    return this.http.get(this.staticJsonFilesEndPoint + 'common.json')
    .pipe(map(this.extractData));
  }
  getReferralReport(obj) {
    return this.http.post(this.apiEndPoint + '/api/reports/referralreport', obj)
    .pipe(map(this.extractData));
  }

  /*To extract json data*/
  private extractData(res: Response) {
    if (res.headers && res.headers.get('token')) {
      localStorage.setItem('token', res.headers.get('token'));
    }
    const body = res.json();
    return body || {};
  }

}
