import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { SafeUrl, DomSanitizer } from '@angular/platform-browser';
import { ReportActivityComparisonService } from './reportactivitycomparison.service';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { JwtHelper } from 'angular2-jwt';
import { SetupCompanyInfoService } from '../../../setup/setupcompany/setupcompanyinfo/setupcompanyinfo.service';
import { SetupWorkersDetailsService } from '../../../setup/setupworkers/setupworkersdetails/setupworkersdetails.service';
@Component({
  selector: 'app-reports-app',
  templateUrl: './reportactivitycomparison.html',
  styleUrls: ['./reportactivitycomparison.scss'],
  providers: [ReportActivityComparisonService, SetupCompanyInfoService, SetupWorkersDetailsService],
})
export class ReportActivityComparisonComponent implements OnInit {
  bsValue = new Date();
  minDate = new Date();
  itemsDisplay = false;
  workerTipsData: any;
  selectDate = new Date();
  datePickerConfig: any;
  reportType = 'company';
  workerDropList = '';
  WorkerList = [];
  decodedToken: any;
  decodeUserToken: any;
  generateReportResult = [];
  CompanyName: any;
  actComName = '';
  numberOf = [];
  toastermessage: any;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private toastr: ToastrService,
    private translateService: TranslateService,
    private setupworkersDetailsservice: SetupWorkersDetailsService,
    private setupCompanyService: SetupCompanyInfoService,
    private reportActivityComparisonService: ReportActivityComparisonService) {

    this.datePickerConfig = Object.assign({},
      {
        showWeekNumbers: false,
        containerClass: 'theme-blue',
      });
  }
  ngOnInit() {
    // ---Start of code for Permissions Implementation--- //
    try {
      this.decodedToken = new JwtHelper().decodeToken(localStorage.getItem('rights'));
      this.decodeUserToken = new JwtHelper().decodeToken(localStorage.getItem('token'));
    } catch (error) {
      this.decodedToken = {};
      this.decodeUserToken = {};
    }
    if (this.decodedToken.data && this.decodedToken.data.permissions) {
      this.decodedToken = JSON.parse(this.decodedToken.data.permissions);
    } else {
      this.decodedToken = {};
    }
    // ---End of code for Permissions Implementation--- //
    this.getAllWorkers();
    this.getCompanyInfo();
  }
  generateReport() {
    const date = new Date();
    if (this.reportType === 'company') {
      this.workerDropList = '';
      this.actComName = 'Company';
    } else {
      if (this.workerDropList) {
        this.actComName = this.WorkerList.filter((obj) => {
          return this.workerDropList === obj.Id;
        })[0]['FullName'];
      }
    }
    if (!this.selectDate) {
      this.toastermessage = this.translateService.get('TICKET_ANALYSIS_REPORT.ENTER_A_VALID_SERCH_DATE');
      this.toastr.error(this.toastermessage.value, null, { timeOut: 3000 });
    } else if ((this.selectDate > date)) {
      this.toastermessage = this.translateService.get('TICKET_ANALYSIS_REPORT.FUTURE_DATE_IS_NOT_VALID');
      this.toastr.error(this.toastermessage.value, null, { timeOut: 3000 });
    } else if (this.reportType === 'worker' && !this.workerDropList) {
      this.toastermessage = this.translateService.get('TICKET_ANALYSIS_REPORT.WORKER_IS_REQUIRED');
      this.toastr.error(this.toastermessage.value, null, { timeOut: 3000 });
    } else {
      this.itemsDisplay = true;
      const sendParam = {
        'date': this.selectDate.getFullYear() + '-' + (this.selectDate.getMonth() + 1) + '-' + this.selectDate.getDate(),
        'workerid': this.workerDropList
      };
      this.reportActivityComparisonService.getGenerateList(sendParam).subscribe(
        data => {
          this.generateReportResult = data['result'];
          const numberActivityList = data['result']['numberActivityList'];
          this.numberOfRes(numberActivityList);
          const clientdata = this.generateReportResult['clientData'].filter(function (obj) { return obj.category === 'Rebook_percent'; });
          if (clientdata.length === 0) {
            this.generateReportResult['clientData'].push({
              category: 'Rebook_percent',
              todayDate: 0,
              weekToDate: 0,
              monthToDate: 0,
              monthToDateLastYear: 0,
              last30Days: 0,
              last90Days: 0,
              monthPercentChange: 0,
              priorCalendarYear: 0,
              priorTwelveMonths: 0,
              quarterPercentChange: 0,
              quarterToDate: 0,
              quarterToDateLastYear: 0,
              yearPercentChange: 0,
              yearToDate: 0,
              yearToDateLastYear: 0,
            });
          }
        },
        error => {
          const errStatus = JSON.parse(error['_body'])['status'];
          if (errStatus === '2085' || errStatus === '2071') {
            if (this.router.url !== '/') {
              localStorage.setItem('page', this.router.url);
              this.router.navigate(['/']).then(() => { });
            }
          }
        });
    }


  }

  getAllWorkers() {
    this.setupworkersDetailsservice.getUserList()
      .subscribe(data => {
        this.WorkerList = data['result'];
      },
        error => {
          const errStatus = JSON.parse(error['_body'])['status'];
          if (errStatus === '2085' || errStatus === '2071') {
            if (this.router.url !== '/') {
              localStorage.setItem('page', this.router.url);
              this.router.navigate(['/']).then(() => { });
            }
          }
        });
  }
  getCompanyInfo() {
    this.setupCompanyService.getCompanyInfo().subscribe(data => {
      this.CompanyName = data['result']['cmpresult'][0]['Name'];
    },
      error => {
        const errStatus = JSON.parse(error['_body'])['status'];
        if (errStatus === '2085' || errStatus === '2071') {
          if (this.router.url !== '/') {
            localStorage.setItem('page', this.router.url);
            this.router.navigate(['/']).then(() => { });
          }
        }
      });
  }

  numberOfRes(data) {
    this.numberOf = [];
    this.numberOf[0] = data[0];
    this.numberOf[1] = data[1];
    this.numberOf[2] = data[2];
    if (!this.workerDropList) {
      this.numberOf[3] = data[4];
      this.numberOf[4] = data[5];
    }
  }

}
