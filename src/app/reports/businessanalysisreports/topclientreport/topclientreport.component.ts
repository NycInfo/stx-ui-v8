import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { TopClientService } from './topclientreport.service';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { JwtHelper } from 'angular2-jwt';
import { CommonService } from '../../../common/common.service';
@Component({
  selector: 'app-reports-app',
  templateUrl: './topclientreport.html',
  styleUrls: ['./topclientreport.scss'],
  providers: [TopClientService, CommonService],
})

export class TopClientComponent implements OnInit {
  startDate: any = new Date('1/1/' + (new Date()).getFullYear());
  endDate: any = new Date();
  minDate: any = new Date();
  companyName: any;
  datePickerConfig: any;
  selectType = '';
  toastermessage: any;
  topClientData = [];
  itemsDisplay = false;
  staticData = [];
  highestServiceSales: any = {};
  decodeUserToken: any;
  decodedToken: any;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private toastr: ToastrService,
    private commonService: CommonService,
    private topClientService: TopClientService,
    private translateService: TranslateService) {
    this.datePickerConfig = Object.assign({},
      {
        showWeekNumbers: false,
        containerClass: 'theme-blue',
      });
  }
  ngOnInit() {
    // ---Start of code for Permissions Implementation--- //
    try {
      this.decodedToken = new JwtHelper().decodeToken(localStorage.getItem('rights'));
      this.decodeUserToken = new JwtHelper().decodeToken(localStorage.getItem('token'));
    } catch (error) {
      this.decodedToken = {};
      this.decodeUserToken = {};
    }
    if (this.decodedToken.data && this.decodedToken.data.permissions) {
      this.decodedToken = JSON.parse(this.decodedToken.data.permissions);
    } else {
      this.decodedToken = {};
    }
    // ---End of code for Permissions Implementation--- //
    this.getStaticAmounts();
  }
  loadCompanyInfo(data) {
    this.companyName = data.Name;
  }
  // Method to get static amount Types //
  getStaticAmounts() {
    this.topClientService.getAmountType().subscribe(
      data => {
        this.staticData = data['reportAmountTypes'];
        this.selectType = this.staticData[0].value;
        if (this.selectType) {
          this.generateTopClientReport();
        }
      },
      error => {
        const errStatus = JSON.parse(error['_body'])['status'];
        if (errStatus === '2085' || errStatus === '2071') {
          if (this.router.url !== '/') {
            localStorage.setItem('page', this.router.url);
            this.router.navigate(['/']).then(() => { });
          }
        }
      });
  }
  selectTypeOnChange(value) {
    this.selectType = value;
  }
  generateTopClientReport() {
    this.topClientData = [];
    const date = new Date();
    if (this.startDate === null) {
      this.toastermessage = this.translateService.get('TICKET_ANALYSIS_REPORT.PLEASE_SELECT_BEGIN_DATE');
      this.toastr.error(this.toastermessage.value, null, { timeOut: 3000 });
    } else if (isNaN(this.startDate.getTime())) {
      this.toastermessage = this.translateService.get('TICKET_ANALYSIS_REPORT.BEGIN_DATE_IS_INVALID');
      this.toastr.error(this.toastermessage.value, null, { timeOut: 3000 });
    } else if (this.endDate === null) {
      this.toastermessage = this.translateService.get('TICKET_ANALYSIS_REPORT.PLEASE_SELECT_END_DATE');
      this.toastr.error(this.toastermessage.value, null, { timeOut: 3000 });
    } else if (isNaN(this.endDate.getTime())) {
      this.toastermessage = this.translateService.get('TICKET_ANALYSIS_REPORT.END_DATAE_IS_INVALID');
      this.toastr.error(this.toastermessage.value, null, { timeOut: 3000 });
    } else {
      const startTime = (this.startDate.getFullYear() + '') + '-' + ('00' + (this.startDate.getMonth() + 1)).slice(-2) + '-' + ('00' + this.startDate.getDate()).slice(-2);
      const endTime = (this.endDate.getFullYear() + '') + '-' + ('00' + (this.endDate.getMonth() + 1)).slice(-2) + '-' + ('00' + this.endDate.getDate()).slice(-2);
      const stDate1 = (date.getFullYear() + '') + '-' + ('00' + (date.getMonth() + 1)).slice(-2) + '-' + ('00' + date.getDate()).slice(-2);
      const edDate1 = (date.getFullYear() + '') + '-' + ('00' + (date.getMonth() + 1)).slice(-2) + '-' + ('00' + date.getDate()).slice(-2);
      if (startTime > endTime) {
        this.toastermessage = this.translateService.get('TICKET_ANALYSIS_REPORT.BEGIN_DATE_MUST_BE_BEFORE_THE_END_DATE');
        this.toastr.error(this.toastermessage.value, null, { timeOut: 3000 });
      } else if ((startTime > stDate1) || (endTime > edDate1)) {
        this.toastermessage = this.translateService.get('TICKET_ANALYSIS_REPORT.FUTURE_DATES_NOT_ALLOWED');
        this.toastr.error(this.toastermessage.value, null, { timeOut: 3000 });
      } else {
        this.topClientService.getTopClientReport(startTime, endTime, this.selectType).subscribe(
          data => {
            if (data['result'].length === 0) {
              this.itemsDisplay = false;
              this.toastermessage = this.translateService.get('TICKET_ANALYSIS_REPORT.REPORT_NOT_FOUND');
              this.toastr.error(this.toastermessage.value, null, { timeOut: 1500 });
            } else {
              data['result'] = data['result'].filter(obj => obj.clientName !== null && obj.clientName !== '');
              data['result'].sort(function(a, b) {
                return b.Net_Price__c - a.Net_Price__c;
              });
              data['result'].forEach(function(obj, index) {
                obj.rank = index + 1;
              });
              data['result'].forEach(elem => {
                if (elem['lastVisitDate']) {
                  elem['lastVisit'] = this.commonService.getUsrDtStrFrmDBStr(elem['lastVisitDate']);
                }
              });
              this.topClientData = data['result'];
              this.itemsDisplay = true;
            }
          },
          error => {
            const status = JSON.parse(error['status']);
            const statuscode = JSON.parse(error['_body']).status;
            switch (status) {
              case 500:
                break;
              case 400:
                break;
            }
            if (statuscode === '2085' || statuscode === '2071') {
              if (this.router.url !== '/') {
                localStorage.setItem('page', this.router.url);
                this.router.navigate(['/']).then(() => { });
              }
            }
          }
        );
      }
    }

  }
  printDiv() {
    let printContents;
    let popupWin;
    printContents = document.getElementById('dvContents').innerHTML;
    printContents = printContents.replace('bsdatepicker=""', 'value="' + (this.startDate.getMonth() + 1) + '/'
      + this.startDate.getDate() + '/' + this.startDate.getFullYear() + '"');
    printContents = printContents.replace('bsdatepicker=""', 'value="' + (this.endDate.getMonth() + 1) + '/'
      + this.endDate.getDate() + '/' + this.endDate.getFullYear() + '"');
    printContents = printContents.replace('value="' + this.selectType + '"', 'selected value="' + this.selectType + '"');
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <title>Top Client Report</title>
          <style>
            table {
              border-collapse: collapse;
            }
            table, th, td {
                border: 0.5px solid black;
            }
            .pri td {
              padding:6px;
            }
            .arc {
              float:left;
              margin:12px;
            }
            .arc button {
              margin-top:10px;
            }
            .total {
              margin:6px;
            }
          </style>
        </head>
        <body onload="window.print();window.close()">${printContents}</body>
      </html>`);
    popupWin.document.close();
  }
}
