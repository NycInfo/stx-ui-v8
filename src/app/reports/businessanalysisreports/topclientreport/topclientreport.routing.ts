import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TopClientComponent } from './topclientreport.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '',
                component: TopClientComponent,
                children: [
                    {
                        path: '',
                        component: TopClientComponent
                    }
                ]
            }
        ])
    ],
    exports: [
        RouterModule
    ]
})
export class TopClientRoutingModule {
}
