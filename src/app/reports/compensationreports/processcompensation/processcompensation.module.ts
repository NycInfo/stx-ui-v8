import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProcessCompensationComponent } from './processcompensation.component';
import { ProcessCompensationRoutingModule } from './processcompensation.routing';
import { FormsModule } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';
import { ShareModule } from '../../../common/share.module';
import { TranslateModule } from '@ngx-translate/core';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        TranslateModule,
        ModalModule.forRoot(),
        ProcessCompensationRoutingModule,
        ShareModule,
        BsDatepickerModule.forRoot()
    ],
    declarations: [
        ProcessCompensationComponent
    ]
})
export class ProcessCompensationModule {
}
