import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { SafeUrl, DomSanitizer } from '@angular/platform-browser';
import { WorkerGoalService } from './workergoals.service';
import { SetupWorkersDetailsService } from '../../../setup/setupworkers/setupworkersdetails/setupworkersdetails.service';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { CommonService } from '../../../common/common.service';
import { months } from 'moment';
// import {Component} from "@angular/core";
@Component({
  selector: 'app-reports-app',
  templateUrl: './workergoals.html',
  styleUrls: ['./workergoals.scss'],
  providers: [WorkerGoalService, CommonService, SetupWorkersDetailsService]
})
export class WorkerGoalComponent implements OnInit {
  itemsDisplay = false;
  workerid = '';
  dataList: any;
  goalsDataList: any;
  yearsDataList: any = [];
  monthsData: any;
  startmonth: any;
  endmonth: any;
  startyears: any;
  endyears: any;
  workerGoal = '';
  workerGoalsData: any;
  workerGoalData = [];
  calculationList: any;
  calculatedGoal = [];
  percentOfGoal = [];
  toastermessage: any;
  startMonth: any;
  endMonth: any;
  name: any;
  Goalname: any = 'All';
  constructor(
    private workerGoalService: WorkerGoalService,
    private setupWorkersdetailsservice: SetupWorkersDetailsService,
    // private route: ActivatedRoute,
    private router: Router, private commonService: CommonService,
    private toastr: ToastrService,
    private translateService: TranslateService,
  ) {

  }
  ngOnInit() {
    this.getEvery();
    this.getyears();
    this.getMonths();
    this.getGoals(); // Goals to calculate list
  }

  generateReport() {
    this.itemsDisplay = false;
    const ValiSDate = this.startmonth + '-' + '01' + '-' + this.startyears;
    const ValiEDate = this.endmonth + '-' + '01' + '-' + this.endyears;
    if (new Date(ValiSDate) > new Date(ValiEDate)) {
      this.toastermessage = this.translateService.get('TICKET_ANALYSIS_REPORT.BEGIN_DATE_MUST_BE_BEFORE_THE_END_DATE');
      this.toastr.error(this.toastermessage.value, null, { timeOut: 3000 });
    } else if (!this.workerid) {
      this.toastermessage = this.translateService.get('TICKET_ANALYSIS_REPORT.WORKER_IS_REQUIRED');
      this.toastr.error(this.toastermessage.value, null, { timeOut: 3000 });
    } else {
      const sendRecored = {
        workerId: this.workerid,
        startDate: this.startyears + this.startmonth,
        endDate: this.endyears + this.endmonth,
        Goal__c: this.workerGoal
      };
      this.workerGoalService.generateReportSer(sendRecored).subscribe(data => {
        this.workerGoalData = data.result;
        if (this.workerGoalData.length === 0) {
          this.itemsDisplay = false;
          this.toastermessage = this.translateService.get('TICKET_ANALYSIS_REPORT.NO_DATA_FOUND');
          this.toastr.error(this.toastermessage.value, null, { timeOut: 3000 });
        } else {
          this.calculatedGoal = [];
          this.percentOfGoal = [];
          this.itemsDisplay = true;
        }
      }, error => {
        const errStatus = JSON.parse(error._body).status;
        if (errStatus === '2085' || errStatus === '2071') {
          if (this.router.url !== '/') {
            localStorage.setItem('page', this.router.url);
            this.router.navigate(['/']).then(() => { });
          }
        }
      });
    }

  }
  getEvery() {
    this.setupWorkersdetailsservice.getUserList().subscribe(
      data => {
        this.dataList = data.result;
      },
      error => {
        const errStatus = JSON.parse(error._body).status;
        if (errStatus === '2085' || errStatus === '2071') {
          if (this.router.url !== '/') {
            localStorage.setItem('page', this.router.url);
            this.router.navigate(['/']).then(() => { });
          }
        }
      });
  }
  getyears() {
    const year = new Date().getFullYear();
    for (let i = 0; i < 6; i++) {
      this.yearsDataList.push(year - i);
      this.startyears = this.yearsDataList[0];
      this.endyears = this.yearsDataList[0];
    }
  }
  getMonths() {
    this.workerGoalService.getMonthsTypes().subscribe(
      data => {
        this.monthsData = data.workerGoalMonths;
        this.startmonth = this.monthsData[0].value;
        this.endmonth = this.monthsData[0].value;
      },
      error => {
        const errStatus = JSON.parse(error._body).status;
        if (errStatus === '2085' || errStatus === '2071') {
          if (this.router.url !== '/') {
            localStorage.setItem('page', this.router.url);
            this.router.navigate(['/']).then(() => { });
          }
        }
      });
  }
  getGoals() {
    this.workerGoalService.getGoalsTypes().subscribe(
      data => {
        this.workerGoalsData = data.result.filter((obj) => obj.Active__c === 1);
      },
      error => {
        const errStatus = JSON.parse(error._body).status;
        if (errStatus === '2085' || errStatus === '2071') {
          if (this.router.url !== '/') {
            localStorage.setItem('page', this.router.url);
            this.router.navigate(['/']).then(() => { });
          }
        }
      });
  }
  showPercentage(goallist, index) {
    goallist.startDate2 = this.commonService.getDBDatStr(new Date(goallist.startDate));
    goallist.endDate2 = this.commonService.getDBDatStr(new Date(goallist.endDate));
    this.setupWorkersdetailsservice.updateGoal(goallist, this.workerid).subscribe(data => {
      this.calculationList = data.result;
      if (this.calculationList) {
        this.calculatedGoal[index] = this.calculationList.calculatedGoal;
        this.percentOfGoal[index] = this.calculationList.percentOfGoal;
        this.percentOfGoal[index] = Math.round(this.percentOfGoal[index] * 100) / 100 + '%';
      }
    }, error => {
      const errStatus = JSON.parse(error._body).status;
      if (errStatus === '2085' || errStatus === '2071') {
        if (this.router.url !== '/') {
          localStorage.setItem('page', this.router.url);
          this.router.navigate(['/']).then(() => { });
        }
      }
    });
  }
  workerOnChange(value) {
    const temp = this.dataList.filter((obj) => obj.Id === value)[0];
    this.name = temp.FirstName + ' ' + temp.LastName;
  }
  workerGoalOnChange(value) {
    const temp = this.workerGoalsData.filter((obj) => obj.Id === value)[0];
    if (value === '') {
      this.Goalname = 'All';
    } else {
      this.Goalname = temp.Name;
    }
  }
  printDiv() {
    for (let i = 0; i < this.monthsData.length; i++) {
      if (this.startmonth === this.monthsData[i].value) {
        this.startMonth = this.monthsData[i].option;
      }
    }
    for (let i = 0; i < this.monthsData.length; i++) {
      if (this.endmonth === this.monthsData[i].value) {
        this.endMonth = this.monthsData[i].option;
      }
    }
    const popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    let printContent = `
    <html>
      <head>
        <title>Worker Goals Report</title>
        <style>
          .reportTable {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
          }
          .reportTable td,
          .reportTable th {border: 1px solid rgb(141, 141, 141);padding: 8px;}
          .reportTable tr:nth-child(even) {background-color: #f2f2f2;}
          .reportTable th {
            padding-top: 6px;
            padding-bottom: 6px;
            text-align: left;
            background-color: #204d74;
            color: white;
          }
        </style>
      </head>
      <body onload="window.print();window.close()">
        <div>
          <table>
            <tr><td><b>start Month: </b></td><td>${this.startMonth}</td></tr>
            <tr><td><b>start year: </b></td><td>${this.startyears}</td></tr>
            <tr><td><b>End Month: </b></td><td>${this.endMonth}</td></tr>
            <tr><td><b>end year: </b></td><td>${this.endyears}</td></tr>
            <tr><td><b>worker goal: </b></td><td>${this.Goalname}</td></tr>
            <tr><td><b>worker id: </b></td><td>${this.name}</td></tr>
          </table>
          <br>
          <table class="reportTable">
            <tr>
              <th>Goal Name</th>
              <th>Start Date</th>
              <th>End Date</th>
              <th>Goal target</th>
              <th>Calculated Goal</th>
              <th>% of Goal</th>
            </tr>
            {{tableContent}}
          </table>
        </div>
      </body>
    </html>`;
    printContent = printContent.replace('{{tableContent}}', document.getElementById('tableContentId').innerHTML);
    popupWin.document.write(printContent);
    popupWin.document.close();
  }

}
