/*
  * Display single user details of front end users
  * extractData(): To extract the data
  * handleError(): To handle error messages.
*/
import { Injectable, Inject } from '@angular/core';
import { Response } from '@angular/http';
// import { Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { HttpClients } from '../../../common/http-client';
import { map } from 'rxjs/operators';

@Injectable()
export class WorkerGoalService {
  constructor(
    private http: HttpClients,
    @Inject('apiEndPoint') private apiEndPoint: string,
    @Inject('staticJsonFilesEndPoint') private staticJsonFilesEndPoint: string
  ) { }
  getYearsType() {
    return this.http.get(this.staticJsonFilesEndPoint + 'common.json')
      .pipe(map(this.extractData));
  }
  getMonthsTypes() {
    return this.http.get(this.staticJsonFilesEndPoint + 'common.json')
      .pipe(map(this.extractData));
  }
  getGoalsTypes() {
    return this.http.get(this.apiEndPoint + '/api/setupworkers/setupgoals')
      .pipe(map(this.extractData));
  }
  generateReportSer(senddata) {
    return this.http.post(this.apiEndPoint + '/api/reports/workergoalsreport', senddata)
      .pipe(map(this.extractData));
  }

  /*To extract json data*/
  private extractData(res: Response) {
    if (res.headers && res.headers.get('token')) {
      localStorage.setItem('token', res.headers.get('token'));
    }
    const body = res.json();
    return body || {};
  }
}
