import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { SafeUrl, DomSanitizer } from '@angular/platform-browser';
import { InventoryusageReportService } from './inventoryusagereport.service';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { JwtHelper } from 'angular2-jwt';
import { CommonService } from '../../../common/common.service';
@Component({
  selector: 'app-reports-app',
  templateUrl: './inventoryusagereport.html',
  styleUrls: ['./inventoryusagereport.scss'],
  providers: [InventoryusageReportService, CommonService],
})
export class InventoryusageReportComponent implements OnInit {
  bsValue = new Date();
  maxDate = new Date();
  bsValue1 = new Date();
  itemsDisplay = false;
  workerTipsData: any;
  datePickerConfig: any;
  usageList = [];
  inventryErr: any;
  grandTotal = 0;
  toastermessage: any;
  decodedToken: any;
  decodeUserToken: any;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private toastr: ToastrService,
    private commonService: CommonService,
    private translateService: TranslateService,
    private inventoryusageReportService: InventoryusageReportService) {
    this.datePickerConfig = Object.assign({},
      {
        showWeekNumbers: false,
        containerClass: 'theme-blue',
      });
  }
  ngOnInit() {
 // ---Start of code for Permissions Implementation--- //
     try {
      this.decodedToken = new JwtHelper().decodeToken(localStorage.getItem('rights'));
      this.decodeUserToken = new JwtHelper().decodeToken(localStorage.getItem('token'));
    } catch (error) {
      this.decodedToken = {};
      this.decodeUserToken = {};
    }
    if (this.decodedToken.data && this.decodedToken.data.permissions) {
      this.decodedToken = JSON.parse(this.decodedToken.data.permissions);
    } else {
      this.decodedToken = {};
    }
    // ---End of code for Permissions Implementation--- //
  }
  clearErr() {
    this.inventryErr = '';
  }
  generateReport() {
    this.grandTotal = 0;
    if (this.bsValue.setHours(0, 0, 0, 0) > this.bsValue1.setHours(0, 0, 0, 0)) {
      this.toastermessage = this.translateService.get('TICKET_ANALYSIS_REPORT.BEGIN_DATE_MUST_BE_BEFORE_THE_END_DATE');
      this.toastr.error(this.toastermessage.value, null, { timeOut: 3000 });
    } else {
      const stDt = this.commonService.getDBDatStr(this.bsValue);
      const endDt = this.commonService.getDBDatStr(this.bsValue1);
      this.inventoryusageReportService.getInvUsageRecords(stDt, endDt).subscribe(data => {
        this.usageList = data['result']['usageData'];
        if (this.usageList.length === 0) {
          this.itemsDisplay = false;
          this.toastermessage = this.translateService.get('TICKET_ANALYSIS_REPORT.NO_DATA_FOUND');
      this.toastr.error(this.toastermessage.value, null, { timeOut: 3000 });
        } else {
          const dates = data['result']['dates'];
          this.itemsDisplay = true;
          this.inventryErr = '';
          const dataList = [];
          for (let i = 0; i < dates.length; i++) {
            let price = 0;
            dataList.push(this.usageList.filter((obj) => obj.date === dates[i]));
            const tempArr = this.usageList.filter((obj) => obj.date === dates[i]);
            if (tempArr && tempArr.length > 0) {
              for (let j = 0; j < tempArr.length; j++) {
                price += parseFloat(tempArr[j]['totalCost']);
              }
            }
            dataList[i]['date'] = dates[i + 1];
            dataList[i]['total'] = price;
          }
          this.usageList = dataList.filter((obj) => obj.length);
          this.usageList.forEach((obj) => {
            this.grandTotal += obj['total'];
          });
        }
      },
        error => {
          const status = JSON.parse(error['status']);
          const statuscode = JSON.parse(error['_body']).status;
          switch (JSON.parse(error['_body']).status) {
            case '2033':
              break;
          }
          if (statuscode === '2085' || statuscode === '2071') {
            if (this.router.url !== '/') {
              localStorage.setItem('page', this.router.url);
              this.router.navigate(['/']).then(() => { });
            }
          }

        });
    }
  }
  printDiv() {
    let printContents, popupWin;
    printContents = document.getElementById('dvContents').innerHTML;
    printContents = printContents.replace('bsdatepicker=""', 'value="' + (this.bsValue.getMonth() + 1) + '/'
      + this.bsValue.getDate() + '/' + this.bsValue.getFullYear() + '"');
    printContents = printContents.replace('bsdatepicker=""', 'value="' + (this.bsValue1.getMonth() + 1) + '/'
      + this.bsValue1.getDate() + '/' + this.bsValue1.getFullYear() + '"');
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
  <html>
    <head>
      <title>Inventory usage Report</title>
      <style>
      //........Customized style.......
      table {
        border-collapse: collapse;
       }
      table, th, td {
          border: 0.5px solid black;
      }
      .pri td {
        padding:6px;
      }
      .arc {
        float:left;
        margin:12px;
      }
      .arc button {
        margin-top:10px;
      }
      .total {
        margin:6px;
      }
      </style>
    </head>
<body onload="window.print();window.close()">${printContents}</body>
  </html>`
    );
    popupWin.document.close();
  }
}
