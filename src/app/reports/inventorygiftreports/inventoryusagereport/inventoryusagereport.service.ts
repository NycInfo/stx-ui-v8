/*
  * Display single user details of front end users
  * extractData(): To extract the data
  * handleError(): To handle error messages.
*/
import { Injectable, Inject } from '@angular/core';
import { Response } from '@angular/http';
// import { Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { HttpClients } from '../../../common/http-client';
import { map } from 'rxjs/operators';

@Injectable()
export class InventoryusageReportService {
  constructor(
    private http: HttpClients,
    @Inject('apiEndPoint') private apiEndPoint: string,
    @Inject('staticJsonFilesEndPoint') private staticJsonFilesEndPoint: string
  ) { }
  getInvUsageRecords(stDt, endDt) {
    return this.http.get(this.apiEndPoint + '/api/reports/inventory/usage/' + stDt + '/' + endDt)
      .pipe(map(this.extractData));
  }
  /*To extract json data*/
  private extractData(res: Response) {
    if (res.headers && res.headers.get('token')) {
      localStorage.setItem('token', res.headers.get('token'));
    }
    const body = res.json();
    return body || {};
  }
}
