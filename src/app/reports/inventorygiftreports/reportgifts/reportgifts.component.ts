import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { SafeUrl, DomSanitizer } from '@angular/platform-browser';
import { ReportGiftsService } from './reportgifts.service';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { JwtHelper } from 'angular2-jwt';
import { CommonService } from '../../../common/common.service';
@Component({
  selector: 'app-reports-app',
  templateUrl: './reportgifts.html',
  styleUrls: ['./reportgifts.scss'],
  providers: [ReportGiftsService, CommonService],
})
export class ReportGiftsComponent implements OnInit {
  bsValue = new Date();
  bsValue1 = new Date();
  startDate = new Date();
  endDate = new Date();
  // minDate = new Date();
  itemsDisplay = false;
  workerTipsData: any;
  datePickerConfig: any;
  decodedToken: any;
  decodeUserToken: any;
  companyName: any;
  type: any;
  isGenerate = false;
  orginalAmt = 0;
  currentAmt = 0;
  ticketCount: any = 0;
  reportTypes = ['Issued', 'Expired', 'No Expires Date'];
  goalsObj: any;
  error: any;
  toastermessage: any;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private toastr: ToastrService,
    private commonService: CommonService,
    private translateService: TranslateService,
    private reportGiftsService: ReportGiftsService) {
    this.datePickerConfig = Object.assign({},
      {
        showWeekNumbers: false,
        containerClass: 'theme-blue',
      });
  }
  ngOnInit() {
    // ---Start of code for Permissions Implementation--- //
    try {
      this.decodedToken = new JwtHelper().decodeToken(localStorage.getItem('rights'));
      this.decodeUserToken = new JwtHelper().decodeToken(localStorage.getItem('token'));
    } catch (error) {
      this.decodedToken = {};
      this.decodeUserToken = {};
    }
    if (this.decodedToken.data && this.decodedToken.data.permissions) {
      this.decodedToken = JSON.parse(this.decodedToken.data.permissions);
    } else {
      this.decodedToken = {};
    }
    // ---End of code for Permissions Implementation--- //
    this.type = this.reportTypes[0];
  }
  loadCompanyInfo(data) {
    this.companyName =data.Name;
  }
  reportTypeOnChange(value) {
    this.isGenerate = false;
    this.type = value;
  }
  clearErrorMsgs() {
    this.error = '';
  }
  generateReport() {
    if (this.bsValue.setHours(0, 0, 0, 0) > this.bsValue1.setHours(0, 0, 0, 0)) {
      this.toastermessage = this.translateService.get('TICKET_ANALYSIS_REPORT.BEGIN_DATE_MUST_BE_BEFORE_THE_END_DATE');
      this.toastr.error(this.toastermessage.value, null, { timeOut: 3000 });
      // this.error = 'TOTAL_SHEETS.BEGIN_DATE_SHOULD_BE_AFTER_END_DATE';
    } else {
      const stDate = this.startDate.getFullYear() + '-' + (this.startDate.getMonth() + 1) + '-' + this.startDate.getDate();
      const edDate = this.endDate.getFullYear() + '-' + (this.endDate.getMonth() + 1) + '-' + this.endDate.getDate();
      const servieObj = {
        'begindate': stDate,
        'enddate': edDate,
        'type': this.type
      };
      this.reportGiftsService.generateReport(servieObj).subscribe(data => {
        this.isGenerate = true;
        this.goalsObj = data['result'];
        if (this.goalsObj.length === 0) {
          this.itemsDisplay = false;
          this.toastermessage = this.translateService.get('TICKET_ANALYSIS_REPORT.NO_DATA_FOUND');
      this.toastr.error(this.toastermessage.value, null, { timeOut: 3000 });
        } else {
          let temp = 0;
          let temp1 = 0;
          this.ticketCount = this.goalsObj.length;
          this.goalsObj.map((obj) => { temp += obj.Amount__c; });
          this.orginalAmt = temp;
          this.goalsObj.map((obj) => { temp1 += ( obj.currentBalnce ? (obj.Amount__c - obj.currentBalnce ) : obj.Amount__c); });
          this.currentAmt = temp1;
          this.itemsDisplay = true;
        }
      },
        error => {
          this.itemsDisplay = false;
          const status = JSON.parse(error['status']);
          const statuscode = JSON.parse(error['_body']).status;
          switch (JSON.parse(error['_body']).status) {
            case '2033':
              break;
          }
          if (statuscode === '2085' || statuscode === '2071') {
            if (this.router.url !== '/') {
              localStorage.setItem('page', this.router.url);
              this.router.navigate(['/']).then(() => { });
            }
          }

        });
    }
  }

  printDiv() {
    let printContents, popupWin;
    printContents = document.getElementById('dvContents').innerHTML;
    printContents = printContents.replace('bsdatepicker=""', 'value="' + (this.bsValue.getMonth() + 1) + '/'
      + this.bsValue.getDate() + '/' + this.bsValue.getFullYear() + '"');
    printContents = printContents.replace('bsdatepicker=""', 'value="' + (this.bsValue1.getMonth() + 1) + '/'
      + this.bsValue1.getDate() + '/' + this.bsValue1.getFullYear() + '"');
      printContents = printContents.replace('value="' + this.type + '"', 'selected value="' + this.type + '"');
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
  <html>
    <head>
      <title>Inventory usage Report</title>
      <style>
      //........Customized style.......
      table {
        border-collapse: collapse;
       }
      table, th, td {
          border: 0.5px solid black;
      }
      .pri td {
        padding:6px;
      }
      .arc {
        float:left;
        margin:12px;
      }
      .arc button {
        margin-top:10px;
      }
      .total {
        margin:6px;
      }
      </style>
    </head>
<body onload="window.print();window.close()">${printContents}</body>
  </html>`
    );
    popupWin.document.close();
  }
}
