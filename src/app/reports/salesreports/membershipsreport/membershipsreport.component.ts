import { Component, OnInit } from '@angular/core';
import { MembershipsreportService } from './membershipsreport.service';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonService } from '../../../common/common.service';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { JwtHelper } from 'angular2-jwt';

@Component({
  selector: 'app-membershipsreport',
  templateUrl: './membershipsreport.component.html',
  styleUrls: ['./membershipsreport.component.scss'],
  providers: [MembershipsreportService, CommonService]
})
export class MembershipsreportComponent implements OnInit {
  sortData = [];
  memberType = [{ 'name': 'Active Memberships' }, { 'name': 'Inactive Memberships' }];
  activedropDownvalue = '';
  dropDownvalue = '';
  memberShipData: any = [];
  toastermessage: any;
  createdDate: any = new Date();
  genDate = ['', ''];
  Value: any;
  Value1: any;
  decodeUserToken: any;
  decodedToken: any;
  constructor(
    private activatedRoute: ActivatedRoute,
    private commonService: CommonService,
    private toastr: ToastrService,
    private translateService: TranslateService,
    private membershipsreportService: MembershipsreportService,
    // private toastr: ToastrService,
    private router: Router
  ) {

  }
  ngOnInit() {
    // ---Start of code for Permissions Implementation--- //
    try {
      this.decodedToken = new JwtHelper().decodeToken(localStorage.getItem('rights'));
      this.decodeUserToken = new JwtHelper().decodeToken(localStorage.getItem('token'));
    } catch (error) {
      this.decodedToken = {};
      this.decodeUserToken = {};
    }
    if (this.decodedToken.data && this.decodedToken.data.permissions) {
      this.decodedToken = JSON.parse(this.decodedToken.data.permissions);
    } else {
      this.decodedToken = {};
    }
    // ---End of code for Permissions Implementation--- //
    this.sort();
    this.activedropDownvalue = this.memberType[0]['name'];
    const temp = this.commonService.getDBDatTmStr(this.createdDate);
    this.genDate = this.commonService.getUsrDtStrFrmDBStr(temp);
  }
  changeDropedown(value) {
    this.Value1 = value;
    if (value === 'Next Bill Date') {
      this.dropDownvalue = 'Next Bill Date';
    } else if (value === 'Amount') {
      this.dropDownvalue = 'Amount';
    } else if (value === 'Type') {
      this.dropDownvalue = 'Type';
    } else if (value === 'Name') {
      this.dropDownvalue = 'Name';
    } else if (value === 'Member Number') {
      this.dropDownvalue = 'Member Number';
    } else {
      this.dropDownvalue = '';
    }
    this.getmemberships(this.activedropDownvalue, this.dropDownvalue);

  }

  changeActiveDropedown(value) {
    this.Value = value;
    if (value === 'Active Memberships') {
      this.activedropDownvalue = 'Active Memberships';
    } else if (value === 'Inactive Memberships') {
      this.activedropDownvalue = 'Inactive Memberships';
    }
    this.getmemberships(this.activedropDownvalue, this.dropDownvalue);
  }
  sort() {
    this.membershipsreportService.sort().subscribe(
      data => {
        this.sortData = data['memberShipSort'];
        this.dropDownvalue = this.sortData[0].value;
        if (this.dropDownvalue) {
          this.getmemberships(this.activedropDownvalue, this.dropDownvalue);
        }
      },
      error => {
        const errStatus = JSON.parse(error['_body'])['status'];
        if (errStatus === '2085' || errStatus === '2071') {
          if (this.router.url !== '/') {
            localStorage.setItem('page', this.router.url);
            this.router.navigate(['/']).then(() => { });
          }
        }
      });
  }


  getmemberships(type, sort) {
    this.membershipsreportService.getData(this.activedropDownvalue, this.dropDownvalue)
      .subscribe(data => {
        if (data['result'].length > 0) {
          this.memberShipData = data['result'];
          for (let j = 0; j < this.memberShipData.length; j++) {
            this.memberShipData[j]['Next_Bill_Date__c'] = this.commonService.getUsrDtStrFrmDBStr(this.memberShipData[j]['Next_Bill_Date__c'])
          }
        } else {
          this.memberShipData = [];
          this.toastermessage = this.translateService.get('COMMON_TOAST_MESSAGES.TOAST_NO_DATA');
          this.toastr.error(this.toastermessage.value, null, { timeOut: 1500 });
        }

      },
        error => {
          const status = JSON.parse(error['status']);
          const statuscode = JSON.parse(error['_body']).status;
          switch (status) {
            case 500:
              break;
            case 400:
              if (statuscode === '2085' || statuscode === '2071') {
                if (this.router.url !== '/') {
                  localStorage.setItem('page', this.router.url);
                  this.router.navigate(['/']).then(() => { });
                }
              } break;
          }
        });
  }
  printDiv() {
    let printContents;
    let popupWin;
    printContents = document.getElementById('dvContents').innerHTML;
    printContents = printContents.replace('value="' + this.Value + '"', 'selected value="' + this.Value + '"');
    printContents = printContents.replace('value="' + this.Value1 + '"', 'selected value="' + this.Value1 + '"');
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <title>Membership Report</title>
          <style>
            table {
              border-collapse: collapse;
            }
            table, th, td {
                border: 0.5px solid black;
                color : black;
            }
            .pri td {
              padding:6px;
            }
            .arc {
              float:left;
              margin:12px;
            }
            .arc button {
              margin-top:10px;
            }
            .total {
              margin:6px;
            }
          </style>
        </head>
        <body onload="window.print();window.close()">${printContents}</body>
      </html>`);
    popupWin.document.close();
  }
}
