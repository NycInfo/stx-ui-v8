import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {  MembershipsreportComponent } from './membershipsreport.component';
import {  MembershipsReportRoutingModule } from './membershipsreport.routing';
import { FormsModule } from '@angular/forms';
import { ShareModule } from '../../../common/share.module';
import { TranslateModule } from '@ngx-translate/core';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        TranslateModule,
        MembershipsReportRoutingModule,
        ShareModule,
        BsDatepickerModule.forRoot(),
    ],
    declarations: [
        MembershipsreportComponent
    ]
})
export class MembershipsReportModule {
}