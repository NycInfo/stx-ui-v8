import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ReportProductSalesService } from './reportproductsales.service';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { JwtHelper } from 'angular2-jwt';
import { CommonService } from '../../../common/common.service';
@Component({
  selector: 'app-reports-app',
  templateUrl: './reportproductsales.html',
  styleUrls: ['./reportproductsales.scss'],
  providers: [ReportProductSalesService, CommonService],
})
export class ReportProductSalesComponent implements OnInit {
  bsValue = new Date();
  bsValue1 = new Date();
  itemsDisplay = false;
  workerTipsData: any;
  datePickerConfig: any;
  minDate = new Date();
  startDate = new Date();
  endDate = new Date();
  error: any;
  workererror: any;
  noDataErr: any;
  type: any;
  decodedToken: any;
  decodeUserToken: any;
  worker: any;
  pdLineId: any = 'All';
  workerName: any;
  isGenerate = false;
  workerList = [];
  reportTypes = ['Company', 'Worker'];
  showWorkers = true;
  productSalesObj = [];
  productNetSalesTotal = 0;
  productLineList: any;
  toastermessage: any;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private toastr: ToastrService,
    private commonService: CommonService,
    private translateService: TranslateService,
    private reportProductSalessService: ReportProductSalesService) {

    this.datePickerConfig = Object.assign({},
      {
        showWeekNumbers: false,
        containerClass: 'theme-blue',
      });
  }
  ngOnInit() {
    // ---Start of code for Permissions Implementation--- //
    try {
      this.decodedToken = new JwtHelper().decodeToken(localStorage.getItem('rights'));
      this.decodeUserToken = new JwtHelper().decodeToken(localStorage.getItem('token'));
    } catch (error) {
      this.decodedToken = {};
      this.decodeUserToken = {};
    }
    if (this.decodedToken.data && this.decodedToken.data.permissions) {
      this.decodedToken = JSON.parse(this.decodedToken.data.permissions);
    } else {
      this.decodedToken = {};
    }
    // ---End of code for Permissions Implementation--- //
    this.getWorkerList();
    this.getProductLine();
    this.type = this.reportTypes[0];
  }
  reportTypeOnChange(value) {
    this.isGenerate = false;
    this.type = value;
    if (value === 'Worker') {
      this.showWorkers = false;
    } else {
      this.showWorkers = true;
    }
  }
  workerListOnChange(value) {
    this.isGenerate = false;
    this.worker = value.split('$')[0];
    this.workerName = value.split('$')[1];
    this.workererror = '';
  }
  pdLineListOnChange(value) {
    this.pdLineId = value;
    // this.workerName = value.split('$')[1];
    this.workererror = '';
  }
  getWorkerList() {
    this.reportProductSalessService.getWorkerList().subscribe(data => {
      this.workerList = [];
      this.workerList = data.result
        .filter(filterList => filterList.IsActive);
    },
      error => {
        const status = JSON.parse(error.status);
        const statuscode = JSON.parse(error._body).status;
        switch (JSON.parse(error._body).status) {
          case '2033':
            break;
        }
        if (statuscode === '2085' || statuscode === '2071') {
          if (this.router.url !== '/') {
            localStorage.setItem('page', this.router.url);
            this.router.navigate(['/']).then(() => { });
          }
        }
      });
  }
  /*-- Method to get product lines list(active) --*/
  getProductLine() {
    this.reportProductSalessService.getProductLineDetails(1)
      .subscribe(data => {
        this.productLineList = data.result;
      },
        error => {
          const errStatus = JSON.parse(error._body).status;
          if (errStatus === '2085' || errStatus === '2071') {
            if (this.router.url !== '/') {
              localStorage.setItem('page', this.router.url);
              this.router.navigate(['/']).then(() => { });
            }
          }
        });
  }
  clearErrorMsgs() {
    this.workererror = '';
    this.error = '';
    this.noDataErr = '';
  }
  generateReport() {
    this.productSalesObj = [];
    if (!this.startDate) {
      this.toastermessage = this.translateService.get('TICKET_ANALYSIS_REPORT.PLEASE_SELECT_BEGIN_DATE');
      this.toastr.error(this.toastermessage.value, null, { timeOut: 3000 });
    } else if (!this.endDate) {
      this.toastermessage = this.translateService.get('TICKET_ANALYSIS_REPORT.PLEASE_SELECT_END_DATE');
      this.toastr.error(this.toastermessage.value, null, { timeOut: 3000 });
    } else if (this.startDate.setHours(0, 0, 0, 0) > this.endDate.setHours(0, 0, 0, 0)) {
      this.toastermessage = this.translateService.get('TICKET_ANALYSIS_REPORT.BEGIN_DATE_MUST_BE_BEFORE_THE_END_DATE');
      this.toastr.error(this.toastermessage.value, null, { timeOut: 3000 });
    } else if (this.type === 'Worker' && !this.worker) {
      this.toastermessage = this.translateService.get('TICKET_ANALYSIS_REPORT.WORKER_IS_REQUIRED');
      this.toastr.error(this.toastermessage.value, null, { timeOut: 3000 });
    } else {
      const stDate = this.startDate.getFullYear() + '-' + (this.startDate.getMonth() + 1) + '-' + this.startDate.getDate();
      const edDate = this.endDate.getFullYear() + '-' + (this.endDate.getMonth() + 1) + '-' + this.endDate.getDate();
      if (this.type === 'Company') {
        this.worker = '';
      }
      const productObj = {
        begindate: stDate,
        enddate: edDate,
        type: this.type,
        worker: this.worker,
        pdLine: this.pdLineId
      };
      this.reportProductSalessService.generateReport(productObj).subscribe(data => {
        const productSalesObj = data.result;
        if (productSalesObj.length === 0) {
          this.isGenerate = false;
          this.toastermessage = this.translateService.get('TICKET_ANALYSIS_REPORT.NO_DATA_FOUND');
          this.toastr.error(this.toastermessage.value, null, { timeOut: 3000 });
          // this.noDataErr = '**Data Not Found';
        } else {
          this.isGenerate = true;
          productSalesObj.forEach(element => {
            element.disaplayDate = this.commonService.getUsrDtStrFrmDBStr(element.Appt_Date_Time__c);
          });
          this.noDataErr = '';
        }
        const items = productSalesObj;
        const lookup = {};
        const result = [];
        let totalSalesVal = 0;
        // tslint:disable-next-line: no-conditional-assignment
        for (let item, i = 0; item = items[i++];) {
          const prodctName = item.productName;

          if (!(prodctName in lookup)) {
            lookup[prodctName] = 1;
            result.push(prodctName);
          }
        }
        const proArray = [];
        this.productNetSalesTotal = 0;
        for (let i = 0; i < result.length; i++) {
          proArray[i] = productSalesObj.filter((obj) => obj.productName === result[i]);
          for (let j = 0; j < proArray[i].length; j++) {
            totalSalesVal += parseFloat(proArray[i][j].Net_Price__c);
            proArray[i].Net_Price__c = totalSalesVal;
          }
          totalSalesVal = 0;
        }
        this.productSalesObj = proArray;
        let temp = 0;
        this.productSalesObj.map((obj) => { temp += obj.Net_Price__c; });
        this.productNetSalesTotal = temp;
        this.itemsDisplay = true;
      },
        error => {
          this.itemsDisplay = false;
          const status = JSON.parse(error.status);
          const statuscode = JSON.parse(error._body).status;
          switch (JSON.parse(error._body).status) {
            case '2033':
              break;
          }
          if (statuscode === '2085' || statuscode === '2071') {
            if (this.router.url !== '/') {
              localStorage.setItem('page', this.router.url);
              this.router.navigate(['/']).then(() => { });
            }
          }
        });
    }
  }
  printDiv() {
    const popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    let popupContent = `
    <html>
      <head>
        <title>Product Sales Report</title>
        <style>
          .reportTable {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
          }
          .reportTable td,
          .reportTable th {border: 1px solid rgb(141, 141, 141);padding: 8px;}
          .reportTable tr:nth-child(even) {background-color: #f2f2f2;}
          .reportTable th {
            padding-top: 6px;
            padding-bottom: 6px;
            text-align: left;
            background-color: #204d74;
            color: white;
          }
        </style>
      </head>
      <body onload="window.print();window.close()">
        <div>
          <table>
            <tr><td><b>Begin Date: </b></td><td>${(this.bsValue.getMonth() + 1)
      + '/' + this.bsValue.getDate() + '/' + this.bsValue.getFullYear()}</td></tr>
            <tr><td><b>End Date: </b></td><td>${(this.bsValue1.getMonth() + 1)
      + '/' + this.bsValue1.getDate() + '/' + this.bsValue1.getFullYear()}</td></tr>
            <tr><td><b>Report Type: </b></td><td>${this.type}</td></tr>
            {{selWorker}}
            <tr><td><b>Product Line: </b></td><td>{{pdtLn}}</td></tr>
          </table>
          <br>
          <table class="reportTable">
            <tr>
              <th>Product Name</th>
              <th>Size</th>
              <th>Unit of Measure</th>
              <th>Appointment / Ticket Number</th>
              <th>Appointment Date Time</th>
              <th>Quantity Sold</th>
              <th>Discounted Price</th>
            </tr>
            {{tableContent}}
          </table>
        </div>
      </body>
    </html>`;
    if (this.type === 'Worker') {
      popupContent = popupContent.replace('{{selWorker}}', `<tr><td><b>Worker: </b></td><td>${this.workerName}</td></tr>`);
    } else {
      popupContent = popupContent.replace('{{selWorker}}', '');
    }
    if (this.pdLineId === 'All') {
      popupContent = popupContent.replace('{{pdtLn}}', this.pdLineId);
    } else {
      popupContent = popupContent.replace('{{pdtLn}}', this.productLineList.filter(obj => obj.Id = this.pdLineId)[0].Name);
    }
    if (this.noDataErr) {
      popupContent = popupContent.replace('{{tableContent}}', '<tr><td colspan="7">**Data Not Found</td></tr>');
    } else {
      popupContent = popupContent.replace('{{tableContent}}', document.getElementById('tableContentId').innerHTML);
    }
    popupWin.document.write(popupContent);
    popupWin.document.close();
  }
}
