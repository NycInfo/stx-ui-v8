import { Component, OnInit } from '@angular/core';
import { PromotionReportService } from './promotionreport.service';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { Router, ActivatedRoute } from '@angular/router';
import { JwtHelper } from 'angular2-jwt';
import { CommonService } from '../../../common/common.service';

@Component({
  selector: 'app-reports-app',
  templateUrl: './promotionreport.html',
  styleUrls: ['./promotionreport.scss'],
  providers: [PromotionReportService, CommonService],
})
export class PromotionReportComponent implements OnInit {

  startDate = new Date();
  endDate = new Date();
  testPrmote: any;
  datePickerConfig: any;
  ticketCount: any;
  itemsDisplay: any;
  showdata: any;
  uniqProName: any = [];
  orginalAmt: any;
  minDate = new Date();
  toastermessage: any;
  isGenerate = false;
  promotionData: any = [];
  grandTotal = 0;
  decodeUserToken: any;
  decodedToken: any;
  constructor(private route: ActivatedRoute,
    private router: Router,
    private toastr: ToastrService,
    private translateService: TranslateService,
    private commonService: CommonService,
    private promotionReportService: PromotionReportService) {
    this.datePickerConfig = Object.assign({},
      {
        showWeekNumbers: false,
        containerClass: 'theme-blue',
      });
  }
  ngOnInit() {
    // ---Start of code for Permissions Implementation--- //
    try {
      this.decodedToken = new JwtHelper().decodeToken(localStorage.getItem('rights'));
      this.decodeUserToken = new JwtHelper().decodeToken(localStorage.getItem('token'));
    } catch (error) {
      this.decodedToken = {};
      this.decodeUserToken = {};
    }
    if (this.decodedToken.data && this.decodedToken.data.permissions) {
      this.decodedToken = JSON.parse(this.decodedToken.data.permissions);
    } else {
      this.decodedToken = {};
    }
    // ---End of code for Permissions Implementation--- //

  }
  getPromtionData(event) {
    const date = new Date();
    if (this.startDate === null) {
      this.toastermessage = this.translateService.get('TICKET_ANALYSIS_REPORT.PLEASE_SELECT_BEGIN_DATE');
      this.toastr.error(this.toastermessage.value, null, { timeOut: 3000 });
    } else if (isNaN(this.startDate.getTime())) {
      this.toastermessage = this.translateService.get('TICKET_ANALYSIS_REPORT.BEGIN_DATE_IS_INVALID');
      this.toastr.error(this.toastermessage.value, null, { timeOut: 3000 });
    } else if (this.endDate.setHours(0, 0, 0, 0) > date.setHours(0, 0, 0, 0)) {
      this.toastermessage = this.translateService.get('TICKET_ANALYSIS_REPORT.END_DATE_CANNOT_BE_FUTURE');
      this.toastr.error(this.toastermessage.value, null, { timeOut: 3000 });
    } else if (this.endDate === null) {
      this.toastermessage = this.translateService.get('TICKET_ANALYSIS_REPORT.PLEASE_SELECT_END_DATE');
      this.toastr.error(this.toastermessage.value, null, { timeOut: 3000 });
    } else if (isNaN(this.endDate.getTime())) {
      this.toastermessage = this.translateService.get('TICKET_ANALYSIS_REPORT.END_DATAE_IS_INVALID');
      this.toastr.error(this.toastermessage.value, null, { timeOut: 3000 });
    } else if (this.startDate.setHours(0, 0, 0, 0) > this.endDate.setHours(0, 0, 0, 0)) {
      this.toastermessage = this.translateService.get('TICKET_ANALYSIS_REPORT.BEGIN_DATE_MUST_BE_BEFORE_THE_END_DATE');
      this.toastr.error(this.toastermessage.value, null, { timeOut: 3000 });
    } else {
      this.promotionData = [];
      this.promotionReportService.getprmtnReport(this.startDate, this.endDate).subscribe(
        data => {
          data['result'] = data['result'].filter((obj) => obj.tsPromotion__c !== null && obj.tsPromotion__c !== '' && obj.tsPromotion__c !== 'null');
          if (data['result'].length > 0) {
            this.isGenerate = true;
            this.promotionData = data['result'];
          } else {
            this.isGenerate = false;
            this.toastermessage = this.translateService.get('COMMON_TOAST_MESSAGES.TOAST_NO_DATA');
            this.toastr.error(this.toastermessage.value, null, { timeOut: 3000 });
          }
          if (this.promotionData.length > 0) {
            this.uniqProName = [];
            this.grandTotal = 0;
            this.promotionData.forEach(obj => {
              this.grandTotal += obj['discAmount'];
              obj['Appt_Date_Time__c'] = this.commonService.getUsrDtStrFrmDBStr(obj['Appt_Date_Time__c'])[0];
              const arr = this.uniqProName.filter(ele => ele['name'] === obj['proName']);
              if (arr.length === 0) {
                this.uniqProName.push(
                  { 'name': obj['proName'] });
              }
            });
            this.uniqProName.forEach(obj => {
              const uniqData = this.promotionData.filter(ele => ele['proName'] === obj['name']);
              obj['data'] = uniqData;
              let sum = 0;
              uniqData.forEach(obj2 => {
                sum += obj2['discAmount'];
              });
              obj['totalDiscAmount'] = sum;
            });
          }
        },
        error => {
          const status = JSON.parse(error['status']);
          const statuscode = JSON.parse(error['_body']).status;
          switch (status) {
            case 500:
              break;
            case 400:
              break;
          }
          if (statuscode === '2085' || statuscode === '2071') {
            if (this.router.url !== '/') {
              localStorage.setItem('page', this.router.url);
              this.router.navigate(['/']).then(() => { });
            }
          }
        }
      );
    }
  }
  printDiv() {
    let printContents, popupWin;
    printContents = document.getElementById('dvContents').innerHTML;
    printContents = printContents.replace('bsdatepicker=""', 'value="' + (this.startDate.getMonth() + 1) + '/'
      + this.startDate.getDate() + '/' + this.startDate.getFullYear() + '"');
    printContents = printContents.replace('bsdatepicker=""', 'value="' + (this.endDate.getMonth() + 1) + '/'
      + this.endDate.getDate() + '/' + this.endDate.getFullYear() + '"');
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <title>Promotion Report</title>
          <style>
            table {
              border-collapse: collapse;
            }
            table, th, td {
                border: 0.5px solid black;
            }
            .pri td {
              padding:6px;
            }
            .arc {
              float:left;
              margin:12px;
            }
            .arc button {
              margin-top:10px;
            }
            .total {
              margin:6px;
            }
          </style>
        </head>
        <body onload="window.print();window.close()">${printContents}</body>
      </html>`);
    popupWin.document.close();
  }
}
