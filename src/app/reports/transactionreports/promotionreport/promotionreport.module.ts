import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PromotionReportComponent } from './promotionreport.component';
import { PromotionReportRoutingModule } from './promotionreport.routing';
import { FormsModule } from '@angular/forms';
import { ShareModule } from '../../../common/share.module';
import { TranslateModule } from '@ngx-translate/core';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        TranslateModule,
        PromotionReportRoutingModule,
        ShareModule,
        BsDatepickerModule.forRoot(),
    ],
    declarations: [
        PromotionReportComponent
    ]
})
export class PromotionReportModule {
}
