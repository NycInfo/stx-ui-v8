import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { PromotionReportComponent } from './promotionreport.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '',
                component: PromotionReportComponent,
                children: [
                    {
                        path: '',
                        component: PromotionReportComponent
                    }
                ]
            }
        ])
    ],
    exports: [
        RouterModule
    ]
})
export class PromotionReportRoutingModule {
}
