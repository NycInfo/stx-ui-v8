import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { SafeUrl, DomSanitizer } from '@angular/platform-browser';
import { ReportDailyCashDrawerService } from './reportdailycashdrawer.service';
import { ToastrService } from 'ngx-toastr';
import { CommonService } from '../../../common/common.service';
import { TranslateService } from '@ngx-translate/core';
import { JwtHelper } from 'angular2-jwt';
// import {Component} from "@angular/core";
@Component({
  selector: 'app-reports-app',
  templateUrl: './reportdailycashdrawer.html',
  styleUrls: ['./reportdailycashdrawer.scss'],
  providers: [ReportDailyCashDrawerService, CommonService],
})
export class ReportDailyCashDrawerComponent implements OnInit {
  bsValue: any;
  maxdate = new Date();
  itemsDisplay = false;
  paymentsData: any;
  cashInOutData: Array<any>;
  cashDrawerData: any;
  tipsData: any;
  sendDate: any;
  decodedToken: any;
  decodeUserToken: any;
  datePickerConfig: any;
  companyName: any;
  totalAmt: number;
  reportData: any = [];
  AmountData: any = {};
  changeBackAmount: any = {};
  cashInOutTot = {};
  ticketPaymentTot = {};
  constructor(
    private route: ActivatedRoute, private commonService: CommonService,
    private router: Router,
    private toastr: ToastrService,
    private translateService: TranslateService,
    private reportDailyCashDrawerService: ReportDailyCashDrawerService) {
    this.datePickerConfig = Object.assign({},
      {
        showWeekNumbers: false,
        containerClass: 'theme-blue',
      });
  }
  ngOnInit() {
    // ---Start of code for Permissions Implementation--- //
        try {
         this.decodedToken = new JwtHelper().decodeToken(localStorage.getItem('rights'));
         this.decodeUserToken = new JwtHelper().decodeToken(localStorage.getItem('token'));
       } catch (error) {
         this.decodedToken = {};
         this.decodeUserToken = {};
       }
       if (this.decodedToken.data && this.decodedToken.data.permissions) {
         this.decodedToken = JSON.parse(this.decodedToken.data.permissions);
       } else {
         this.decodedToken = {};
       }
       // ---End of code for Permissions Implementation--- //
  }
  loadCompanyInfo(data) {
    this.companyName = data.Name;
  }
  calculateSum(total: number, value: number) {
    return total + value;
  }
  generateReport(date) {
    this.bsValue = new Date(date);
    this.sendDate = this.commonService.getDBDatTmStr(this.bsValue).split(' ')[0];
    this.reportDailyCashDrawerService.getCashDrawer(this.sendDate)
      .subscribe(data => {
        const payments = data['result']['paymentObjdata'];
        const tips = data['result']['tipsObjData'].filter(obj => obj.drawerNumber !== 'undefined');

        this.itemsDisplay = true;
        this.reportData = [];
        this.cashInOutData = data['result']['cashInOutData'];
        const balanceDue = data['result']['balanceDueAmountPerAppt'];
        this.AmountData = {};
        this.cashInOutTot = {};
        this.ticketPaymentTot = {};
        const cashDrawersList = data['result']['drawerData']; // cash drwaer list from ticke prf //
        this.cashDrawerData = data['result']['cashDrawerData']; // it has onnly cas drawer info only //
        const paymentMap = new Map();
        const cashDrawerMap = new Map(); // this is for new cash drawer map//
        /* cash drawer info */
        this.cashDrawerData.forEach((drawerData) => {
          const name = drawerData.Cash_Drawer_Number__c.toString();
          const obj = {
            'cashDrawer': drawerData,
            'cashInOut': [],
            'paymentsMade': [],
            'cashDrawerNumber': name, // cash drawer number //
            'cashDrawerName': this.getCashDrawerName(cashDrawersList, name)
          };
          if (!cashDrawerMap.has(name)) {
            cashDrawerMap.set(name, obj);
            if (!this.AmountData.hasOwnProperty(name)) {
              this.AmountData[name] = 0;
            }
          } else {
            cashDrawerMap.set(name, obj);
          }
        });
        /* cas drawer info completed */

        /* cash in out info */
        this.cashInOutData.forEach((cashData) => {
          /* from here is used to disaplay the cash in out total for individual casdrawer */
          if (this.cashInOutTot[cashData['Drawer_Number__c']]) {
            this.cashInOutTot[cashData['Drawer_Number__c']] += cashData['Amount__c'];
          } else {
            this.cashInOutTot[cashData['Drawer_Number__c']] = cashData['Amount__c'];
          }
          /*  end of cash in out total for individual casdrawer */
          const name = cashData.Drawer_Number__c ? cashData.Drawer_Number__c.toString() : ' ';
          const obj = {
            'cashDrawer': undefined,
            'cashInOut': [cashData],
            'paymentsMade': [],
            'cashDrawerNumber': name,
            'cashDrawerName': this.getCashDrawerName(cashDrawersList, name)
          };
          if (!this.AmountData.hasOwnProperty(name)) { /* hasOwnProperty return boolean  value in amount data is having property name or not? */
            this.AmountData[name] = 0;
          }
          if (this.AmountData.hasOwnProperty(name)) {
            // if (cashData.Type__c === 'Cash Paid In') {
            //   this.AmountData[name] += +cashData.Amount__c;
            // } else {
            //   this.AmountData[name] -= +cashData.Amount__c;
            // }
            this.AmountData[name] += +cashData.Amount__c; // this is for adding cash inout amount adding to total cash drawer amount//
          }
          if (!cashDrawerMap.has(name)) {/* has is used here to whethr element(name) is their or not?  */
            cashDrawerMap.set(name, obj);
          } else {
            const mappedObj = cashDrawerMap.get(name); // checking with previous map whether drawer number exists then push casData into cashDrawerMap //
            mappedObj.cashInOut.push(cashData);
            cashDrawerMap.set(name, mappedObj); // this is updated cashDrawerMap object//
          }
        });
        /* cash in out completed */
        /* cas drawer info completed */
        /* payments Data info */
        payments.forEach((payment) => {
          const name = payment.drawerNumber.toString();
          const apptName = payment.apptTicketName.toString();
          /* from here is used to disaplay the ticket total for individual casdrawer */
          if (payment.paymentType !== 'Prepaid Package' && payment.paymentType !== 'Gift Redeem') {
            if (this.ticketPaymentTot[payment['drawerNumber']]) {
              // this.ticketPaymentTot[payment['drawerNumber']] += payment['paymentAmount'];
              const balanceDueFilterAmount = balanceDue.filter((bDue) => bDue.apptTicketName.toString() === apptName + name);
              const balanceDueAmount = balanceDueFilterAmount.length > 0 ? balanceDueFilterAmount[0].balanceDue : false;
              if (balanceDueAmount || balanceDueAmount === 0) {
                this.ticketPaymentTot[payment['drawerNumber']] = payment['paymentAmount'] + balanceDueAmount;
              }
            } else {
              const balanceDueFilterAmount = balanceDue.filter((bDue) => bDue.apptTicketName.toString() === apptName + name);
              const balanceDueAmount = balanceDueFilterAmount.length > 0 ? balanceDueFilterAmount[0].balanceDue : false;
              if (balanceDueAmount || balanceDueAmount === 0) {
                this.ticketPaymentTot[payment['drawerNumber']] = payment['paymentAmount'] + balanceDueAmount;
              }
            }
          }
          /*  end of ticket total for individual casdrawer */

          const obj = {
            'cashDrawer': undefined,
            'cashInOut': [],
            'paymentsMade': [payment],
            'cashDrawerNumber': name,
            'cashDrawerName': this.getCashDrawerName(cashDrawersList, name)
          };
          if (!this.AmountData.hasOwnProperty(name)) {
            this.AmountData[name] = 0;
          }
          if (this.AmountData.hasOwnProperty(name)) {
            if (payment.paymentType !== 'Prepaid Package' && payment.paymentType !== 'Gift Redeem') {
              // this.paymentTotal += +payment.paymentAmount; /* this is used to dispaly ticket total filed in html */
              // if Cash Drawer total does not inclue prepaid package amount then add if condition here to remove prepaid amount to the total.
              this.AmountData[name] += +payment.paymentAmount;
            }
          }
          if (!paymentMap.has(apptName + name)) {/* here paymentmap is not having appt ticket with drawernumber, (apptName + name) means appt ticket number  */
            const balanceDueFilterAmount = balanceDue.filter((bDue) => bDue.apptTicketName.toString() === apptName + name);
            const balanceDueAmount = balanceDueFilterAmount.length > 0 ? balanceDueFilterAmount[0].balanceDue : false;
            if (balanceDueAmount || balanceDueAmount === 0) {
              this.AmountData[name] += balanceDueAmount;
              this.changeBackAmount[apptName + name] = balanceDueAmount;
            }
            paymentMap.set(apptName + name, balanceDueAmount);
          }
          if (!cashDrawerMap.has(name)) {
            cashDrawerMap.set(name, obj);
          } else {
            const mappedObj = cashDrawerMap.get(name);
            mappedObj.paymentsMade.push(payment);
            cashDrawerMap.set(name, mappedObj);
          }
        });
        /* payments Data info completed*/
        /* tips Data info */
        tips.forEach((tip) => {
          const apptName = tip.apptTicketName.toString();
          const name = tip.drawerNumber.toString();
          const obj = {
            'cashDrawer': undefined,
            'cashInOut': [],
            'paymentsMade': [tip], // here all the tips payments are added irrespective of appt //
            'cashDrawerNumber': name,
            'cashDrawerName': this.getCashDrawerName(cashDrawersList, name)
          };
          if (!this.AmountData.hasOwnProperty(name)) {
            this.AmountData[name] = 0;
          }
          /* added for substracting tip paid out for the issue 1679 */
          if (!this.ticketPaymentTot.hasOwnProperty(name)) {
            this.ticketPaymentTot[name] = 0;
          }
          if (this.ticketPaymentTot.hasOwnProperty(name)) {
            if (tip.paymentType === 'Tip Paid Out') {
              this.ticketPaymentTot[name] -= +tip.paymentAmount;
            }
          }
          /*  end of added for substracting tip paid out for the issue 1679 */

          if (this.AmountData.hasOwnProperty(name)) {
            if (tip.paymentType !== 'Tip Left in Drawer') {
              /* in if condition tip Left in Drawer amount is excluded in drawer total amount but amount is only displayed agnst drawer */
              /* tips paid out amount is substracting frm payment toatal */
              this.AmountData[name] -= +tip.paymentAmount;
            }
          }
          if (!cashDrawerMap.has(name)) {
            cashDrawerMap.set(name, obj);
          } else {
            const mappedObj = cashDrawerMap.get(name);
            mappedObj.paymentsMade.push(tip);
            cashDrawerMap.set(name, mappedObj);
          }
        });
        /* tips Data info completed */
        /* below code is used to group all payments based on appt against cashdrawer */
        this.reportData = [];
        let i = 0;
        cashDrawerMap.forEach((elment, key) => {
          const name = elment.cashDrawerNumber.toString();
          this.AmountData[name] = +this.AmountData[name].toFixed(2);
          const dat12 = elment;
          // console.log(elment['paymentsMade']); /* here payments types will come in single array */
          dat12['paymentsMade'] = this.getPaymentsByTicketNumber(elment['paymentsMade']);
          i++;
          this.reportData.push(dat12);
          // console.log(dat12['paymentsMade']); /* here all payment types came array in another array */
        });
        //  this.tipsData = data['result']['tipsObjData'];
      }, error => {
        const status = JSON.parse(error['status']);
        const statuscode = JSON.parse(error['_body']).status;
        switch (JSON.parse(error['_body']).status) {
          case '2033':
            window.scrollTo(0, 0);
            break;
        }
        if (statuscode === '2085' || statuscode === '2071') {
          if (this.router.url !== '/') {
            localStorage.setItem('page', this.router.url);
            this.router.navigate(['/']).then(() => { });
          }
        }
      });
  }

  getCashDrawerName(cashDrawerList, number): string | boolean {
    const selectedCashDrawer = cashDrawerList.filter((drawer) => drawer.drawerNumber === number.toString());
    if (selectedCashDrawer.length > 0) {
      return selectedCashDrawer[0]['drawerName'];
    } else {
      return false;
    }
  }
  getPaymentsByTicketNumber(paymentByTicket: Array<any>): Array<any> {
    const paymentDataByDrawer = [];
    const payments = paymentByTicket;
    const paymentsByTicketMap = new Map();
    payments.forEach((Pdata) => {
      if (Pdata.paymentType !== 'Account Charge') {
        if (paymentsByTicketMap.has(Pdata.apptTicketName)) {
          const data = paymentsByTicketMap.get(Pdata.apptTicketName);
          data.push(Pdata);
          paymentsByTicketMap.set(Pdata.apptTicketName, data);
        } else {
          paymentsByTicketMap.set(Pdata.apptTicketName, [Pdata]);
        }
      }
    });
    paymentsByTicketMap.forEach((value) => {
      paymentDataByDrawer.push(value);
    });
    return paymentDataByDrawer;
  }
  printDiv() {
    let printContents, popupWin;
    printContents = document.getElementById('dvContents').innerHTML;
    printContents = printContents.replace('bsdatepicker=""', 'value="' + (this.bsValue.getMonth() + 1) + '/'
      + this.bsValue.getDate() + '/' + this.bsValue.getFullYear() + '"');
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <title>Daily Cash Drawer</title>
          <style>
            table {
              border-collapse: collapse;
            }
            table, th, td {
                border: 0.5px solid black;
            }
            .pri td {
              padding:6px;
            }
            .arc {
              float:left;
              margin:12px;
            }
            .arc button {
              margin-top:10px;
            }
            .total {
              margin:6px;
            }
          </style>
        </head>
        <body onload="window.print();window.close()">${printContents}</body>
      </html>`);
    popupWin.document.close();
  }
}

