import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { SafeUrl, DomSanitizer } from '@angular/platform-browser';
import { TotalSheetsService } from './totalsheets.service';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { JwtHelper } from 'angular2-jwt';
// import {Component} from "@angular/core";
@Component({
  selector: 'app-reports-app',
  templateUrl: './totalsheets.html',
  styleUrls: ['./totalsheets.scss'],
  providers: [TotalSheetsService],
})
export class TotalSheetsComponent implements OnInit {
  startDate = new Date();
  endDate = new Date();
  minDate = new Date();
  itemsDisplay = false;
  isGenerate = false;
  datePickerConfig: any;
  checkOutList: any;
  total = 0;
  count = 0;
  workerSalesObj = [];
  companySalesObj = [];
  paymentsData = [];
  grandTotalObj = [];
  accountBalanceObj = {
    deposit: 0,
    depositOnline: 0,
    prepayment: 0,
    receivedOnAccount: 0,
    totals: 0
  };
  bsValue = new Date();
  bsValue1 = new Date();
  error: any;
  decodedToken: any;
  decodeUserToken: any;
  companyName: any;
  toastermessage: any;
  trigger = true;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private toastr: ToastrService,
    private translateService: TranslateService,
    private totalSheetsService: TotalSheetsService) {
    this.datePickerConfig = Object.assign({},
      {
        showWeekNumbers: false,
        containerClass: 'theme-blue',
      });
  }
  ngOnInit() {
    // ---Start of code for Permissions Implementation--- //
    try {
      this.decodedToken = new JwtHelper().decodeToken(localStorage.getItem('rights'));
      this.decodeUserToken = new JwtHelper().decodeToken(localStorage.getItem('token'));
    } catch (error) {
      this.decodedToken = {};
      this.decodeUserToken = {};
    }
    if (this.decodedToken.data && this.decodedToken.data.permissions) {
      this.decodedToken = JSON.parse(this.decodedToken.data.permissions);
    } else {
      this.decodedToken = {};
    }
    // ---End of code for Permissions Implementation--- //
  }
  loadCompanyData(data) {
    this.companyName = data.Name;
  }
  generateReport() {
    this.total = 0;
    this.count = 0;
    const date = new Date();
    if (this.startDate === null) {
      this.toastermessage = this.translateService.get('TICKET_ANALYSIS_REPORT.PLEASE_SELECT_BEGIN_DATE');
      this.toastr.error(this.toastermessage.value, null, { timeOut: 3000 });
    } else if (isNaN(this.startDate.getTime())) {
      this.toastermessage = this.translateService.get('TICKET_ANALYSIS_REPORT.BEGIN_DATE_IS_INVALID');
      this.toastr.error(this.toastermessage.value, null, { timeOut: 3000 });
    } else if (this.endDate === null) {
      this.toastermessage = this.translateService.get('TICKET_ANALYSIS_REPORT.PLEASE_SELECT_END_DATE');
      this.toastr.error(this.toastermessage.value, null, { timeOut: 3000 });
    } else if (isNaN(this.endDate.getTime())) {
      this.toastermessage = this.translateService.get('TICKET_ANALYSIS_REPORT.END_DATAE_IS_INVALID');
      this.toastr.error(this.toastermessage.value, null, { timeOut: 3000 });
    } else if (this.startDate.setHours(0, 0, 0, 0) > this.endDate.setHours(0, 0, 0, 0)) {
      this.toastermessage = this.translateService.get('TICKET_ANALYSIS_REPORT.BEGIN_DATE_MUST_BE_BEFORE_THE_END_DATE');
      this.toastr.error(this.toastermessage.value, null, { timeOut: 3000 });
    } else {
      if (this.trigger) {
        this.trigger = false;
        this.totalSheetsService.getDailyTotalSheetRecords(this.startDate, this.endDate).subscribe(
          data => {
            this.isGenerate = true;
            this.workerSalesObj = data.result.workerSalesObj;
            this.companySalesObj = data.result.companySalesObj;
            this.accountBalanceObj = data.result.accountBalanceObj;
            this.paymentsData = data.result.paymentsData;
            this.grandTotalObj = data.result.grandTotalObj;
            this.itemsDisplay = true;
            if (this.paymentsData.length > 0) {
              this.getUnpaidTcktBlnce();
            }
            this.trigger = true;
          },
          error => {
            const status = JSON.parse(error.status);
            const statuscode = JSON.parse(error._body).status;
            this.trigger = true;
            switch (status) {
              case 500:
                break;
              case 400:
                break;
            }
            if (statuscode === '2085' || statuscode === '2071') {
              if (this.router.url !== '/') {
                localStorage.setItem('page', this.router.url);
                this.router.navigate(['/']).then(() => { });
              }
            }
          }
        );
      }

    }
  }
  getUnpaidTcktBlnce() {
    this.total = 0;
    this.totalSheetsService.getCheckOutList(this.startDate, this.endDate)
      .subscribe(data1 => {
        this.checkOutList = data1.result;
        this.count = this.checkOutList.length;
        for (let i = 0; i < this.checkOutList.length; i++) {
          this.total += this.checkOutList[i].balancedue;
        }
        for (let i = 0; i < this.paymentsData.length; i++) {
          if (this.paymentsData[i].paymentType === 'Payment Total') {
            this.paymentsData.splice((i + 1), 0, {
              amountPaid: this.total,
              electronic: 0,
              number: this.count,
              paymentType: 'Unpaid Ticket Amount'
            });
          }
        }
        this.paymentsData[(this.paymentsData.length - 1)].amountPaid += this.total;
      }, error => {
        this.trigger = true;
        const status = JSON.parse(error.status);
        const statuscode = JSON.parse(error._body).status;
        switch (JSON.parse(error._body).status) {
          case '2033':
            this.error = 'COMMON_STATUS_CODES.' + JSON.parse(error._body).status;
            window.scrollTo(0, 0);
            break;
        }
        if (statuscode === '2085' || statuscode === '2071') {
          if (this.router.url !== '/') {
            localStorage.setItem('page', this.router.url);
            this.router.navigate(['/']).then(() => { });
          }
        }
      });
  }
  printDiv() {
    let printContents, popupWin;
    printContents = document.getElementById('divContents').innerHTML;
    printContents = printContents.replace('bsdatepicker=""', 'value="' + (this.bsValue.getMonth() + 1) + '/'
      + this.bsValue.getDate() + '/' + this.bsValue.getFullYear() + '"');
    printContents = printContents.replace('bsdatepicker=""', 'value="' + (this.bsValue1.getMonth() + 1) + '/'
      + this.bsValue1.getDate() + '/' + this.bsValue1.getFullYear() + '"');
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <title>Total Sheet Report</title>
          <style>
            table {
              border-collapse: collapse;
            }
            table, th, td {
                border: 0.5px solid black;
            }
            .pri td {
              padding:6px;
            }
            .arc {
              float:left;
              margin:12px;
            }
            .arc button {
              margin-top:10px;
            }
            .total {
              margin:6px;
            }
          </style>
        </head>
        <body onload="window.print();window.close()">${printContents}</body>
      </html>`);
    popupWin.document.close();
  }
}
