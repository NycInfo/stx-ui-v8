/*
  * Display single user details of front end users
  * extractData(): To extract the data
  * handleError(): To handle error messages.
*/
import { Injectable, Inject } from '@angular/core';
import { Response } from '@angular/http';
// import { Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { HttpClients } from '../../common/http-client';
import { map } from 'rxjs/operators';

@Injectable()
export class ClientAppointmentsService {
  private bookingDataUrl = 'common.json';
  constructor(
    private http: HttpClients,
    @Inject('apiEndPoint') private apiEndPoint: string,
    @Inject('staticJsonFilesEndPoint') private staticJsonFilesEndPoint: string
  ) { }
  // booking methods starts
  /*--- Method used to create client booking data ---*/
  clientBookingData(bookingData) {
    return this.http.post(this.apiEndPoint + '/api/appointmentsandemails/booking', bookingData)
      .pipe(map(this.extractData));
  }
  /*--- Method used to get booking data ---*/
  getBookingData() {
    return this.http.get(this.apiEndPoint + '/api/appointmentsandemails/booking')
      .pipe(map(this.extractData));
  }
  /*--- Method used to get static data ---*/
  bookingStaticData() {
    return this.http.get(this.staticJsonFilesEndPoint + this.bookingDataUrl)
      .pipe(map(this.extractData));
  }
  // booking methods ends
  // notifications methods starts
  getCommonData() {
    return this.http.get(this.staticJsonFilesEndPoint + 'common.json')
      .pipe(map(this.extractData));
  }
  // setup notification data
  setupNotificationData(notificationData) {
    return this.http.post(this.apiEndPoint + '/api/setup/appointmentsandemails/notifications', notificationData)
      .pipe(map(this.extractData));
  }
  // method to send email notifications
  sendEmailNotifications(emailData) {
    return this.http.post(this.apiEndPoint + '/api/setup/appointmentsandemails/sendemailnotifications', emailData)
      .pipe(map(this.extractData));
  }
  // method to get notifications
  getNotifications() {
    return this.http.get(this.apiEndPoint + '/api/setup/appointmentsandemails/notifications')
      .pipe(map(this.extractData));
  }
  // notifications methods ends
  // reminders methods starts
  getHourse() {
    return this.http.get(this.apiEndPoint + '/api/v1/lookups/HOURS')
      .pipe(map(this.extractData));
  }
  // method to save remainder
  saveRemainder(dataObject) {
    return this.http.post(this.apiEndPoint + '/api/setup/appointmentsandemails/reminders', dataObject)
      .pipe(map(this.extractData));
  }
  // method to get remainder
  getRemainder() {
    return this.http.get(this.apiEndPoint + '/api/setup/appointmentsandemails/reminders')
      .pipe(map(this.extractData));
  }
  // method to send email
  sendMail(mailData) {
    return this.http.post(this.apiEndPoint + '/api/setup/appointmentsandemails/sendemailreminders', mailData)
      .pipe(map(this.extractData));
  }
  // reminders methods ends
  // online booking methods starts
  onlineData(onlineBookingData, imgFile) {
    const data = new FormData();
    data.append('workerInfo', JSON.stringify(onlineBookingData));
    data.append('workerImage', imgFile);
    return this.http.post(this.apiEndPoint + '/api/appointments/onlinebooking', data)
      .pipe(map(this.extractData));
  }
  // method to get online data
  getOnlineData() {
    return this.http.get(this.apiEndPoint + '/api/appointmentandemails/onlinebooking')
      .pipe(map(this.extractData));
  }
  // method to format time
  timeFormats() {
    return this.http.get(this.staticJsonFilesEndPoint + this.bookingDataUrl)
      .pipe(map(this.extractData));
  }
  // method to get hours for online
  hoursForOnline() {
    return this.http.get(this.apiEndPoint + '/api/setup/company/companyhours')
      .pipe(map(this.extractData));
  }
  failedDepositeOfNotifications() {
    return this.http.get(this.apiEndPoint + '/api/checkout/product/workers/' + 'retailonly')
      .pipe(map(this.extractData));
  }
  // online booking methods ends
  // gifts online methods starts
  Giftonline(dataObject) {
    return this.http.post(this.apiEndPoint + '/api/appointment/onlinegifts', dataObject)
      .pipe(map(this.extractData));
  }
  getGiftOnlineData() {
    return this.http.get(this.apiEndPoint + '/api/appointment/onlinegifts')
      .pipe(map(this.extractData));
  }
  /*--- Method used to get merge fields ---*/
  mergefield() {
    return this.http.get(this.apiEndPoint + '/api/v1/lookups/states/India')
      .pipe(map(this.extractData));
  }
  /*To extract json data*/
  private extractData(res: Response) {
    if (res.headers && res.headers.get('token')) {
      localStorage.setItem('token', res.headers.get('token'));
    }
    const body = res.json();
    return body || {};
  }
}
