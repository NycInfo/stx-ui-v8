import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { SetupCompanyHoursComponent } from './setupcompanyhrs.component';
import { SetupCompanyHoursRoutingModule } from './setupcompanyhours.routing';
import { ShareModule } from '../../../common/share.module';
import { FormsModule } from '@angular/forms';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
@NgModule({
    imports: [
        CommonModule,
        TranslateModule,
        SetupCompanyHoursRoutingModule,
        ShareModule,
        FormsModule,
        BsDatepickerModule.forRoot()
    ],
    declarations: [
        SetupCompanyHoursComponent
    ]
})
export class SetupCompanyHoursModule {
}
