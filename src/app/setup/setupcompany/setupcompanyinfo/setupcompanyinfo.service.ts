/*
  * Display single user details of front end users
  * extractData(): To extract the data
  * handleError(): To handle error messages.
*/
import { Injectable, Inject } from '@angular/core';
import { Response } from '@angular/http';
import { HttpClients } from '../../../common/http-client';
import { map } from 'rxjs/operators';

@Injectable()
export class SetupCompanyInfoService {
  constructor(
    private http: HttpClients,
    @Inject('apiEndPoint') private apiEndPoint: string,
    @Inject('staticJsonFilesEndPoint') private staticJsonFilesEndPoint: string
  ) { }
    createSetupCompanyData(company, companyLogo) {
    const data = new FormData();
    data.append('company', JSON.stringify(company));
    data.append('companyLogo', companyLogo);
    return this.http.post(this.apiEndPoint + '/api/setup/companies', data)
      .pipe(map(this.extractData));
  }
  timings() {
    return this.http.get(this.staticJsonFilesEndPoint + 'timings.json')
      .pipe(map(this.extractData));

  }
  booking() {
    return this.http.get(this.staticJsonFilesEndPoint + 'timings.json')
      .pipe(map(this.extractData));

  }
  getLookupsList(lookupType) {
    return this.http.get(this.apiEndPoint + '/api/v1/lookups/' + lookupType)
      .pipe(map(this.extractData));
  }
  getStates(countryName) {
    return this.http.get(this.apiEndPoint + '/api/v1/lookups/states/' + countryName)
      .pipe(map(this.extractData));
  }
  getCompanyInfo() {
    return this.http.get(this.apiEndPoint + '/api/setup/compananyinfo')
    .pipe(map(this.extractData));
  }
  editCompanyInfo(company, companyLogo, companyId) {
    const data = new FormData();
    data.append('company', JSON.stringify(company));
    data.append('companyLogo', companyLogo);
    return this.http.put(this.apiEndPoint + '/api/setup/companyinfo/' + companyId, data)
      .pipe(map(this.extractData));
  }
  postPostalcode(postalCode) {
    return this.http.get(this.apiEndPoint + '/api/setup/companies/postalCode/' + postalCode)
      .pipe(map(this.extractData));
  }

  /*To extract json data*/
  private extractData(res: Response) {
    if (res.headers && res.headers.get('token')) {
      localStorage.setItem('token', res.headers.get('token'));
    }
    const body = res.json();
    return body || {};
  }
}
