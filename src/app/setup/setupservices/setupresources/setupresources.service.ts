/*
  * Display single user details of front end users
  * extractData(): To extract the data
  * handleError(): To handle error messages.
*/
import { Injectable, Inject } from '@angular/core';
import { Response } from '@angular/http';
import { HttpClients } from '../../../common/http-client';
import { map } from 'rxjs/operators';

@Injectable()
export class SetupServiceResourceService {
  constructor(
    private http: HttpClients,
    @Inject('apiEndPoint') private apiEndPoint: string,
    @Inject('staticJsonFilesEndPoint') private staticJsonFilesEndPoint: string
  ) { }
  createSetupResourceServiceData(setupResourseDataObj) {
    return this.http.post(this.apiEndPoint + '/api/setupservices/resources', setupResourseDataObj)
      .pipe(map(this.extractData));
  }
  getSetupResourceServiceData(showInactiveRecords) {
    return this.http.get(this.apiEndPoint + '/api/setupservices/resources/' + showInactiveRecords)
      .pipe(map(this.extractData));
  }
  editSetupResourceData(editResourceServiceData, updateId) {
    return this.http.put(this.apiEndPoint + '/api/setupservices/resources/' + updateId, editResourceServiceData)
      .pipe(map(this.extractData));
  }
  /*To extract json data*/
  private extractData(res: Response) {
    if (res.headers && res.headers.get('token')) {
      localStorage.setItem('token', res.headers.get('token'));
    }
    const body = res.json();
    return body || {};
  }
}
