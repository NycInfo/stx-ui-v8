import { Component, OnInit, Inject } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { SafeUrl, DomSanitizer } from '@angular/platform-browser';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { JwtHelper } from 'angular2-jwt';
import { SetupbillboardgoalsService } from './setupbillboardgoals.service';
@Component({
    selector: 'app-setupbillboardgoals-popup',
    templateUrl: './setupbillboardgoals.html',
    styleUrls: ['./setupbillboardgoals.css'],
    providers: [SetupbillboardgoalsService]
})
export class SetupbillboardgoalsComponent implements OnInit {
    constructor(
        private toastr: ToastrService,
        private setupbillboardgoalsService: SetupbillboardgoalsService,
        private router: Router,
        private translateService: TranslateService) {
    }
    ngOnInit() {
    }
}
