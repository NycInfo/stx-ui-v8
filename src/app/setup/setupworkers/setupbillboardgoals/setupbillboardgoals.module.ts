import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SetupbillboardgoalsComponent } from './setupbillboardgoals.component';
import { SetupbillboardgoalsRoutingModule } from './setupbillboardgoals.routing';
import { TranslateModule } from '@ngx-translate/core';
import { ShareModule } from '../../../common/share.module';
import { FormsModule } from '@angular/forms';
@NgModule({
    imports: [
        CommonModule,
        SetupbillboardgoalsRoutingModule,
        TranslateModule,
        FormsModule,
        ShareModule
    ],
    declarations: [
        SetupbillboardgoalsComponent
    ]
})
export class SetupbillboardgoalsModule {
}
