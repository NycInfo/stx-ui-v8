import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SetupbillboardgoalsComponent } from './setupbillboardgoals.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '',
                component: SetupbillboardgoalsComponent,
                children: [
                    {
                        path: '',
                        component: SetupbillboardgoalsComponent
                    }
                ]
            }
        ])
    ],
    exports: [
        RouterModule
    ]
})
export class SetupbillboardgoalsRoutingModule {
}
