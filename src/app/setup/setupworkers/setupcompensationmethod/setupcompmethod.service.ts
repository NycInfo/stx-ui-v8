/*
  * Display single user details of front end users
  * extractData(): To extract the data
  * handleError(): To handle error messages.
*/
import { Injectable, Inject } from '@angular/core';
import { Response } from '@angular/http';
// import { Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { HttpClients } from '../../../common/http-client';
import { map } from 'rxjs/operators';

@Injectable()
export class SetupCompMethodService {
  constructor(
    private http: HttpClients,
    @Inject('apiEndPoint') private apiEndPoint: string,
    @Inject('staticJsonFilesEndPoint') private staticJsonFilesEndPoint: string
  ) { }
  getMethods() {
    return this.http.get(this.apiEndPoint + '/api/setupworkers/setupcompensationmethods')
    .pipe(map(this.extractData));
  }
  saveMethods(methodObj) {
    return this.http.post(this.apiEndPoint + '/api/setupworkers/setupcompensationmethods', methodObj)
    .pipe(map(this.extractData));
  }
  editMethods(updateId, updateMethodObj) {
    return this.http.put(this.apiEndPoint + '/api/setupworkers/setupcompensationmethods/' + updateId, updateMethodObj)
    .pipe(map(this.extractData));
  }
  getValues() {
    return this.http.get(this.staticJsonFilesEndPoint + 'common.json')
    .pipe(map(this.extractData));
  }
  getActions() {
    return this.http.get(this.staticJsonFilesEndPoint + 'common.json')
    .pipe(map(this.extractData));
  }
  getServices() {
    const type = 1;
    return this.http.get(this.apiEndPoint + '/api/setupservices/servicegroups/' + type)
    // return this.http.get(this.apiEndPoint + '/api/setup/setupclass/activeinactive/' + type)
    .pipe(map(this.extractData));
  }
  getscales() {
    const type = 1;
    return this.http.get(this.apiEndPoint + '/api/setupworkers/setupcompensation/' + type)
    .pipe(map(this.extractData));
  }
  getInventoryGroupData() {
    return this.http.get(this.apiEndPoint + '/api/setupinventory/inventorygroups')
    .pipe(map(this.extractData));
  }
  getProductLineDetails() {
    const type = 0;
    return this.http.get(this.apiEndPoint + '/api/setupinventory/setupproductline/' + type)
    .pipe(map(this.extractData));
  }
  getStaticServiceGroups() {
    return this.http.get(this.staticJsonFilesEndPoint + 'common.json')
    .pipe(map(this.extractData));
  }
    /*To extract json data*/
  private extractData(res: Response) {
    if (res.headers && res.headers.get('token')) {
      localStorage.setItem('token', res.headers.get('token'));
    }
    const body = res.json();
    return body || {};
  }
}
