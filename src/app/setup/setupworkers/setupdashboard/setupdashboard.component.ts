import { Component, OnInit, Inject } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { SafeUrl, DomSanitizer } from '@angular/platform-browser';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { JwtHelper } from 'angular2-jwt';
import { SetupdashboardService } from './setupdashboard.service';
@Component({
  selector: 'app-setupdashboard-popup',
  templateUrl: './setupdashboard.html',
  styleUrls: ['./setupdashboard.css'],
  providers: [SetupdashboardService]
})
export class SetupdashboardComponent implements OnInit {
  reportTypes = ['Company', 'Worker'];
  workerList = [];
  selectType: any;
  showWorkers = true;
  workerId = '';
  constructor(
    private toastr: ToastrService,
    private setupdashboardService: SetupdashboardService,
    private router: Router,
    private translateService: TranslateService) {
  }
  ngOnInit() {
    this.getWorkerList();
    this.selectType = this.reportTypes[0];
  }
  getWorkerList() {
    this.setupdashboardService.getWorkerList().subscribe(data => {
      this.workerList = [];
      this.workerList = data['result']
        .filter(filterList => filterList.IsActive);
    },
      error => {
        const status = JSON.parse(error['status']);
        const statuscode = JSON.parse(error['_body']).status;
        switch (JSON.parse(error['_body']).status) {
          case '2033':
            break;
        }
        if (statuscode === '2085' || statuscode === '2071') {
          if (this.router.url !== '/') {
            localStorage.setItem('page', this.router.url);
            this.router.navigate(['/']).then(() => { });
          }
        }
      });
  }
  reportTypeOnChange(value) {
    this.selectType = value;
    if (value === 'Worker') {
      this.showWorkers = false;
    } else {
      this.showWorkers = true;
    }
  }
  workerListOnChange(value) {
    this.workerId = value;
  }

}
