import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SetupdashboardComponent } from './setupdashboard.component';
import { SetupdashboardRoutingModule } from './setupdashboard.routing';
import { TranslateModule } from '@ngx-translate/core';
import { ShareModule } from '../../../common/share.module';
import { FormsModule } from '@angular/forms';
@NgModule({
    imports: [
        CommonModule,
        SetupdashboardRoutingModule,
        TranslateModule,
        FormsModule,
        ShareModule
    ],
    declarations: [
        SetupdashboardComponent
    ]
})
export class SetupdashboardModule {
}
