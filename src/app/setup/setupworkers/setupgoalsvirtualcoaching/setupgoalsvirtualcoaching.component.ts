import { Component, OnInit, Inject } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { SafeUrl, DomSanitizer } from '@angular/platform-browser';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { JwtHelper } from 'angular2-jwt';
import { SetupgoalsvirtualcoachingService } from './setupgoalsvirtualcoaching.service';
@Component({
    selector: 'app-setupgoalsvirtualcoaching-popup',
    templateUrl: './setupgoalsvirtualcoaching.html',
    styleUrls: ['./setupgoalsvirtualcoaching.css'],
    providers: [SetupgoalsvirtualcoachingService]
})
export class SetupgoalsvirtualcoachingComponent implements OnInit {
    constructor(
        private toastr: ToastrService,
        private setupgoalsvirtualcoachingService: SetupgoalsvirtualcoachingService,
        private router: Router,
        private translateService: TranslateService) {
    }
    ngOnInit() {
    }
}
