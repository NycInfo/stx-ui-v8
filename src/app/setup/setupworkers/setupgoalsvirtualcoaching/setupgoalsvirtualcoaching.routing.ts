import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SetupgoalsvirtualcoachingComponent } from './setupgoalsvirtualcoaching.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '',
                component: SetupgoalsvirtualcoachingComponent,
                children: [
                    {
                        path: '',
                        component: SetupgoalsvirtualcoachingComponent
                    }
                ]
            }
        ])
    ],
    exports: [
        RouterModule
    ]
})
export class SetupgoalsvirtualcoachingRoutingModule {
}
