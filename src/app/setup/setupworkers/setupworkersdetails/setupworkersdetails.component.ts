import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { SafeUrl, DomSanitizer } from '@angular/platform-browser';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { SetupWorkersDetailsService } from './setupworkersdetails.service';
import * as config from '../../../app.config';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { CommonService } from '../../../common/common.service';
import { JwtHelper } from 'angular2-jwt';
import { HttpClient } from '@angular/common/http';
import { element } from 'protractor';
@Component({
  selector: 'app-setupworkersdetails-popup',
  templateUrl: './setupworkersdetails.html',
  providers: [SetupWorkersDetailsService, CommonService],
  styleUrls: ['./setupworkersdetails.scss']
})
export class SetupWorkersDetailsComponent implements OnInit {
  allButton: boolean;
  allOlButton: boolean;
  decodedToken: any;
  bsValue: any = new Date();
  toDate: any = new Date();
  fromDate: any = new Date();
  minDate: any = new Date();
  fromDate1: any = new Date();
  customEndDate: any = new Date();
  customStartDate: any = new Date();
  inactive = false;
  // maxDate = new Date(2018, 9, 15);
  isClass: any;
  disableDiv = true;
  addDiv = false;
  hideTable = true;
  editDiv = false;
  serviceTab = false;
  showInactiveData: any;
  userList: any;
  updateId: any = '';
  activeStatus: any = false;
  workerDataAdd = false;
  firstName: any;
  middleName: any;
  lastName: any;
  email: any;
  startDate: any;
  workerPin: any;
  userName: any;
  displayOrder: any;
  password1 = '';
  password2 = '';
  glyphiconClass1 = 'glyphicon-eye-open';
  passwordType1 = 'password';
  glyphiconClass2 = 'glyphicon-eye-open';
  passwordType2 = 'password';
  apiEndPoints = config['S3_URL'];
  legalFirstName: any;
  legalMiddleName: any;
  legalLastName: any;
  street: any;
  country: any;
  zipCode: any;
  city: any;
  state: any;
  primaryPhone: any;
  mobilePhone: any;
  mobileCarrier: any;
  isSendNotificationForBookAppointment: any;
  isSendNotificationForCancelAppointment: any;
  birthMonth: any;
  birthDay: any;
  birthYear: any;
  workerNotes: any;
  emergencyName: any;
  emergencyPrimaryPhone: any;
  emergencySecondaryPhone: any;
  monthsArray: any;
  daysArray: any;
  yearsDataList: any = [];
  countries: any;
  selectedCountry: any;
  errorMessage: any;
  states: any;
  userData: any;
  userListData: any;
  profileData: any;
  carrierData: any;
  onlineValues: any;
  onlineValue: any;
  appointmentsBookEveryValue: any;
  onlineBookingHoursValue: any;
  appointmentsValue: any;
  serviceGroupList: any;
  serviceDetailsList = [];
  selectedServiceGroup: any;
  addServiceGroupName: any;
  showInactive: any;
  serviceName: any;
  serviceLevel: any;
  serviceavailable_1: any;
  serviceavailable_2: any;
  serviceavailable_3: any;
  serviceDuration_1: any;
  serviceDuration_2: any;
  serviceDuration_3: any;
  serviceData1: any;
  servicePrice: any;
  defaultPrice: any;
  defaultDur1: any;
  defaultDur2: any;
  defaultDur3: any;
  serBufferAfter: any;
  serFeeAmt: any;
  serFeePernt: any;
  serAvailable1: any;
  serAvailable2: any;
  serAvailable3: any;
  servicePriceValue: any;
  serviceBufferAfter: any;
  serviceFeePercentage: any;
  serviceFeeAmount: any;
  bookingDataList: any;
  bookingIntervalMinutes: any;
  bookingIntervalMinutesForDurations: any;
  usesTimeClockValue: any = false;
  retailOnlyValue: any = false;
  permissioneMethodValue: any;
  workerRole = '';
  workerRoledData: any = [];
  hourlyWageValue: any;
  salaryValue: any;
  compensationMethodValue: any;
  viewOnlyMyApptsValues: any;
  canViewApptValues: any;
  paymentGateWay: any;
  merchantTerminalId: any;
  merchantAccountTest: any;
  merchantAccountKey: any;
  intervalValues = [];
  workerServiceData: any = [];
  methodsListing: any;
  permissioneListing: any;
  goalsList: any;
  workerServicesData: any = [];
  priceValue: any;
  levelValues: any;
  serviceId: any;
  performs = [];
  online: any;
  test: any;
  test1: any;
  test2: any;
  error: any;
  error1: any;
  error2: any;
  error3: any;
  error4: any;
  error5: any;
  error6: any;
  error7: any;
  error8: any;
  error9: any;
  error10: any;
  goalError: any;
  goalListError1: any;
  goalListError2: any;
  pcodeErr: any;
  mcodeErr: any;
  editError: any;
  primaryCountryCode = '01';
  mobileCountryCode = '01';
  emrgencyCountryCode2 = '01';
  emrgencyCountryCode1 = '01';
  goalListError: any;
  workerServiceError: any;
  sun_start = '9:00 AM';
  sun_end = '5:00 PM';
  mon_start = '9:00 AM';
  mon_end = '5:00 PM';
  tue_start = '9:00 AM';
  tue_end = '5:00 PM';
  wed_start = '9:00 AM';
  wed_end = '5:00 PM';
  thur_start = '9:00 AM';
  thur_end = '5:00 PM';
  fri_start = '9:00 AM';
  fri_end = '5:00 PM';
  sat_start = '9:00 AM';
  sat_end = '5:00 PM';
  sun_start1 = '';
  sun_end1 = '';
  mon_start1 = '';
  mon_end1 = '';
  tue_start1 = '';
  tue_end1 = '';
  wed_start1 = '';
  wed_end1 = '';
  thur_start1 = '';
  thur_end1 = '';
  fri_start1 = '';
  fri_end1 = '';
  sat_start1 = '';
  sat_end1 = '';
  same: any = false;
  workerHours: any = {};
  workerOnlineHours: any = {};
  workerData: any;
  worker = '';
  onlinehours: any = {};
  inhouseHours: any = {};
  customList = [];
  customData: any;
  showField: any = false;
  dispalyweeks: any = false;
  sunday: any = 0;
  monday: any = 0;
  tuesday: any = 0;
  wednesday: any = 0;
  thursday: any = 0;
  friday: any = 0;
  saturday: any = 0;
  hoursStartTime: any = '';
  hoursEndTime: any = '';
  repeat: any = false;
  everyWeeks: any = 1;
  hoursNote: any = '';
  getHoursData: any = [];
  displayList: any = [];
  displayTempList: any = [];
  unqCustList: any = [];
  errorTime: any;
  popupErr: any;
  timeErr: any;
  actionPopup = 'add';
  uniqueId = '';
  showExpiry: any = false;
  disField = false;
  disField1 = false;
  // goals
  goalsRows = [];
  goalsObj: any;
  goalList: any;   // new Date();
  currentDate = new Date();
  goalStartDate: any;
  goalEndDate: any;
  addrows = false;
  previousGoals: any;
  goalListRows = false;
  goals: any;
  updateGoalId: any;
  updateWorkerId: any;
  lineCount: any;
  calculation: any;
  calculatedGoal = [];
  percentOfGoal = [];
  target = [];
  deleteWorkerGoalId: any;
  // other
  otherError: any;
  otherErr1: any;
  otherErr2: any;
  calculationList: any;
  toastermessage: any;
  toasterAction = '';
  prevUserName: any;
  // FOR DATE
  datePickerConfig: any;
  workerImage: any = {};
  image: any = '';
  serviceIndex: any;
  serviceErrorName = '';
  goallineerror = [];
  decodedUserToken: any;
  hideClientContactInfo: any = 0;
  errorDuplicateDisOrd = false;
  // newClient = false;
  /*newSerive tab */
  srvName: any;
  srvLstDisParm = 0;
  srvLstDisBtnParm = 0;
  serviceFee: any = '';
  serviceFeeType: any;
  serviceFeeType1: any;
  addOnServiceValue: any;
  addOnservice = false;
  showRemoveButton = false;
  monthData: any;
  @ViewChild('workerHoursModel', {static: false}) public workerHoursModel: ModalDirective;
  @ViewChild('workerServiceModel', {static: false}) public workerServiceModel: ModalDirective;
constructor(
    private toastr: ToastrService,
    private translateService: TranslateService,
    private setupWorkersDetailsService: SetupWorkersDetailsService,
    private router: Router,
    private sanitizer: DomSanitizer,
    private commonService: CommonService, private http: HttpClient,
    @Inject('defaultActive') public defaultActive: string,
    @Inject('defaultInActive') public defaultInActive: string) {
    this.datePickerConfig = Object.assign({},
      {
        showWeekNumbers: false,
        containerClass: 'theme-blue',
      });
  }
  ngOnInit() {
    this.getUserList();
    this.getCountries();
    this.getMobileCarriersData();
    this.getServiceLevels();
    this.hoursForOnlineData();
    this.getGoals();
    this.getBookingData();
    this.getworkerRole();
    this.getSecurityPermissionemethods();
    // this.getPreviousGoals();
    this.getCountryStates();
    this.getCountries();
    this.getYear();
    // this.mobileCarriersComponent.getMobileCarriersData();
    this.isClass = config.environment.booleanFalse;
    this.compensationMethodValue = '--None--';
    // ---Start of code for Permissions Implementation--- //
    try {
      this.decodedToken = new JwtHelper().decodeToken(localStorage.getItem('rights'));
      this.decodedUserToken = new JwtHelper().decodeToken(localStorage.getItem('token'));
    } catch (error) {
      this.decodedToken = {};
      this.decodedUserToken = {};
    }
    if (this.decodedToken.data && this.decodedToken.data.permissions) {
      this.decodedToken = JSON.parse(this.decodedToken.data.permissions);
    } else {
      this.decodedToken = {};
    }
    // ---End of code for permissions Implementation--- //
    // hours methods starts//
    this.customweekList();
  }
  cancel() {
    this.disableDiv = true;
    this.addDiv = false;
    this.hideTable = true;
    this.addrows = false;
    this.goalListRows = false;
    this.goalList = [];
    this.goalsRows = [];
    this.workerDataAdd = false;
    this.previousGoals = '';
    this.clearErrMsg();
    this.clear();
    this.workerImage = {};
    this.same = false;
    this.sun_start1 = '';
    this.sun_end1 = '';
    this.mon_start1 = '';
    this.mon_end1 = '';
    this.tue_start1 = '';
    this.tue_end1 = '';
    this.wed_start1 = '';
    this.wed_end1 = '';
    this.thur_start1 = '';
    this.thur_end1 = '';
    this.fri_start1 = '';
    this.fri_end1 = '';
    this.sat_start1 = '';
    this.sat_end1 = '';
    this.displayList = [];
    this.displayTempList = [];
    this.hoursNote = '';
    this.same = '';
    this.sun_start = '';
    this.sun_end = '';
    this.mon_start = '';
    this.mon_end = '';
    this.tue_start = '';
    this.tue_end = '';
    this.wed_start = '';
    this.wed_end = '';
    this.thur_start = '';
    this.thur_end = '';
    this.fri_start = '';
    this.fri_end = '';
    this.sat_start = '';
    this.sat_end = '';
    this.sun_start1 = '';
    this.sun_end1 = '';
    this.mon_start1 = '';
    this.mon_end1 = '';
    this.tue_start1 = '';
    this.tue_end1 = '';
    this.wed_start1 = '';
    this.wed_end1 = '';
    this.thur_start1 = '';
    this.thur_end1 = '';
    this.fri_start1 = '';
    this.fri_end1 = '';
    this.sat_start1 = '';
    this.sat_end1 = '';
    this.showExpiry = false;
  }
  /*Method to clear all fields */
  clear() {
    this.activeStatus = '';
    this.firstName = '';
    this.lastName = '';
    this.middleName = '';
    this.email = '';
    this.bsValue = '';
    this.workerPin = '';
    this.userName = '';
    this.displayOrder = '';
    this.legalFirstName = '';
    this.legalMiddleName = '';
    this.legalLastName = '';
    this.street = '';
    this.country = '';
    this.zipCode = '';
    this.city = '';
    this.state = '';
    this.primaryPhone = '';
    this.mobilePhone = '';
    this.mobileCarrier = '';
    this.isSendNotificationForBookAppointment = '';
    this.isSendNotificationForCancelAppointment = '';
    this.birthDay = '';
    this.birthMonth = '';
    this.birthYear = '';
    this.workerNotes = '';
    this.emergencyName = '';
    this.emergencyPrimaryPhone = '';
    this.emergencySecondaryPhone = '';
    this.onlineBookingHoursValue = '';
    this.appointmentsValue = '';
    this.serviceLevel = this.levelValues.length > 0 ? this.levelValues[0].level : undefined;
    this.usesTimeClockValue = '';
    this.retailOnlyValue = '';
    this.hourlyWageValue = '';
    this.salaryValue = '';
    this.viewOnlyMyApptsValues = '';
    this.permissioneMethodValue = '';
    this.workerRole = '';
    this.compensationMethodValue = '';
    this.canViewApptValues = '';
    this.paymentGateWay = '';
    this.merchantTerminalId = '';
    this.merchantAccountTest = '';
    this.merchantAccountKey = '';
    this.appointmentsBookEveryValue = '';
    this.password1 = '';
    this.password2 = '';
    this.serviceDetailsList = [];
    this.goals = [];
    this.sun_start = '';
    this.sun_end = '';
    this.mon_start = '';
    this.mon_end = '';
    this.tue_start = '';
    this.tue_end = '';
    this.wed_start = '';
    this.wed_end = '';
    this.thur_start = '';
    this.thur_end = '';
    this.fri_start = '';
    this.fri_end = '';
    this.sat_start = '';
    this.sat_end = '';
    this.sun_start1 = '';
    this.sun_end1 = '';
    this.mon_start1 = '';
    this.mon_end1 = '';
    this.tue_start1 = '';
    this.tue_end1 = '';
    this.wed_start1 = '';
    this.wed_end1 = '';
    this.thur_start1 = '';
    this.thur_end1 = '';
    this.fri_start1 = '';
    this.fri_end1 = '';
    this.sat_start1 = '';
    this.sat_end1 = '';
    this.showExpiry = false;
    this.hideClientContactInfo = 0;
    this.prevUserName = '';
  }
  getUserList() {
    this.setupWorkersDetailsService.getUserList()
      .subscribe(data => {
        this.userListData = data['result'];
        this.userList = this.userListData.filter(
          filterList => filterList.IsActive === 1);
        // .filter(
        //   filterList => filterList.IsActive === 1);
      },
        error => {
          const errStatus = JSON.parse(error['_body'])['status'];
          if (errStatus === '2085' || errStatus === '2071') {
            if (this.router.url !== '/') {
              localStorage.setItem('page', this.router.url);
              this.router.navigate(['/']).then(() => { });
            }
          }
        });
  }
  showInActive() {
    if (this.inactive === false) {
      this.userList = this.userListData;
    } else if (this.inactive === true) {
      this.userList = this.userListData.filter(
        filterList => filterList.IsActive === 1);
    }
  }
  getCountriesList(value) {
    this.country = value;
    this.getCountryStates();
  }
  getStatesOnChange(value) {
    this.state = value;
  }
  getMobileCarriersOnChange(value) {
    this.mobileCarrier = value;
  }
  compensationMethodOnChange(value) {
    this.compensationMethodValue = value;
  }
  onChangePermissioneSet(value) {
    this.permissioneMethodValue = value;
  }
  onChangeWorkerRole(value) {
    this.workerRole = value;
  }
  getworkerRole() {
    this.setupWorkersDetailsService.getWorekrRoleData().subscribe(
      data => {
        this.workerRoledData = data['workerRole'];
        this.workerRole = this.workerRoledData[0].value;
      },
      error => {
        const errStatus = JSON.parse(error['_body'])['status'];
        if (errStatus === '2085' || errStatus === '2071') {
          if (this.router.url !== '/') {
            localStorage.setItem('page', this.router.url);
            this.router.navigate(['/']).then(() => { });
          }
        }
      });
  }
  getCountryStates() {
    this.state = '';
    this.setupWorkersDetailsService.getStates(this.country)
      .subscribe(statesValues => {
        this.states = statesValues['result'];
        if (this.states.length > 0) {
          this.state = this.states[0].STATE;
        }
      },
        error => {
          this.errorMessage = <any>error;
          const errStatus = JSON.parse(error['_body'])['status'];
          if (errStatus === '2085' || errStatus === '2071') {
            if (this.router.url !== '/') {
              localStorage.setItem('page', this.router.url);
              this.router.navigate(['/']).then(() => { });
            }
          }
        });
  }
  getCountries() {
    this.setupWorkersDetailsService.getLookupsList('COUNTRIES').subscribe(
      data => {
        this.countries = data['result'];
        this.country = this.countries[0].NAME;
      },
      error => {
        const status = JSON.parse(error['status']);
        const statuscode = JSON.parse(error['_body']).status;
        switch (status) {
          case 500:
            break;
          case 400:
            if (statuscode === '9961') {
            }
            break;
        }
        if (statuscode === '2085' || statuscode === '2071') {
          if (this.router.url !== '/') {
            localStorage.setItem('page', this.router.url);
            this.router.navigate(['/']).then(() => { });
          }
        }
      }
    );
  }

  imageUpload(event) {
    if (this.workerImage.hasOwnProperty('error')) {
      delete this.workerImage.error;
    }
    const eventObj: MSInputMethodContext = <MSInputMethodContext> event;
    const target: HTMLInputElement = <HTMLInputElement> eventObj.target;
    const files: FileList = event.target.files;
    if (files.length > 0) {
      this.workerImage.file = files[0];
      this.workerImage.fileName = files[0].name;
      const fSize = this.workerImage.file.size;
      if (!this.workerImage.fileName.match(/.(jpg|jpeg|png|gif|svg|bmp)$/i)) {
        this.workerImage.error = 'COMMON_STATUS_CODES.2058';
        delete this.workerImage.file;
      } else if (fSize > 1000000) {
        this.workerImage.allowableSize = false;
        this.workerImage.error = 'SETUPCOMPANY.VALID_IMAGE_FILENAME';
        delete this.workerImage.file;
      } else {
        if (!this.image) {
          this.image = this.workerImage.fileName;
        }
        this.workerImage.srcURL = this.apiEndPoints + this.image + '?time=' + new Date().getTime();
        this.workerImage.allowableSize = true;
        this.workerImage.srcURL = this.sanitizer.bypassSecurityTrustUrl(window.URL.createObjectURL(this.workerImage.file));
      }
      if (this.workerImage.file || this.image) {
        this.showRemoveButton = true;
      } else {
        this.showRemoveButton = false;
      }
    }
  }

  showData(workerData) {
    // this.newClient = false;
    this.clearData();
    this.serviceTab = false;
    // this.workerImage = {};
    this.disableDiv = false;
    this.addDiv = true;
    this.hideTable = false;
    this.getServiceGroupsList();
    this.birthDateAndDays(workerData.Birth_Month__c);
    this.userHours(workerData.Id);
    this.getCompansationMethods();
    if (workerData === true) {
      this.workerDataAdd = true;
      this.appointmentsBookEveryValue = this.bookingIntervalMinutesForDurations;
    } else {
      this.appointmentsBookEveryValue = workerData.Book_Every__c;
      this.updateId = workerData.Id;
      this.activeStatus = workerData.IsActive;
      this.firstName = workerData.FirstName;
      this.lastName = workerData.LastName;
      this.middleName = workerData.MiddleName;
      this.image = workerData.image;
      if (this.image) {
        this.showRemoveButton = true;
      } else {
        this.showRemoveButton = false;
      }
      if (workerData.image) {
        this.workerImage.srcURL = this.apiEndPoints + workerData.image + '?time=' + new Date().getTime();
      }
      if (this.middleName === 'null' || this.middleName === null) {
        this.middleName = '';
      }
      this.email = workerData.Email;
      this.bsValue = this.commonService.getDateFrmDBDateStr(workerData.StartDay);
      this.userName = workerData.Username;
      this.prevUserName = workerData.Username;
      this.displayOrder = workerData.Display_Order__c;
      if (this.displayOrder === null || this.displayOrder === 0) {
        this.displayOrder = '';
      }
      this.password1 = '';
      this.password2 = '';
      this.legalFirstName = workerData.Legal_First_Name__c;
      if (this.legalFirstName === 'null' || this.legalFirstName === null) {
        this.legalFirstName = '';
      }
      this.legalMiddleName = workerData.Legal_Middle_Name__c;
      if (this.legalMiddleName === 'null' || this.legalMiddleName === null) {
        this.legalMiddleName = '';
      }
      this.legalLastName = workerData.Legal_Last_Name__c;
      if (this.legalLastName === 'null' || this.legalLastName === null) {
        this.legalLastName = '';
      }
      this.street = workerData.Street;
      if (this.street === 'null' || this.street === null) {
        this.street = '';
      }
      this.country = workerData.Country;
      this.zipCode = workerData.PostalCode;
      this.city = workerData.City;
      this.state = workerData.State;
      if (workerData.Phone) {
        this.primaryCountryCode = workerData.Phone.split('-')[0];
        this.primaryPhone = workerData.Phone.split('-')[1] + '-' + workerData.Phone.split('-')[2];
      }
      if (workerData.MobilePhone) {
        this.mobileCountryCode = workerData.MobilePhone.split('-')[0];
        this.mobilePhone = workerData.MobilePhone.split('-')[1] + '-' + workerData.MobilePhone.split('-')[2];
      }
      this.mobileCarrier = workerData.Mobile_Carrier__c;
      this.isSendNotificationForBookAppointment = workerData.Send_Notification_for_Booked_Appointment__c;
      this.isSendNotificationForCancelAppointment = workerData.Send_Notification_for_Canceled_Appt__c;
      if (workerData.Birth_Date__c) {
        this.birthDay = workerData.Birth_Date__c;
      } else {
        this.birthDay = '';
      }
      if (workerData.Birth_Month__c) {
        this.birthMonth = workerData.Birth_Month__c;
      } else {
        this.birthMonth = '';
      }
      if (workerData.Birth_Year__c) {
        this.birthYear = workerData.Birth_Year__c;
      } else {
        this.birthYear = '';
      }
      this.workerNotes = workerData.Worker_Notes__c;
      this.emergencyName = workerData.Emergency_Name__c;
      // this.emergencyPrimaryPhone = workerData.Emergency_Primary_Phone__c;
      if (workerData.Emergency_Primary_Phone__c) {
        this.emrgencyCountryCode1 = workerData.Emergency_Primary_Phone__c.split('-')[0];
        this.emergencyPrimaryPhone = workerData.Emergency_Primary_Phone__c.split('-')[1] + '-' + workerData.Emergency_Primary_Phone__c.split('-')[2];
      }
      // this.emergencySecondaryPhone = workerData.Emergency_Secondary_Phone__c;
      if (workerData.Emergency_Secondary_Phone__c) {
        this.emrgencyCountryCode2 = workerData.Emergency_Secondary_Phone__c.split('-')[0];
        this.emergencySecondaryPhone = workerData.Emergency_Secondary_Phone__c.split('-')[1] + '-' + workerData.Emergency_Secondary_Phone__c.split('-')[2];
      }
      this.permissioneMethodValue = workerData.Permission_Set__c;
      /* prasent we r storing user type column in db not confirmed till now */
      if (workerData.UserType === '' || workerData.UserType === null || workerData.UserType === undefined) {
        this.workerRole = '';
      } else {
        this.workerRole = workerData.UserType;
      }
      this.compensationMethodValue = workerData.Compensation__c;
      this.hourlyWageValue = workerData.Hourly_Wage__c;
      this.merchantTerminalId = workerData.Merchant_Account_ID__c;
      this.merchantAccountKey = workerData.Merchant_Account_Key__c;
      this.onlineBookingHoursValue = workerData.Online_Hours__c ? workerData.Online_Hours__c : '';
      this.appointmentsValue = workerData.Appointment_Hours__c ? workerData.Appointment_Hours__c : '';
      this.salaryValue = workerData.Salary__c;
      this.serviceLevel = workerData.Service_Level__c ? workerData.Service_Level__c : (this.levelValues.length > 0 ? this.levelValues[0].level : undefined);
      this.retailOnlyValue = workerData.Retail_Only__c;
      this.usesTimeClockValue = workerData.Uses_Time_Clock__c;
      this.viewOnlyMyApptsValues = workerData.View_Only_My_Appointments__c;
      this.canViewApptValues = workerData.Can_View_Appt_Values_Totals__c;
      this.workerPin = workerData.Worker_Pin__c;
      this.paymentGateWay = workerData.Payment_Gateway__c;
      this.hideClientContactInfo = workerData.Hide_Client_Contact_Info__c;
      // this.getCountryStates();
      // this.getServiceGroupsList();
      // this.birthDateAndDays();
      // this.getCompansationMethods();
      this.getWorkerServices();
      // goals
      this.getGoals();
      this.addRows();
      this.previousGoals = false;
      this.setupWorkersDetailsService.previousGoals(this.previousGoals, this.updateId, this.commonService.getDBDatStr(new Date()))
        .subscribe(data1 => {
          this.goalList = data1['result'];
          this.goalList = this.updateDateFields(this.goalList);
          this.goalsRows = [];
          if (this.goalList.length === 0) {
            this.goalsRows = [];
            this.addRows();
          }
        },
          error => {
            const errStatus = JSON.parse(error['_body'])['status'];
            if (errStatus === '2085' || errStatus === '2071') {
              this.router.navigate(['/']).then(() => { });
            }
          });
      this.goalListRows = false;
    }
    /* hours tab */
    this.workerData = this.userList.filter((obj) => obj.Id !== this.updateId);
    this.getCustomHours();
  }
  userHours(id) {
    this.setupWorkersDetailsService.getUserHours(id).subscribe(
      data => {
        data = data['result'];
        this.onlinehours = {};
        this.inhouseHours = {};
        if (data && data.length > 0) {
          data.forEach(elem => {
            if (elem.Id.split('-')[1] === 'OLB') {
              this.onlinehours = elem;
            } else {
              this.inhouseHours = elem;
            }
          });
          this.sun_start1 = this.onlinehours.SundayStartTime__c;
          this.sun_end1 = this.onlinehours.SundayEndTime__c;

          this.mon_start1 = this.onlinehours.MondayStartTime__c;
          this.mon_end1 = this.onlinehours.MondayEndTime__c;

          this.tue_start1 = this.onlinehours.TuesdayStartTime__c;
          this.tue_end1 = this.onlinehours.TuesdayEndTime__c;

          this.wed_start1 = this.onlinehours.WednesdayStartTime__c;
          this.wed_end1 = this.onlinehours.WednesdayEndTime__c;

          this.thur_start1 = this.onlinehours.ThursdayStartTime__c;
          this.thur_end1 = this.onlinehours.ThursdayEndTime__c;

          this.fri_start1 = this.onlinehours.FridayStartTime__c;
          this.fri_end1 = this.onlinehours.FridayEndTime__c;

          this.sat_start1 = this.onlinehours.SaturdayStartTime__c;
          this.sat_end1 = this.onlinehours.SaturdayEndTime__c;
          /* inhouse hours */
          this.sun_start = this.inhouseHours.SundayStartTime__c;
          this.sun_end = this.inhouseHours.SundayEndTime__c;

          this.mon_start = this.inhouseHours.MondayStartTime__c;
          this.mon_end = this.inhouseHours.MondayEndTime__c;

          this.tue_start = this.inhouseHours.TuesdayStartTime__c;
          this.tue_end = this.inhouseHours.TuesdayEndTime__c;

          this.wed_start = this.inhouseHours.WednesdayStartTime__c;
          this.wed_end = this.inhouseHours.WednesdayEndTime__c;

          this.thur_start = this.inhouseHours.ThursdayStartTime__c;
          this.thur_end = this.inhouseHours.ThursdayEndTime__c;

          this.fri_start = this.inhouseHours.FridayStartTime__c;
          this.fri_end = this.inhouseHours.FridayEndTime__c;

          this.sat_start = this.inhouseHours.SaturdayStartTime__c;
          this.sat_end = this.inhouseHours.SaturdayEndTime__c;
        }
      },
      error => {
        const errStatus = JSON.parse(error['_body'])['status'];
        if (errStatus === '2085' || errStatus === '2071') {
          if (this.router.url !== '/') {
            localStorage.setItem('page', this.router.url);
            this.router.navigate(['/']).then(() => { });
          }
        }
      }
    );

  }
  customweekList() {
    this.setupWorkersDetailsService.customHrsList().subscribe(
      data => {
        this.customList = data['customweeks'];
        this.customData = this.customList[0].name;
        this.showField = true;
      },
      error => {
        const errStatus = JSON.parse(error['_body'])['status'];
        if (errStatus === '2085' || errStatus === '2071') {
          if (this.router.url !== '/') {
            localStorage.setItem('page', this.router.url);
            this.router.navigate(['/']).then(() => { });
          }
        }
      });
  }
  checkServiceDurations(serviceData) {
    const durationKeys = ['Duration_1__c', 'Duration_2__c', 'Duration_3__c', 'Buffer_After__c'];
    const isDurationExsists = durationKeys.map((key) => serviceData[key] ? serviceData[key] : 0).findIndex((val) => (val !== 0 && val)) !== -1 ? true : false;
    if (isDurationExsists) {
      let totalDuration = 0;
      totalDuration += serviceData['Duration_1__c'] ? +serviceData['Duration_1__c'] : 0;
      totalDuration += serviceData['Duration_2__c'] ? +serviceData['Duration_2__c'] : 0;
      totalDuration += serviceData['Duration_3__c'] ? +serviceData['Duration_3__c'] : 0;
      totalDuration += serviceData['Buffer_After__c'] ? +serviceData['Buffer_After__c'] : 0;
      return (totalDuration % this.bookingIntervalMinutesForDurations !== 0) ? true : false;
    } else {
      return false;
    }
  }
  checkServiceErrOnPopupAndSave() {
    let workerData = [];
    workerData = this.workerServicesData.filter((data) => data['Removed'] === false && data['isTouched']);
    for (let i = 0; i < workerData.length; i++) {
      if (workerData[i].Service_Fee_Amount__c && workerData[i].Price__c) {
        if (+workerData[i].Service_Fee_Amount__c > +workerData[i].Price__c) {
          this.serviceErrorName = workerData[i]['Name'];
          this.workerServiceError = 'SETUP_WORKERS_DETAILS.VALID_SERVICE_FEE_AMOUNT_NOT_GREATERTHAN_SERVICE_PRICE';
          window.scrollTo(0, 0);
          break;
        }
      }
      if ((workerData[i].Service_Fee_Amount__c && workerData[i].Service_Fee_Percent__c)) {
        this.serviceErrorName = workerData[i]['Name'];
        this.workerServiceError = 'SETUP_WORKERS_DETAILS.VALID_ALLOW_EITHER_SERVICE_FEE_AMOUNT_OR_SERVICE_FEE_PERCENTAGE';
        window.scrollTo(0, 0);
        break;
      }
      if (+workerData[i].Service_Fee_Percent__c < 0 || +workerData[i].Service_Fee_Percent__c > 99 || parseFloat(workerData[i].Service_Fee_Percent__c) > 99) {
        this.serviceErrorName = workerData[i]['Name'];
        this.workerServiceError = 'SETUP_WORKERS_DETAILS.VALID_SERVICE_FEE_PERCENTAGE_LIMIT';
        window.scrollTo(0, 0);
        break;
      }
      if (this.checkServiceDurations(workerData[i])) {
        this.serviceErrorName = workerData[i]['Name'];
        this.workerServiceError = `service total duration must be a multiple of the appointment booking ${this.bookingIntervalMinutesForDurations} minute interval`;
        window.scrollTo(0, 0);
        break;
      }
    }
  }
  commonSave() {
    this.workerServiceError = '';
    this.error = '';
    /*services */
    // this.workerServiceData = this.workerServiceData.filter(function () { return true; });
    //  this.updateWorkerServiceValues();

    // removed for calculations of service durations
    // if (+workerData[i].Duration_1__c % this.appointmentsBookEveryValue !== 0) {
    //   this.serviceErrorName = workerData[i]['Name'];
    //   this.workerServiceError = 'SETUP_WORKERS_DETAILS.VALID_MULTIPLE_OF_INTERVAL_MINUTES';
    //   window.scrollTo(0, 0);
    // } if (+workerData[i].Duration_2__c % this.appointmentsBookEveryValue !== 0) {
    //   this.serviceErrorName = workerData[i]['Name'];
    //   this.workerServiceError = 'SETUP_WORKERS_DETAILS.VALID_MULTIPLE_OF_INTERVAL_MINUTES';
    //   window.scrollTo(0, 0);
    // } if (+workerData[i].Duration_3__c % this.appointmentsBookEveryValue !== 0) {
    //   this.serviceErrorName = workerData[i]['Name'];
    //   this.workerServiceError = 'SETUP_WORKERS_DETAILS.VALID_MULTIPLE_OF_INTERVAL_MINUTES';
    //   window.scrollTo(0, 0);
    // }
    this.checkServiceErrOnPopupAndSave();
    /*services end here */
    /*create goals() */
    if (this.goalList && this.goalList.length > 0) {
      for (let j = 0; j < this.goalList.length; j++) {
        if (this.goalList[j].goalsId !== '') {
          if (!this.goalList[j].hasOwnProperty('delete')) {
            if ((this.goalList[j].goalsId !== undefined || this.goalList[j].goalsId !== 'undefined' || this.goalList[j].goalsId !== '')
              && (this.goalList[j].startDate === undefined || this.goalList[j].startDate === 'undefined'
                || this.goalList[j].startDate === '' || this.goalList[j].startDate === null || this.goalList[j].startDate === 'null')
              && (this.goalList[j].endDate === undefined || this.goalList[j].endDate === 'undefined' || this.goalList[j].endDate === ''
                || this.goalList[j].endDate === null || this.goalList[j].endDate === 'null'
              )) {
              this.goalError = 'SETUP_WORKERS_DETAILS.VALID_NO_BLANK_START_AND_END_DATE';
              window.scrollTo(0, 0);
              break;
            } else if (this.goalList[j].startDate > this.goalList[j].endDate) {
              this.lineCount = j;
              this.goalListError1 = this.translateService.get('SETUP_WORKERS_DETAILS.LINE');
              this.goalListError2 = this.translateService.get('SETUP_WORKERS_DETAILS.VALID_STARTDATE_MUST_EARLIER_THAN_ENDDATE');
              this.goalListError = this.goalListError1.value + this.lineCount + ':' + this.goalListError2.value;
              window.scrollTo(0, 0);
              break;
            }
          }
        } else {
          this.goalList = [];
        }
      }
    }
    if (this.goalsRows && this.goalsRows.length > 0) {
      for (let i = 0; i < this.goalsRows.length; i++) {
        if (this.goalsRows[i].goalsId !== '') {
          if ((this.goalsRows[i].goalsId !== undefined || this.goalsRows[i].goalsId !== 'undefined' || this.goalsRows[i].goalsId !== '')
            && (this.goalsRows[i].startDate === undefined || this.goalsRows[i].startDate === 'undefined'
              || this.goalsRows[i].startDate === '' || this.goalsRows[i].startDate === null || this.goalsRows[i].startDate === 'null' || this.goalsRows[i].startDate === 'Invalid Date')
            && (this.goalsRows[i].endDate === undefined || this.goalsRows[i].endDate === 'undefined' || this.goalsRows[i].endDate === ''
              || this.goalsRows[i].endDate === null || this.goalsRows[i].endDate === 'null'
              || this.goalsRows[i].endDate === 'Invalid Date')) {
            this.goalError = 'SETUP_WORKERS_DETAILS.VALID_NO_BLANK_START_AND_END_DATE';
            window.scrollTo(0, 0);
            break;
          } else if (this.goalsRows[i].startDate > this.goalsRows[i].endDate) {
            this.goalError = 'SETUP_WORKERS_DETAILS.VALID_STARTDATE_MUST_EARLIER_THAN_ENDDATE';
            window.scrollTo(0, 0);
            break;
          }
        } else {
          this.goalsRows = [];
        }
      }
    } /*creategoals end here */
    // WorkerProfile  AND other tab validations
    // tslint:disable-next-line:max-line-length
    //   const NUM_REGEXP = /^^(0|[1-9][0-9]*)$/;
    this.getDuplicateDisOrd(this.displayOrder, this.updateId);
    const NUM_REGEXP = /^[0-9]$/;
    const EMAIL_REGEXP = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    const TIME_REGEXP = /^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9][ ][AP][M]$/;
    if (this.decodedUserToken['data']['id'] === this.updateId && !this.activeStatus) {
      this.error = 'creating/updating worker. To self-deactivate, the org must enable the user self-deactivation setting and the usertype must be external.';
      window.scrollTo(0, 0);
    } else if (this.firstName === '' || this.firstName === undefined) {
      this.error = 'SETUP_WORKERS_DETAILS.VALID_NOBLANK_FIRST_NAME_FIELD';
      window.scrollTo(0, 0);
    } else if (this.lastName === '' || this.lastName === undefined) {
      this.error1 = 'SETUP_WORKERS_DETAILS.VALID_NOBLANK_LAST_NAME_FIELD';
      window.scrollTo(0, 0);
    } else if (this.email === '' || this.email === undefined) {
      this.error3 = 'SETUP_WORKERS_DETAILS.VALID_NOBLANK_EMAIL_FIELD';
      window.scrollTo(0, 0);
    } else if (this.email !== '' && !EMAIL_REGEXP.test(this.email)) {
      this.error3 = 'COMMON.VALID_EMAIL_ID_FORMAT';
      window.scrollTo(0, 0);
    } else if (this.bsValue === '' || this.bsValue === undefined || this.bsValue === null) {
      this.error6 = 'SETUP_WORKERS_DETAILS.VALID_NOBLANK_START_DATE_FIELD';
      window.scrollTo(0, 0);
    } else if (this.displayOrder < 0) {
      this.error7 = 'SETUP_WORKERS_DETAILS.DISPLAY_ORDER_ONLY_POSITIVE';
      window.scrollTo(0, 0);
    } else if (this.errorDuplicateDisOrd) {
      this.error7 = 'SETUP_WORKERS_DETAILS.VALID_NOBLANK_DISPLAY_ORDER';
      window.scrollTo(0, 0);
      // } else if (this.workerPin === '') {
      //   this.error4 = 'SETUP_WORKERS_DETAILS.VALID_NOBLANK_WORKER_PIN_FIELD';
      //   window.scrollTo(0, 0);
      // } else if (this.workerPin.toString().length !== 4) {
      //   this.error4 = 'SETUP_WORKERS_DETAILS.VALID_WORK_PIN_LIMIT';
      //   window.scrollTo(0, 0);
    } else if (this.userName === '' || this.userName === undefined) {
      this.error2 = 'SETUP_WORKERS_DETAILS.VALID_NOBLANK_USER_NAME_FIELD';
      window.scrollTo(0, 0);
    } else if (this.userName !== '' && !EMAIL_REGEXP.test(this.userName)) {
      this.error2 = 'SETUP_WORKERS_DETAILS.VALID_WORK_USERNAME_FEILD';
      window.scrollTo(0, 0);
    } else if (this.password1.length > 0 && this.password2.length === 0) {
      this.error8 = 'COMMON.RETYPE_PASSWORD_VALID';
      window.scrollTo(0, 0);
    } else if (this.password1.length > 0 && this.password2.length > 0 && this.password1 !== this.password2) {
      this.error8 = 'COMMON.PASSWORD_MISMATCH';
      window.scrollTo(0, 0);
    } else if (this.primaryPhone !== '' && this.primaryPhone !== undefined && this.primaryCountryCode === '') {
      this.pcodeErr = 'SETUP_WORKERS_DETAILS.COUNTRY_CODE_REQUIRED_FOR_PRIMARY_PHONE';
      window.scrollTo(0, 0);
    } else if (this.primaryPhone && this.primaryPhone.length !== 13) {
      this.error = 'SETUP_WORKERS_DETAILS.INVALID_PRIMARY_PHONE';
      window.scrollTo(0, 0);
    } else if (this.mobilePhone !== '' && this.mobilePhone !== undefined && this.mobileCountryCode === '') {
      this.mcodeErr = 'SETUP_WORKERS_DETAILS.COUNTRY_CODE_REQUIRED_FOR_MOBILE_PHONE';
      window.scrollTo(0, 0);
    } else if (this.mobilePhone && this.mobilePhone.length !== 13) {
      this.error = 'SETUP_WORKERS_DETAILS.INVALID_MOBILE_PHONE';
      window.scrollTo(0, 0);
    } else if (!this.birthDay && (this.birthMonth)) {
      this.error10 = 'SETUP_WORKERS_DETAILS.INVALID_BIRTH_DAY';
      window.scrollTo(0, 0);
    } else if (!this.birthMonth && (this.birthDay)) {
      this.error10 = 'SETUP_WORKERS_DETAILS.INVALID_BIRTH_MONTH';
      window.scrollTo(0, 0);
    } else if (this.emergencyPrimaryPhone !== '' && this.emergencyPrimaryPhone !== undefined && this.emrgencyCountryCode1 === '') {
      this.pcodeErr = 'SETUP_WORKERS_DETAILS.COUNTRYCODE_REQUIRED_FOR_EMERGENCY_PRIMARY_PHONE';
      window.scrollTo(0, 0);
    } else if (this.emergencyPrimaryPhone && this.emergencyPrimaryPhone.length !== 13) {
      this.error = 'SETUP_WORKERS_DETAILS.INVALID_EMERGENCY_PRIMARY_PHONE';
      window.scrollTo(0, 0);
    } else if (this.emergencySecondaryPhone !== '' && this.emergencySecondaryPhone !== undefined && this.emrgencyCountryCode2 === '') {
      this.mcodeErr = 'SETUP_WORKERS_DETAILS.COUNTRYCODE_REQUIRED_FOR_EMERGENCY_SECONDRY_PHONE';
      window.scrollTo(0, 0);
    } else if (this.emergencySecondaryPhone && this.emergencySecondaryPhone.length !== 13) {
      this.error = 'SETUP_WORKERS_DETAILS.INVALID_EMERGENCY_SECONDRY_PHONE';
      window.scrollTo(0, 0);
      // } else if ((this.serviceLevel === '' || this.serviceLevel === '0' || this.serviceLevel === 0
      //   || this.serviceLevel === undefined || this.serviceLevel === 'None') && this.workerServiceData && this.workerServiceData.length > 0) {
    } else if (this.hourlyWageValue < 0 || this.salaryValue < 0) {
      this.otherError = 'SETUP_WORKERS_DETAILS.VALID_HOURLY_WAGE_AND_SALARY_FIELDS_ZERO_OR_POSITIVE';
    } else if (this.hourlyWageValue > 9999.999) {
      this.otherError = 'SETUP_WORKERS_DETAILS.VALID_HOURLY_WAGE_FIELDS_LIMIT';
    } else if (this.salaryValue > 9999999.999) {
      this.otherError = 'SETUP_WORKERS_DETAILS.VALID_SALARY_FIELDS_LIMIT';
    } else if ((this.merchantTerminalId !== '' && this.merchantTerminalId !== null) && (this.merchantAccountKey === '' || this.merchantAccountKey === null)) {
      this.otherErr1 = 'VALIDATION_MSG.INVALID_MERCHANT_KEY';
      window.scrollTo(0, 0);
    } else if (this.merchantAccountKey !== '' && (this.merchantTerminalId === '' || this.merchantTerminalId === null)) {
      this.otherErr2 = 'VALIDATION_MSG.INVALID_MERCHANT_TERMINAL_ID';
      window.scrollTo(0, 0);
    } else if ((this.sun_start !== '' && this.sun_end === '') || (this.sun_end !== '' && this.sun_start === '')) {
      this.errorTime = 'SETUP_COMPANY_HOURSE.VALID_SUNDAY_START_TIMES_END_TIMES_NOBLANK';
      window.scrollTo(0, 0);
    } else if ((this.mon_start !== '' && this.mon_end === '') || (this.mon_end !== '' && this.mon_start === '')) {
      this.errorTime = 'SETUP_COMPANY_HOURSE.VALID_MONDAY_START_TIMES_END_TIMES_NOBLANK';
      window.scrollTo(0, 0);
    } else if ((this.tue_start !== '' && this.tue_end === '') || (this.tue_end !== '' && this.tue_start === '')) {
      this.errorTime = 'SETUP_COMPANY_HOURSE.VALID_TUESDAY_START_TIMES_END_TIMES_NOBLANK';
      window.scrollTo(0, 0);
    } else if ((this.wed_start !== '' && this.wed_end === '') || (this.wed_end !== '' && this.wed_start === '')) {
      this.errorTime = 'SETUP_COMPANY_HOURSE.VALID_WEDNESDAY_START_TIMES_END_TIMES_NOBLANK';
      window.scrollTo(0, 0);
    } else if ((this.thur_start !== '' && this.thur_end === '') || (this.thur_end !== '' && this.thur_start === '')) {
      this.errorTime = 'SETUP_COMPANY_HOURSE.VALID_THURSDAY_START_TIMES_END_TIMES_NOBLANK';
      window.scrollTo(0, 0);
    } else if ((this.fri_start !== '' && this.fri_end === '') || (this.fri_end !== '' && this.fri_start === '')) {
      this.errorTime = 'SETUP_COMPANY_HOURSE.VALID_FRIDAY_START_TIMES_END_TIMES_NOBLANK';
      window.scrollTo(0, 0);
    } else if ((this.sat_start !== '' && this.sat_end === '') || (this.sat_end !== '' && this.sat_start === '')) {
      this.errorTime = 'SETUP_COMPANY_HOURSE.VALID_SATURDAY_START_TIMES_END_TIMES_NOBLANK';
      window.scrollTo(0, 0);
    } else if (this.sun_start !== '' && !TIME_REGEXP.test(this.sun_start)) {
      this.errorTime = 'SETUP_COMPANY_HOURSE.VALID_SUNDAY_TIME_FORMAT';
      window.scrollTo(0, 0);
    } else if (this.sun_end !== '' && !TIME_REGEXP.test(this.sun_end)) {
      this.errorTime = 'SETUP_COMPANY_HOURSE.VALID_SUNDAY_TIME_FORMAT';
      window.scrollTo(0, 0);
    } else if (this.mon_start !== '' && !TIME_REGEXP.test(this.mon_start)) {
      this.errorTime = 'SETUP_COMPANY_HOURSE.VALID_MONDAY_TIME_FORMAT';
      window.scrollTo(0, 0);
    } else if (this.mon_end !== '' && !TIME_REGEXP.test(this.mon_end)) {
      this.errorTime = 'SETUP_COMPANY_HOURSE.VALID_MONDAY_TIME_FORMAT';
      window.scrollTo(0, 0);
    } else if (this.tue_start !== '' && !TIME_REGEXP.test(this.tue_start)) {
      this.errorTime = 'SETUP_COMPANY_HOURSE.VALID_TUESDAY_TIME_FORMAT';
      window.scrollTo(0, 0);
    } else if (this.tue_end !== '' && !TIME_REGEXP.test(this.tue_end)) {
      this.errorTime = 'SETUP_COMPANY_HOURSE.VALID_TUESDAY_TIME_FORMAT';
      window.scrollTo(0, 0);
    } else if (this.wed_start !== '' && !TIME_REGEXP.test(this.wed_start)) {
      this.errorTime = 'SETUP_COMPANY_HOURSE.VALID_WEDNESDAY_TIME_FORMAT';
      window.scrollTo(0, 0);
    } else if (this.wed_end !== '' && !TIME_REGEXP.test(this.wed_end)) {
      this.errorTime = 'SETUP_COMPANY_HOURSE.VALID_WEDNESDAY_TIME_FORMAT';
      window.scrollTo(0, 0);
    } else if (this.thur_start !== '' && !TIME_REGEXP.test(this.thur_start)) {
      this.errorTime = 'SETUP_COMPANY_HOURSE.VALID_THURSDAY_TIME_FORMAT';
      window.scrollTo(0, 0);
    } else if (this.thur_end !== '' && !TIME_REGEXP.test(this.thur_end)) {
      this.errorTime = 'SETUP_COMPANY_HOURSE.VALID_THURSDAY_TIME_FORMAT';
      window.scrollTo(0, 0);
    } else if (this.fri_start !== '' && !TIME_REGEXP.test(this.fri_start)) {
      this.errorTime = 'SETUP_COMPANY_HOURSE.VALID_FRIDAY_TIME_FORMAT';
      window.scrollTo(0, 0);
    } else if (this.fri_end !== '' && !TIME_REGEXP.test(this.fri_end)) {
      this.errorTime = 'SETUP_COMPANY_HOURSE.VALID_FRIDAY_TIME_FORMAT';
      window.scrollTo(0, 0);
    } else if (this.sat_start !== '' && !TIME_REGEXP.test(this.sat_start)) {
      this.errorTime = 'SETUP_COMPANY_HOURSE.VALID_SATURDAY_TIME_FORMAT';
      window.scrollTo(0, 0);
    } else if (this.sat_end !== '' && !TIME_REGEXP.test(this.sat_end)) {
      this.errorTime = 'SETUP_COMPANY_HOURSE.VALID_SATURDAY_TIME_FORMAT';
      window.scrollTo(0, 0);
    } else if ((this.sun_start1 !== '' && this.sun_end1 === '') || (this.sun_end1 !== '' && this.sun_start1 === '')) {
      this.errorTime = 'SETUP_COMPANY_HOURSE.VALID_SUNDAY_START_TIMES_END_TIMES_NOBLANK';
      window.scrollTo(0, 0);
    } else if ((this.mon_start1 !== '' && this.mon_end1 === '') || (this.mon_end1 !== '' && this.mon_start1 === '')) {
      this.errorTime = 'SETUP_COMPANY_HOURSE.VALID_MONDAY_START_TIMES_END_TIMES_NOBLANK';
      window.scrollTo(0, 0);
    } else if ((this.tue_start1 !== '' && this.tue_end1 === '') || (this.tue_end1 !== '' && this.tue_start1 === '')) {
      this.errorTime = 'SETUP_COMPANY_HOURSE.VALID_TUESDAY_START_TIMES_END_TIMES_NOBLANK';
      window.scrollTo(0, 0);
    } else if ((this.wed_start1 !== '' && this.wed_end1 === '') || (this.wed_end1 !== '' && this.wed_start1 === '')) {
      this.errorTime = 'SETUP_COMPANY_HOURSE.VALID_WEDNESDAY_START_TIMES_END_TIMES_NOBLANK';
      window.scrollTo(0, 0);
    } else if ((this.thur_start1 !== '' && this.thur_end1 === '') || (this.thur_end1 !== '' && this.thur_start1 === '')) {
      this.errorTime = 'SETUP_COMPANY_HOURSE.VALID_THURSDAY_START_TIMES_END_TIMES_NOBLANK';
      window.scrollTo(0, 0);
    } else if ((this.fri_start1 !== '' && this.fri_end1 === '') || (this.fri_end1 !== '' && this.fri_start1 === '')) {
      this.errorTime = 'SETUP_COMPANY_HOURSE.VALID_FRIDAY_START_TIMES_END_TIMES_NOBLANK';
      window.scrollTo(0, 0);
    } else if ((this.sat_start1 !== '' && this.sat_end1 === '') || (this.sat_end1 !== '' && this.sat_start1 === '')) {
      this.errorTime = 'SETUP_COMPANY_HOURSE.VALID_SATURDAY_START_TIMES_END_TIMES_NOBLANK';
      window.scrollTo(0, 0);
    } else if (this.sun_start1 && !TIME_REGEXP.test(this.sun_start1)) {
      this.errorTime = 'SETUP_COMPANY_HOURSE.VALID_SUNDAY_TIME_FORMAT';
      window.scrollTo(0, 0);
    } else if (this.sun_end1 && !TIME_REGEXP.test(this.sun_end1)) {
      this.errorTime = 'SETUP_COMPANY_HOURSE.VALID_SUNDAY_TIME_FORMAT';
      window.scrollTo(0, 0);
    } else if (this.mon_start1 && !TIME_REGEXP.test(this.mon_start1)) {
      this.errorTime = 'SETUP_COMPANY_HOURSE.VALID_MONDAY_TIME_FORMAT';
      window.scrollTo(0, 0);
    } else if (this.mon_end1 && !TIME_REGEXP.test(this.mon_end1)) {
      this.errorTime = 'SETUP_COMPANY_HOURSE.VALID_MONDAY_TIME_FORMAT';
      window.scrollTo(0, 0);
    } else if (this.tue_start1 && !TIME_REGEXP.test(this.tue_start1)) {
      this.errorTime = 'SETUP_COMPANY_HOURSE.VALID_TUESDAY_TIME_FORMAT';
      window.scrollTo(0, 0);
    } else if (this.tue_end1 && !TIME_REGEXP.test(this.tue_end1)) {
      this.errorTime = 'SETUP_COMPANY_HOURSE.VALID_TUESDAY_TIME_FORMAT';
      window.scrollTo(0, 0);
    } else if (this.wed_start1 && !TIME_REGEXP.test(this.wed_start1)) {
      this.errorTime = 'SETUP_COMPANY_HOURSE.VALID_WEDNESDAY_TIME_FORMAT';
      window.scrollTo(0, 0);
    } else if (this.wed_end1 && !TIME_REGEXP.test(this.wed_end1)) {
      this.errorTime = 'SETUP_COMPANY_HOURSE.VALID_WEDNESDAY_TIME_FORMAT';
      window.scrollTo(0, 0);
    } else if (this.thur_start1 && !TIME_REGEXP.test(this.thur_start1)) {
      this.errorTime = 'SETUP_COMPANY_HOURSE.VALID_THURSDAY_TIME_FORMAT';
      window.scrollTo(0, 0);
    } else if (this.thur_end1 && !TIME_REGEXP.test(this.thur_end1)) {
      this.errorTime = 'SETUP_COMPANY_HOURSE.VALID_THURSDAY_TIME_FORMAT';
      window.scrollTo(0, 0);
    } else if (this.fri_start1 && !TIME_REGEXP.test(this.fri_start1)) {
      this.errorTime = 'SETUP_COMPANY_HOURSE.VALID_FRIDAY_TIME_FORMAT';
      window.scrollTo(0, 0);
    } else if (this.fri_end1 && !TIME_REGEXP.test(this.fri_end1)) {
      this.errorTime = 'SETUP_COMPANY_HOURSE.VALID_FRIDAY_TIME_FORMAT';
      window.scrollTo(0, 0);
    } else if (this.sat_start1 && !TIME_REGEXP.test(this.sat_start1)) {
      this.errorTime = 'SETUP_COMPANY_HOURSE.VALID_SATURDAY_TIME_FORMAT';
      window.scrollTo(0, 0);
    } else if (this.sat_end1 && !TIME_REGEXP.test(this.sat_end1)) {
      this.errorTime = 'SETUP_COMPANY_HOURSE.VALID_SATURDAY_TIME_FORMAT';
      window.scrollTo(0, 0);
    } else if (this.prevUserName && ((this.prevUserName !== this.userName) &&
      (this.prevUserName.toLowerCase() === this.userName.toLowerCase()))) {
      this.errorTime = 'SETUP_WORKERS_DETAILS.VALID_USERNAME';
      window.scrollTo(0, 0);
    } else {
      if (this.activeStatus === undefined || this.activeStatus === false) {
        this.activeStatus = this.defaultInActive;
      }
      if (this.activeStatus === true) {
        this.activeStatus = this.defaultActive;
      }
      if (!this.isSendNotificationForBookAppointment || this.isSendNotificationForBookAppointment === false) {
        this.isSendNotificationForBookAppointment = this.defaultInActive;
      }
      if (this.isSendNotificationForBookAppointment === true) {
        this.isSendNotificationForBookAppointment = this.defaultActive;
      }
      if (!this.isSendNotificationForCancelAppointment || this.isSendNotificationForCancelAppointment === false) {
        this.isSendNotificationForCancelAppointment = this.defaultInActive;
      }
      if (this.isSendNotificationForCancelAppointment === true) {
        this.isSendNotificationForCancelAppointment = this.defaultActive;
      }
      if (!this.usesTimeClockValue || this.usesTimeClockValue === false) {
        this.usesTimeClockValue = this.defaultInActive;
      }
      if (this.usesTimeClockValue === true) {
        this.usesTimeClockValue = this.defaultActive;
      }
      if (!this.retailOnlyValue || this.retailOnlyValue === false) {
        this.retailOnlyValue = this.defaultInActive;
      }
      if (this.retailOnlyValue === true) {
        this.retailOnlyValue = this.defaultActive;
      }
      if (!this.canViewApptValues || this.canViewApptValues === undefined || this.canViewApptValues === false) {
        this.canViewApptValues = this.defaultInActive;
      }
      if (this.canViewApptValues === true) {
        this.canViewApptValues = this.defaultActive;
      }
      if (!this.viewOnlyMyApptsValues || this.viewOnlyMyApptsValues === false) {
        this.viewOnlyMyApptsValues = this.defaultInActive;
      }
      if (this.viewOnlyMyApptsValues === true) {
        this.viewOnlyMyApptsValues = this.defaultActive;
      }
      if (!this.merchantAccountTest || this.merchantAccountTest === undefined || this.merchantAccountTest === false) {
        this.merchantAccountTest = this.defaultInActive;
      }
      if (this.merchantAccountTest === true) {
        this.merchantAccountTest = this.defaultActive;
      } else if ((this.workerServiceError === '' || this.workerServiceError === 'undefined' || this.workerServiceError === undefined)
        && (this.goalListError === '' || this.goalListError === 'undefined' || this.goalListError === undefined)
        && (this.goalError === '' || this.goalError === 'undefined' || this.goalError === undefined)
        && (this.error === undefined || this.error === 'undefined' || this.error === '') &&
        (this.otherError === undefined || this.otherError === '' || this.otherError === 'undefined')) {
        if (this.displayOrder === null || this.displayOrder === 0) {
          this.displayOrder = '';
        }
        this.userData = {
          'activeStatus': this.activeStatus,
          'firstName': this.firstName,
          'lastName': this.lastName,
          'middleName': this.middleName,
          'email': this.email,
          'startDate': this.commonService.getDBDatStr(this.bsValue),
          'workerPin': this.workerPin,
          'userName': this.userName,
          'displayOrder': this.displayOrder,
          'legalFirstName': this.legalFirstName,
          'legalMiddleName': this.legalMiddleName,
          'legalLastName': this.legalLastName,
          'street': this.street,
          'country': this.country,
          'zipCode': this.zipCode,
          'city': this.city,
          'state': this.state,
          'primaryPhone': this.primaryPhone !== '' && this.primaryPhone ? this.primaryCountryCode + '-' + this.primaryPhone : '',
          'mobilePhone': this.mobilePhone !== '' && this.mobilePhone ? this.mobileCountryCode + '-' + this.mobilePhone : '',
          'mobileCarrier': this.mobileCarrier,
          'isSendNotificationForBookAppointment': this.isSendNotificationForBookAppointment,
          'isSendNotificationForCancelAppointment': this.isSendNotificationForCancelAppointment,
          'birthDay': this.birthDay,
          'birthMonth': this.birthMonth,
          'birthYear': this.birthYear,
          'workerNotes': this.workerNotes,
          'emergencyName': this.emergencyName,
          'emergencyPrimaryPhone': this.emergencyPrimaryPhone !== '' && this.emergencyPrimaryPhone ? this.emrgencyCountryCode1 + '-' + this.emergencyPrimaryPhone : '',
          'emergencySecondaryPhone': this.emergencySecondaryPhone !== '' && this.emergencySecondaryPhone ? this.emrgencyCountryCode2 + '-' + this.emergencySecondaryPhone : '',
          'onlineBookingHoursValue': this.onlineBookingHoursValue,
          'appointmentsValue': this.appointmentsValue,
          'serviceLevel': this.serviceLevel,
          'usesTimeClockValue': this.usesTimeClockValue,
          'retailOnlyValue': this.retailOnlyValue,
          'hourlyWageValue': this.hourlyWageValue,
          'salaryValue': this.salaryValue ? this.salaryValue : null,
          'viewOnlyMyApptsValues': this.viewOnlyMyApptsValues,
          'permissioneMethodValue': this.permissioneMethodValue,
          'workerRole': this.workerRole,
          'compensationMethodValue': this.compensationMethodValue,
          'canViewApptValues': this.canViewApptValues,
          'paymentGateWay': this.paymentGateWay,
          'merchantTerminalId': this.merchantTerminalId,
          'merchantAccountTest': this.merchantAccountTest,
          'merchantAccountKey': this.merchantAccountKey,
          'appointmentsBookEveryValue': this.appointmentsBookEveryValue,
          'password': this.password1,
          'image': this.image,
          'hideClientContactInfo': this.hideClientContactInfo === false || this.hideClientContactInfo === 0 ? 0 : 1
        };
        this.workerHours = {
          'sun_start': this.sun_start.toUpperCase(),
          'sun_end': this.sun_end.toUpperCase(),

          'mon_start': this.mon_start.toUpperCase(),
          'mon_end': this.mon_end.toUpperCase(),

          'tue_start': this.tue_start.toUpperCase(),
          'tue_end': this.tue_end.toUpperCase(),

          'wed_start': this.wed_start.toUpperCase(),
          'wed_end': this.wed_end.toUpperCase(),

          'thur_start': this.thur_start.toUpperCase(),
          'thur_end': this.thur_end.toUpperCase(),

          'fri_start': this.fri_start.toUpperCase(),
          'fri_end': this.fri_end.toUpperCase(),

          'sat_start': this.sat_start.toUpperCase(),
          'sat_end': this.sat_end.toUpperCase(),
        };
        this.workerOnlineHours = {
          'sun_start': this.sun_start1 ? this.sun_start1.toUpperCase() : '',
          'sun_end': this.sun_end1 ? this.sun_end1.toUpperCase() : '',

          'mon_start': this.mon_start1 ? this.mon_start1.toUpperCase() : '',
          'mon_end': this.mon_end1 ? this.mon_end1.toUpperCase() : '',

          'tue_start': this.tue_start1 ? this.tue_start1.toUpperCase() : '',
          'tue_end': this.tue_end1 ? this.tue_end1.toUpperCase() : '',

          'wed_start': this.wed_start1 ? this.wed_start1.toUpperCase() : '',
          'wed_end': this.wed_end1 ? this.wed_end1.toUpperCase() : '',

          'thur_start': this.thur_start1 ? this.thur_start1.toUpperCase() : '',
          'thur_end': this.thur_end1 ? this.thur_end1.toUpperCase() : '',

          'fri_start': this.fri_start1 ? this.fri_start1.toUpperCase() : '',
          'fri_end': this.fri_end1 ? this.fri_end1.toUpperCase() : '',

          'sat_start': this.sat_start1 ? this.sat_start1.toUpperCase() : '',
          'sat_end': this.sat_end1 ? this.sat_end1.toUpperCase() : '',
        };
        // goals starts here
        let goalsUpdateList;
        goalsUpdateList = this.goalList;
        if (goalsUpdateList) {
          this.goals = goalsUpdateList.concat(this.goalsRows);
        }
        if (this.prevUserName !== this.userData.userName) {
          this.userData['namechanged'] = true;
          this.userData['prevUserName'] = this.prevUserName;
        }
        const workerservices = this.workerServicesData.filter((data) => ((data['Edit'] === false && data['Removed'] === false && data['srv'])) ||
          (data['Edit'] === true && data['isTouched']));
        this.goals.forEach(elemnt => {
          elemnt.startDBDate = this.commonService.getDBDatStr(elemnt.startDate);
          elemnt.endDBDate = this.commonService.getDBDatStr(elemnt.endDate);
        });
        for (let i = 0; i < workerservices.length; i++) {
          if (workerservices[i]['Buffer_After__c'] === '') {
            workerservices[i]['Buffer_After__c'] = null;
          }
        }
        const dataObj = {
          'workerServiceData': workerservices,
          'workerData': this.userData,
          'goalsData': this.goals,
          'workerHours': this.workerHours,
          'workerOnlineHours': this.workerOnlineHours,
        };
        //  return;
        if (this.goalTargetError()) {
          this.setupWorkersDetailsService.saveUser(this.updateId, dataObj, this.workerImage.file).subscribe(
            data => {
              this.workerImage.srcURL = '';
              this.workerImage.file = '';
              this.profileData = data['result'];
              this.disableDiv = true;
              this.addDiv = false;
              this.hideTable = true;
              this.clear();
              this.clearErrMsg();
              this.inactive = false;
              this.getUserList();
              if (this.workerDataAdd === true) {
                this.toasterAction = 'Created';
              } else if (this.workerDataAdd === false) {
                this.toasterAction = 'Edited';
                if (data['result'] && data['result'].rights.length > 0) {
                  localStorage.setItem('rights', data['result'].rights);
                }
              }
              this.password1 = '';
              this.password2 = '';
              this.toastermessage = this.translateService.get('Record   ' + this.toasterAction + '   Successfully');
              this.toastr.success(this.toastermessage.value, null, { timeOut: 1500 });
            },
            error => {
              const status = JSON.parse(error['_body']).result;
              const statuscode = JSON.parse(error['_body'])['status'];
              if (status.workerDetailError && status.workerDetailError.sqlMessage.indexOf('USER_ACCOUNT') > -1) {
                this.error = 'SETUP_WORKERS_DETAILS.VALID_USERNAME';
                window.scrollTo(0, 0);
              } else if (statuscode && statuscode === '2099') {
                this.error = 'SETUP_WORKERS_DETAILS.VALID_WORKERNAME';
                window.scrollTo(0, 0);
              } else if (statuscode && statuscode === '2098') {
                this.error = 'SETUP_WORKERS_DETAILS.VALID_USER_PIN';
                window.scrollTo(0, 0);
              } else if (statuscode && statuscode === '2062') {
                this.error = 'SETUP_WORKERS_DETAILS.VALID_SUN_STARTTIME_MUST_PRIOR_TO_ENDTIME';
                window.scrollTo(0, 0);
              } else if (statuscode && statuscode === '2100') {
                this.error = 'SETUP_WORKERS_DETAILS.VALID_USER_NAME';
                window.scrollTo(0, 0);
              } else if (statuscode && statuscode === '2063') {
                this.error = 'SETUP_WORKERS_DETAILS.VALID_MON_STARTTIME_MUST_PRIOR-TO_ENDTIME';
                window.scrollTo(0, 0);
              } else if (statuscode && statuscode === '2064') {
                this.error = 'SETUP_WORKERS_DETAILS.VALID_TUE_STARTTIME_MUST_PRIOR-TO_ENDTIME';
                window.scrollTo(0, 0);
              } else if (statuscode && statuscode === '2065') {
                this.error = 'SETUP_WORKERS_DETAILS.VALID_WED_STARTTIME_MUST_PRIOR-TO_ENDTIME';
                window.scrollTo(0, 0);
              } else if (statuscode && statuscode === '2066') {
                this.error = 'SETUP_WORKERS_DETAILS.VALID_THU_STARTTIME_MUST_PRIOR-TO_ENDTIME';
                window.scrollTo(0, 0);
              } else if (statuscode && statuscode === '2067') {
                this.error = 'SETUP_WORKERS_DETAILS.VALID_FRI_STARTTIME_MUST_PRIOR-TO_ENDTIME';
                window.scrollTo(0, 0);
              } else if (statuscode && statuscode === '2068') {
                this.error = 'SETUP_WORKERS_DETAILS.VALID_SAT_STARTTIME_MUST_PRIOR-TO_ENDTIME';
                window.scrollTo(0, 0);
              }
              if (statuscode === '2085' || statuscode === '2071') {
                this.router.navigate(['/']).then(() => { });
              }
            }
          );
        }
      }/*2 else end here */
    } /*1 else  end here */
  }/*commonSave end here */
  goalTargetError() {
    let res = true;
    this.goallineerror = [];
    if (this.goals.length > 0) {
      this.goals.forEach((ele, index) => {
        if (parseInt(ele.target, 10) > 99999) {
          this.changeTabs(undefined, 'goals', 'goalsnav');
          this.goallineerror.push({ error: 'Goals Line ' + (index + 1) + ' must be less than 99,999.99' });
          return res = false;
        }
      });
    }
    return res;
  }

  getDuplicateDisOrd(val, id) {
    this.errorDuplicateDisOrd = false;
    if (val) {
      this.userListData.forEach(elem => {
        if (+elem.Display_Order__c === +val && elem.Id !== id) {
          this.errorDuplicateDisOrd = true;
        }
      });
    }
  }
  /*Adding New Worker Method starts here */
  addNewWorker() {
    this.updateId = undefined;
    const workerData = true;
    this.clear();
    this.showData(workerData);
    this.addRows();
    this.workerServicesData = [];
    this.activeStatus = true;
    this.sun_start = '9:00 AM';
    this.sun_end = '5:00 PM';
    this.mon_start = '9:00 AM';
    this.mon_end = '5:00 PM';
    this.tue_start = '9:00 AM';
    this.tue_end = '5:00 PM';
    this.wed_start = '9:00 AM';
    this.wed_end = '5:00 PM';
    this.thur_start = '9:00 AM';
    this.thur_end = '5:00 PM';
    this.fri_start = '9:00 AM';
    this.fri_end = '5:00 PM';
    this.sat_start = '9:00 AM';
    this.sat_end = '5:00 PM';
    this.showRemoveButton = false;
  }
  /*new Worker Method End */
  /*methods */
  getMobileCarriersData() {
    this.setupWorkersDetailsService.getMobileData().subscribe(
      data => {
        this.carrierData = data['result'];
        this.mobileCarrier = this.carrierData[0].mobileCarrierName;
      },
      error => {
        const errStatus = JSON.parse(error['_body'])['status'];
        if (errStatus === '2085' || errStatus === '2071') {
          if (this.router.url !== '/') {
            localStorage.setItem('page', this.router.url);
            this.router.navigate(['/']).then(() => { });
          }
        }
      }
    );
  }
  changeMonths(value) {
    this.birthMonth = value;
    this.birthDay = '';
    this.birthDateAndDays('');
  }
  changeYear(value) {
    this.birthYear = value;
  }
  birthDateAndDays(monthValue) {
    if (monthValue === '') {
      this.monthData = this.birthMonth;
    } else {
      this.monthData = monthValue;
    }
    const d = new Date();
    const n = d.getFullYear();
    const days = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15',
      '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31'];
    this.monthsArray = ['January', 'February', 'March', 'April', 'May', 'June', 'July',
      'August', 'September', 'October', 'November', 'December'];
    // this.birthDay = '1';
    if (!this.monthData) {
      this.birthMonth = '';
      this.daysArray = [];
    } else if (n % 4 === 0 && this.monthData === this.monthsArray[1]) {
      this.daysArray = days.slice(0, days.length - 2);
    } else if (this.monthData === this.monthsArray[0] || this.monthData === this.monthsArray[2] ||
      this.monthData === this.monthsArray[4] || this.monthData === this.monthsArray[6] ||
      this.monthData === this.monthsArray[7] || this.monthData === this.monthsArray[9] ||
      this.monthData === this.monthsArray[11]) {
      this.daysArray = days;
    } else if (this.monthData === this.monthsArray[3] || this.monthData === this.monthsArray[5] ||
      this.monthData === this.monthsArray[8] || this.monthData === this.monthsArray[10]) {
      this.daysArray = days.slice(0, days.length - 1);
    } else if (this.monthData === this.monthsArray[1]) {
      this.daysArray = days.slice(0, days.length - 2); // length - 2 means displaying 29 days in february for every year as an option//
    }
  }
  getYear() {
    const years = new Date().getFullYear();
    for (let i = 0; i <= 100; i++) {
      this.yearsDataList.push(years - i);
      this.birthYear = this.yearsDataList[0];
    }
  }
  getSecurityPermissionemethods() {
    this.setupWorkersDetailsService.getpermissioneList()
      .subscribe(data => {
        this.permissioneListing = data['result'];
        this.permissioneMethodValue = this.permissioneListing[0].Id;

      },
        error => {
          const errStatus = JSON.parse(error['_body'])['status'];
          if (errStatus === '2085' || errStatus === '2071') {
            if (this.router.url !== '/') {
              localStorage.setItem('page', this.router.url);
              this.router.navigate(['/']).then(() => { });
            }
          }
        });
  }
  getCompansationMethods() {
    this.setupWorkersDetailsService.getMethods()
      .subscribe(data => {
        this.methodsListing = data['result']
          .filter(
            filterList => filterList.Active__c === 1);
      },
        error => {
          const errStatus = JSON.parse(error['_body'])['status'];
          if (errStatus === '2085' || errStatus === '2071') {
            if (this.router.url !== '/') {
              localStorage.setItem('page', this.router.url);
              this.router.navigate(['/']).then(() => { });
            }
          }
        });
  }
  getGoals() {
    this.setupWorkersDetailsService.getGoals()
      .subscribe(data => {
        this.goalsList = data['result']
          .filter(
            filterList => filterList.Active__c === 1);
      },
        error => {
          const errStatus = JSON.parse(error['_body'])['status'];
          if (errStatus === '2085' || errStatus === '2071') {
            if (this.router.url !== '/') {
              localStorage.setItem('page', this.router.url);
              this.router.navigate(['/']).then(() => { });
            }
          }
        });
  }
  changeTabs(evt, tab, tabnav) {
    let i;
    let tabcontent;
    let tablinks;
    tabcontent = document.getElementsByClassName('tabcontent');
    for (i = 0; i < tabcontent.length; i++) {
      tabcontent[i].style.display = 'none';
    }
    tablinks = document.getElementsByClassName('tablinks');
    for (i = 0; i < tablinks.length; i++) {
      tablinks[i].className = tablinks[i].className.replace(' active', '');
    }
    if (tab === 'services') {
      this.getWorkerServices();
      this.serviceGroupListOnChange(this.addServiceGroupName, 0);

    }
    document.getElementById(tab).style.display = 'block';
    document.getElementById(tabnav).classList.add('active');
  }
  clearErrMsg() {
    this.goalListError = '';
    this.goalError = '';
    this.error = '';
    this.error1 = '';
    this.error2 = '';
    this.error3 = '';
    this.error4 = '';
    this.error5 = '';
    this.pcodeErr = '';
    this.mcodeErr = '';
    this.error6 = '';
    this.error7 = '';
    this.error8 = '';
    this.otherErr1 = '';
    this.otherErr2 = '';
    this.error9 = '';
    this.error10 = '';
    this.workerServiceError = '';
    this.otherError = '';
    this.calculatedGoal = [];
    this.percentOfGoal = [];
    this.serviceErrorName = '';
    this.errorTime = '';
    this.popupErr = '';
    this.timeErr = '';
    this.editError = '';
  }
  /* --------- worker services --------*/
  getOnlineBookingHoursOnChange(value) {
    this.onlineBookingHoursValue = value;
  }
  getAppointmentsOnChange(value) {
    this.appointmentsValue = value;
  }
  getAppointmentBookEveryOnChange(value) {
    this.appointmentsBookEveryValue = value;
  }

  hoursForOnlineData() {
    this.setupWorkersDetailsService.hoursForOnline().subscribe(
      data => {
        this.onlineValues = data['result'].filter(filterList => filterList.isActive__c === 1);
        this.onlineBookingHoursValue = this.onlineValues[0].Id;
        this.appointmentsValue = this.onlineValues[0].Id;
      },
      error => {
        const errStatus = JSON.parse(error['_body'])['status'];
        if (errStatus === '2085' || errStatus === '2071') {
          if (this.router.url !== '/') {
            localStorage.setItem('page', this.router.url);
            this.router.navigate(['/']).then(() => { });
          }
        }
      });
  }
  getWorkerServices() {
    this.setupWorkersDetailsService.getservicesByUser(this.updateId)
      .subscribe(data => {
        this.workerServicesData = data['result'];
        this.workerServicesData = this.workerServicesData.map((workerservice) => {
          workerservice['Removed'] = false;
          workerservice['Edit'] = true;
          workerservice['isTouched'] = false;
          workerservice['isTouched'] = '';
          return workerservice;
        });
        this.assaignWorkerServices();
      },
        error => {
          const errStatus = JSON.parse(error['_body'])['status'];
          if (errStatus === '2085' || errStatus === '2071') {
            if (this.router.url !== '/') {
              localStorage.setItem('page', this.router.url);
              this.router.navigate(['/']).then(() => { });
            }
          }
        });
  }

  assaignWorkerServices() {
    if (this.serviceDetailsList.length > 0 && this.workerServicesData.length > 0) {
      for (let i = 0; i < this.serviceDetailsList.length; i++) {
        const index = this.getServiceIndex(this.serviceDetailsList[i]['Id']);
        if (index !== -1) {
          this.serviceDetailsList[i]['performs'] = true;
          if (this.workerServicesData[index]['Self_Book__c'] === 0) {
            this.serviceDetailsList[i]['online'] = false;
          } else if (this.workerServicesData[index]['Self_Book__c'] === 1) {
            this.serviceDetailsList[i]['online'] = true;
          }
          if (this.workerServicesData[index]['isTouched']) {
            if (this.workerServicesData[index]['srv']) {
              this.serviceDetailsList[i]['performs'] = true;
            } else {
              this.serviceDetailsList[i]['performs'] = false;
            }
            if (this.workerServicesData[index]['onl']) {
              this.serviceDetailsList[i]['online'] = true;
            } else {
              this.serviceDetailsList[i]['online'] = false;
            }
          }
          this.serviceDetailsList[i]['priceValue'] = this.workerServicesData[index]['Price__c'];
          // if (this.workerServicesData[index]['Removed'] === false && this.workerServicesData[index]['Self_Book__c'] === 0 && !this.workerServicesData[index]['onl']) {
          //   this.serviceDetailsList[i]['performs'] = true;
          //   // this.workerServicesData[index]['isTouched'] = false;
          // } else if (this.serviceDetailsList[i]['Id'] && this.workerServicesData[index]['Self_Book__c'] === 1) {
          //   this.serviceDetailsList[i]['online'] = true;
          //   this.serviceDetailsList[i]['performs'] = true;
          // } else if (this.workerServicesData[index]['Id'] && this.workerServicesData[index]['onl']) {
          //   this.serviceDetailsList[i]['performs'] = true;
          //   this.serviceDetailsList[i]['online'] = this.workerServicesData[index]['onl'];
          // } else {
          //   this.serviceDetailsList[i]['online'] = this.workerServicesData[index]['onl'];
          //   this.serviceDetailsList[i]['performs'] = this.workerServicesData[index]['srv'];
          // }
        } else {
          this.serviceDetailsList[i]['performs'] = false;
          this.serviceDetailsList[i]['online'] = false;
        }
      }
    }
    this.applyPriceLevel();
  }
  getServiceGroupsList() {
    this.setupWorkersDetailsService.getSetupServicesData()
      .subscribe(serviceGroupResult => {
        this.serviceGroupList = [];
        const tempActive = config.environment.booleanTrue; // default Active records
        this.serviceGroupList = serviceGroupResult['result']
          .filter(filterList => filterList.active);
        for (let i = 0; i < this.serviceGroupList.length; i++) {
          if (this.serviceGroupList[i]['serviceGroupName'] === 'System Class') {
            this.serviceGroupList[i]['serviceGroupName'] = 'Classes';
          }
        }

        if (this.serviceGroupList.length > 0) {
          this.selectedServiceGroup = this.serviceGroupList[0].serviceGroupName;
          this.addServiceGroupName = this.serviceGroupList[0].serviceGroupName;
          this.setupWorkersDetailsService.showInactiveServiceListByGroupName(tempActive, this.selectedServiceGroup)
            .subscribe(data => {
              this.serviceDetailsList = data['result'];
              this.serviceDetailsList = this.serviceDetailsList.map((service) => {
                service['performs'] = false;
                service['priceValue'] = null;
                return service;
              });
              this.assaignWorkerServices();
            },
              error => {
                const errStatus = JSON.parse(error['_body'])['status'];
                if (errStatus === '2085' || errStatus === '2071') {
                  if (this.router.url !== '/') {
                    localStorage.setItem('page', this.router.url);
                    this.router.navigate(['/']).then(() => { });
                  }
                }
              });
        }
      });
  }
  serviceGroupListOnChange(value, index) {
    this.srvLstDisParm = index;
    this.srvLstDisBtnParm = index;
    // this.selectAll(index);
    // this.unSelectAll(index);
    const cls = document.getElementById('srvIconId' + index).className;
    if (cls.indexOf('fa-caret-right') > -1) {
      for (let i = 0; i < this.serviceGroupList.length; i++) {
        document.getElementById('srvIconId' + i).className = 'fa fa-caret-right';
      }
      document.getElementById('srvIconId' + index).className = 'fa fa-caret-down';
      this.srvLstDisBtnParm = index;
      this.selectedServiceGroup = value;
      let tempActive;
      if (this.showInactive) {
        tempActive = config.environment.booleanFalse;
      } else {
        tempActive = config.environment.booleanTrue;
      }
      this.clearData();
      this.serviceTab = false;
      this.setupWorkersDetailsService.showInactiveServiceListByGroupName(tempActive, this.selectedServiceGroup)
        .subscribe(data => {
          this.serviceDetailsList = data['result'];
          this.serviceDetailsList = this.serviceDetailsList.map((service) => {
            service['performs'] = false;
            service['priceValue'] = null;
            return service;
          });
          this.workerServiceData = [];
          if (this.selectedServiceGroup === 'Class') {
            this.setupWorkersDetailsService.getClasses()
              .subscribe(classesData => {
                this.serviceDetailsList = [];
                this.serviceDetailsList = classesData['result'];
                this.serviceDetailsList = this.serviceDetailsList.map((service) => {
                  service['performs'] = false;
                  service['priceValue'] = null;
                  return service;
                });
                this.assaignWorkerServices();
              },
                error1 => {
                  const errStatus = JSON.parse(error1['_body'])['status'];
                  if (errStatus === '2085' || errStatus === '2071') {
                    if (this.router.url !== '/') {
                      localStorage.setItem('page', this.router.url);
                      this.router.navigate(['/']).then(() => { });
                    }
                  }
                });
          }
          this.assaignWorkerServices();
        },
          error => {
            const errStatus = JSON.parse(error['_body'])['status'];
            if (errStatus === '2085' || errStatus === '2071') {
              if (this.router.url !== '/') {
                localStorage.setItem('page', this.router.url);
                this.router.navigate(['/']).then(() => { });
              }
            }
          });
    } else {
      document.getElementById('srvIconId' + index).className = 'fa fa-caret-right';
      delete this.srvLstDisBtnParm;
      this.serviceDetailsList = [];
    }
  }
  getBookingData() {
    this.setupWorkersDetailsService.getBookingData().subscribe(
      data => {
        this.bookingDataList = data['result'];
        this.bookingIntervalMinutes = this.bookingDataList.bookingIntervalMinutes;
        this.bookingIntervalMinutesForDurations = this.bookingIntervalMinutes;
        this.appointmentsBookEveryValue = this.bookingIntervalMinutes;
        for (let i = 0; i < 6; i++) {
          this.intervalValues.push({ 'IntValue': this.bookingIntervalMinutes });
          this.bookingIntervalMinutes = this.bookingIntervalMinutesForDurations + this.bookingIntervalMinutes;
        }
      },
      error => {
        const errStatus = JSON.parse(error['_body'])['status'];
        if (errStatus === '2085' || errStatus === '2071') {
          if (this.router.url !== '/') {
            localStorage.setItem('page', this.router.url);
            this.router.navigate(['/']).then(() => { });
          }
        }
      });
  }
  selectedIn(value, index) {
    if (value === true) {
      this.allButton = true;
      this.selectAll(index);
    } else {
      this.allButton = false;
      this.unSelectAll(index);
    }
  }
  selectedOl(value, index) {
    if (value === true) {
      this.allOlButton = true;
      this.selectAllOnline(index);
    } else {
      this.allOlButton = false;
      this.unSelectAllOnline(index);
    }
  }
  selectAll(index) {
    if (index === this.srvLstDisParm) {
      for (let i = 0; i < this.serviceDetailsList.length; i++) {
        this.selectedValues(true, 'performs', this.serviceDetailsList, i);
      }
    }
  }
  unSelectAll(index) {
    if (index === this.srvLstDisParm) {
      for (let i = 0; i < this.serviceDetailsList.length; i++) {
        this.selectedValues(false, 'performs', this.serviceDetailsList, i);
        // this.workerServiceData[i]['Removed'] = true;
      }
      for (let i = 0; i < this.workerServiceData.length; i++) {
        this.workerServiceData[i]['Removed'] = true;
      }
    }
  }
  selectAllOnline(index) {
    if (index === this.srvLstDisParm) {
      for (let i = 0; i < this.serviceDetailsList.length; i++) {
        this.selectedValues(true, 'online', this.serviceDetailsList, i);
        // this.serviceDetailsList[i]['online'] = true;
      }
    }
  }
  unSelectAllOnline(index) {
    if (index === this.srvLstDisParm) {
      for (let i = 0; i < this.serviceDetailsList.length; i++) {
        this.selectedValues(false, 'online', this.serviceDetailsList, i);
        // this.workerServiceData[i]['Removed'] = true;
      }
      for (let i = 0; i < this.serviceDetailsList.length; i++) {
        this.serviceDetailsList[i]['online'] = false;
      }
    }
  }
  getServiceLevels() {
    this.setupWorkersDetailsService.getLevels().subscribe(
      data => {
        this.levelValues = data['serviceLevels'];
        if (this.levelValues.length > 0) {
          this.serviceLevel = this.levelValues[0].level;
        }
      },
      error => {
        const errStatus = JSON.parse(error['_body'])['status'];
        if (errStatus === '2085' || errStatus === '2071') {
          if (this.router.url !== '/') {
            localStorage.setItem('page', this.router.url);
            this.router.navigate(['/']).then(() => { });
          }
        }
      });
  }
  changeServiceLevel(value) {
    this.serviceLevel = value;
    // this.applyPriceLevel();
  }
  applyPriceLevel() {
    let test = [];
    if (this.serviceLevel === 'None' && this.levelValues[0].level === 'None') {
      this.serviceLevel = 1;
    }
    if (this.serviceLevel !== this.levelValues[0].level) {
      for (let i = 0; i < this.serviceDetailsList.length; i++) {
        if (this.selectedServiceGroup === 'Class') {
          test.push({ 'price': JSON.parse(this.serviceDetailsList[i].Price_per_Attendee__c) });
          this.priceValue = test[i];
        } else {
          // if (this.serviceDetailsList[i].performs === true) {
          test = JSON.parse(this.serviceDetailsList[i].Levels__c);
          if (test) {
            const levelIndex = test.findIndex((level) => +level.levelNumber === +this.serviceLevel);
            const serviceLevelValues = levelIndex !== -1 ? test[levelIndex] : test[0];
            this.serviceDetailsList[i]['pric'] = serviceLevelValues.price;
          }
          // const index = this.workerServicesData.findIndex(workerservice => workerservice['Service__c'] === this.serviceDetailsList[i]['Id']);
          // if (index !== -1) {
          //   if (this.workerServicesData[index]['Removed'] === false) {
          //     this.workerServicesData[index]['isTouched'] = true;
          //     this.workerServicesData[index]['Name'] = this.serviceDetailsList[i]['Name'];
          // this.workerServicesData[index]['Pric'] = serviceLevelValues.price;
          // this.workerServicesData[index]['DurA1'] = serviceLevelValues['duration1AvailableForOtherWork'] ? 1 : 0;
          // this.workerServicesData[index]['DurA2'] = serviceLevelValues['duration2AvailableForOtherWork'] ? 1 : 0;
          // this.workerServicesData[index]['DurA3'] = serviceLevelValues['duration3AvailableForOtherWork'] ? 1 : 0;
          // this.workerServicesData[index]['Dur1'] = serviceLevelValues['duration1'] ? +serviceLevelValues['duration1'] : 0;
          // this.workerServicesData[index]['Dur2'] = serviceLevelValues['duration2'] ? +serviceLevelValues['duration2'] : 0;
          // this.workerServicesData[index]['Dur3'] = serviceLevelValues['duration3'] ? +serviceLevelValues['duration3'] : 0;
          // this.workerServicesData[index]['BuffAft'] = serviceLevelValues['bufferAfter'] ? +serviceLevelValues['bufferAfter'] : 0;
          // this.workerServicesData[index]['ServiceFeeAmt'] = serviceLevelValues['Service_Fee_Amount__c'] ? +serviceLevelValues['Service_Fee_Amount__c'] : '';
          // this.workerServicesData[index]['ServiceFeePrcnt'] = serviceLevelValues['Service_Fee_Percent__c'] ? +serviceLevelValues['Service_Fee_Percent__c'] : '';
          // if (this.serviceId === this.serviceDetailsList[i]['Id']) {
          //   this.assaignValuesForApplyServices(this.serviceDetailsList[i]['Id'], i);
          // }
          //   }
          // }
          // }
        }
      }
    }
  }

  selectedValues(value, seleitem, serviceDetailsList, i) {
    const index = this.getServiceIndex(this.serviceDetailsList[i]['Id']);
    const name = this.serviceDetailsList[i]['Name'];
    if (value === true) {
      if (seleitem === 'performs') {
        this.serviceDetailsList[i]['performs'] = true;
      } else {
        this.serviceDetailsList[i]['online'] = true;
      }
      if (index === -1) {
        const workerService = {
          'Edit': false,
          'Price__c': null,
          'Service__c': this.serviceDetailsList[i]['Id'],
          'Duration_1_Available_for_Other_Work__c': null,
          'Duration_2_Available_for_Other_Work__c': null,
          'Duration_3_Available_for_Other_Work__c': null,
          'Duration_1__c': null,
          'Duration_2__c': null,
          'Duration_3__c': null,
          'Service_Fee_Percent__c': null,
          'Service_Fee_Amount__c': null,
          'Worker__c': this.updateId,
          'Removed': false,
          'isTouched': true,
          'Name': name,
          'seleitem': seleitem,
          'srv': this.serviceDetailsList[i]['performs'],
          'onl': this.serviceDetailsList[i]['online']
        };
        if (seleitem === 'performs') {
          workerService['srv'] = true;
        } else {
          workerService['onl'] = true;
        }
        this.workerServicesData.push(workerService);
      } else {
        this.workerServicesData[index]['srv'] = this.serviceDetailsList[i]['performs'];
        this.workerServicesData[index]['onl'] = this.serviceDetailsList[i]['online'];
        this.workerServicesData[index]['Removed'] = false;
        this.workerServicesData[index]['isTouched'] = true;
        this.workerServicesData[index]['Name'] = name;
        this.workerServicesData[index]['online'] = value;
        if (seleitem === 'performs') {
          this.workerServicesData[index]['srv'] = true;
        } else if (seleitem === 'online') {
          this.workerServicesData[index]['onl'] = true;
        }
      }
    } else if (this.workerServicesData[index]) {
      if (seleitem === 'online' && value === false) {
        this.workerServicesData[index]['Removed'] = false;
        this.workerServicesData[index]['Self_Book__c'] = 0;
      } else {
        this.workerServicesData[index]['Removed'] = true;
      }
      this.workerServicesData[index]['srv'] = this.serviceDetailsList[i]['performs'];
      this.workerServicesData[index]['onl'] = this.serviceDetailsList[i]['online'];
      this.workerServicesData[index]['isTouched'] = true;
      this.workerServicesData[index]['Name'] = name;
      if (seleitem === 'performs') {
        this.serviceDetailsList[i]['performs'] = false;
        this.workerServicesData[index]['srv'] = false;
      } else {
        this.workerServicesData[index]['online'] = false;
        this.workerServicesData[index]['onl'] = false;
      }
    }
  }
  getServiceIndex(serviceId): number {
    const index = this.workerServicesData.findIndex(workerservice => workerservice['Service__c'] === serviceId);
    return index;
  }
  showServiceData(serviceData, serviceDetailsList, i) {
    this.serviceName = serviceData.Name;
    this.serviceIndex = undefined;
    // this.updateWorkerServiceValues();
    this.serviceTab = true;
    if (this.serviceDetailsList[i]['performs'] === false) {
      this.clearData();
    }
    this.assaignValuesForApplyServices(serviceData['Id'], i);
    this.getAddOnService(serviceData['Id'], i);
    this.serviceId = serviceData['Id'];
    // const LevelC = JSON.parse(this.serviceData1.Levels__c);
    // this.servicePrice = LevelC[0].price;
    // this.serviceBufferAfter = LevelC[0].bufferAfter;
  }

  isNumeric(e): boolean {
    return this.commonService.IsNumeric(e);
  }
  assaignValuesForApplyServices(serviceId, index) {
    const defaultLevels = JSON.parse(this.serviceDetailsList[index]['Levels__c']);
    const levelIndex = defaultLevels.findIndex((level) => +level.levelNumber === +this.serviceLevel);
    const serviceLevelValues = levelIndex !== -1 ? defaultLevels[levelIndex] : defaultLevels[0];
    this.defaultPrice = serviceLevelValues.price;
    this.defaultDur1 = serviceLevelValues.duration1;
    this.defaultDur2 = serviceLevelValues.duration2;
    this.defaultDur3 = serviceLevelValues.duration3;
    this.serBufferAfter = serviceLevelValues.bufferAfter;
    this.serFeeAmt = serviceLevelValues.Service_Fee_Amount__c;
    this.serFeePernt = serviceLevelValues.Service_Fee_Percent__c;
    this.serAvailable1 = serviceLevelValues.duration1AvailableForOtherWork;
    if (this.serAvailable1 === true) {
      this.serAvailable1 = 'Available';
    } else {
      this.serAvailable1 = '';
    }
    this.serAvailable2 = serviceLevelValues.duration2AvailableForOtherWork;
    if (this.serAvailable2 === true) {
      this.serAvailable2 = 'Available';
    } else {
      this.serAvailable2 = '';
    }
    this.serAvailable3 = serviceLevelValues.duration3AvailableForOtherWork;
    if (this.serAvailable3 === true) {
      this.serAvailable3 = 'Available';
    } else {
      this.serAvailable3 = '';
    }
    for (let k = 0; k < this.workerServicesData.length; k++) {
      if (serviceId === this.workerServicesData[k]['Service__c'] && this.serviceDetailsList[index]['performs'] === true) {
        this.serviceIndex = k;
        this.workerServicesData[k]['Name'] = this.serviceDetailsList[index]['Name'];
        // const servLevels = this.serviceDetailsList['Level']
        this.workerServicesData[k]['isTouched'] = true;
        this.serviceId = this.workerServicesData[k]['Service__c'];
        this.servicePrice = this.workerServicesData[k]['Price__c'];
        this.serviceavailable_1 = this.workerServicesData[k]['Duration_1_Available_for_Other_Work__c'];
        this.serviceavailable_2 = this.workerServicesData[k]['Duration_2_Available_for_Other_Work__c'];
        this.serviceavailable_3 = this.workerServicesData[k]['Duration_3_Available_for_Other_Work__c'];
        this.serviceDuration_1 = this.workerServicesData[k]['Duration_1__c'];
        this.serviceDuration_2 = this.workerServicesData[k]['Duration_2__c'];
        this.serviceDuration_3 = this.workerServicesData[k]['Duration_3__c'];
        this.serviceBufferAfter = this.workerServicesData[k]['Buffer_After__c'];
        if (this.workerServicesData[k]['Service_Fee_Percent__c'] && this.workerServicesData[k]['Service_Fee_Percent__c'] !== '' && this.workerServicesData[k]['Service_Fee_Percent__c'] > 0) {
          this.serviceFeeType1 = 'percent';
          this.serviceFee = this.workerServicesData[k]['Service_Fee_Percent__c'];
        } else if (this.workerServicesData[k]['Service_Fee_Amount__c'] && this.workerServicesData[k]['Service_Fee_Amount__c'] !== ''
          && this.workerServicesData[k]['Service_Fee_Amount__c'] > 0) {
          this.serviceFeeType = 'Amount';
          this.serviceFee = this.workerServicesData[k]['Service_Fee_Amount__c'];
        } else if ((this.workerServicesData[k]['Service_Fee_Percent__c'] === null || this.workerServicesData[k]['Service_Fee_Percent__c'] === '')
          && (this.workerServicesData[k]['Service_Fee_Amount__c'] === null || this.workerServicesData[k]['Service_Fee_Amount__c'] === '')) {
          this.serviceFeeType = '';
          this.serviceFeeType1 = '';
          this.serviceFee = '';
        }
        // this.serviceFeeAmount = this.workerServicesData[k]['Service_Fee_Amount__c'];
        // this.serviceFeePercentage = this.workerServicesData[k]['Service_Fee_Percent__c'];
      }
    }
  }
  updateWorkerServiceValues() {
    const index = this.serviceDetailsList.findIndex(service => service['Id'] === this.serviceId);
    if (this.serviceDetailsList[index]['performs'] === true) {
      this.serviceDetailsList[index]['priceValue'] = this.servicePrice;
      this.workerServicesData[this.serviceIndex]['Price__c'] = this.servicePrice;
      this.workerServicesData[this.serviceIndex]['Duration_1_Available_for_Other_Work__c'] = this.serviceavailable_1 ? 1 : 0;
      this.workerServicesData[this.serviceIndex]['Duration_2_Available_for_Other_Work__c'] = this.serviceavailable_2 ? 1 : 0;
      this.workerServicesData[this.serviceIndex]['Duration_3_Available_for_Other_Work__c'] = this.serviceavailable_3 ? 1 : 0;
      this.workerServicesData[this.serviceIndex]['Duration_1__c'] = this.serviceDuration_1;
      this.workerServicesData[this.serviceIndex]['Duration_2__c'] = this.serviceDuration_2;
      this.workerServicesData[this.serviceIndex]['Duration_3__c'] = this.serviceDuration_3;
      this.workerServicesData[this.serviceIndex]['Buffer_After__c'] = this.serviceBufferAfter;
      if (this.serviceFee && this.serviceFeeType === 'Amount') {
        this.workerServicesData[this.serviceIndex]['Service_Fee_Amount__c'] = this.serviceFee;
        this.workerServicesData[this.serviceIndex]['Service_Fee_Percent__c'] = '';
      } else if (this.serviceFeeType1 === 'percent' && this.serviceFee) {
        this.workerServicesData[this.serviceIndex]['Service_Fee_Percent__c'] = this.serviceFee;
        this.workerServicesData[this.serviceIndex]['Service_Fee_Amount__c'] = '';
      } else {
        this.workerServicesData[this.serviceIndex]['Service_Fee_Percent__c'] = '';
        this.workerServicesData[this.serviceIndex]['Service_Fee_Amount__c'] = '';
        this.serviceFeeType1 = '';
        this.serviceFeeType = '';
      }
      // this.workerServicesData[this.serviceIndex]['Service_Fee_Amount__c'] = this.serviceFeeAmount;
      // this.workerServicesData[this.serviceIndex]['Service_Fee_Percent__c'] = this.serviceFeePercentage;
    }
  }
  /* method to restrict specialcharecters  */
  keyPress(event: any) {
    const pattern = /[a-zA-Z0-9\+g]/;
    const inputChar = String.fromCharCode(event.charCode);
    if (!pattern.test(inputChar)) {
      // invalid character, prevent input
      event.preventDefault();
    }
  }
  /*--- goals ---*/
  /*--- To add goal row ---*/
  addRows() {
    this.addrows = true;
    this.goalsRows.push({
      goalsId: '', startDate: '', endDate: '',
      target: '', Id: ''
    });
  }
  /*--- To delete row ---*/
  deleteFieldValue(goalsRows, index) {
    this.goalsRows.splice(index, 1);
    this.goalError = '';
  }
  deleteFieldValue1(goallist, index) {
    this.goalList[index].delete = true;
    this.clearErrMsg();
  }
  addPreviousGoals() {
    this.goalListRows = true;
    this.goalList.push({ updateWorkerId: this.updateWorkerId });
  }
  getPreviousGoals(value) {
    if (value.target.checked === true) {
      this.previousGoals = value.target.checked;
      this.setupWorkersDetailsService.previousGoals(this.previousGoals, this.updateId, this.commonService.getDBDatStr(new Date()))
        .subscribe(data => {
          this.goalList = data['result'];
          this.goalList = this.updateDateFields(this.goalList);
          this.goalsRows = [];
          this.calculatedGoal = [];
          this.percentOfGoal = [];
          if (this.goalList.length === 0) {
            this.goalsRows = [];
            this.addRows();
          }
        },
          error => {
            const status = JSON.parse(error['status']);
            const statuscode = JSON.parse(error['_body']).status;
            switch (JSON.parse(error['_body']).status) {
              case '2033':
                this.goalError = 'COMMON_STATUS_CODES.' + JSON.parse(error['_body']).status;
                break;
              case '2043':
                this.goalError = 'COMMON_STATUS_CODES.' + JSON.parse(error['_body']).status;
                break;
            }
            if (statuscode === '2085' || statuscode === '2071') {
              if (this.router.url !== '/') {
                localStorage.setItem('page', this.router.url);
                this.router.navigate(['/']).then(() => { });
              }
            }
          });
    } else if (value.target.checked === false) {
      this.previousGoals = value.target.checked;
      this.setupWorkersDetailsService.previousGoals(this.previousGoals, this.updateId, this.commonService.getDBDatStr(new Date()))
        .subscribe(data1 => {
          this.goalList = data1['result'];
          this.goalList = this.updateDateFields(this.goalList);
          this.goalsRows = [];
          this.calculatedGoal = [];
          this.percentOfGoal = [];
          if (this.goalList.length === 0) {
            // this.goalsRows = [];
            this.addRows();
          }
        },
          error => {
            const errStatus = JSON.parse(error['_body'])['status'];
            if (errStatus === '2085' || errStatus === '2071') {
              if (this.router.url !== '/') {
                localStorage.setItem('page', this.router.url);
                this.router.navigate(['/']).then(() => { });
              }
            }
          });
      // this.addRows();
    }
  }

  updateDateFields(dataArray: any) {
    if (dataArray && dataArray.length > 0) {
      for (let i = 0; i < dataArray.length; i++) {
        dataArray[i].startDate = this.commonService.getDateFrmDBDateStr(dataArray[i].startDate);
        dataArray[i].endDate = this.commonService.getDateFrmDBDateStr(dataArray[i].endDate);
      }
    }
    return dataArray;
  }
  updatePercentageOfGoal(goallist, index) {
    goallist.startDate2 = this.commonService.getDBDatStr(goallist.startDate);
    goallist.endDate2 = this.commonService.getDBDatStr(goallist.endDate);
    this.setupWorkersDetailsService.updateGoal(goallist, this.updateId).subscribe(data => {
      this.calculationList = data['result'];
      this.calculatedGoal[index] = this.calculationList.calculatedGoal;
      this.percentOfGoal[index] = this.calculationList.percentOfGoal;
      this.percentOfGoal[index] = Math.round(this.percentOfGoal[index] * 100) / 100;
    }, error => {
      const errStatus = JSON.parse(error['_body'])['status'];
      if (errStatus === '2085' || errStatus === '2071') {
        if (this.router.url !== '/') {
          localStorage.setItem('page', this.router.url);
          this.router.navigate(['/']).then(() => { });
        }
      }
    });
  }

  togglePassword(index) {
    if (index === 1) {
      if (this.glyphiconClass1 === 'glyphicon-eye-open') {
        this.glyphiconClass1 = 'glyphicon-eye-close';
        this.passwordType1 = 'text';
      } else {
        this.glyphiconClass1 = 'glyphicon-eye-open';
        this.passwordType1 = 'password';
      }
    } else if (index === 2) {
      if (this.glyphiconClass2 === 'glyphicon-eye-open') {
        this.glyphiconClass2 = 'glyphicon-eye-close';
        this.passwordType2 = 'text';
      } else {
        this.glyphiconClass2 = 'glyphicon-eye-open';
        this.passwordType2 = 'password';
      }
    }
  }
  hyphen_generate_mobile1(value) {
    if (value === undefined) {
      value = '';
    }
    if (value.length === 0) {
      (<HTMLInputElement>document.getElementById('mobile_id1')).value = value.concat('(');
    }
    if (value.length === 4) {
      (<HTMLInputElement>document.getElementById('mobile_id1')).value = value.concat(')');
    }
    if (value.length === 8) {
      (<HTMLInputElement>document.getElementById('mobile_id1')).value = value.concat('-');
    }
  }
  hyphen_generate_mobile2(value) {
    if (value === undefined) {
      value = '';
    }
    if (value.length === 0) {
      (<HTMLInputElement>document.getElementById('mobile_id2')).value = value.concat('(');
    }
    if (value.length === 4) {
      (<HTMLInputElement>document.getElementById('mobile_id2')).value = value.concat(')');
    }
    if (value.length === 8) {
      (<HTMLInputElement>document.getElementById('mobile_id2')).value = value.concat('-');
    }
  }
  hyphen_generate_mobile3(value) {
    if (value === undefined) {
      value = '';
    }
    if (value.length === 0) {
      (<HTMLInputElement>document.getElementById('mobile_id3')).value = value.concat('(');
    }
    if (value.length === 4) {
      (<HTMLInputElement>document.getElementById('mobile_id3')).value = value.concat(')');
    }
    if (value.length === 8) {
      (<HTMLInputElement>document.getElementById('mobile_id3')).value = value.concat('-');
    }
  }
  hyphen_generate_mobile4(value) {
    if (value === undefined) {
      value = '';
    }
    if (value.length === 0) {
      (<HTMLInputElement>document.getElementById('mobile_id4')).value = value.concat('(');
    }
    if (value.length === 4) {
      (<HTMLInputElement>document.getElementById('mobile_id4')).value = value.concat(')');
    }
    if (value.length === 8) {
      (<HTMLInputElement>document.getElementById('mobile_id4')).value = value.concat('-');
    }
  }

  imageErrorHandler(event) {
    event.target.src = '/assets/images/no-preview.png';
  }

  clearData() {
    this.servicePrice = '';
    this.serviceavailable_1 = '';
    this.serviceavailable_2 = '';
    this.serviceavailable_3 = '';
    this.serviceDuration_1 = '';
    this.serviceDuration_2 = '';
    this.serviceDuration_3 = '';
    this.serviceBufferAfter = '';
    this.serviceFeeAmount = '';
    this.serviceFeePercentage = '';
  }

  alphaOnly(e) {
    const specialKeys = new Array();
    specialKeys.push(8); // Backspace
    specialKeys.push(9); // Tab
    specialKeys.push(46); // Delete
    specialKeys.push(36); // Home
    specialKeys.push(35); // End
    specialKeys.push(37); // Left
    specialKeys.push(39); // Right
    const keyCode = e.keyCode === 0 ? e.charCode : e.keyCode;
    const ret = ((keyCode >= 48 && keyCode <= 57) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122) ||
      (specialKeys.indexOf(e.keyCode) !== -1 && e.charCode !== e.keyCode));
    return ret;
  }
  numberOnly(event: any) {
    const pattern = /[0-9.]/;
    const inputChar = String.fromCharCode(event.charCode);
    if (!pattern.test(inputChar)) {
      // invalid character, prevent input
      event.preventDefault();
    }
  }
  /* method to restrict specialcharecters  */
  numOnly(event: any) {
    const pattern = /[a-zA-Z0-9]$/;
    const inputChar = String.fromCharCode(event.charCode);
    if (!pattern.test(inputChar)) {
      // invalid character, prevent input
      event.preventDefault();
    }
  }
  getLocation() {
    if (this.zipCode.length > 4) {
      this.http.get('https://ziptasticapi.com/' + this.zipCode).subscribe(
        result => {
          if (result['error']) {
            const toastermessage: any = this.translateService.get('SETUPCOMPANY.ZIP_CODE_NOT_FOUND');
            this.toastr.error(toastermessage.value, null, { timeOut: 1500 });
          } else {
            if (result['country'] === 'US') {
              this.country = 'United States';
              config.states.forEach(state => {
                if (state.abbrev === result['state']) {
                  this.state = state.name;
                }
              });

            }
            const cityArray = result['city'].split(' ');
            for (let i = 0; i < cityArray.length; i++) {
              if (i === 0) {
                this.city = cityArray[i].charAt(0).toUpperCase() + cityArray[i].slice(1).toLowerCase() + ' ';
              } else {
                this.city += cityArray[i].charAt(0).toUpperCase() + cityArray[i].slice(1).toLowerCase() + ' ';
              }
            }
          }
        },
        error => {
        }
      );
    }
  }
  copyHours(value) {
    if (value === true) {
      this.sun_start1 = this.sun_start;
      this.sun_end1 = this.sun_end;
      this.mon_start1 = this.mon_start;
      this.mon_end1 = this.mon_end;
      this.tue_start1 = this.tue_start;
      this.tue_end1 = this.tue_end;
      this.wed_start1 = this.wed_start;
      this.wed_end1 = this.wed_end;
      this.thur_start1 = this.thur_start;
      this.thur_end1 = this.thur_end;
      this.fri_start1 = this.fri_start;
      this.fri_end1 = this.fri_end;
      this.sat_start1 = this.sat_start;
      this.sat_end1 = this.sat_end;
    }
    // else {
    //   this.sun_start1 = '';
    //   this.sun_end1 = '';
    //   this.mon_start1 = '';
    //   this.mon_end1 = '';
    //   this.tue_start1 = '';
    //   this.tue_end1 = '';
    //   this.wed_start1 = '';
    //   this.wed_end1 = '';
    //   this.thur_start1 = '';
    //   this.thur_end1 = '';
    //   this.fri_start1 = '';
    //   this.fri_end1 = '';
    //   this.sat_start1 = '';
    //   this.sat_end1 = '';
    // }
  }
  hoursModel() {
    this.actionPopup = 'add';
    this.customStartDate = new Date();
    this.customEndDate = new Date();
    this.fromDate1 = new Date();
    this.toDate = new Date();
    this.fromDate = new Date();
    this.workerHoursModel.show();
    this.customData = 'DAY(S) OFF';
    this.showField = true;
    // this.customweekList();
  }
  closeHoursModel() {
    this.workerHoursModel.hide();
    this.dispalyweeks = false;
    this.repeat = false;
    this.sunday = 0;
    this.monday = 0;
    this.tuesday = 0;
    this.wednesday = 0;
    this.thursday = 0;
    this.friday = 0;
    this.saturday = 0;
    this.hoursNote = '';
    this.same = '';
    this.hoursStartTime = '';
    this.hoursEndTime = '';
    this.repeat = false;
    this.popupErr = '';
    this.timeErr = '';
    this.disField = false;
    this.disField1 = false;
  }
  changeCustomData(data) {
    if (data === 'DAY(S) OFF') {
      this.showField = true;
      this.repeat = false;
      this.dispalyweeks = false;
      this.hoursNote = '';
      this.sunday = 0;
      this.monday = 0;
      this.tuesday = 0;
      this.wednesday = 0;
      this.thursday = 0;
      this.friday = 0;
      this.saturday = 0;
    } else {
      this.toDate = new Date();
      this.fromDate = new Date();
      this.showField = false;
      this.repeat = false;
      this.dispalyweeks = false;
      this.hoursNote = '';
      this.sunday = 0;
      this.monday = 0;
      this.tuesday = 0;
      this.wednesday = 0;
      this.thursday = 0;
      this.friday = 0;
      this.saturday = 0;
    }
  }
  displayWeeks(value) {
    if (value === true) {
      this.dispalyweeks = true;
    } else {
      this.dispalyweeks = false;
      this.sunday = 0;
      this.monday = 0;
      this.tuesday = 0;
      this.wednesday = 0;
      this.thursday = 0;
      this.friday = 0;
      this.saturday = 0;
    }
  }
  hoursCopy() {
    if (this.same === true) {
      this.copyHours(this.same);
    }
  }
  saveHours(id) {
    const obj = [];
    if (this.sunday) {
      obj.push(0);
    }
    if (this.monday) {
      obj.push(1);
    }
    if (this.tuesday) {
      obj.push(2);
    }
    if (this.wednesday) {
      obj.push(3);
    }
    if (this.thursday) {
      obj.push(4);
    }
    if (this.friday) {
      obj.push(5);
    }
    if (this.saturday) {
      obj.push(6);
    }
    const TIME_REGEXP = /^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9][ ][AP][M]$/;
    if (this.toDate === null) {
      this.popupErr = 'SETUP_COMPANY_HOURSE.STARTDATE_CANNOT_BLANK';
    } else if (!id && (this.toDate.setHours(0, 0, 0, 0) < new Date().setHours(0, 0, 0, 0))) {
      this.popupErr = 'SETUP_COMPANY_HOURSE.STARTDATE_SHOULD_NOT_BE_PAST';
    } else if (this.fromDate === null) {
      this.popupErr = 'SETUP_COMPANY_HOURSE.ENDDATE_CANNOT_BLANK';
    } else if (this.toDate > this.fromDate) {
      this.popupErr = 'SETUP_COMPANY_HOURSE.STARTDATE_MUST_BEFORE_ENDDATE';
    } else if ((this.customData === 'CUSTOM HOURS') && (this.hoursStartTime === '' || this.hoursEndTime === '')) {
      this.timeErr = 'SETUP_COMPANY_HOURSE.HOURS_STARTENDTIME_SHOULD_NOT_BLANK';
    } else if ((this.hoursStartTime !== '' && this.hoursEndTime === '') || (this.hoursStartTime === '' && this.hoursEndTime !== '')) {
      this.timeErr = 'Hours start/end times must both be empty (denoting open 24 hours) or have a valid values';
    } else if ((this.hoursStartTime !== '' && !TIME_REGEXP.test(this.hoursStartTime)) || (this.hoursEndTime !== '' && !TIME_REGEXP.test(this.hoursEndTime))) {
      this.timeErr = 'SETUP_COMPANY_HOURSE.HOURS_STARTENDTIME_MUST_IN_AMPM_FORMAT';
    } else if (this.fromDate1 === null) {
      this.popupErr = 'SETUP_COMPANY_HOURSE.STARTDATE_CANNOT_BLANK';
    } else if (this.customStartDate === null) {
      this.popupErr = 'SETUP_COMPANY_HOURSE.STARTDATE_CANNOT_BLANK';
    } else if (!id && (this.customStartDate.setHours(0, 0, 0, 0) < new Date().setHours(0, 0, 0, 0))) {
      this.popupErr = 'SETUP_COMPANY_HOURSE.STARTDATE_SHOULD_NOT_BE_PAST';
    } else if (this.customEndDate === null) {
      this.popupErr = 'SETUP_COMPANY_HOURSE.ENDDATE_CANNOT_BLANK';
    } else if (this.customStartDate > this.customEndDate) {
      this.popupErr = 'SETUP_COMPANY_HOURSE.CUSTOM_STARTDATE_MUST_BEFORE_ENDDATE';
    } else if (this.everyWeeks === '' || this.everyWeeks < 1) {
      this.popupErr = 'SETUP_COMPANY_HOURSE.VALID_EVERY_WEEK';
    } else if (this.repeat && obj.length === 0) {
      this.popupErr = 'SETUP_COMPANY_HOURSE.SELECT_WEEKS';
    } else {
      const csDate = this.customStartDate.getFullYear() + '-' + (this.customStartDate.getMonth() + 1) + '-' + this.customStartDate.getDate();
      const ceDate = this.customEndDate.getFullYear() + '-' + (this.customEndDate.getMonth() + 1) + '-' + this.customEndDate.getDate();
      const tDate = this.toDate.getFullYear() + '-' + (this.toDate.getMonth() + 1) + '-' + this.toDate.getDate();
      const fDate = this.fromDate.getFullYear() + '-' + (this.fromDate.getMonth() + 1) + '-' + this.fromDate.getDate();
      const fDate1 = this.fromDate1.getFullYear() + '-' + (this.fromDate1.getMonth() + 1) + '-' + this.fromDate1.getDate();
      const hoursData = {
        'customType': this.customData,
        'repeat': this.repeat,
        'hoursNote': this.hoursNote,
        'weeks': obj,
        'workerId': this.updateId,
        'everyWeeks': this.everyWeeks
      };
      if (id) {
        hoursData['groupId'] = id;
      }
      if (this.customData === 'CUSTOM HOURS') {
        if (this.repeat) {
          hoursData['toDate'] = csDate;
          hoursData['fromDate'] = ceDate;
        } else {
          hoursData['toDate'] = fDate1;
          hoursData['fromDate'] = fDate1;
        }
        hoursData['hoursStartTime'] = this.hoursStartTime;
        hoursData['hoursEndTime'] = this.hoursEndTime;
      } else {
        hoursData['toDate'] = tDate;
        hoursData['fromDate'] = fDate;
      }
      this.setupWorkersDetailsService.saveHours(hoursData).subscribe(
        data => {
          data = data['result'];
          this.toastermessage = this.translateService.get('Record Saved Successfully');
          this.toastr.success(this.toastermessage.value, null, { timeOut: 1500 });
          this.workerHoursModel.hide();
          this.clearHoursData();
          this.getCustomHours();
          this.disField = false;
          this.disField1 = false;
        },
        error => {
          const status = JSON.parse(error['_body']).result;
          const statuscode = JSON.parse(error['_body'])['status'];
          switch (JSON.parse(error['_body']).status) {
            case '2096':
              this.popupErr = 'SETUP_COMPANY_HOURSE.INVALID_SELECTION';
              break;
            case '2069':
              this.popupErr = 'SETUP_COMPANY_HOURSE.CUSTOMHOURS_STARTTIME_MUST_PRIOR_TO_ENDTIME';
              break;
            case '2097':
              this.popupErr = 'SETUP_COMPANY_HOURSE.RECORD_WITH_SAMEDATE_ALLREADY_EXIST';
              break;
          }
          if (statuscode === '2085' || statuscode === '2071') {
            this.router.navigate(['/']).then(() => { });
          }
        }
      );
    }
  }
  clearHoursData() {
    this.customStartDate = new Date();
    this.customEndDate = new Date();
    this.fromDate1 = new Date();
    this.repeat = false;
    this.toDate = new Date();
    this.fromDate = new Date();
    this.repeat = false;
    this.dispalyweeks = false;
    this.hoursNote = '';
    this.popupErr = '';
    this.hoursStartTime = '';
    this.hoursEndTime = '';
    this.everyWeeks = 1;
    this.sunday = 0;
    this.monday = 0;
    this.tuesday = 0;
    this.wednesday = 0;
    this.thursday = 0;
    this.friday = 0;
    this.saturday = 0;
  }
  getCustomHours() {
    this.setupWorkersDetailsService.getCustomHrs(this.updateId).subscribe(
      data => {
        this.getHoursData = data['result'];
        this.displayList = [];
        this.displayTempList = [];
        this.unqCustList = [];
        this.getHoursData.forEach(obj => {
          if (this.unqCustList.indexOf(obj['UserId__c']) === -1) {
            this.unqCustList.push(obj['UserId__c']);
          }
        });
        this.unqCustList.forEach(obj => {
          const tempList = this.getHoursData.filter(obj2 => obj2['UserId__c'] === obj);
          let curDate = new Date();
          curDate = this.commonService.getDateFrmDBDateStr(curDate.getFullYear()
            + '-' + ('0' + (curDate.getMonth() + 1)).slice(-2)
            + '-' + ('0' + curDate.getDate()).slice(-2));
          const tempDate = this.commonService.getDateFrmDBDateStr(tempList[tempList.length - 1]['Date__c']);
          let expStatus = true;
          if (tempDate < curDate) {
            expStatus = false;
          }
          const tempDate2 = this.commonService.getDateFrmDBDateStr(tempList[0]['Date__c']);
          let delStatus = true;
          if (tempDate2 < curDate) {
            delStatus = false;
          }
          if (tempList.length === 1) {
            if (tempList[0]['All_Day_Off__c'] && (tempList[0]['IsWorkerHours__c'] === '0' || tempList[0]['IsWorkerHours__c'] === 0)) {
              this.displayList.push({
                'uniqId': obj,
                'message': 'ALL DAY OFF: ' + tempList[0]['date'] + ' (' + tempList[0]['Name'] + ')',
                'expStatus': expStatus,
                'delStatus': delStatus
              });
            } else if (tempList[0]['All_Day_Off__c']) {
              this.displayList.push({
                'uniqId': obj,
                'message': 'COMPANY DAYS OFF: ' + tempList[0]['date'] + ' (' + tempList[0]['Name'] + ')',
                'expStatus': expStatus,
                'delStatus': false,
                'editStatus': 'edit'
              });
            } else {
              this.displayList.push({
                'uniqId': obj,
                'message': 'CUSTOM HOURS: ' + tempList[0]['date'] + ' ' + tempList[0]['StartTime__c'] + ' - ' + tempList[0]['EndTime__c'] + ' (' + tempList[0]['Name'] + ')',
                'expStatus': expStatus,
                'delStatus': delStatus
              });
            }
          } else {
            if (tempList[0]['All_Day_Off__c']) {
              if (tempList[0]['BusinessHoursId__c'].length > 0 && JSON.parse(tempList[0]['BusinessHoursId__c']).length > 0) {
                this.displayList.push({
                  'uniqId': obj,
                  'message': 'ALL DAY OFF (Every ' + JSON.parse(tempList[0]['Every_Weeks__c'])
                    + ' Weeks): ' + this.rtnDaysStr(tempList[0]['BusinessHoursId__c'])
                    + ' ' + tempList[0]['date'] + ' - ' + tempList[tempList.length - 1]['date'] + ' (' + tempList[0]['Name'] + ')',
                  'expStatus': expStatus,
                  'delStatus': delStatus
                });
              } else if (tempList[0]['IsWorkerHours__c'] === '1' || tempList[0]['IsWorkerHours__c'] === 1) {
                this.displayList.push({
                  'uniqId': obj,
                  'message': 'COMPANY DAYS OFF: ' + tempList[0]['date'] + ' - ' + tempList[tempList.length - 1]['date'] + ' (' + tempList[0]['Name'] + ')',
                  'expStatus': expStatus,
                  'delStatus': false,
                  'editStatus': 'edit'
                });
              } else {
                this.displayList.push({
                  'uniqId': obj,
                  'message': 'ALL DAY OFF: ' + tempList[0]['date'] + ' - ' + tempList[tempList.length - 1]['date'] + ' (' + tempList[0]['Name'] + ')',
                  'expStatus': expStatus,
                  'delStatus': delStatus
                });
              }
            } else {
              if (JSON.parse(tempList[0]['BusinessHoursId__c']).length > 0) {
                this.displayList.push({
                  'uniqId': obj,
                  'message': 'CUSTOM HOURS (Every ' + JSON.parse(tempList[0]['Every_Weeks__c'])
                    + ' Weeks): ' + this.rtnDaysStr(tempList[0]['BusinessHoursId__c']) + ' ' + tempList[0]['date'] + ' - ' + tempList[tempList.length - 1]['date']
                    + ' ' + tempList[0]['StartTime__c'] + ' - ' + tempList[0]['EndTime__c'] + ' (' + tempList[0]['Name'] + ')',
                  'expStatus': expStatus,
                  'delStatus': delStatus
                });
              } else {
                this.displayList.push({
                  'uniqId': obj,
                  'message': 'CUSTOM HOURS: ' + tempList[0]['StartTime__c'] + ' - ' + tempList[tempList.length - 1]['EndTime__c'] + ' (' + tempList[0]['Name'] + ')',
                  'expStatus': expStatus,
                  'delStatus': delStatus
                });
              }


            }
          }
        });
        this.displayList.forEach(obj => obj['message'] = obj['message'].replace('()', ''));
        this.showExpiredHrs();
      },
      error => {
        const status = JSON.parse(error['_body']).result;
        const statuscode = JSON.parse(error['_body'])['status'];
        if (statuscode === '2085' || statuscode === '2071') {
          this.router.navigate(['/']).then(() => { });
        }
      }
    );
  }
  deleteHoursData(id) {
    this.setupWorkersDetailsService.deleteCustomHours(id).subscribe(
      data => {
        const data1 = data['result'];
        if (data1) {
          this.toastr.success('Record successfully deleted', null, { timeOut: 1500 });
          this.getCustomHours();
          this.disField = false;
          this.disField1 = false;
        }
      },
      error => {
        const status = JSON.parse(error['_body']).result;
        const statuscode = JSON.parse(error['_body'])['status'];
        if (statuscode === '2085' || statuscode === '2071') {
          this.router.navigate(['/']).then(() => { });
        }
      }
    );
  }

  rtnDaysStr(daysAryStr) {
    const daysAry = JSON.parse(daysAryStr);
    let rtnstr = '';
    const weekDays = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    if (daysAry.length === 1) {
      rtnstr = weekDays[daysAry[0]];
    } else {
      for (let x = 0; x < daysAry.length; x++) {
        if (x === daysAry.length - 2) {
          rtnstr += weekDays[daysAry[x]] + ' & ';
        } else if (x === daysAry.length - 1) {
          rtnstr += weekDays[daysAry[x]];
        } else {
          rtnstr += weekDays[daysAry[x]] + ', ';
        }
      }
    }
    return rtnstr;
  }
  editHoursData(id) {
    this.uniqueId = id;
    this.actionPopup = 'edit';
    const tempList = this.getHoursData.filter(obj2 => obj2['UserId__c'] === id);
    this.hoursNote = tempList[0]['Name'];
    this.sunday = 0;
    this.monday = 0;
    this.tuesday = 0;
    this.wednesday = 0;
    this.thursday = 0;
    this.friday = 0;
    this.saturday = 0;
    if (JSON.parse(tempList[0]['BusinessHoursId__c']).length > 0) {
      this.repeat = true;
      this.dispalyweeks = true;
      this.everyWeeks = JSON.parse(tempList[0]['Every_Weeks__c']);
      const obj = JSON.parse(tempList[0]['BusinessHoursId__c']);
      obj.forEach(days => {
        if (days === 0) {
          this.sunday = 1;
        } else if (days === 1) {
          this.monday = 1;
        } else if (days === 2) {
          this.tuesday = 1;
        } else if (days === 3) {
          this.wednesday = 1;
        } else if (days === 4) {
          this.thursday = 1;
        } else if (days === 5) {
          this.friday = 1;
        } else if (days === 6) {
          this.saturday = 1;
        }
      });
    } else {
      this.repeat = false;
      this.dispalyweeks = false;
    }
    if (tempList[0]['All_Day_Off__c']) {
      this.customData = 'DAY(S) OFF';
      this.showField = true;
      this.toDate = this.commonService.getDateFrmDBDateStr(tempList[0]['Date__c']);
      let date1 = new Date();
      date1 = this.commonService.getDateFrmDBDateStr(date1.getFullYear()
        + '-' + ('0' + (date1.getMonth() + 1)).slice(-2)
        + '-' + ('0' + date1.getDate()).slice(-2));
      if (this.toDate < date1) {
        this.fromDate = date1;
      } else {
        this.fromDate = this.commonService.getDateFrmDBDateStr(tempList[tempList.length - 1]['Date__c']);
      }
    } else {
      this.customData = 'CUSTOM HOURS';
      this.showField = false;
      this.hoursStartTime = tempList[0]['StartTime__c'];
      this.hoursEndTime = tempList[0]['EndTime__c'];
      if (JSON.parse(tempList[0]['BusinessHoursId__c']).length > 0) {
        this.customStartDate = this.commonService.getDateFrmDBDateStr(tempList[0]['Date__c']);
        let date2 = new Date();
        date2 = this.commonService.getDateFrmDBDateStr(date2.getFullYear()
          + '-' + ('0' + (date2.getMonth() + 1)).slice(-2)
          + '-' + ('0' + date2.getDate()).slice(-2));
        if (this.customStartDate < date2) {
          this.customEndDate = date2;
        } else {
          this.customEndDate = this.commonService.getDateFrmDBDateStr(tempList[tempList.length - 1]['Date__c']);
        }
      } else {
        this.fromDate1 = this.commonService.getDateFrmDBDateStr(tempList[0]['Date__c']);
      }
    }
    let curDate = new Date();
    curDate = this.commonService.getDateFrmDBDateStr(curDate.getFullYear()
      + '-' + ('0' + (curDate.getMonth() + 1)).slice(-2)
      + '-' + ('0' + curDate.getDate()).slice(-2));
    const tempDate2 = this.commonService.getDateFrmDBDateStr(tempList[0]['Date__c']);
    this.disField = false;
    if (tempDate2 < curDate) {
      this.disField = true;
    }
    const tempDate = this.commonService.getDateFrmDBDateStr(tempList[tempList.length - 1]['Date__c']);
    this.disField1 = false;
    if (tempDate < curDate) {
      this.disField1 = true;
    }
    this.workerHoursModel.show();
  }
  showExpiredHrs() {
    if (!this.showExpiry) {
      this.displayTempList = this.displayList.filter(
        filterList => filterList.expStatus === true);
    } else {
      this.displayTempList = this.displayList;
    }
  }

  invAddWork() {
    this.toastermessage = this.translateService.get('Please upgrade your package to perform this action');
    this.toastr.info(this.toastermessage.value, null, { timeOut: 3000 });
  }

  populateFromDate(type) {
    if (type === 'dayoff' && this.toDate > this.fromDate) {
      this.fromDate = this.toDate;
    } else if (type === 'custom' && this.customStartDate > this.customEndDate) {
      this.customEndDate = this.customStartDate;
    }
  }

  serviceModel() {
    this.workerServiceModel.show();
  }
  clearSerType(value) {
    if (value === 'Amount') {
      this.serviceFeeType = 'Amount';
      this.serviceFeeType1 = '';
    } else {
      this.serviceFeeType = '';
      this.serviceFeeType1 = 'percent';
    }
  }
  closeModel() {
    const currencyPattern = /^[0-9]+(\.[0-9]{1,2})?$/;
    if (this.serviceFee) {
      if (this.serviceFeeType === 'Amount') {
        if (!currencyPattern.test(this.serviceFee)) {
          this.editError = 'SETUP_WORKER_SERVICES.SHOULD_ALLOW_POSITIVE_DECIMAL';
        } else if (parseFloat(this.serviceFee) > parseFloat(this.servicePrice)) {
          this.editError = 'SETUP_WORKER_SERVICES.PRICE_TO_BE_GREATERTHAN_SERVICE_FEE';
        } else {
          this.editError = '';
        }
      }
      if (this.serviceFeeType1 === 'percent') {
        if (!currencyPattern.test(this.serviceFee)) {
          this.editError = 'SETUP_WORKER_SERVICES.SHOULD_ALLOW_POSITIVE_DECIMAL';
        } else if (parseFloat(this.serviceFee) < 0 || parseFloat(this.serviceFee) > 100) {
          this.editError = 'SETUP_SERVICES.SERVICE_FEE_PERCENT';
        } else {
          this.editError = '';
        }
      }
    } else {
      this.serviceFeeType = '';
      this.serviceFeeType1 = '';
    }
    // if ((this.serviceDuration_1 === null || this.serviceDuration_1 === 0) &&
    //   ((this.serviceDuration_2 !== null && this.serviceDuration_2 > 0) ||
    //     (this.serviceDuration_3 !== null && this.serviceDuration_3 > 0) ||
    //     (this.serviceBufferAfter !== null && this.serviceBufferAfter > 0 ||
    //       this.serviceBufferAfter !== '' && parseInt(this.serviceBufferAfter, 10) > 0))) {
    //   this.editError = 'VALIDATION_MSG.DURATION1_REQUIRED';
    // }
    // this.checkServiceErrOnPopupAndSave();
    // if (this.workerServiceError === '' || this.workerServiceError === undefined) {
    //   this.workerServiceModel.hide();
    // } else {
    //   this.workerServiceModel.show();
    // }
    if (this.editError === '' || this.editError === undefined || this.editError === 'undefined') {
      this.workerServiceModel.hide();
      this.serviceFeeType = '';
      this.serviceFeeType1 = '';
    }
  }

  closeServiceModel() {
    this.workerServiceModel.hide();
    this.editError = '';
    // this.applyPriceLevel();
    //  this.assaignWorkerServices();
    this.getWorkerServices();
  }
  resetService() {
    this.applyPriceLevel();
  }
  getAddOnService(serviceId, index) {
    this.addOnServiceValue = this.serviceDetailsList[index]['Add_On_Service__c'];
    for (let i = 0; i < this.serviceDetailsList.length; i++) {
      if (serviceId === this.serviceDetailsList[index]['Id']) {
        if (this.addOnServiceValue === 1) {
          this.addOnservice = true;
          this.serviceDuration_1 = 0;
          this.serviceDuration_2 = 0;
          this.serviceDuration_3 = 0;
        } else {
          this.addOnservice = false;
        }
      }
    }
  }
  removeProPic() {
    this.showRemoveButton = false;
    this.workerImage.srcURL = '';
    this.image = '';
    delete this.workerImage.file;
  }
}
