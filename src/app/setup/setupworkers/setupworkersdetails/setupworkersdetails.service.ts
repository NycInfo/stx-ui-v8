/*
  * Display single user details of front end users
  * extractData(): To extract the data
  * handleError(): To handle error messages.
*/
import { Injectable, Inject } from '@angular/core';
import { Response } from '@angular/http';
import { Headers } from '@angular/http';
import { HttpClients } from '../../../common/http-client';
import { map } from 'rxjs/operators';

@Injectable()
export class SetupWorkersDetailsService {
  private bookingDataUrl = 'common.json';
  constructor(
    private http: HttpClients,
    @Inject('apiEndPoint') private apiEndPoint: string,
    @Inject('staticJsonFilesEndPoint') private staticJsonFilesEndPoint: string
  ) { }
  getUserList() {
    return this.http.get(this.apiEndPoint + '/api/setupworkers/setupworkerdetail')
      .pipe(map(this.extractData));
  }
  saveUser(updateId, dataObj, imgFile) {
    if (updateId) {
      dataObj.page = 'edit';
    } else {
      dataObj.page = 'add';
    }
    const data = new FormData();
    data.append('workerInfo', JSON.stringify(dataObj));
    data.append('workerImage', imgFile);
    return this.http.put(this.apiEndPoint + '/api/setupworkers/setupworkerdetail/' + updateId, data)
      .pipe(map(this.extractData));
  }
  saveWorkerServices(workerServiceData) {
    return this.http.put(this.apiEndPoint + '/api/setupworkers/setupworkerservice', workerServiceData)
      .pipe(map(this.extractData));
  }
  getLookupsList(lookupType) {
    return this.http.get(this.apiEndPoint + '/api/v1/lookups/' + lookupType)
      .pipe(map(this.extractData));
  }
  getWorekrRoleData() {
    return this.http.get(this.staticJsonFilesEndPoint + 'common.json')
      .pipe(map(this.extractData));
  }
  getStates(countryName) {
    return this.http.get(this.apiEndPoint + '/api/v1/lookups/states/' + countryName)
      .pipe(map(this.extractData));
  }
  getMobileData() {
    return this.http.get(this.apiEndPoint + '/api/setup/clientpreferences/mobilecarriers')
      .pipe(map(this.extractData));
  }
  /* below service used to get worker hours to display */
  getUserHours(id) {
    return this.http.get(this.apiEndPoint + '/api/setupworkers/setupworkerdetail/workerhours/' + id)
      .pipe(map(this.extractData));
  }
  saveHours(data) {
    return this.http.post(this.apiEndPoint + '/api/setupworkers/setupworkerdetail/workercustomhours', data)
      .pipe(map(this.extractData));
  }
  getCustomHrs(id) {
    return this.http.get(this.apiEndPoint + '/api/setupworkers/setupworkerdetail/workercustomhours/' + id)
      .pipe(map(this.extractData));
  }
  deleteCustomHours(id) {
    return this.http.delete(this.apiEndPoint + '/api/setupworkers/setupworkerdetail/customhours/' + id)
      .pipe(map(this.extractData));
  }
  customHrsList() {
    return this.http.get(this.staticJsonFilesEndPoint + 'common.json')
      .pipe(map(this.extractData));
  }
  getSetupServicesData() {
    const inactive = 1;
    return this.http.get(this.apiEndPoint + '/api/setupservices/servicegroups/' + inactive)
      .pipe(map(this.extractData));
  }
  getSetupServiceGroupsList() {
    return this.http.get(this.apiEndPoint + '/api/setupservices/servicegroups/')
      .pipe(map(this.extractData));
  }
  showInactiveServiceListByGroupName(active, serviceGroupName) {
    if (serviceGroupName === 'Classes') {
      serviceGroupName = 'System Class';
    }
    return this.http.get(this.apiEndPoint + '/api/setup/setupservice/activeinactive/' + active + '/' + serviceGroupName)
      .pipe(map(this.extractData));
  }
  getClasses() {
    const type = 'true';
    return this.http.get(this.apiEndPoint + '/api/setup/setupclass/activeinactive')
      .pipe(map(this.extractData));
  }
  getBookingData() {
    return this.http.get(this.apiEndPoint + '/api/appointmentsandemails/booking')
      .pipe(map(this.extractData));
  }
  hoursForOnline() {
    return this.http.get(this.apiEndPoint + '/api/setup/company/companyhours')
      .pipe(map(this.extractData));
  }
  getservicesByUser(updateId) {
    return this.http.get(this.apiEndPoint + '/api/setupworkers/setupworkerdetail/workerservices/' + updateId)
      .pipe(map(this.extractData));
  }
  getpermissioneList() {
    return this.http.get(this.apiEndPoint + '/api/setupworkers/setuppermissions')
      .pipe(map(this.extractData));
  }
  getMethods() {
    return this.http.get(this.apiEndPoint + '/api/setupworkers/setupcompensationmethods')
      .pipe(map(this.extractData));
  }
  getLevels() {
    return this.http.get(this.staticJsonFilesEndPoint + 'common.json')
      .pipe(map(this.extractData));
  }
  /*--- Goals code ---*/
  getGoals() {
    return this.http.get(this.apiEndPoint + '/api/setupworkers/setupgoals')
      .pipe(map(this.extractData));
  }
  previousGoals(goals, workerId, date) {
    const header = new Headers();
    const dataObj = {
      'type': goals,
      'workerId': workerId,
      'date': date
    };
    header.append('params', JSON.stringify(dataObj));
    return this.http.getHeader(this.apiEndPoint + '/api/setup/workers/workergoals', header)
      .pipe(map(this.extractData));
  }
  createGoals(goalsData) {
    //  alert('service' + JSON.stringify(goalsData));
    return this.http.post(this.apiEndPoint + '/api/setup/workers/workergoals', goalsData)
      .pipe(map(this.extractData));
  }
  updateGoal(goallist, workerId) {
    //  alert(workerId);
    return this.http.put(this.apiEndPoint + '/api/setup/workers/workergoals/calculategoal/' + workerId, goallist)
      .pipe(map(this.extractData));
  }
  updatePassword(userId, password) {
    return this.http.put(this.apiEndPoint + '/api/users/password/' + userId, { 'password': password })
      .pipe(map(this.extractData));
  }
  // deleteGoal(workerGoalId) {
  //   return this.http.delete(this.apiEndPoint + '/api/setup/workersworkers/workergoals/' + workerGoalId)
  //     .pipe(map(this.extractData))
  //     .catch(this.handleError);

  // }
  /*To extract json data*/
  private extractData(res: Response) {
    if (res.headers && res.headers.get('token')) {
      localStorage.setItem('token', res.headers.get('token'));
    }
    const body = res.json();
    return body || {};
  }
}
