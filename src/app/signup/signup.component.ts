import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { SignupService } from './signup.service';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss'],
  providers: [SignupService]
})

export class SignupComponent implements OnInit {

  @ViewChild('miscModal' , {static: false}) public miscModal: ModalDirective;
  constructor(private signupService: SignupService, private router: Router, private toastr: ToastrService) { }
  country = '';
  mailingState = '';
  errorMessage: any;
  statesList: any;
  allCountry: any;
  getTimezones: any;
  firstName = '';
  lastName = '';
  email = '';
  countryCode = '01';
  phone = '';
  timeZone = '';
  companyName = '';
  key = '';
  password = '';
  passwordType1 = 'password';
  passwordType2 = 'password';
  confirmPassword = '';
  glyphiconClass1 = 'glyphicon-eye-open';
  glyphiconClass2 = 'glyphicon-eye-open';
  package = '';

  ngOnInit() {
    this.getCountry();
    this.listingTimeZones();
  }

  signupData() {
    const date = new Date();
    const EMAIL_REGEXP = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (this.firstName === '') {
      this.toastr.error('First Name is required', null, { timeOut: 3000 });
    } else if (this.lastName === '') {
      this.toastr.error('Last Name is required', null, { timeOut: 3000 });
    } else if (this.email === '') {
      this.toastr.error('Email is required', null, { timeOut: 3000 });
    } else if (this.email !== '' && !EMAIL_REGEXP.test(this.email)) {
      this.toastr.error('Email is invalid', null, { timeOut: 3000 });
    } else if (this.phone === '') {
      this.toastr.error('Phone is required', null, { timeOut: 3000 });
    } else if (this.companyName === '') {
      this.toastr.error('Company Name is required', null, { timeOut: 3000 });
    } else if (this.timeZone === '') {
      this.toastr.error('Time zone is required', null, { timeOut: 3000 });
    } else if (this.country === '') {
      this.toastr.error('Country is required', null, { timeOut: 3000 });
    } else if (this.password === '') {
      this.toastr.error('Password is required', null, { timeOut: 3000 });
    } else if (this.confirmPassword === '') {
      this.toastr.error('Confirm Password is required', null, { timeOut: 3000 });
    } else if (this.password !== this.confirmPassword) {
      this.toastr.error('Password and Confirm Password should be same', null, { timeOut: 3000 });
    } else if (this.key === '') {
      this.toastr.error('Key is required', null, { timeOut: 3000 });
    } else if (this.key.length !== 5) {
      this.toastr.error('The Key must be a 5 digit number', null, { timeOut: 3000 });
    } else if (this.package === '') {
      this.toastr.error('Please select Package', null, { timeOut: 3000 });
    } else {
      const date1 = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
      const dataObj = {
        firstName: this.firstName,
        lastName: this.lastName,
        email: this.email,
        countryCode: this.countryCode,
        phone: this.phone !== '' && this.phone ? this.countryCode + '-' + this.phone : '',
        companyName: this.companyName,
        timeZone: this.getTmZone(this.timeZone),
        country: this.country,
        state: this.mailingState,
        key: this.key,
        password: this.password,
        date: date1,
        package: this.package
      };

      this.signupService.signUp(dataObj).subscribe(
        data => {
          this.toastr.success('We accepted your request. You will receive a mail after creation of your account.', null, { timeOut: 10000 });
          this.router.navigate(['/']);
        },
        error => {
          const status = JSON.parse(error['status']);
          const statuscode = JSON.parse(error['_body']).status;
          switch (JSON.parse(error['_body']).status) {
            case '2094':
              this.toastr.error('Duplicate Key', null, { timeOut: 5000 });
              break;
            case '2093':
              this.toastr.error('Duplicate Email', null, { timeOut: 5000 });
              break;
          }
          if (statuscode === '2085' || statuscode === '2071') {
            if (this.router.url !== '/') {
              localStorage.setItem('page', this.router.url);
              this.router.navigate(['/']).then(() => { });
            }
          }
        }
      );
    }
  }

  getStates(coun) {
    this.signupService.getStates(coun).subscribe(
      statesValues => {
        this.statesList = statesValues['result'];
      },
      error => {
        this.errorMessage = <any>error;
        const errStatus = JSON.parse(error['_body'])['status'];
        if (errStatus === '2085' || errStatus === '2071') {
          if (this.router.url !== '/') {
            localStorage.setItem('page', this.router.url);
            this.router.navigate(['/']).then(() => { });
          }
        }
      }
    );
  }

  getCountry() {
    this.signupService.getCountry('COUNTRIES').subscribe(
      data => {
        this.allCountry = data['result'];
      }, error => {
        this.errorMessage = <any>error;
        const errStatus = JSON.parse(error['_body'])['status'];
        if (errStatus === '2085' || errStatus === '2071') {
          if (this.router.url !== '/') {
            localStorage.setItem('page', this.router.url);
            this.router.navigate(['/']).then(() => { });
          }
        }
      }
    );
  }

  listingTimeZones() {
    this.signupService.timeZones().subscribe(
      data => {
        this.getTimezones = data['result'];
      },
      error => {
        const errStatus = JSON.parse(error['_body'])['status'];
        if (errStatus === '2085' || errStatus === '2071') {
          if (this.router.url !== '/') {
            localStorage.setItem('page', this.router.url);
            this.router.navigate(['/']).then(() => { });
          }
        }
      }
    );
  }

  hyphen_generate_mobile1(value) {
    if (value === undefined) {
      value = '';
    }
    if (value.length === 0) {
      (<HTMLInputElement>document.getElementById('mobile_id1')).value = value.concat('(');
    }
    if (value.length === 4) {
      (<HTMLInputElement>document.getElementById('mobile_id1')).value = value.concat(')');
    } if (value.length === 8) {
      (<HTMLInputElement>document.getElementById('mobile_id1')).value = value.concat('-');
    }
  }

  /* method to restrict specialcharecters  */
  numOnly(event: any) {
    const pattern = /[0-9]/;
    const inputChar = String.fromCharCode(event.charCode);
    if (!pattern.test(inputChar)) {
      // invalid character, prevent input
      event.preventDefault();
    }
  }

  togglePassword(index) {
    if (index === 1) {
      if (this.glyphiconClass1 === 'glyphicon-eye-open') {
        this.glyphiconClass1 = 'glyphicon-eye-close';
        this.passwordType1 = 'text';
      } else {
        this.glyphiconClass1 = 'glyphicon-eye-open';
        this.passwordType1 = 'password';
      }
    } else if (index === 2) {
      if (this.glyphiconClass2 === 'glyphicon-eye-open') {
        this.glyphiconClass2 = 'glyphicon-eye-close';
        this.passwordType2 = 'text';
      } else {
        this.glyphiconClass2 = 'glyphicon-eye-open';
        this.passwordType2 = 'password';
      }
    }
  }

  getTmZone(tmZnStr) {
    let tempVal = tmZnStr.split(')')[0];
    if (tempVal.indexOf('+') > -1) {
        tempVal = '+' + tempVal.split('+')[1];
    } else {
        tempVal = '-' + tempVal.split('-')[1];
    }
    return tempVal;
  }

}
