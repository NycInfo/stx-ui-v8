import { Injectable, Inject } from '@angular/core';
import { Response } from '@angular/http';
// import { Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { HttpClients } from '../common/http-client';
import { map } from 'rxjs/operators';

@Injectable()
export class SignupService {
    constructor(
        private http: HttpClients,
        @Inject('apiEndPoint') private apiEndPoint: string,
        @Inject('staticJsonFilesEndPoint') private staticJsonFilesEndPoint: string
    ) { }
    getStates(countryName) {
        return this.http.get(this.apiEndPoint + '/api/v1/lookups/states/' + countryName)
            .pipe(map(this.extractData));
    }
    getCountry(lookupType) {
        return this.http.get(this.apiEndPoint + '/api/v1/lookups/' + lookupType)
            .pipe(map(this.extractData));
    }
    signUp(dataObj) {
        return this.http.post(this.apiEndPoint + '/api/signup', dataObj)
            .pipe(map(this.extractData));
    }
    timeZones() {
        return this.http.get(this.apiEndPoint + '/api/timezones')
            .pipe(map(this.extractData));
    }
    private extractData(res: Response) {
        if (res.headers && res.headers.get('token')) {
            localStorage.setItem('token', res.headers.get('token'));
        }
        const body = res.json();
        return body || {};
    }
}