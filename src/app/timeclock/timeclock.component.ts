import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { TimeClockService } from './timeclock.service';
import { CheckOutEditTicketService } from '../checkout/editticket/checkouteditticket.service';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { JwtHelper } from 'angular2-jwt';
import { CommonService } from '../common/common.service';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'app-setuprewards-app',
  templateUrl: './timeclock.html',
  styleUrls: ['./timeclock.component.scss'],
  providers: [TimeClockService, CheckOutEditTicketService, CommonService],
})
export class TimeClockComponent implements OnInit {
  activeTab2 = [false, false];
  activeTabClass = ['active', ''];
  datePickerConfig: any;
  currentDate = new Date();
  rows = [];
  rowLength: any;
  rowLengthInc: any;
  workerPin: any;
  workerData: any;
  showButton = false;
  buttonVale = '';
  inTime: any;
  outTime: any;
  clockIn: any;
  clockOut: any;
  hours: any;
  showSave = false;
  schedule: any;
  timeClockData: any;
  timeData = [];
  errorMessage = '';
  adjustmentErrorMessage: any = '';
  adjustmentErrMessage: any = '';
  workersList = [];
  workerId: any;
  activeRowsLength: any = 0;
  selectedDate = new Date();
  companyHours = [];
  workerHours: any = {};
  isScheduleApplied = false;
  hideField = 'inline';
  weekDays = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
  decodedUserToken: any;
  workerRole: any;
  test: any;
  showApplyScheduleCheckBox = false;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private toastr: ToastrService,
    private commonService: CommonService,
    private translateService: TranslateService,
    private timeClockService: TimeClockService,
    private checkOutEditTicketService: CheckOutEditTicketService) {
    this.datePickerConfig = Object.assign({},
      {
        showWeekNumbers: false,
        containerClass: 'theme-blue',
      });
  }
  ngOnInit() {
    try {
      this.decodedUserToken = new JwtHelper().decodeToken(localStorage.getItem('token'));
      this.workerRole = this.decodedUserToken.data.workerRole;
    } catch (error) {
      this.decodedUserToken = {};
    }
    this.updateTabs(0);
    // this.addRows();
    this.getWorkerTimeClock(this.selectedDate);
    this.method();
    this.getAllActiveWorkers();
  }
  getWorkerByPin() {
    this.timeClockService.getWorkerByPin(this.workerPin, this.commonService.getDBDatTmStr(this.currentDate)).subscribe(data => {
      if (data.result.workerresult.length > 0 && data.result.workerresult !== '2103') {
        this.errorMessage = '';
        this.workerData = data.result.workerresult[0];
        if (this.workerData.workerId !== '') {
          this.hideField = 'none';
        }
        this.companyHours = data.result.companyhours;
        if (data.result.timeClockresult.length > 0) {
          this.workerData.Time_In__c = data.result.timeClockresult[0].Time_In__c;
          this.workerData.tcId = data.result.timeClockresult[0].tcId;
          this.clockIn = this.commonService.getDateTmFrmDBDateStr(data.result.timeClockresult[0].Time_In__c);
          this.inTime = this.clockIn.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true });
          this.buttonVale = 'Clock Out';
          this.showApplyScheduleCheckBox = true;
        } else {
          this.buttonVale = 'Clock In';
          this.showApplyScheduleCheckBox = true;
        }
        this.showButton = true;
        this.showSave = true;
        if (this.companyHours && this.companyHours[0].All_Day_Off__c === 1) {
          this.schedule = 'Closed';
        } else if (this.companyHours && this.companyHours[0].EndTime__c && this.companyHours[0].StartTime__c) {
          this.workerHours = !isNullOrUndefined(this.companyHours) ? this.getWorkerHours(this.currentDate, this.companyHours[0]) : {};
        } else {
          this.workerHours = !isNullOrUndefined(this.companyHours) ? this.getWorkerHours(this.currentDate, this.companyHours[0]) : {};
        }
        if (this.schedule !== 'Closed' && this.workerData.Uses_Time_Clock__c === 1
          && !isNullOrUndefined(this.companyHours) && this.workerHours.timeIn !== '') {
          this.schedule = this.workerHours.timeIn + ' to ' + this.workerHours.timeOut;
        } else {
          this.schedule = 'Closed';
        }
      } else if (data.result.workerresult && data.result.workerresult === '2103') {
        this.errorMessage = 'Worker is inActive';
      } else {
        this.errorMessage = 'Invalid Pin';
        this.workerData = '';
        this.showApplyScheduleCheckBox = false;
      }
    },
      error => {
        const errStatus = JSON.parse(error._body).status;
        if (errStatus === '2085' || errStatus === '2071') {
          if (this.router.url !== '/') {
            localStorage.setItem('page', this.router.url);
            this.router.navigate(['/']).then(() => { });
          }
        }
      });
  }
  applySchedule() {
    if (this.schedule !== 'Closed' && this.isScheduleApplied) {
      this.inTime = this.workerHours.timeIn;
      this.outTime = this.workerHours.timeOut;
      this.clockIn = this.timeConversionToDate(this.inTime);
      this.clockOut = this.timeConversionToDate(this.outTime);
      this.hours = this.calculateDurations(this.clockIn, this.clockOut);
      this.showButton = false;
    } else {
      this.inTime = undefined;
      this.outTime = undefined;
      this.hours = undefined;
      this.buttonVale = 'Clock In';
      this.showButton = this.schedule === 'Closed' && this.isScheduleApplied ? false : true;
    }
  }
  checkAction(value) {
    if (value === 'Clock In') {
      this.buttonVale = 'Clock Out';
      this.clockIn = new Date();
      this.inTime = this.clockIn.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true });
    } else if (this.buttonVale === 'Clock Out') {
      this.clockOut = new Date();
      this.outTime = this.clockOut.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true });
      this.showButton = false;
      this.hours = this.calculateDurations(this.clockIn, this.clockOut);
    }
  }
  getWorkerHours(timeInDate: Date, workerHours): { 'timeIn': string, 'timeOut': string } {
    const day = this.weekDays[timeInDate.getDay()];
    const workerTimings: any = {};
    if (workerHours.StartTime__c && workerHours.EndTime__c) {
      workerTimings.timeIn = workerHours.StartTime__c;
      workerTimings.timeOut = workerHours.EndTime__c;
    } else {
      for (const key in workerHours) {
        if (key === day + 'StartTime__c') {
          workerTimings.timeIn = workerHours[key];
        } else if (key === day + 'EndTime__c') {
          workerTimings.timeOut = workerHours[key];
        }
      }
    }
    return workerTimings;
  }
  calculateDurations(inTime: Date, outTime: Date): string {
    this.getTimeFormat();
    let hours: number;
    if (!outTime.getTime()) {
      hours = 0;
    } else {
      if (inTime.getTime() <= outTime.getTime()) {
        hours = (outTime.getTime() - inTime.getTime()) / (1000 * 60 * 60);
        return hours.toFixed(3);
      } else {
        this.adjustmentErrorMessage = 'Time In must be less than Time Out';
      }
    }
  }
  clear() {
    this.hideField = 'inline';
    this.workerData = [];
    this.schedule = undefined;
    this.showButton = false;
    this.workerPin = undefined;
    this.isScheduleApplied = false;
    this.inTime = undefined;
    this.outTime = undefined;
    this.hours = undefined;
    this.clockOut = '';
    this.errorMessage = '';
  }
  saveWorkerTimeClock() {
    if (isNullOrUndefined(this.inTime) && isNullOrUndefined(this.outTime)) {
      this.router.navigate(['/timeclock']);
      this.clear();
      this.ngOnInit();
    } else {
      const dataObj = {
        Apply_Schedule__c: '',
        Hours__c: this.hours,
        Schedule__c: this.schedule,
        Time_In__c: this.clockIn ? this.commonService.getDBDatTmStr(this.clockIn) : null,
        Time_Out__c: this.clockOut ? this.commonService.getDBDatTmStr(this.clockOut) : null,
        Worker__c: this.workerData.workerId,
        isNew: this.workerData.Time_In__c ? false : true,
        Id: this.workerData.Time_In__c ? this.workerData.tcId : true
      };
      this.timeClockService.saveMultiple([dataObj]).subscribe(data => {
        const dataResult = data.result;
        this.router.navigate(['/timeclock']).then(() => {
          const toastermessage: any = this.translateService.get('Record Created Succesfully');
          this.toastr.success(toastermessage.value, null, { timeOut: 1500 });
          this.clear();
          this.ngOnInit();
        });
      },
        error => {
          const errStatus = JSON.parse(error._body).status;
          if (errStatus === '2085' || errStatus === '2071') {
            if (this.router.url !== '/') {
              localStorage.setItem('page', this.router.url);
              this.router.navigate(['/']).then(() => { });
            }
          }

        });
    }
  }
  getWorkerTimeClock(date: Date) {
    this.adjustmentErrorMessage = '';
    this.rows = [];
    if (!isNullOrUndefined(date) && !isNaN(date.getTime())) {
      this.timeClockService.getWorkerTimeClock(this.commonService.getDBDatStr(date)).subscribe(data => {
        this.rows = data.result;
        if (this.rows && this.rows.length > 0) {
          for (let i = 0; i < this.rows.length; i++) {
            // this.rows[i]['timeIn'] = new DatePipe('en-Us').transform(this.rows[i]['Time_In__c'], 'hh:mm a');
            this.rows[i].timeIn = this.commonService.getUsrDtStrFrmDBStr(this.rows[i].Time_In__c)[1];
            // this.rows[i]['timeOut'] = new DatePipe('en-Us').transform(this.rows[i]['Time_Out__c'], 'hh:mm a');
          //  this.rows[i].timeOut = this.commonService.getUsrDtStrFrmDBStr(this.rows[i].Time_Out__c)[1];
            this.rows[i].timeOut = this.rows[i].Time_Out__c ? this.commonService.getUsrDtStrFrmDBStr(this.rows[i].Time_Out__c)[1] : null;
            this.rows[i].default1 = true;
            this.rows[i].default2 = true;
            this.rows[i].isNew = false;
            this.rows[i].isTouched = true;
            this.rows[i].Removed = false;
            for (let j = 0; j < this.timeData.length; j++) {
              if (this.rows[i].timeIn === this.timeData[j]) {
                this.rows[i].default1 = false;
              }
              if (this.rows[i].timeOut === this.timeData[j]) {
                this.rows[i].default2 = false;
              }
            }
          }
          this.calculateActiveRows();
        } else {
          this.addRows();
        }
      },
        error => {
          const errStatus = JSON.parse(error._body).status;
          if (errStatus === '2085' || errStatus === '2071') {
            if (this.router.url !== '/') {
              localStorage.setItem('page', this.router.url);
              this.router.navigate(['/']).then(() => { });
            }
          }

        });
    } else {
      this.adjustmentErrorMessage = 'Enter a valid Search Date';
    }

  }
  timeConversionToDate(time: string): Date {
    let date;
    let hours: any;
    const timeInDate = this.selectedDate;
    let minutes: any = time.split(' ')[0].split(':')[1];
    if (time.split(' ')[1] === 'AM') {
      hours = time.split(' ')[0].split(':')[0];
      if (+hours === 12) {
        hours = 0;
      }
    } else if (time.split(' ')[1] === 'PM') {
      hours = time.split(' ')[0].split(':')[0];
      if (parseInt(hours, 10) !== 12) {
        hours = parseInt(hours, 10) + 12;
      }
    }
    minutes = parseInt(minutes, 10);
    date = new Date(timeInDate.getFullYear(), timeInDate.getMonth(), timeInDate.getDate(), hours, minutes);
    return date;
  }
  method() {
    let datIndex = 0;
    const crDate = new Date();
    const startDate = new Date(0, 0, 0, 0, 0, 0, 0);
    const endDate = new Date(0, 0, 1, 0, 0, 0, 0);
    this.timeData = [];
    do {
      let elem = '';
      if (startDate.getHours() < 12) {
        if (startDate.getHours() === 0) {
          elem = '12:' + ('0' + startDate.getMinutes()).slice(-2) + ' AM';
        } else {
          elem = ('0' + startDate.getHours()).slice(-2) + ':' + ('0' + startDate.getMinutes()).slice(-2) + ' AM';
        }
      } else {
        if ((startDate.getHours() - 12) === 0) {
          elem = '12:' + ('0' + startDate.getMinutes()).slice(-2) + ' PM';
        } else {
          elem = ('0' + (startDate.getHours() - 12)).slice(-2) + ':' + ('0' + startDate.getMinutes()).slice(-2) + ' PM';
        }
      }
      // this.timeData.push(elem);
      this.timeData.push(
        { label: elem, value: elem }
      );
      if (crDate.getHours() < startDate.getHours()) {
        datIndex++;
      }
      startDate.setMinutes(startDate.getMinutes() + 15);
    }
    while (startDate < endDate);
  }
  getAllActiveWorkers() {
    this.checkOutEditTicketService.getAllWorkers().subscribe(data => {
      this.workersList = [];
      this.workersList = data.result;
      if (this.workersList.length > 0) {
        this.workerId = this.workersList[0].Id;
      }
    },
      error => {
        const status = JSON.parse(error.status);
        const statuscode = JSON.parse(error._body).status;
        switch (JSON.parse(error._body).status) {
          case '2033':
            break;
        }
        if (statuscode === '2085' || statuscode === '2071') {
          if (this.router.url !== '/') {
            localStorage.setItem('page', this.router.url);
            this.router.navigate(['/']).then(() => { });
          }
        }
      });
  }
  calculateHours(isSave?: string): Array<any> {
    this.rows = this.rows.map((data) => {
      if (!data.Removed && !isNullOrUndefined(data.timeIn) && !isNullOrUndefined(data.timeOut)
        && data.timeOut) {
        const timeIn = this.timeConversionToDate(data.timeIn);
        const timeOut = this.timeConversionToDate(data.timeOut);
        data.Hours__c = this.calculateDurations(timeIn, timeOut);
        setTimeout(() => {
          if (+data.Hours__c < 0) {
            this.adjustmentErrorMessage = 'Time In must be less than Time Out';
          }
        }, 1000);
      }
      if (!isNullOrUndefined(isSave)) {
        if (!isNullOrUndefined(data.timeIn) && data.timeIn) {
          data.Time_In__c = this.commonService.getDBDatTmStr(this.timeConversionToDate(data.timeIn));
        } else {
          data.Time_In__c = '';
          this.adjustmentErrorMessage = 'Time in is required';
        }
        if (!isNullOrUndefined(data.timeOut) && data.timeOut) {
          data.Time_Out__c = this.commonService.getDBDatTmStr(this.timeConversionToDate(data.timeOut));
        } else {
          data.Time_Out__c = '';
        }
      }

      return data;
    });
    return this.rows;
  }
  addRows() {
    this.rows.push({ Worker__c: '', isNew: true, isTouched: true, Removed: false, timeIn: null, timeOut: null });
    this.calculateActiveRows();
  }
  saveMultipleWorkerTimeClock() {
    if (this.rows.length > 0) {
      this.adjustmentErrorMessage = '';
      this.checkForTimings();
      let dataObj = this.calculateHours('save');
      setTimeout(() => {
        if (this.adjustmentErrorMessage === '') {
          dataObj = dataObj.filter((data) => (data.isNew && !data.isRemoved) || (!data.isNew && (data.isRemoved || data.isTouched)));
          this.timeClockService.saveMultiple(dataObj).subscribe(data => {
            const dataStatus = data.result;
            this.router.navigate(['/home']).then(() => {
              const toastermessage: any = this.translateService.get('Record Edited Succesfully');
              this.toastr.success(toastermessage.value, null, { timeOut: 1500 });
              this.clear();
              this.updateTabs(0);
              this.ngOnInit();
            });
          },
            error => {
              const status = JSON.parse(error.status);
              const statuscode = JSON.parse(error._body).status;
              switch (JSON.parse(error._body).status) {
                case '2033':
                  break;
              }
              if (statuscode === '2085' || statuscode === '2071') {
                if (this.router.url !== '/') {
                  localStorage.setItem('page', this.router.url);
                  this.router.navigate(['/']).then(() => { });
                }
              }
            });
        } else {
          window.scrollTo(0, 0);
        }
      }, 1000);
    } else {
      window.scrollTo(0, 0);
    }
  }
  deleteRow(index) {
    this.rows[index].Removed = true;
    if (this.rows[index].isNew) {
      this.rows.splice(index, 1);
    }
    this.calculateActiveRows();
    if (this.activeRowsLength === 0) {
      this.addRows();
    }
  }

  calculateActiveRows() {
    const activeRows = this.rows.filter((obj) => !obj.Removed);
    this.activeRowsLength = activeRows.length;
  }
  cancel() {
    this.router.navigate(['/home']);
  }
  updateTabs(order: number) {
    for (let i = 0; i < this.activeTab2.length; i++) {
      if (i === order) {
        this.activeTab2[i] = true;
        this.activeTabClass[i] = 'active';
      } else {
        this.activeTab2[i] = false;
        this.activeTabClass[i] = '';
      }
    }
  }
  clearErrorMsg() {
    this.adjustmentErrorMessage = '';
  }
  getTimeFormat() {
    const TIME_REGEXP = /^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9][ ][AP][M]$/;
    if (this.rows && this.rows.length > 0) {
      for (let i = 0; i < this.rows.length; i++) {
        if (!TIME_REGEXP.test(this.rows[i].timeIn)) {
          this.adjustmentErrorMessage = 'TIME_CLOCK.TIME_In_MUSTBE_HH:MM_[AM][PM]_FORMAT';
        } else if (this.rows[i].timeOut && !TIME_REGEXP.test(this.rows[i].timeOut)) {
          this.adjustmentErrorMessage = 'TIME_CLOCK.TIME_OUT_MUSTBE_HH:MM_[AM][PM]_FORMAT';
        }
      }
    }
  }
  checkForTimings() {
    this.getTimeFormat();
    if (this.rows.findIndex((data) => data.Worker__c === '') !== -1) {
      this.adjustmentErrorMessage = 'Worker is Required';
    } else if (this.rows.findIndex((data) => data.timeIn === null) !== -1) {
      this.adjustmentErrorMessage = 'Time in is required';
    } else {
      if (this.commonService.getDBDatStr(new Date()) !== this.commonService.getDBDatStr(this.selectedDate)) {
        if (this.rows.findIndex((data) => data.timeOut === null) !== -1) {
          this.adjustmentErrorMessage = 'Time out is Required';
        }
      }
    }
  }
}
